package com.jnieto.utilidades;

import javax.servlet.http.HttpServletRequest;

// TODO: Auto-generated Javadoc
/**
 * The Class IpCliente.
 */
public class IpCliente {

	/**
	 * Instantiates a new ip cliente.
	 */
	public IpCliente() {

	}

	/**
	 * Gets the ip cliente.
	 *
	 * @param request the request
	 * @return the ip cliente
	 */
	public static String getIpCliente(HttpServletRequest request) {

		String remoteAddr = "";

		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		return remoteAddr;
	}
}
