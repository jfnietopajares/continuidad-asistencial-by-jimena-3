package com.jnieto.utilidades;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.vaadin.server.Page;
import com.vaadin.server.WebBrowser;

/**
 * The Class Utilidades.
 */
public class Utilidades {

	/**
	 * Validar NIF.
	 *
	 * @param nif the nif
	 * @return true, if successful
	 */
	static final int MINUTES_PER_HOUR = 60;
	static final int SECONDS_PER_MINUTE = 60;
	static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;

	public static boolean validarNIF(String nif) {

		boolean correcto = false;

		Pattern pattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");

		Matcher matcher = pattern.matcher(nif);

		if (matcher.matches()) {

			String letra = matcher.group(2);

			String letras = "TRWAGMYFPDXBNJZSQVHLCKE";

			int index = Integer.parseInt(matcher.group(1));

			index = index % 23;

			String reference = letras.substring(index, index + 1);

			if (reference.equalsIgnoreCase(letra)) {

				correcto = true;

			} else {

				correcto = false;

			}

		} else {

			correcto = false;

		}

		return correcto;

	}

	/**
	 * Validar telefono fijo.
	 *
	 * @param telefono the telefono
	 * @return true, if successful
	 */

	public static boolean validarTelefonoFijo(String telefono) {
		if (telefono.matches("9[0-9]{1,2} [0-9]{7}/")) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean validarTele(String telefono) {
		if (telefono.matches("(?:\\d{3}|\\(\\d{3}\\))([-\\/\\.])\\d{3}\\1\\d{4}")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Validar telefono.
	 *
	 * @param cadena the cadena
	 * @return true, if successful
	 */
	public static boolean validarTelefono(String cadena) {
		if (cadena.matches("[0-9]{9}")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Convertir.
	 *
	 * @param numero the numero
	 * @return the int
	 */
	public static int convertir(String numero) {
		numero = numero.replaceAll("[^0-9]", "");
		if (numero.equals("")) {
			numero = "0";
		}

		return Integer.parseInt(numero);
	}

	/*
	 * public static String getTraceException(Exception ex) { String mensaje = null;
	 * for (StackTraceElement elemnt : ex.getStackTrace()) { mensaje +=
	 * elemnt.toString() + "\n"; } return mensaje; }
	 */

	/**
	 * Validar email.
	 *
	 * @param email the email
	 * @return true, if successful
	 */
	public static boolean validarEmail(String email) {

		if (email == null) {
			return false;
		}
		String regex = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(email);

		return matcher.matches();
	}

	/**
	 * Gets the sexo nombre.
	 *
	 * @param sexo the sexo
	 * @return the sexo nombre
	 */
	public static String getSexoNombre(int sexo) {
		String nombresexo = "";
		switch (sexo) {
		case 1:
			nombresexo = Constantes.NOMBRESEXOHOMBRE;
			break;
		case 2:
			nombresexo = Constantes.NOMBRESEXOMUJER;
			break;
		}
		return nombresexo;
	}

	/**
	 * Gets the sexo valor.
	 *
	 * @param sexonombre the sexonombre
	 * @return the sexo valor
	 */
	public static Integer getSexoValor(String sexonombre) {
		Integer sexovalor = new Integer(0);
		switch (sexonombre) {
		case "Hombre":
			sexovalor = 1;
			break;
		case "Mujer":
			sexovalor = 2;
			break;
		}
		return sexovalor;
	}

	/**
	 * Gets the fech formatoaddmmyyyy.
	 *
	 * @param fecha     the fecha
	 * @param separador the separador
	 * @return the fech formatoaddmmyyyy
	 */
	public static String getFechFormatoaddmmyyyy(Long fecha, String separador) {
		String unaFecha = "";
		String cadena = Long.toString(fecha);

		unaFecha = cadena.substring(6, 8).concat(separador).concat(cadena.substring(4, 6)).concat(separador)
				.concat(cadena.substring(0, 4));

		return unaFecha;
	}

	/**
	 * Gets the fech formatoayyyymmdd.
	 *
	 * @param fecha     the fecha
	 * @param separador the separador
	 * @return the fech formatoayyyymmdd
	 */
	public static String getFechFormatoayyyymmdd(long fecha, String separador) {
		String unaFecha = "";
		String cadena = Long.toString(fecha);

		unaFecha = cadena.substring(0, 4).concat(separador).concat(cadena.substring(4, 6)).concat(separador)
				.concat(cadena.substring(6, 8));

		return unaFecha;
	}

	/**
	 * Gets the fecha numeroyyymmdd defecha.
	 *
	 * @param fecha the fecha
	 * @return the fecha numeroyyymmdd defecha
	 */
	public static Long getFechaNumeroyyymmddDefecha(LocalDate fecha) {
		long unaFecha = 0;
		int dd, mm, yyyy;
		if (fecha != null) {
			dd = fecha.getDayOfMonth();
			// if (dd<10) dd=dd*10;
			mm = fecha.getMonthValue();
			// if (mm<10) mm=mm*10;
			yyyy = fecha.getYear();

			unaFecha = yyyy * 10000 + mm * 100 + dd;
		}

		return unaFecha;

	}

	/**
	 * Gets the fecha numeroyyymmdd.
	 *
	 * @param fecha the fecha
	 * @return the fecha numeroyyymmdd
	 */
	public static Long getFechaNumeroyyymmdd(String fecha) {
		long unaFecha = 0;
		String dd, mm, yyyy;
		dd = fecha.substring(0, 2);
		mm = fecha.substring(3, 5);
		yyyy = fecha.substring(6, 10);

		unaFecha = Integer.parseInt(yyyy) * 10000 + Integer.parseInt(mm) * 100 + Integer.parseInt(dd);

		return unaFecha;
	}

	/**
	 * Gets the date formatdd mm yyyy.
	 *
	 * @return the date formatdd mm yyyy
	 */
	public static DateTimeFormatter getDateFormatdd_mm_yyyy() {
		return DateTimeFormatter.ofPattern("dd/mm/yyyy");
	}

	/**
	 * Gets the fecha local date.
	 *
	 * @param fecha the fecha
	 * @return the fecha local date
	 */
	public static LocalDate getFechaLocalDate(Long fecha) {
		LocalDate date = null;
		if (fecha != null) {
			if (fecha > 19000000) {
				String cadena = Long.toString(fecha);
				int year = Integer.parseInt(cadena.substring(0, 4));
				int month = Integer.parseInt(cadena.substring(4, 6));
				int day = Integer.parseInt(cadena.substring(6, 8));
				date = LocalDate.of(year, month, day);
			}
		}
		return date;
	}

	public static LocalDate getFechaLocalDate(String cadena) {
		LocalDate date = null;
		if (cadena != null) {
			if (cadena.length() == 8) {
				int year = Integer.parseInt(cadena.substring(0, 4));
				int month = Integer.parseInt(cadena.substring(4, 6));
				int day = Integer.parseInt(cadena.substring(6, 8));
				date = LocalDate.of(year, month, day);
				// return date;
			}
		}
		return date;
	}

	/**
	 * Gets the hora numero acual.
	 *
	 * @return the hora numero acual
	 */
	public static Long getHoraNumeroAcual() {

		LocalTime time = LocalTime.now();
		int hh = time.getHour();
		int mm = time.getMinute();

		return new Long(hh * 100 + mm);

	}

	public static String getHoraAcualString() {

		LocalTime time = LocalTime.now();

		int hh = time.getHour();
		int mm = time.getMinute();
		String hhString = Integer.toString(hh);
		String mmString = Integer.toString(mm);
		if (hh < 10) {
			hhString = "0" + hhString;
		}
		if (mm < 10) {
			mmString = "0" + mmString;
		}
		return hhString + ":" + mmString;

	}

	/**
	 * Gets the hora.
	 *
	 * @param unahora the unahora
	 * @return the hora
	 */
	public static int getHora(String unahora) {
		int valor;
		String cadena1;
		cadena1 = unahora.substring(0, 2);
		valor = Integer.parseInt(cadena1);
		return valor;
	}

	public static Long getHoraLong(String unahora) {
		Long valor;
		String cadena1;
		cadena1 = unahora.substring(0, 2);
		valor = Long.parseLong(cadena1);
		return valor;
	}

	/**
	 * Gets the minuto.
	 *
	 * @param unahora the unahora
	 * @return the minuo
	 */
	public static int getMinuo(String unahora) {
		int valor;
		String cadena1;
		cadena1 = unahora.substring(3, 5);
		valor = Integer.parseInt(cadena1);
		return valor;
	}

	public static String getHoraHH_MM(Long hora) {
		String hhmmString = null;
		String hh = null;
		String mm = null;
		hhmmString = Long.toString(hora);
		if (hhmmString.length() == 4) {
			hh = hhmmString.substring(0, 2);
			mm = hhmmString.substring(2, 4);
		} else if (hhmmString.length() == 3) {
			hh = "0" + hhmmString.substring(0, 1);
			mm = hhmmString.substring(1, 3);
		} else if (hhmmString.length() == 2) {
			hh = "00";
			mm = hhmmString.substring(1, 1);
		} else if (hhmmString.length() == 1) {
			hh = "00";
			mm = "0" + hhmmString;
		}
		return hh + ":" + mm;
	}

	public static int getHoraInt(String hhmmString) {
		int valor;
		String hh = null;
		if (hhmmString.length() == 4) {
			hh = hhmmString.substring(0, 2);
		} else if (hhmmString.length() == 3) {
			hh = "0" + hhmmString.substring(0, 1);
		} else if (hhmmString.length() == 2) {
			hh = "00";
		} else if (hhmmString.length() == 1) {
			hh = "00";
		}
		valor = Integer.parseInt(hh);
		return valor;
	}

	public static int getHoraInt(Long unahora) {
		int valor;
		String hh = null;
		String hhmmString = Long.toString(unahora);

		if (hhmmString.length() == 4) {
			hh = hhmmString.substring(0, 2);
		} else if (hhmmString.length() == 3)

		{
			hh = "0" + hhmmString.substring(0, 1);
		} else if (hhmmString.length() == 2) {
			hh = "00";
		} else if (hhmmString.length() == 1) {
			hh = "00";
		}
		valor = Integer.parseInt(hh);
		return valor;
	}

	public static int getMinuto(Long unahora) {
		int valor;
		String mm = null;
		String hhmmString = Long.toString(unahora);
		if (hhmmString.length() == 4) {
			mm = hhmmString.substring(2, 4);
		} else if (hhmmString.length() == 3) {
			mm = hhmmString.substring(1, 3);
		} else if (hhmmString.length() == 2) {
			mm = hhmmString.substring(1, 1);
		} else if (hhmmString.length() == 1) {
			mm = "0" + hhmmString;
		}
		valor = Integer.parseInt(mm);
		return valor;
	}

	public static long[] tiempoTrascurridoHoras(LocalDate fecha1, Long hora1) {
		LocalDateTime instanteInicial = LocalDateTime.of(fecha1.getYear(), fecha1.getMonth(), fecha1.getDayOfMonth(),
				Utilidades.getHoraInt(hora1), Utilidades.getMinuto(hora1));

		LocalDateTime instanteFinal = LocalDateTime.now();
		Period periodo = Period.between(instanteInicial.toLocalDate(), instanteFinal.toLocalDate());
		long time[] = getTime(instanteInicial, instanteFinal);
		return time;
	}

	private static long[] getTime(LocalDateTime dob, LocalDateTime now) {
		LocalDateTime today = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(), dob.getHour(),
				dob.getMinute(), dob.getSecond());
		Duration duration = Duration.between(dob, now);

		long seconds = duration.getSeconds();

		long hours = seconds / SECONDS_PER_HOUR;
		long minutes = ((seconds % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE);
		long secs = (seconds % SECONDS_PER_MINUTE);

		return new long[] { hours, minutes, secs };
	}

	/**
	 * Checks if is numeric.
	 *
	 * @param cadena the cadena
	 * @return true, if is numeric
	 */
	public static boolean isNumeric(String cadena) {
		try {
			Integer.parseInt(cadena);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}

	/**
	 * Checks if is numero.
	 *
	 * @param cadena the cadena
	 * @return true, if is numero
	 */
	public static boolean isNumero(String cadena) {
		if (cadena == null || cadena.isEmpty()) {
			return false;
		} else {
			int i = 0;
			for (i = 0; i < cadena.length(); i++) {
				if (!Character.isDigit(cadena.charAt(i))) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Gets the alto grid.
	 *
	 * @param nfilas the nfilas
	 * @return the alto grid
	 */
	public static String getAltoGrid(int nfilas) {
		int alto = nfilas * 72;
		String altopx;
		altopx = Integer.toString(alto);
		altopx = altopx.concat("px");
		return altopx;
	}

	/**
	 * Gets the informacion cliente.
	 *
	 * @return the informacion cliente
	 */
	public static String getInformacionCliente() {
		final WebBrowser navegador = Page.getCurrent().getWebBrowser();

		String cadenaString = "<hr> <b>Información del puesto de trabajo</b><br><b>Ip:</b>" + navegador.getAddress()
				+ "<br>";

		cadenaString = cadenaString.concat("<b>Navegador:</b>" + navegador.getBrowserApplication() + "<br>");
		cadenaString = cadenaString.concat("<b>Versión:</b>" + navegador.getBrowserVersion() + "<br>");
		cadenaString = cadenaString
				.concat("<b>Ventana:</b>" + navegador.getScreenHeight() + "x" + navegador.getScreenWidth() + "<br>");

		cadenaString = cadenaString.concat("<b>Locale:</b>" + navegador.getLocale().getDisplayName() + "<br>");
		cadenaString = cadenaString.concat("<b>Fecha:</b>" + navegador.getCurrentDate() + "<br>");

		return cadenaString;
	}

}
