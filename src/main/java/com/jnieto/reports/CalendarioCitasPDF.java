package com.jnieto.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.IOException;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Variable;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

public class CalendarioCitasPDF {
	private static final Logger logger = LogManager.getLogger(CalendarioCitasPDF.class);

	public static final String DEST = Constantes.DIRECTORIOREPORTS + "citasde"
			+ Long.toString(Utilidades.getHoraNumeroAcual()) + ".pdf";

	private File file;

	private LocalDate fecha;
	private Centro centro;
	private Servicio servicio;
	private Agenda agenda;
	private ArrayList<Variable> listaItemsPrestacion;
	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	public CalendarioCitasPDF(Centro centro, Servicio servicio, LocalDate fecha) {
		this.centro = centro;
		this.servicio = servicio;
		this.fecha = fecha;

		file = new File(DEST);
		if (file.exists())
			file.delete();

		file.getParentFile().mkdirs();

		this.createPdf(DEST);
	}

	public static String getDest() {
		return DEST;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void createPdf(String dest) {

		PdfDocument pdf;
		try {

			pdf = new PdfDocument(new PdfWriter(dest));
			Document document = new Document(pdf, PageSize.A4).setTextAlignment(TextAlignment.JUSTIFIED);
			document.setMargins(75, 36, 75, 36);

			String cabecera = "Listado de pacientes citados  por prestacion del servicio de " + servicio.getCodigo()
					+ " desde " + fechadma.format(fecha);
			if (servicio != null)
				cabecera = cabecera.concat(" Servicio: " + servicio.getCodigo());
			if (agenda != null)
				cabecera = cabecera.concat(" Agenda: " + agenda.getCodigo());

			ArrayList<Episodio> listaCitados = new ArrayList<Episodio>();

			listaCitados = new EpisodioDAO().getListaEpisodiosCitas(Episodio.CLASE_CONSULTAS.getId(), centro, servicio,
					null, fecha, null, null);

			PdfEventoPagina evento = new PdfEventoPagina(document, cabecera);

			pdf.addEventHandler(PdfDocumentEvent.END_PAGE, evento);

			PdfFont normal = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);

			PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
			Float altura = new Float(30f);
			int sizeFont = 12;

			float[] anchos = { 40f, 180f, 35f };
			Table tabla = new Table(anchos);

			String[] tituloSrings = { "Fecha", "Prestación ", "Citados" };
			tabla.setMarginTop(tituloSrings.length);
			for (int i = 0; i < tituloSrings.length; i++) {
				Cell celdaCell = new Cell();
				celdaCell.add(new Paragraph(tituloSrings[i]).setFontSize(sizeFont));
				celdaCell.setHeight(altura);
				tabla.addCell(celdaCell);
			}
			for (Episodio fecha : listaCitados) {

				tabla.addCell(new Cell().add(new Paragraph(fechadma.format(fecha.getFinicio()))).setFontSize(sizeFont));

				tabla.addCell(
						new Cell().add(new Paragraph(fecha.getPrestacion().getDescripcion())).setFontSize(sizeFont));

				tabla.addCell(
						new Cell().add(new Paragraph(Integer.toString(fecha.getCitasDadas()))).setFontSize(sizeFont));
			}
			document.add(tabla);

			document.close();

		} catch (FileNotFoundException e) {
			logger.error(NotificacionInfo.EXCEPTION_FILENOTFOUND, e);
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (java.io.IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

}
