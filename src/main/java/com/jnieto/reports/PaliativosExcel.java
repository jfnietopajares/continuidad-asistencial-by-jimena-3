package com.jnieto.reports;

import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;

public class PaliativosExcel {

	private static final Logger logger = LogManager.getLogger(PaliativosExcel.class);

	private static String[] columns = { "Nhc", "Apellidos y nombre", "Fecha Inicio,", "Servicio", "Motivo",
			"Fecha Baja.", "Motivo Baja " };

	private final LocalDate desde;

	private final LocalDate hasta;

	private final String filenameString = Constantes.DIRECTORIOREPORTS + "paliativos.xlsx";

	public PaliativosExcel(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		try {
			Workbook workbook = new XSSFWorkbook();

			CreationHelper createHelper = workbook.getCreationHelper();

			Sheet sheet = workbook.createSheet("RegistroMama");

			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());

			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);

			// Creating cells
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(headerCellStyle);
			}

			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

			ArrayList<Proceso> listadatos = new ProcesoDAO().getListaProcesos(Proceso.SUBAMBITO_PALIATIVOS, desde,
					hasta);

			int rowNum = 1;
			for (Proceso proceso : listadatos) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(proceso.getId());

				row.createCell(1).setCellValue(proceso.getPaciente().getApellidosNombre());

				Cell dateOfBirthCell = row.createCell(2);
				if (proceso.getFechaini() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(proceso.getFechaini()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				} else {
				}

				row.createCell(3).setCellValue(proceso.getServicio().getCodigo());

				row.createCell(4).setCellValue(proceso.getMotivo());

				Cell dateOfBirthCell1 = row.createCell(5);
				if (proceso.getFechafin() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(proceso.getFechafin()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

				if (proceso.getMotivo_baja() != null) {
					row.createCell(6).setCellValue(proceso.getMotivo_baja());
				} else {
					row.createCell(6).setCellValue("");
				}
			}
			for (int i = 0; i < columns.length; i++) {
				sheet.autoSizeColumn(i);
			}
			FileOutputStream fileOut;
			fileOut = new FileOutputStream(filenameString);

			workbook.write(fileOut);
			fileOut.close();
			workbook.close();

		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	public String getFilename() {
		return filenameString;
	}
}
