package com.jnieto.reports;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Proceso;

public class PaliativosHtml {
	private LocalDate desde;
	private LocalDate hasta;
	ArrayList<Proceso> listadatos;

	public PaliativosHtml(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		listadatos = new ProcesoDAO().getListaProcesos(Proceso.SUBAMBITO_PALIATIVOS,
				desde, hasta);
	}

	public String getTablaHtmlMama() {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String htmlTabla;
		htmlTabla = "<table border=\"1\">";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"10\">").concat(
				"Listado de casos de paliativos desde " + fechadma.format(desde) + " hasta " + fechadma.format(hasta))
				.concat("</td></tr>");
		htmlTabla = htmlTabla.concat(
				"<tr><th>Historia</th><th>Apellidos y nombre</th><th>Fecha I</th><th>Motivo</th><th>Fecha Fin </th>"
						+ "<th>Motivo Fin</th><th>Observaciones</th></tr>");
		Iterator<Proceso> it = listadatos.iterator();
		while (it.hasNext()) {
			Proceso proceso = it.next();
			String fila = this.toHtmlFila(proceso);
			htmlTabla = htmlTabla.concat(fila);
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	public String toHtmlFila(Proceso pr) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String html;
		html = "<tr><td>" + pr.getPaciente().getNumerohc() + "</td>" + "<td>" + pr.getPaciente().getApellidosNombre()
				+ " </td> ";
		if (pr.getFechaini() != null) {
			html = html.concat("<td>" + fechadma.format(pr.getFechaini()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (pr.getMotivo() != null) {
			html = html.concat("<td>" + pr.getMotivo() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (pr.getFechafin() != null) {
			html = html.concat("<td>" + fechadma.format(pr.getFechafin()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (pr.getMotivo_baja() != null) {
			html = html.concat("<td>" + pr.getMotivo_baja() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (pr.getObservaciones() != null) {
			html = html.concat("<td>" + pr.getObservaciones() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		html = html.concat("</tr>");
		return html;
	}
}
