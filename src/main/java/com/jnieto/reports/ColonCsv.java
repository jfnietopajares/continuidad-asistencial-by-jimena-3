package com.jnieto.reports;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.dao.RegistroColonDAO;
import com.jnieto.entity.RegistroColon;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.opencsv.CSVWriter;

/**
 * The Class ColonCsv. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class ColonCsv {

	private static final Logger logger = LogManager.getLogger(MamaCsv.class);

	private static List<String[]> toStringArray(ArrayList<RegistroColon> datos) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		List<String[]> records = new ArrayList<String[]>();

		records.add(new String[] { "Nhc", "Apellidos y nombre", "Fecha TSOH,", "TSHO", "Cita Colon", "Inf.Colon ",
				"Fecha Anato ", "Observaciones" });

		for (RegistroColon rm : datos) {

			records.add(new String[] { rm.getPaciente().getId().toString(), rm.getPaciente().getApellidosNombre(),
					(rm.getAnaliticaDate() == null) ? "" : fechadma.format(rm.getAnaliticaDate()),
					(rm.getResultadoString() == null) ? " " : rm.getResultadoString(),
					(rm.getCitaColonoDate() == null) ? "" : fechadma.format(rm.getCitaColonoDate()),
					(rm.getInformeColonoDate() == null) ? "" : fechadma.format(rm.getInformeColonoDate()),
					(rm.getInformeAnatoDate() == null) ? "" : fechadma.format(rm.getInformeAnatoDate()),
					(rm.getObservacionesValor() == null) ? " " : rm.getObservacionesValor() });
		}
		return records;
	}

	public final String DESTINO = Constantes.DIRECTORIOREPORTS + Long.toString(Utilidades.getHoraNumeroAcual())
			+ "colon.csv";

	private final String URLFILE = Constantes.URLREPORTS + Long.toString(Utilidades.getHoraNumeroAcual()) + "colon.csv";

	private LocalDate desde;

	private LocalDate hasta;

	public ColonCsv(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		this.generaFichero();
	}

	public void generaFichero() {
		try {
			ArrayList<RegistroColon> listadatos = new RegistroColonDAO().listaRegistroColon(desde, hasta);

			Writer writer = new FileWriter(DESTINO);

			CSVWriter csvWriter = new CSVWriter(writer, ',', '\'');

			List<String[]> data = toStringArray(listadatos);

			csvWriter.writeAll(data);

			csvWriter.close();
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	public String getNombreFichero() {
		return DESTINO;
	}

	public String geturlFile() {
		return URLFILE;
	}
}
