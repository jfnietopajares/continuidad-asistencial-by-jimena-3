package com.jnieto.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.IOException;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.jnieto.dao.RegistroColonDAO;
import com.jnieto.entity.RegistroColon;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class ColonPdf. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class ColonPdf {
	private static final Logger logger = LogManager.getLogger(MamaPdf.class);

	public static final String DEST = Constantes.DIRECTORIOREPORTS + "mama"
			+ Long.toString(Utilidades.getHoraNumeroAcual()) + ".pdf";

	private File file;

	private LocalDate desde;

	private LocalDate hasta;

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	public ColonPdf(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		file = new File(DEST);
		if (file.exists())
			file.delete();

		file.getParentFile().mkdirs();

		this.createPdf(DEST);

	}

	public static String getDest() {
		return DEST;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void createPdf(String dest) {

		PdfDocument pdf;
		try {

			pdf = new PdfDocument(new PdfWriter(dest));

			Document document = new Document(pdf, PageSize.A4).setTextAlignment(TextAlignment.JUSTIFIED);

			document.setMargins(75, 36, 75, 36);

			PdfEventoPagina evento = new PdfEventoPagina(document,
					"Listado de casos de colon desde " + fechadma.format(desde) + " hasta " + fechadma.format(hasta));

			pdf.addEventHandler(PdfDocumentEvent.END_PAGE, evento);

			PdfFont normal = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);

			PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);

			Float altura = new Float(30f);
			int sizeFont = 7;

			float[] anchos = { 30f, 140f, 45f, 45f, 45f, 45f, 45f, 100f };
			Table tabla = new Table(anchos);

			String[] tituloSrings = { "Nhc", "Apellidos y nombre", "Fecha TSOH,", "TSHO", "Cita Colon", "Inf.Colon ",
					"Fecha Anato ", "Observaciones" };
			tabla.setMarginTop(10);
			for (int i = 0; i < tituloSrings.length; i++) {
				Cell celdaCell = new Cell();
				celdaCell.add(new Paragraph(tituloSrings[i]).setFontSize(sizeFont));
				celdaCell.setHeight(altura);
				tabla.addCell(celdaCell);
			}

			ArrayList<RegistroColon> listadatos = new RegistroColonDAO().listaRegistroColon(desde, hasta);

			for (RegistroColon rc : listadatos) {
				if (rc.getPaciente().getDni() != null) {
					tabla.addCell(new Cell().add(new Paragraph(rc.getPaciente().getDni())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getPaciente().getApellidosNombre() != null) {
					if (rc.getPaciente().getApellidosNombre().length() > 32) {
						tabla.addCell(
								new Cell().add(new Paragraph(rc.getPaciente().getApellidosNombre().substring(0, 32)))
										.setFontSize(sizeFont));
					} else {
						tabla.addCell(new Cell().add(new Paragraph(rc.getPaciente().getApellidosNombre()))
								.setFontSize(sizeFont));
					}
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getAnaliticaDate() != null) {
					tabla.addCell(new Cell().add(new Paragraph(fechadma.format(rc.getAnaliticaDate())))
							.setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getResultadoString() != null) {
					tabla.addCell(new Cell().add(new Paragraph(rc.getResultadoString())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getCitaColonoDate() != null) {
					tabla.addCell(new Cell().add(new Paragraph(fechadma.format(rc.getCitaColonoDate())))
							.setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getIdColonoVarlor() != null) {
					tabla.addCell(new Cell().add(new Paragraph(rc.getIdColonoVarlor())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getAnaliticaDate() != null) {
					tabla.addCell(new Cell().add(new Paragraph(fechadma.format(rc.getAnaliticaDate())))
							.setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				if (rc.getObservacionesValor() != null) {
					tabla.addCell(new Cell().add(new Paragraph(rc.getObservacionesValor())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

			}
			document.add(tabla);

			document.close();

		} catch (FileNotFoundException e) {
			logger.error(NotificacionInfo.EXCEPTION_FILENOTFOUND, e);
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (java.io.IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

}
