package com.jnieto.reports;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import com.jnieto.dao.RegistroMamaDAO;
import com.jnieto.entity.RegistroMama;

public class MamaHtml {
	private LocalDate desde;
	private LocalDate hasta;
	ArrayList<RegistroMama> listadatos;

	public MamaHtml(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		listadatos = new RegistroMamaDAO().getListaRegistros(desde, hasta);

	}

	public String getTablaHtmlMama() {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String htmlTabla;
		htmlTabla = "<table border=\"1\">";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"10\">")
				.concat("Listado de casos de mama desde " + fechadma.format(desde) + " hasta " + fechadma.format(hasta))
				.concat("</td></tr>");
		htmlTabla = htmlTabla
				.concat("<tr><th>Historia</th><th>Apellidos y nombre</th><th>FechaAP</th><th>Birdras</th><th>Tnm</th>"
						+ "<th>Fecha RX.</th><th>Birdras 2</th><th>Fecha Gin</th><th>F. Comite</th><th>F TTO</th><th>Observaciones</th></tr>");
		Iterator<RegistroMama> it = listadatos.iterator();
		while (it.hasNext()) {
			RegistroMama registroMama = it.next();
			String fila = this.toHtmlFila(registroMama);
			htmlTabla = htmlTabla.concat(fila);
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	public String toHtmlFila(RegistroMama rm) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String html;
		html = "<tr><td>" + rm.getPaciente().getId() + "</td>" + "<td>" + rm.getPaciente().getApellidosNombre()
				+ " </td> ";
		if (rm.getFechaap().getValor() != null) {
			html = html.concat("<td>" + fechadma.format(rm.getFechaapValue()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (rm.getBirads().getValor() != null) {
			html = html.concat("<td>" + rm.getBirads().getValor() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (rm.getTnm().getValor() != null) {
			html = html.concat("<td>" + rm.getTnm().getValor() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rm.getFechapruebaValue() != null) {
			html = html.concat("<td>" + fechadma.format(rm.getFechapruebaValue()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (rm.getFechagineValue() != null) {
			html = html.concat("<td>" + fechadma.format(rm.getFechagineValue()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rm.getFechagineValue() != null) {
			html = html.concat("<td>" + fechadma.format(rm.getFechagineValue()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rm.getFechatratamientoValue() != null) {
			html = html.concat("<td>" + fechadma.format(rm.getFechatratamientoValue()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (rm.getObservaciones().getValor() != null) {
			html = html.concat("<td>" + rm.getObservaciones().getValor() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		html = html.concat("</tr>");
		return html;
	}
}
