package com.jnieto.reports;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.io.IOException;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.events.PdfDocumentEvent;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;
import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Variable;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

public class HdiaOncoPdf {
	private static final Logger logger = LogManager.getLogger(HdiaOncoPdf.class);

	public static final String DEST = Constantes.DIRECTORIOREPORTS + "hdiaOnco"
			+ Long.toString(Utilidades.getHoraNumeroAcual()) + ".pdf";

	private File file;

	private LocalDate fecha;
	private Centro centro;
	private ArrayList<Servicio> servicios = new ArrayList<Servicio>();
	private Agenda agenda;
	private ArrayList<Variable> listaItemsPrestacion;
	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	public HdiaOncoPdf(Centro centro, ArrayList<Servicio> servicios, Agenda agenda, LocalDate fecha,
			ArrayList<Variable> listaItemsPrestacion) {
		this.centro = centro;
		this.servicios = servicios;
		this.agenda = agenda;
		this.fecha = fecha;
		this.listaItemsPrestacion = listaItemsPrestacion;
		file = new File(DEST);
		if (file.exists())
			file.delete();

		file.getParentFile().mkdirs();

		this.createPdf(DEST);
	}

	public static String getDest() {
		return DEST;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void createPdf(String dest) {

		PdfDocument pdf;
		try {

			pdf = new PdfDocument(new PdfWriter(dest));
			Document document = new Document(pdf, PageSize.A4.rotate()).setTextAlignment(TextAlignment.JUSTIFIED);
			document.setMargins(75, 36, 75, 36);

			String cabecera = "Listado de pacientes citados  el día " + fechadma.format(fecha);
			if (servicios != null) {
				cabecera = cabecera.concat(" Servicio: ");
				for (Servicio servicio : servicios) {
					cabecera = cabecera.concat(servicio.getCodigo());
				}
			}
			if (agenda != null)
				cabecera = cabecera.concat(" Agenda: " + agenda.getCodigo());
			ArrayList<Episodio> listaCitados = new ArrayList<Episodio>();
			if (listaItemsPrestacion == null || listaItemsPrestacion.size() == 0) {
				for (Servicio servicio : servicios) {
					ArrayList<Episodio> listaCitados1 = new EpisodioDAO().getListaEpisodios(
							Episodio.CLASE_CONSULTAS.getId(), centro, servicio, agenda, null, fecha, "FECHAHORA");
					listaCitados.addAll(listaCitados1);
				}
			} else {
				cabecera = cabecera.concat("\n Prestaciones: ");
				for (Variable presta : listaItemsPrestacion) {
					cabecera = cabecera.concat(presta.getDescripcion() + " ");
				}
				listaCitados = new EpisodioDAO().getListaEpisodios(Episodio.CLASE_CONSULTAS.getId(), Centro.HNSS,
						servicios, agenda, null, fecha, "FECHAHORA", listaItemsPrestacion);

			}

			PdfEventoPagina evento = new PdfEventoPagina(document, cabecera);

			pdf.addEventHandler(PdfDocumentEvent.END_PAGE, evento);

			PdfFont normal = PdfFontFactory.createFont(FontConstants.TIMES_ROMAN);

			PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
			Float altura = new Float(30f);
			int sizeFont = 7;

			float[] anchos = { 20f, 35f, 180f, 35f, 30f, 35f, 110f, 70f, 70f, 70f, 70f };
			Table tabla = new Table(anchos);

			String[] tituloSrings = { "Nº", "Nhc", "Apellidos y nombre", "Fecha,", "Serv", "Agenda", "Presta", "Diag ",
					"TTo", "F. TTo", "...." };
			tabla.setMarginTop(10);
			for (int i = 0; i < tituloSrings.length; i++) {
				Cell celdaCell = new Cell();
				celdaCell.add(new Paragraph(tituloSrings[i]).setFontSize(sizeFont));
				celdaCell.setHeight(altura);
				tabla.addCell(celdaCell);
			}
			int contador = 1;
			for (Episodio epi : listaCitados) {
				tabla.addCell(new Cell().add(new Paragraph(Integer.toString(contador)).setFontSize(sizeFont)));
				contador++;
				if (epi.getPaciente().getNumerohc() != null) {
					tabla.addCell(new Cell().add(new Paragraph(epi.getPaciente().getNumerohc())).setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}
				if (epi.getPaciente().getApellidosNombre() != null) {
					tabla.addCell(new Cell().add(new Paragraph(epi.getPaciente().getApellidosNombre()))
							.setFontSize(sizeFont));
				} else {
					tabla.addCell(new Cell());
				}

				tabla.addCell(new Cell().add(new Paragraph(fechadma.format(epi.getFinicio()))).setFontSize(sizeFont));

				tabla.addCell(new Cell().add(new Paragraph(epi.getServicioCodigo())).setFontSize(sizeFont));

				if (epi.getAgendaCodigo() != null)
					tabla.addCell(new Cell().add(new Paragraph(epi.getAgendaCodigo())).setFontSize(sizeFont));
				else
					tabla.addCell(new Cell());

				tabla.addCell(new Cell().add(new Paragraph(epi.getPrestacionDescripcion())).setFontSize(sizeFont));

				tabla.addCell(new Cell());
				tabla.addCell(new Cell());
				tabla.addCell(new Cell());
				tabla.addCell(new Cell());
			}
			document.add(tabla);

			document.close();

		} catch (FileNotFoundException e) {
			logger.error(NotificacionInfo.EXCEPTION_FILENOTFOUND, e);
		} catch (IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_IO, e);
		} catch (java.io.IOException e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}
}
