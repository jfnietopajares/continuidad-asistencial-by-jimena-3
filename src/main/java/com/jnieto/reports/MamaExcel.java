package com.jnieto.reports;

import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.jnieto.dao.RegistroMamaDAO;
import com.jnieto.entity.RegistroMama;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

public class MamaExcel {

	private static final Logger logger = LogManager.getLogger(MamaExcel.class);

	private static String[] columns = { "Nhc", "Apellidos y nombre", "Fecha AP.,", "Birdras", "Tnm", "Fecha RX.",
			"Birdras ", "Fecha Gin", "F. Comite", "TTO." };

	private final LocalDate desde;

	private final LocalDate hasta;

	private final String filenameString = Constantes.DIRECTORIOREPORTS + Long.toString(Utilidades.getHoraNumeroAcual())
			+ "mama.xlsx";

	private final String urlfile = Constantes.URLREPORTS + Long.toString(Utilidades.getHoraNumeroAcual()) + "mama.xlsx";

	public MamaExcel(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		try {
			// Create a Workbook
			Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

			/*
			 * CreationHelper helps us create instances for various things like DataFormat,
			 * Hyperlink, RichTextString etc in a format (HSSF, XSSF) independent way
			 */
			CreationHelper createHelper = workbook.getCreationHelper();

			// Create a Sheet
			Sheet sheet = workbook.createSheet("RegistroMama");

			// Create a Font for styling header cells
			Font headerFont = workbook.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 14);
			headerFont.setColor(IndexedColors.RED.getIndex());

			// Create a CellStyle with the font
			CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);

			// Create a Row

			org.apache.poi.ss.usermodel.Row headerRow = sheet.createRow(0);

			// Creating cells
			for (int i = 0; i < columns.length; i++) {
				Cell cell = headerRow.createCell(i);
				cell.setCellValue(columns[i]);
				cell.setCellStyle(headerCellStyle);
			}
			// Cell Style for formatting Date
			CellStyle dateCellStyle = workbook.createCellStyle();
			dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

			ArrayList<RegistroMama> listadatos = new RegistroMamaDAO().getListaRegistros(desde, hasta);

			int rowNum = 1;
			for (RegistroMama rm : listadatos) {
				Row row = sheet.createRow(rowNum++);

				row.createCell(0).setCellValue(rm.getId());

				if (rm.getPaciente() != null) {
					row.createCell(1).setCellValue(rm.getPaciente().getApellidosNombre());
				} else {
					row.createCell(1).setCellValue("");
				}
				Cell dateOfBirthCell = row.createCell(2);
				if (rm.getFechaapValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechaapValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				} else {
				}

				row.createCell(3).setCellValue(rm.getBiradsValue());

				row.createCell(4).setCellValue(rm.getTnmValue());

				row.createCell(5);
				if (rm.getFechapruebaValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechapruebaValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				} else {
				}

				row.createCell(6).setCellValue(rm.getBiradscorregidoValue());

				row.createCell(7);
				if (rm.getFechagineValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechagineValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

				row.createCell(8);
				if (rm.getFechacomiteValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechacomiteValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

				row.createCell(9);
				if (rm.getFechacomiteValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechacomiteValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

				row.createCell(10);
				if (rm.getFechatratamientoValue() != null) {
					dateOfBirthCell.setCellValue(fechadma.format(rm.getFechatratamientoValue()));
					dateOfBirthCell.setCellStyle(dateCellStyle);
				}

			}
			// Resize all columns to fit the content size
			for (int i = 0; i < columns.length; i++) {
				sheet.autoSizeColumn(i);
			}

			// Write the output to a file
			FileOutputStream fileOut;

			// fileOut = new FileOutputStream("/tmp/poi-generated-file.xlsx");
			fileOut = new FileOutputStream(filenameString);

			workbook.write(fileOut);
			fileOut.close();
			workbook.close();

		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
	}

	public String getUrl() {
		return urlfile;
	}

	public String getFilename() {
		return filenameString;
	}
}
