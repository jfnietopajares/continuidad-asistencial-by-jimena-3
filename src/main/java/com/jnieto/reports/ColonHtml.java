package com.jnieto.reports;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import com.jnieto.dao.RegistroColonDAO;
import com.jnieto.entity.RegistroColon;

/**
 * The Class ColonHtml. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class ColonHtml {

	private LocalDate desde;
	private LocalDate hasta;
	ArrayList<RegistroColon> listadatos;

	public ColonHtml(LocalDate desde, LocalDate hasta) {
		this.desde = desde;
		this.hasta = hasta;
		listadatos = new RegistroColonDAO().listaRegistroColon(desde, hasta);

	}

	public String getTablaHtmlColon() {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String htmlTabla;
		htmlTabla = "<table border=\"1\">";
		htmlTabla = htmlTabla.concat("<tr><td colspan=\"10\">").concat(
				"Listado de casos de colon desde " + fechadma.format(desde) + " hasta " + fechadma.format(hasta))
				.concat("</td></tr>");
		htmlTabla = htmlTabla.concat(
				"<tr><th>Historia</th><th>Apellidos y nombre</th><th>Fecha Ana</th><th>Resultado</th><th>Tnm</th>"
						+ "<th>Fecha Cita Colon.</th><th>Informe Colon</th><th>Fecha Ana</th><th>Observaciones</th></tr>");
		Iterator<RegistroColon> it = listadatos.iterator();
		while (it.hasNext()) {
			RegistroColon registroColon = it.next();
			String fila = this.toHtmlFila(registroColon);
			htmlTabla = htmlTabla.concat(fila);
		}
		htmlTabla = htmlTabla.concat("</table>");
		return htmlTabla;
	}

	public String toHtmlFila(RegistroColon rc) {
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		String html;
		html = "<tr><td>" + rc.getPaciente().getId() + "</td>" + "<td>" + rc.getPaciente().getApellidosNombre()
				+ " </td> ";
		if (rc.getAnaliticaDate() != null) {
			html = html.concat("<td>" + fechadma.format(rc.getAnaliticaDate()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		if (rc.getResultadoString() != null) {
			html = html.concat("<td>" + rc.getResultadoString() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rc.getCitaColonoDate() != null) {
			html = html.concat("<td>" + fechadma.format(rc.getCitaColonoDate()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rc.getInformeAnatoDate() != null) {
			html = html.concat("<td>" + fechadma.format(rc.getInformeAnatoDate()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rc.getInformeAnatoDate() != null) {
			html = html.concat("<td>" + fechadma.format(rc.getInformeAnatoDate()) + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}

		if (rc.getObservacionesValor() != null) {
			html = html.concat("<td>" + rc.getObservacionesValor() + "</td> ");
		} else {
			html = html.concat("<td>&nbsp</td> ");
		}
		html = html.concat("</tr>");
		return html;
	}
}
