package com.jnieto.controlador;

import com.jnieto.dao.PacienteDAO;
import com.jnieto.entity.Paciente;

/**
 * The Class PacienteControlador.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PacienteControlador {

	/**
	 * Instantiates a new paciente controlador.
	 */
	public PacienteControlador() {
	}

	/**
	 * Inserta paciente.
	 *
	 * @param paciente the paciente
	 * @return true, if successful
	 */
	public boolean insertaPaciente(Paciente paciente) {
		PacienteDAO pacientaDAO = new PacienteDAO();
		return pacientaDAO.actualizaDatos(paciente);
	}

	/**
	 * Actualiza paciente.
	 *
	 * @param paciente the paciente
	 * @return true, if successful
	 */
	public boolean actualizaPaciente(Paciente paciente) {
		PacienteDAO pacientaDAO = new PacienteDAO();
		return pacientaDAO.actualizaDatos(paciente);
	}

}
