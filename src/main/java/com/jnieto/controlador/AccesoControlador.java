/*
 * 
 */
package com.jnieto.controlador;

import com.jnieto.dao.AccesosDAO;
import com.jnieto.entity.Acceso;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.PantallaLogin;
import com.jnieto.utilidades.Constantes;
import com.vaadin.server.VaadinSession;

/**
 * The Class AccesoControlador.
 * 
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class AccesoControlador {

	/** The acceso. */
	private Acceso acceso = null;
	private String prefijo = "Co.";

	/**
	 * Instantiates a new acceso controlador.
	 */
	public AccesoControlador() {
		acceso = new Acceso();

	}

	/**
	 * Haz acceso usuario no autorizado. Como el usuario no existe en la base de
	 * dados por problemas de integridad no se puede inserta en la tablaa accesos
	 * por lo que se inserta el comentario en la descripcion del motivo con el
	 * código del usuario con el que se ha intentado acceder.
	 * 
	 * @param userid the userid
	 */
	public void doAccesoUsuarioNoAutorizado(String userid) {
		acceso.setTipo(Acceso.ACCESO_USUARIO_NO_ENCONTRADO);
		// como el usuario no existe por integridad se guarda en el campo motivo
		acceso.setMotivo(prefijo + "Usuario no encontrado en bbdd  " + userid);
		new AccesosDAO().doInsertAccesoUsuarioNoAutorizado(acceso);
	}

	/**
	 * Haz acceso usuario de baja.
	 *
	 * @param userid the userid
	 */
	public void doAccesoUsuarioDeBaja(Usuario userid) {
		// Acceso acceso = new Acceso();
		acceso.setTipo(Acceso.ACCESO_USUARIO_DE_BAJA);
		acceso.setUserid(userid);
		acceso.setMotivo(prefijo + PantallaLogin.LOGIN_USUARIO_NOACTIVO);
		new AccesosDAO().doInsertAccesoUsuarioDebaja(acceso);
	}

	/**
	 * Haz acceso usuario clave incorrecta.
	 *
	 * @param userid the userid
	 */
	public void doAccesoUsuarioClaveIncorrecta(Usuario userid) {
		acceso.setTipo(Acceso.ACCESO_USUARIO_CLAVE_INCORRECTA);
		acceso.setUserid(userid);
		acceso.setMotivo(prefijo + PantallaLogin.LOGIN_CONTRASEÑAINCORRECTA);
		new AccesosDAO().doInsertAccesoUsuarioDebaja(acceso);
	}

	/**
	 * Haz acceso usuario login ok.
	 *
	 * @param userid the userid
	 */
	public void doAccesoUsuarioLoginOk(Usuario userid) {
		acceso.setTipo(Acceso.ACCESO_LOGIN);
		acceso.setUserid(userid);
		acceso.setMotivo(prefijo + PantallaLogin.LOGIN_OK);
		new AccesosDAO().doInsertAccesoLoginOk(acceso);
	}

	public void doAccesoUsuarioLogout(Usuario userid) {
		acceso.setTipo(Acceso.ACCESO_LOGOUT);
		acceso.setUserid(userid);
		acceso.setMotivo(prefijo + Acceso.ACCESO_LOGOUT.getDescripcion());
		new AccesosDAO().doInsertAccesoLoginOk(acceso);
	}

	/**
	 * Haz acceso usuario paciente.
	 *
	 * @param paciente the paciente
	 */
	public void doAccesoUsuarioPaciente(Paciente paciente) {
		acceso.setTipo(Acceso.ACCESO_PACIENTE);
		acceso.setUserid((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME));
		acceso.setMotivo(prefijo + Acceso.ACCESO_PACIENTE.getDescripcion());
		acceso.setPaciente(paciente);
		new AccesosDAO().doInsertAccesoPaciente(acceso);
	}

	public void doAccesoUsuarioRegistro(Registro registro) {
		acceso.setTipo(Acceso.ACCESO_REGISTRO);
		acceso.setUserid((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME));
		acceso.setMotivo(prefijo + Acceso.ACCESO_REGISTRO.getDescripcion());
		acceso.setPaciente(registro.getPaciente());
		acceso.setEpiinfo(registro.getId());
		new AccesosDAO().doInsertAccesoRegistro(acceso);
	}

	public void doAccesoUsuarioProceso(Proceso proceso) {
		acceso.setTipo(Acceso.ACCESO_PROCESO);
		acceso.setUserid((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME));
		acceso.setMotivo(prefijo + Acceso.ACCESO_PROCESO.getDescripcion());
		acceso.setPaciente(proceso.getPaciente());
		acceso.setEpiinfo(proceso.getId());
		new AccesosDAO().doInsertAccesoRegistro(acceso);
	}

	public void doAccesoUsuarioPacienteRest(Paciente paciente, Usuario usuario) {
		acceso.setTipo(Acceso.ACCESO_SERVICO_RES);
		acceso.setUserid(usuario);
		acceso.setMotivo(prefijo + Acceso.ACCESO_SERVICO_RES.getDescripcion());
		acceso.setPaciente(paciente);
		new AccesosDAO().doInsertAccesoPaciente(acceso);
	}

}
