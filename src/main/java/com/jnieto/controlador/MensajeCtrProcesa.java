/*
 * 
 */
package com.jnieto.controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.dao.MensajesDAO;
import com.jnieto.dao.ParametroDAO;
import com.jnieto.entity.Mensaje;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.excepciones.MensajesExcepciones;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.MandaMail;
import com.jnieto.utilidades.Utilidades;

/**
 * 
 * Classe MensajeCtrProcesa. Procesa los mensajes pendienentes de la tabla
 * enviado al canal definido para cada uno de ellos
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class MensajeCtrProcesa {

	private static final Logger logger = LogManager.getLogger(MensajeCtrProcesa.class);

	/**
	 * 
	 * Constructor.
	 * 
	 */
	public MensajeCtrProcesa() {
		ArrayList<Mensaje> mensajesPendientes = new ArrayList<>();
		mensajesPendientes = new MensajesDAO().getListaPendientes10();
		while (mensajesPendientes.size() > 0) {
			for (Mensaje mensaje : mensajesPendientes) {
				mensaje.setFecha_proceso(Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
				mensaje.setHora_proceso(Utilidades.getHoraNumeroAcual());
				try {
					if (mensaje.getTipo().getTipo() == Mensaje.MENSAJE_TIPO_MAIL.getTipo()) {
						new MandaMail().sendEmail(mensaje.getUserid_destino(), " Aviso ", mensaje.getContenido());
						mensaje.setEstado(Mensaje.MENSAJE__ENVIADO);
						new MensajesDAO().setMarcaEnviado(mensaje);
					} else if (mensaje.getTipo().getTipo() == Mensaje.MENSAJE_TIPO_SMS.getTipo()) {
						doMandaSms(mensaje);
						mensaje.setEstado(Mensaje.MENSAJE__ENVIADO);
						new MensajesDAO().setMarcaEnviado(mensaje);
					} else if (mensaje.getTipo().getTipo() == Mensaje.MENSAJE_TIPO_WHASTASP.getTipo()) {
						doMandaWhastasp(mensaje);
						mensaje.setEstado(Mensaje.MENSAJE__ENVIADO);
						new MensajesDAO().setMarcaEnviado(mensaje);
					}
				} catch (Exception e) {
					mensaje.setError(e.getMessage());
					mensaje.setEstado(Mensaje.MENSAJE_ERROR_ENVIO);
					new MensajesDAO().setMarcaError(mensaje);
				}
			}
			return;
			// mensajesPendientes = new MensajesDAO().getListaPendientes10();
		}
	}

	/**
	 * Do manda whastasp.
	 *
	 * @param mensaje the mensaje
	 * @throws MensajesExcepciones
	 */
	public void doMandaWhastasp(Mensaje mensaje) throws MensajesExcepciones {
		String urlString = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.URL_WHATSAPP).getValor();
		if (urlString == null) {
			new NotificacionInfo("Sin datos para el parámetro " + ParametBBDD.URL_WHATSAPP);
		} else {
			urlString = urlString.replace("%NUMERO%", mensaje.getUserid_destino());
			urlString = urlString.replace("%MENSAJE%", "'" + mensaje.getContenidoCorto().trim() + "'");

			try {
				URL pcgull = new URL(urlString);
				URLConnection pcg;
				pcg = pcgull.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(pcg.getInputStream()));

				String inputLine;

				while ((inputLine = in.readLine()) != null)
					System.out.println(inputLine);
				in.close();
			} catch (IOException e) {
				logger.error(e.getMessage());
				throw new MensajesExcepciones("Error envio whatsapp " + e.getMessage());
			}
		}
	}

	/**
	 * Do manda sms.
	 *
	 * @param mensaje the mensaje
	 */
	public void doMandaSms(Mensaje mensaje) {

	}
}
