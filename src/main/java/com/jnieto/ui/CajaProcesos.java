package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.jnieto.utilidades.Constantes;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Grid;

public class CajaProcesos extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3516664772766185898L;
	private Grid<Proceso> grid = new Grid<Proceso>();
	private ArrayList<Proceso> listaProcesos = new ArrayList<>();
	private Long subAmbitoLong;

	public CajaProcesos(Paciente paciente, Long subambito) {
		super(paciente, subambito);
		lbltitulo.setCaption(Proceso.getDescripcionSubambito(subambito));
		grid.setSizeUndefined();
		grid.setHeightByRows(5);
		// grid.setColumnOrder(".");
		if (((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
				.getEstado() == Usuario.USUARIO_ESTADOADMINISTRADOR) {
			grid.addColumn(Proceso::getId).setCaption("Id");
		}
		grid.addColumn(Proceso::getFechaHoraInicio).setCaption("Fecha");
		grid.addColumn(Proceso::getDescripcion);
		grid.setItems(listaProcesos);
		grid.setItems(listaProcesos);
		grid.addSelectionListener(e -> seleccionaClick());
		grid.setStyleGenerator(e -> getEstilo(e));
		contenedorGrid.addComponent(grid);
		refrescarClick();
	}

	private String getEstilo(Proceso proceso) {
		if (proceso.getFechafin() != null) {
			return "RED";
		}
		return null;
	}

	@Override
	public void nuevoClick() {
		proceso = new Proceso();
		proceso.setPaciente(paciente);
		proceso.setSubambito(subambito);
		FrmProceso frmRegistroProceso = new FrmProceso(proceso);
		VentanaFrm ventanaFrm = new VentanaFrm(this.getUI(), frmRegistroProceso, proceso.getDescripcion());
		ventanaFrm.addCloseListener(e -> cierraVentana());
	}

	public void cierraVentana() {
		refrescarClick();
	}

	@Override
	public void refrescarClick() {
		listaProcesos = new ProcesoDAO().getListaProcesosPacienteSubambito(paciente, subambito);
		grid.setItems(listaProcesos);
		if (listaProcesos.size() > 0) {
			grid.select(listaProcesos.get(0));
		}
		doActivaBotones();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {
		if (grid.getSelectedItems().size() > 0) {
			proceso = grid.getSelectedItems().iterator().next();
//			Object object = getUI().getParent();

//			System.out.print(object.getClass().getName());
			// ((ConsolaMaster) getUI().getParent()).refrescar();

		} else {
			if (listaProcesos.size() > 0)
				proceso = listaProcesos.get(0);
		}

	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			proceso = grid.getSelectedItems().iterator().next();
			FrmProceso frmRegistroProceso = new FrmProceso(proceso);
			VentanaFrm ventanaFrm = new VentanaFrm(this.getUI(), frmRegistroProceso,
					" Datos del proceso de " + proceso.getDescripcion());
			// ventanaFrm.addCloseListener(e -> cierraVentana());
		} else {
			new NotificacionInfo(NotificacionInfo.PROCESO_PACIENTE_SIN_ELEIGR_PROCESO);
		}
	}

	public void doActivaBotones() {
		if (grid.getSelectedItems().size() > 0)
			editar.setEnabled(true);
		else
			editar.setEnabled(false);

		nuevo.setEnabled(new ProcesoDAO().getTodosLosProcesosCerrados(paciente.getId(), subambito));

	}

}
