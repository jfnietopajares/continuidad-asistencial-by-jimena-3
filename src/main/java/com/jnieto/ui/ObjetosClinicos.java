package com.jnieto.ui;

import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.ComboBox;

public class ObjetosClinicos {

	public static ArrayList<String> PARTOS_GESTACION_SEROLO_RUBEOLA = new ArrayList<String>() {
		{
			add("Positivo");
			add("Negativo");
			add("Dudoso");
		}
	};

	public static ArrayList<String> PARTOS_TIPOS_INGRESO = new ArrayList<String>() {
		{
			add("");
			add("Trabajo de parto");
			add("RPM");
			add("Inducción");
			add("Cesárea programada");
		}
	};

	public static ArrayList<String> PARTOS_TIPOS_SITUACION = new ArrayList<String>() {
		{
			add("");
			add("Longitudinal");
			add("Transversa");
			add("Oblícua");
		}
	};

	public static ArrayList<String> PARTOS_TIPOS_PRESENTACION = new ArrayList<String>() {
		{

			add("");
			add("Longitudinal");
			add("Podálica");
			add("Otras");
		}
	};

	public static ArrayList<String> PARTOS_INDU_PATOLOGIAFETAL = new ArrayList<String>() {
		{

			add("");
			add("1");
			add("2");
			add("3");
		}
	};

	public static ArrayList<String> PARTOS_DOLOR_FARMACOLOGICO = new ArrayList<String>() {
		{
			add("");
			add("A.Epidural");
			add("A.Raquídea");
			add("Mórficos");
			add("A.General");
			add("AE+AG");
			add("AE+AR");
		}
	};

	public static ArrayList<String> PARTOS_CTE_TIPO_CARDIOTOGRAFO = new ArrayList<String>() {
		{
			add("");
			add("Externo");
			add("Interno");
			add("No registro");
		}
	};

	public static ArrayList<String> PARTOS_EXPUL_TIPOPARTO = new ArrayList<String>() {
		{
			add("");
			add("Parto vaginal");
			add("Eutócico");
			add("Ventosa");
			add("Forceps");
			add("Espátulas");
		}
	};

	public static ArrayList<String> PARTOS_EXPUL_INDICACIONCESAREA = new ArrayList<String>() {
		{
			add("");
			add("A");
			add("B");
			add("C");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_TIPOPARTO = new ArrayList<String>() {
		{
			add("");
			add("Dirigido");
			add("Espontáneo");
			add("Manual");
		}
	};
	public static ArrayList<String> PARTOS_ALUMBRA_PLACENTA = new ArrayList<String>() {
		{
			add("");
			add("Normal");
			add("Patológica");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_MEMBRANAS = new ArrayList<String>() {
		{
			add("");
			add("Integras");
			add("Desgarradas");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_CORDONVASOS = new ArrayList<String>() {
		{
			add("");
			add("3 Vasos");
			add("Arteria única");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_CORDONINSERCION = new ArrayList<String>() {
		{
			add("");
			add("Central");
			add("Lateral");
			add("Velamentosa");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_CORDONDONACION = new ArrayList<String>() {
		{
			add("");
			add("No");
			add("Pública");
			add("Privada");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_HEMORRAGIAVOLUMEN = new ArrayList<String>() {
		{
			add("");
			add("<500ml");
			add(">500ml");
			add(">1000ml");
		}
	};

	public static ArrayList<String> PARTOS_ALUMBRA_HEMORRAGIATRATO = new ArrayList<String>() {
		{
			add("");
			add("Trendelemburg");
			add("Sueroterapia");
			add("Tratamiento");
		}
	};

	public static ArrayList<String> PARTOS_GESTA_ECO20SEMANAS = new ArrayList<String>() {
		{
			add("");
			add("Normal");
			add("Alterada");
		}
	};

	public static ArrayList<String> PARTOS_GESTA_VALORSGB = new ArrayList<String>() {
		{
			add("");
			add("Positivo");
			add("Negativo");
			add("Desconocido");
		}
	};

	public static ArrayList<String> PARTOS_GESTA_TECREPROASIST = new ArrayList<String>() {
		{
			add("");
			add("Inseminación asistida");
			add("Fecundación in vitro");
			add("Ovodonación");
		}
	};
	public static ArrayList<String> PARTOS_RN_REANIMIACION = new ArrayList<String>() {
		{
			add("");
			add("0-Piel con piel");
			add("No reanimación");
			add("I-Secado");
			add("Aspiración");
			add("II-CPAP");
			add("III-Presión Positiva");
			add("IV-Intubación y/o Masaje cardiaco");
			add("V-Medicación");
		}
	};
	public static ArrayList<String> PARTOS_RN_CORDON = new ArrayList<String>() {
		{
			add("");
			add("Precoz");
			add("Tardío");
		}
	};
	public static ArrayList<String> PARTOS_RN_PIELPIEL = new ArrayList<String>() {
		{
			add("");
			add("Madre");
			add("Padre");
		}
	};

	public static ArrayList<String> PARTOS_RN_PROFILAXISSBG = new ArrayList<String>() {
		{
			add("");
			add("Completa");
			add("Incompleta");
			add("Sin profilaxis");
		}
	};

	public static ArrayList<String> GRUPO_ABO = new ArrayList<String>() {
		{
			add("");
			add("O+");
			add("A+");
			add("B+");
			add("AB+");
			add("O-");
			add("A-");
			add("B-");
			add("AB-");

		}
	};

	public static ArrayList<String> HDIA_ONCO_DIETAHABITUAL = new ArrayList<String>() {
		{
			add("");
			add("Normal");
			add("Blanda");
			add("Turmix");
			add("Diabético");
			add("Sin sal");
			add("Probre en grasas");

		}
	};
	public static ArrayList<String> HDIA_ONCO_INGESTALIQUIDOS = new ArrayList<String>() {
		{
			add("");
			add("1 l día");
			add("1.5 l día");
			add("2 l días");
			add("Escada");
			add("Abundadnte");
		}
	};
	public static ArrayList<String> HDIA_ONCO_MOVILIZACION = new ArrayList<String>() {
		{
			add("");
			add("Autónomo");
			add("Ayuda parcial");
			add("Ayuda total");
		}
	};
	public static ArrayList<String> HDIA_ONCO_KARNOFSKY = new ArrayList<String>() {
		{
			add("");
			add("100 - Normal. Sin quejas. Sin indicios de enfermedad");
			add("90 - Actividades normales pero con signos y síntomas leves de enfermedad");
			add("80 - Actividad normal con esfuerzo con algunos signos y síntomas de enfermedad");
			add("70 - Capaz de cuidarse. Pero incapaz de llevar a término actividades normales de trabajo activo");
			add("60 - Requiere atención ocasional pero puede cuidarse a si mismo");
			add("50 - Requiere gran atención incluso de tipo médico. Encamado menos del 50% del día");
			add("40 - Inválido incapacitado necesita cuidados y atenciones especiales. Encamado más del 50% del día");
			add("30 - Inválido grave severamente incapacitado tratamiento de soporte activo");
			add("20 - Encamado por completo. Paciente muy grave. Necesita hospitalización y tratamiento activo");
			add("10 - Moribundo");
			add("0 - Fallecido");
		}
	};

	public static ArrayList<String> HDIA_ONCO_TRANSTORNOSUEÑO = new ArrayList<String>() {
		{
			add("");
			add("No manifiesta alteración");
			add("Insomnio");
			add("Problemas para conciliar el sueño");
		}
	};

	public static ArrayList<String> HDIA_ONCO_VESTIDOASEO = new ArrayList<String>() {
		{
			add("");
			add("Autónomo");
			add("Necesita ayuda parcial");
			add("Necesita ayuda total");
		}
	};

	public static ArrayList<String> HDIA_ONCO_PIELHERIDAS = new ArrayList<String>() {
		{
			add("");
			add("Quirúrgica");
			add("Quemadura");
			add("UPP");
			add("Traumática");
			add("Úlcera vascular");
		}
	};

	public static ArrayList<String> HDIA_ONCO_ACCESOS = new ArrayList<String>() {
		{
			add("");
			add("Reservorio");
			add("Catéter Hickman");
			add("PICC");
			add("Abbocath");
			add("Abbocath 20");
			add("Abbocath 22");
		}
	};

	public static ArrayList<String> EVA_DOLOR = new ArrayList<String>() {
		{
			add("");
			add("0");
			add("1");
			add("2");
			add("3");
			add("4");
			add("5");
			add("6");
			add("7");
			add("8");
			add("9");
			add("10");
		}
	};
	public static ArrayList<String> VALORES_0_5 = new ArrayList<String>() {
		{
			add("");
			add("0");
			add("1");
			add("2");
			add("3");
			add("4");
			add("5");
		}
	};

	public static ArrayList<String> ACOMPANANTES = new ArrayList<String>() {
		{
			add("");
			add("Conyuge");
			add("Padre");
			add("Madre");
			add("Hijo");
			add("Hija");
			add("Familiar 2º grado");
			add("Amigo");
		}
	};

	public static ArrayList<String> POSITIVO_NEGATIVO = new ArrayList<String>() {
		{
			add("Positivo");
			add("Negativo");
		}
	};

	public ObjetosClinicos() {
	}

	public ComboBox<String> getComoSiNo(String captacion, String valorDefecto) {
		@SuppressWarnings("serial")
		ArrayList<String> listaTipos = new ArrayList<String>() {
			{
				add("Si");
				add("No");

			}
		};
		ComboBox<String> combo = new ComboBox<>();
		combo.setCaption(captacion);
		combo.setItems(listaTipos);
		combo.setWidth("80px");
		combo.setEmptySelectionAllowed(true);
		if (valorDefecto != null)
			combo.setSelectedItem(valorDefecto);
		else if (listaTipos.size() == 1)
			combo.setSelectedItem(listaTipos.get(0));

		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.COMBOBOX_CUSTOM);
		return combo;
	}

	public ComboBox<String> getComboString(String captacion, String valorDefecto, ArrayList<String> listaTipos,
			String ancho) {
		ComboBox<String> combo = new ComboBox<>();
		combo.setCaption(captacion);
		combo.setItems(listaTipos);
		combo.setWidth(ancho);
		combo.setEmptySelectionAllowed(true);
		if (valorDefecto != null)
			combo.setSelectedItem(valorDefecto);
		else if (listaTipos.size() == 1)
			combo.setSelectedItem(listaTipos.get(0));

		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.COMBOBOX_CUSTOM);
		return combo;
	}

}
