package com.jnieto.ui;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroEvolutivoMedico;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.TextArea;

public class FrmRegistroEvolutivoMedico extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3965350295889367817L;

	private TextArea situAcutal = new TextArea("Situación actal");
	private TextArea tratamiento = new TextArea("Tratamiento");

	private RegistroEvolutivoMedico registroEvolutivo;
	private Binder<RegistroEvolutivoMedico> binder = new Binder<RegistroEvolutivoMedico>();

	public FrmRegistroEvolutivoMedico(Registro registroParam) {
		this.setMargin(false);
		this.setSpacing(false);
		this.registroEvolutivo = (RegistroEvolutivoMedico) registroParam;
		this.addStyleName(MaterialTheme.LAYOUT_WELL);
		contenedorCampos.removeAllComponents();

		situAcutal.setWidth("400px");
		situAcutal.setHeight("100px");
		// situAcutal.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM +
		// MaterialTheme.TEXTAREA_SMALL + MaterialTheme.CARD_4);
		situAcutal.addStyleName(MaterialTheme.LAYOUT_WELL);
		binder.forField(situAcutal).bind(RegistroEvolutivoMedico::getSitActualString,
				RegistroEvolutivoMedico::setSitActual);
		fila1.addStyleName(MaterialTheme.LAYOUT_WELL);
		fila1.addComponent(situAcutal);

		tratamiento.setWidth("400px");
		tratamiento.setHeight("100px");
		tratamiento.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		fila2.addComponent(tratamiento);
		fila2.addStyleName(MaterialTheme.LAYOUT_WELL);
		contenedorCampos.addComponents(fila1, fila2);

		binder.forField(tratamiento).bind(RegistroEvolutivoMedico::getTratamientoString,
				RegistroEvolutivoMedico::setTratamiento);

		doActivaBotones();
		binder.readBean(registroEvolutivo);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroEvolutivo);
			if (!new RegistroDAO().grabaDatos(registroEvolutivo)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}

	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
