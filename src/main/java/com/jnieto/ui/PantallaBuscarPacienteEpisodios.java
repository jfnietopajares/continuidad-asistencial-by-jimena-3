
package com.jnieto.ui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.dao.AgendaDAO;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.ServiciosDAO;
import com.jnieto.dao.ZonaDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Perfil;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Zona;
import com.jnieto.ui.master.PantallaPaginacion;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.ui.quirofano.ConsolaQuirofano;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.LocalDateRenderer;

/**
 * The Class PantallaBuscarPacientes.
 *
 * @author JuanNieto *
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 *          Clase para las busquedas de pacientes de diferentes tipos
 */
public class PantallaBuscarPacienteEpisodios extends PantallaPaginacion {

	private static final long serialVersionUID = 6802394604145200557L;

	private Grid<Paciente> grid = new Grid<>();

	public TextField filtro = new TextField();

	private HorizontalLayout filaBuscador = new HorizontalLayout();

	private Button buscar, nuevo, ayuda, cerrar;

	private ArrayList<Paciente> listaPacientes = new ArrayList<>();

	private TabSheet tabsheet = new TabSheet();

	private EpisodioClase clase = Episodio.CLASE_HOSPITALIZACION;

	private Label lblDescripcionFiltro = new Label();

	private DateField fecha = null;

	private ComboBox<EpisodioClase> comboClasePaci = null;

	private ComboBox<Centro> comboCentro = null;

	private ComboBox<Servicio> comboServicio = null;

	private ComboBox<Agenda> comboAgenda = new ComboBox<Agenda>();

	private ComboBox<Zona> comboZona = new ComboBox<Zona>();

	private ArrayList<Zona> listaZonas = new ArrayList<>();

	private ArrayList<Agenda> listaAgendas = new ArrayList<>();

	private ArrayList<Servicio> listaServicios = new ArrayList<>();

	private Servicio servicio = null;

	private Centro centro = null;

	private Zona zona = null;

	private Agenda agenda = null;

	private Button pdfCalendario = new ObjetosComunes().getBotonCalendarioVerCitas();

	private DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	private Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);
	private Perfil perfil = new Perfil();

	private VerticalLayout vtLayout = new VerticalLayout();

	public PantallaBuscarPacienteEpisodios(EpisodioClase clase) {
		this.setResponsive(true);
		if (clase != null) {
			this.clase = clase;
			// lblDescripcionFiltro.setCaption("Pacientes de " +
			// this.clase.getDescripcion());
			iniciaComponentes();
		} else {
			new NotificacionInfo("Sin clase de episodio en la llamada");
		}

	}

	public void iniciaComponentes() {
		this.addComponent(tabsheet);
		this.setMargin(false);
		this.setSpacing(false);
		this.setSizeFull();
		this.setResponsive(true);

		this.vtLayout.setWidth("1200px");
		this.vtLayout.setMargin(false);
		this.vtLayout.setSpacing(false);

		filaBuscador.setMargin(false);
		filaBuscador.setResponsive(true);
		filaBuscador.setSpacing(true);

		tabsheet.setStyleName(MaterialTheme.TABSHEET_FRAMED);
		tabsheet.setResponsive(true);
		tabsheet.setSizeFull();

		filtro.setCursorPosition(0);
		filtro.setMaxLength(100);
		filtro.setWidth("150px");

		comboCentro = new ObjetosComunes().getComboCentros(Centro.CENTRO_DEFECTO, Centro.CENTRO_DEFECTO);
		comboCentro.setEmptySelectionAllowed(false);
		comboCentro.addSelectionListener(event -> cambiaCentro());
		comboCentro.setResponsive(true);
		comboCentro.setWidth("120px");

		comboServicio = new ObjetosComunes().getComboServicioCorto(comboCentro.getSelectedItem().get(), null, null,
				"HOS");
		comboServicio.addSelectionListener(event -> cambiaServicio());
		comboServicio.setResponsive(true);
		comboServicio.setWidth("90px");

		comboClasePaci = new ObjetosComunes().getComboEpisodioClase(clase, comboCentro.getSelectedItem().get());
		// comboClasePaci.addBlurListener(event -> cambiaClase());
		comboClasePaci.addSelectionListener(event -> cambiaClase());
		if (clase != null) {
			comboClasePaci.setEnabled(false);
			comboClasePaci.setVisible(false);
		}
		pdfCalendario.setVisible(false);
		if (clase.equals(Episodio.CLASE_HOSPITALIZACION)) {
			comboZona = new ObjetosComunes().getComboZona(null, null, clase.getTipoZona(), comboCentro.getValue(),
					comboServicio.getSelectedItem().get());
			comboZona.setWidth("100px");
			comboZona.addSelectionListener(event -> cambiaZona());
			comboZona.setVisible(true);
			comboAgenda.setVisible(false);
		} else if (clase.equals(Episodio.CLASE_CONSULTAS)) {
			comboAgenda = new ObjetosComunes().getComboAgenda(comboCentro.getSelectedItem().get(),
					comboServicio.getSelectedItem().get(), null, null);
			comboAgenda.addSelectionListener(event -> cambiaAgenda());
			comboAgenda.setVisible(true);
			comboZona.setVisible(false);

			pdfCalendario.setVisible(true);
			pdfCalendario.setCaption("Citados");
			pdfCalendario.setIcon(VaadinIcons.CALENDAR);
			pdfCalendario.addClickListener(e -> clickPdfCalendario());

		} else if (clase.equals(Episodio.CLASE_QUI_PROGRMACION)) {
			comboZona = new ObjetosComunes().getComboZona(null, null, clase.getTipoZona(), comboCentro.getValue(),
					comboServicio.getSelectedItem().get());
			comboZona.setWidth("100px");
			comboZona.addSelectionListener(event -> cambiaZona());
			comboZona.setVisible(true);
			comboAgenda.setVisible(false);
		}
		/*
		 * if (clase.equals(Episodio.CLASE_CONSULTAS)) { comboAgenda.setVisible(true);
		 * listaAgendas = new
		 * AgendaDAO().getListaAgendas(comboCentro.getSelectedItem().get(),
		 * comboServicio.getSelectedItem().get()); comboAgenda.setWidth("150px");
		 * comboAgenda.setItemCaptionGenerator(Agenda::getDescripcion);
		 * comboAgenda.setItems(listaAgendas);
		 * comboAgenda.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM); if
		 * (listaAgendas.size() > 0) { comboAgenda.setSelectedItem(listaAgendas.get(0));
		 * } }
		 */

		fecha = new DateField();
		fecha.setDateFormat("dd/MM/yyyy");
		fecha.setWidth("140px");
		fecha.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		fecha.setValue(LocalDate.now());
		fecha.addValueChangeListener(event -> cambiaFecha());

		doGrid();
		buscar = new ObjetosComunes().getBotonBuscar();
		buscar.addClickListener(event -> clickBuscar());
		buscar.setClickShortcut(KeyCode.ENTER);

		// nuevo = new ObjetosComunes().getBotonNuevo(Constantes.BOTON_TIPO_NORMAL);
		nuevo = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "+", VaadinIcons.PLUS,
				"Añade un nuevo dato.");

		nuevo.addClickListener(event -> clickNuevo());

//		ayuda = new ObjetosComunes().getBotonAyuda(Constantes.BOTON_TIPO_NORMAL);
		ayuda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "?", VaadinIcons.QUESTION,
				"Muestra ayuda de la pantalla");

		ayuda.addClickListener(event -> clickAyuda());

		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(event -> clickCerrar());

		filaBuscador.addComponents(filtro, comboCentro, comboClasePaci, comboServicio, comboZona, comboAgenda, fecha,
				pdfCalendario, buscar, nuevo, ayuda, cerrar, lblDescripcionFiltro);

		vtLayout.addComponents(filaBuscador, grid, filaPaginacion);

		tabsheet.addTab(vtLayout, this.clase.getDescripcion());

		clickBuscar();
	}

	public void clickPdfCalendario() {
		/*
		 * File fpdf = new CalendarioCitasPDF(Centro.HNSS, Servicio.SERVICIO_ONCOLOGIA,
		 * fechaCitas.getValue()).getFile(); if (fpdf != null) { Embedded pdf = new
		 * Embedded("", new FileResource(fpdf)); pdf.setMimeType("application/pdf");
		 * pdf.setType(Embedded.TYPE_BROWSER); pdf.setWidth("750px");
		 * pdf.setHeight("610px"); VerticalLayout contenedorTabLayout = new
		 * VerticalLayout(); contenedorTabLayout.addComponent(pdf);
		 * tabsheet.addTab(contenedorTabLayout, "Calendario").setClosable(true);
		 * tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout)); }
		 */
		PantallaCalendario pt = new PantallaCalendario(fecha.getValue(), Centro.CENTRO_DEFECTO,
				comboServicio.getValue(), comboAgenda.getValue());
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(pt);
		tabsheet.addTab(contenedorTabLayout, "Calendario").setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));

	}

	public void doGrid() {
		capturaValoresCombos();
		grid = new Grid<>();
		grid.setHeightMode(HeightMode.ROW);
		grid.setResponsive(true);
		grid.addColumn(Paciente::getNumerohc).setCaption("Nhc");
		grid.addColumn(Paciente::getFnac, new LocalDateRenderer("dd/MM/yyyy")).setCaption("F.Nac");
		grid.addColumn(Paciente::getApellidosNombre).setCaption("Nombre");
		if (clase != null) {
			if (clase.getId().equals(Episodio.CLASE_HOSPITALIZACION.getId())) {
				grid.addColumn(Paciente::getEpicamaNcama).setCaption("Cama");
				grid.addColumn(Paciente::getEpiservicioCodigo).setCaption("Servicio");
				grid.addColumn(Paciente::getFechaHoraInicio).setCaption("Ingreso");
			} else if (clase.getId().equals(Episodio.CLASE_CONSULTAS.getId())) {
				grid.addColumn(Paciente::getEpiagendaCodigo).setCaption("Agenda");
				grid.addColumn(Paciente::getEpiservicioCodigo).setCaption("Servicio");
				grid.addColumn(Paciente::getFechaHoraInicio).setCaption("Cita");
			} else if (clase.getId().equals(Episodio.CLASE_URGENCIAS.getId())) {
				grid.addColumn(Paciente::getEpiagendaCodigo).setCaption("Agenda");
				grid.addColumn(Paciente::getEpiservicioCodigo).setCaption("Servicio");
				grid.addColumn(Paciente::getFechaHoraInicio).setCaption("Ingreso");
			} else if (clase.getId().equals(Episodio.CLASE_HDIA.getId())) {
				grid.addColumn(Paciente::getEpiagendaCodigo).setCaption("Zona");
				grid.addColumn(Paciente::getEpiservicioCodigo).setCaption("Servicio");
				grid.addColumn(Paciente::getFechaHoraInicio).setCaption("Ingreso");
			} else if (clase.getId().equals(Episodio.CLASE_QUI_PROGRMACION.getId())) {
				grid.addColumn(Paciente::getEpiagendaCodigo).setCaption("Zona");
				grid.addColumn(Paciente::getEpiservicioCodigo).setCaption("Servicio");
				grid.addColumn(Paciente::getFechaHoraInicio).setCaption("F.Intev");
			}
		}

		if (perfil.getPertenecePerfil(usuario, perfil.MEDICOS)) {

		} else if (perfil.getPertenecePerfil(usuario, perfil.ENFERMERAS)) {

		}

		grid.addItemClickListener(event -> selecciona());
		grid.setSizeFull();
		grid.asSingleSelect();
		grid.setHeightByRows(10);
		vtLayout.removeAllComponents();
		vtLayout.addComponents(filaBuscador, grid, filaPaginacion);
	}

	public void capturaValoresCombos() {
		if (comboCentro.getSelectedItem().isPresent()) {
			centro = comboCentro.getSelectedItem().get();
		} else {
			centro = null;
		}
		if (comboClasePaci.getSelectedItem().isPresent()) {
			clase = comboClasePaci.getSelectedItem().get();
		}
		if (comboServicio.getSelectedItem().isPresent()) {
			servicio = comboServicio.getSelectedItem().get();
		} else {
			servicio = null;

		}
		if (comboZona.getSelectedItem().isPresent()) {
			zona = comboZona.getSelectedItem().get();
		} else {
			zona = null;
		}
		if (comboAgenda.getSelectedItem().isPresent()) {
			agenda = comboAgenda.getSelectedItem().get();
		} else {
			agenda = null;
		}
	}

	public void cambiaFecha() {
		capturaValoresCombos();
		clickBuscar();
	}

	public void cambiaCentro() {
		capturaValoresCombos();
		listaZonas = new ZonaDAO().getListaRegistrosTipo(clase.getTipoZona(), comboCentro.getSelectedItem().get());
		comboZona.setItems(listaZonas);
		comboZona.focus();
		if (listaZonas.size() > 0) {
			comboZona.setSelectedItem(listaZonas.get(0));
			cambiaClase();
		}
	}

	public void cambiaClase() {
		capturaValoresCombos();
		listaServicios = new ServiciosDAO().getListaRegistos(centro, clase);
		comboServicio.setItems(listaServicios);
		if (listaServicios.size() > 0) {
			comboServicio.setSelectedItem(listaServicios.get(0));
			cambiaServicio();
		}
		doGrid();
	}

	public void cambiaServicio() {
		capturaValoresCombos();
		if (clase.equals(Episodio.CLASE_CONSULTAS)) {
			listaAgendas = new AgendaDAO().getListaAgendas(centro, servicio);
			comboAgenda.setItems(listaAgendas);
			comboAgenda.focus();
			if (listaAgendas.size() > 0) {
				// comboAgenda.setSelectedItem(listaAgendas.get(0));
			}
			comboAgenda.setVisible(true);
			comboZona.setVisible(false);

		} else {
			listaZonas = new ZonaDAO().getListaRegistrosTipo(clase.getTipoZona(), centro);
			comboZona.setItems(listaZonas);
			comboZona.focus();
			if (listaZonas.size() > 0) {
				// comboZona.setSelectedItem(listaZonas.get(0));
			}
			comboAgenda.setVisible(false);
			comboZona.setVisible(true);
		}
		clickBuscar();
	}

	public void cambiaAgenda() {
		capturaValoresCombos();
		clickBuscar();
	}

	public void cambiaZona() {
		capturaValoresCombos();
		clickBuscar();
	}

	/**
	 * Click buscar.
	 */
	public void clickBuscar() {
		capturaValoresCombos();
		doGrid();
		paginacion = new PacienteDAO().getPaginacionPaciente(filtro.getValue().toUpperCase(), "EPISODIOS",
				clase.getId(), centro, servicio, zona, fecha.getValue(), agenda);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
	}

	/**
	 * Click nuevo.
	 */
	public void clickNuevo() {
		((MyUI) getUI()).actualiza(new FrmPaciente(new Paciente()));
	}

	/**
	 * Click ayuda.
	 */
	public void clickAyuda() {
		new VentanaHtml(this.getUI(), new Label(PantallaBuscarPacientesProcesos.AYUDA_PANTALLA_BUSCAPACIENTE));

	}

	/**
	 * Click cerrar.
	 */
	public void clickCerrar() {
		this.removeAllComponents();
	}

	/**
	 * Selecciona.
	 * 
	 * Cada paciente abre una nueva solapa
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			Paciente paciente = grid.getSelectedItems().iterator().next();
			if (clase.getId() > 0) {
				VerticalLayout contenedorTabLayout = new VerticalLayout();
				contenedorTabLayout.setMargin(false);
				contenedorTabLayout.setSpacing(false);
				contenedorTabLayout.setResponsive(true);
				new AccesoControlador().doAccesoUsuarioPaciente(paciente);
				if (paciente.getEpisodioActual().getClaseLong().equals(Episodio.CLASE_HDIA.getId())) {
					contenedorTabLayout.addComponent(new ConsolaUCA(paciente, Episodio.CLASE_HDIA));
				} else if (paciente.getEpisodioActual().getClaseLong().equals(Episodio.CLASE_QUI_PROGRMACION.getId())) {
					contenedorTabLayout.addComponent(new ConsolaQuirofano(paciente, Episodio.CLASE_QUI_PROGRMACION));
				} else {
					// if
					// (paciente.getEpisodioActual().getClaseLong().equals(Episodio.CLASE_HOSPITALIZACION.getId()))
					// {
					PantallaHCE pche = new PantallaHCE(paciente, this.centro, this.servicio);
					contenedorTabLayout.removeAllComponents();
					contenedorTabLayout.addComponent(pche);
				}
				tabsheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
				tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
			} else {
				((MyUI) getUI()).actualiza(new FrmPaciente(paciente));
			}
		}

	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		capturaValoresCombos();

		listaPacientes = new PacienteDAO().getListaPacientes(filtro.getValue().toUpperCase(), paginacion, "EPISODIOS",
				clase.getId(), centro, servicio, zona, fecha.getValue(), agenda);
		setValoresPgHeredados(listaPacientes);
		if (listaPacientes.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaPacientes);
			if (listaPacientes.size() == 1) {
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaPacientes the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Paciente> listaPacientes) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();

		for (Paciente paciente : listaPacientes) {
			listaValoresArrayList.add(paciente.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Gets the fila buscador.
	 *
	 * @return the fila buscador
	 */
	public HorizontalLayout getFilaBuscador() {
		return filaBuscador;
	}

	/**
	 * Gets the filtro.
	 *
	 * @return the filtro
	 */
	public TextField getFiltro() {
		return filtro;
	}

	/**
	 * Sets the filtro.
	 *
	 * @param filtro the new filtro
	 */
	public void setFiltro(TextField filtro) {
		this.filtro = filtro;
	}

	public TabSheet getTabsheet() {
		return tabsheet;
	}

	public void setTabsheet(TabSheet tabsheet) {
		this.tabsheet = tabsheet;
	}

	/** The Constant AYUDA_FRM_CENTROS. */
	public static final String AYUDA_PANTALLA_BUSCAPACIENTE = "<b>Pantalla de  búsqueda de pacientes. \n  </b> <br>"
			+ " Si hay un proceso seleccionado, permite buscar pacientes con procesos activos.<br>"
			+ " Si noy hay un proceso seleccionado, realiza una búsqued general de pacientes.<br>"
			+ " Seleccionando una paciente se accede a la pantalla de edición del procesos y los <br>"
			+ " registros asociados al proceso. <br>"
			+ " El formulario de cada paciente se abre en una nueva solapa lo que permite  trabajar. <br>"
			+ " con varios pacientes de forma simultánea <br>" + "<hr>" + "<ul> "
			+ "<li><b>Filtro:</b>  Es posible buscar por nombre, uno o dos apellidos, dni o historia.</li>"
			+ "<li>        Las búsquedas se realizan por combinaciones de todos los campos.</li>"
			+ "<li><b>Buscar:</b>Botón para buscar.</li>"
			+ "<li><b>+</b> Abre a ficha completa de datos del paciente	</li>" + "<li><b>?</b> Esta pantall</li>"
			+ "<li><b>x</b> Cierra la ventana </li>" + "</ul>  <hr> ";

}
