package com.jnieto.ui.admin;

import java.util.ArrayList;

import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.PagiLisReg;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.PantallaMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;

/**
 * The Class PantallaUsuarios. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaUsuarios extends PantallaMaster {

	private static final long serialVersionUID = -2108893681545784391L;

	private Grid<Usuario> grid = new Grid<>();

	private Usuario usuario = new Usuario();

	private UsuarioDAO usuarioDao = new UsuarioDAO();

	private ArrayList<Usuario> listaUsuarios = new ArrayList<>();

	/**
	 * Instantiates a new pantalla usuarios.
	 */
	public PantallaUsuarios() {
		super();
		filtro.setPlaceholder("filtro apellidos userid o  mail ");
		filtro.setValue("");

		grid.setCaption(" Lista de usuarios ");
		grid.addColumn(Usuario::getUserid).setCaption("Id");
		grid.addColumn(Usuario::getApellido1).setCaption("Apellido1");
		grid.addColumn(Usuario::getApellido2).setCaption("Apellido2");
		grid.addColumn(Usuario::getNombre).setCaption("Nombre");
		grid.setItems();
		grid.addItemClickListener(event -> selecciona());
		grid.setStyleGenerator(e -> getEstilo(e));

		paginacion = new UsuarioDAO().getPaginacionDatosaUsuariosApellidos(filtro.getValue());
		paginacion.setNumeroRegistrosPagina(paginacion.getNumeroRegistrosPagina());
		grid.setHeight(Utilidades.getAltoGrid(paginacion.getNumeroRegistrosPagina()));
		setValoresGridBotones();
		contenedorFormulario.setVisible(false);
		contenedorGrid.addComponents(contenedorBotones, grid, filaPaginacion);
	}

	private String getEstilo(Usuario e) {
		if (e.getEstado() == 0) {
			return "RED";
		}
		return null;
	}

	/**
	 * Buscar click.
	 */
	public void buscarClick() {
		paginacion = new UsuarioDAO().getPaginacionDatosaUsuariosApellidos(filtro.getValue());
		paginacion = new PagiLisReg();
		setValoresGridBotones();
		if (listaUsuarios.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
			filtro.clear();
			filtro.focus();
		} else {

			if (listaUsuarios.size() == 1) {
				try {
					usuario = new UsuarioDAO().getUsuarioLogin(listaUsuarios.get(0).getUserid());
				} catch (LoginException e) {
					e.printStackTrace();
				} catch (UsuarioBajaException e) {
					e.printStackTrace();
				}
				doFormularioUsuario(usuario);
			} else {
				setValoresPgHeredados(listaUsuarios);
			}
		}
	}

	/**
	 * Anadir click.
	 */
	public void anadirClick() {
		usuario = new Usuario();
		doFormularioUsuario(usuario);
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		filtro.clear();
		filtro.focus();
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		contenedorGrid.setEnabled(true);
		paginacion = new UsuarioDAO().getPaginacionDatosaUsuariosApellidos(filtro.getValue());
		setValoresGridBotones();
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(PantallaUsuarios.AYUDA_PANTALLA_USUARIOS));
	}

	/**
	 * Cerrar click.
	 */
	public void cerrarClick() {
		this.removeAllComponents();
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			usuario = usuarioDao.getUsuarioUserid(grid.getSelectedItems().iterator().next().getUserid(), false);
			doFormularioUsuario(usuario);
		}
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {

		listaUsuarios = new UsuarioDAO().getListaUsuariosPaginados(filtro.getValue().toUpperCase(), paginacion);

		setValoresPgHeredados(listaUsuarios);
		if (listaUsuarios.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			grid.setItems(listaUsuarios);
			if (listaUsuarios.size() == 1) {
				usuario = listaUsuarios.get(0);
				doFormularioUsuario(usuario);
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaUsuarios Construye arrayList con los núemeros de orden del array
	 *                      list de registros para registar valores en los botones
	 *                      de paginación
	 */
	public void setValoresPgHeredados(ArrayList<Usuario> listaUsuarios) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (Usuario usuario : listaUsuarios) {
			listaValoresArrayList.add(usuario.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Do formulario usuario.
	 *
	 * @param usuario the usuario
	 */
	public void doFormularioUsuario(Usuario usuario) {
		contenedorGrid.setEnabled(false);
		contenedorFormulario.setVisible(true);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmUsuario(usuario));
	}

	/**
	 * Gets the grid.
	 *
	 * @return the grid
	 */
	public Grid<Usuario> getGrid() {
		return grid;
	}

	/**
	 * Sets the grid.
	 *
	 * @param grid the new grid
	 */
	public void setGrid(Grid<Usuario> grid) {
		this.grid = grid;
	}

	/**
	 * Do control botones.
	 */
	@Override
	public void doControlBotones() {
		// TODO Auto-generated method stub

	}

	public static final String AYUDA_PANTALLA_USUARIOS = " <b>Pantalla de revisión  de usuarios  :\n  </b> <br>"
			+ " Permite buscar y seleccionar entre los usuarios actuales <br>" + " Es posible buscar por un:<br>"
			+ "   - un apellido: Escribiendo una palabra busca cualquier apellido o usuario parecidos  <br>"
			+ "   -  dos apellido: Primera palabra primer apellido parecedo, segunda segundos<br>"
			+ "   -  dos apellidos y nombre: Escribiendo 3 palabras<br> "
			+ "   - por dni : Si escribes un número o un DNI.<br>" + "    - usuario: Una palabra<br>"
			+ "    -   mail: Escribiendo un mal válido <br>"
			+ " Para seleccionar un usuario debes hacer click en la fila de datos.<br>"
			+ " Si la búsqueda devuelve un único valor, muestra el formulario con los campos.<br>"
			+ " A continuación se puede modificar su valores en el formulario de la dercha.<br><hr>"
			+ Ayudas.AYUDA_BOTONES_BARCA;
}
