package com.jnieto.ui.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.AccesosDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.LoginException;
import com.jnieto.excepciones.MailExcepciones;
import com.jnieto.excepciones.UsuarioBajaException;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.utilidades.MandaMail;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class FrmAvisos extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2100945889764922782L;

	private static final Logger logger = LogManager.getLogger(AccesosDAO.class);

	private TextField dni;
	private TextField nombre = new TextField("Nombre");
	private TextField asunto = new TextField("Asunto");
	private TextArea contenido = new TextArea("Contenido");

	public FrmAvisos() {
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		dni = new ObjetosComunes().getDni("Dni", " escribe tu dni");
		dni.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		dni.addBlurListener(e -> saltaDni());

		nombre.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		nombre.setVisible(false);

		fila1.addComponents(dni, nombre);
		asunto.setWidth("500px");
		asunto.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);

		fila2.addComponent(asunto);
		contenido.setWidth("500px");
		contenido.setHeight("100px");
		contenido.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);

		fila3.addComponent(contenido);
		borrar.setVisible(false);

	}

	public void saltaDni() {
		Usuario usuario;
		try {
			if (Utilidades.validarNIF(dni.getValue())) {
				usuario = new UsuarioDAO().getUsuarioLogin(dni.getValue());
				nombre.setValue(usuario.getApellidosNombre());
				nombre.setVisible(true);
			} else {
				new NotificacionInfo("Dni incorrecto");
				dni.focus();
				nombre.clear();
				nombre.setVisible(false);
			}
		} catch (LoginException | UsuarioBajaException e) {
			new NotificacionInfo("Usuario inexistente o de baja");
		}

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			new MandaMail().sendEmail("jnieto@saludcastillayleon.es", asunto.getValue(),
					dni.getValue() + ":" + contenido.getValue());
			cerrarClick();

		} catch (MailExcepciones e) {
			logger.error("Error de envio", e);
		}
	}

	@Override
	public void borrarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
