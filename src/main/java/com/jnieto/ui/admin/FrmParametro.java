package com.jnieto.ui.admin;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.dao.ParametroDAO;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

/**
 * The Class FrmParametro. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmParametro extends FrmMaster {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -9157040299474360559L;

	private ParametBBDD parametro = null;

	private TextField id = null;

	private TextField descripcion = null;

	private TextArea valor = null;

	private TextField codigo = null;

	Binder<ParametBBDD> binder = new Binder<>();

	/**
	 * Instantiates a new frm parametro.
	 *
	 * @param parametro the parametro
	 */
	public FrmParametro(ParametBBDD parametro) {
		super();
		this.parametro = parametro;

		this.lbltitulo.setCaption("Formulario de parámetro");

		id = new TextField("Id");
		id.setEnabled(false);
		id.setWidth("100px");
		binder.forField(id).bind(ParametBBDD::getStringId, ParametBBDD::setStringId);

		codigo = new TextField("Código");
		codigo.setPlaceholder(" código ");
		codigo.setWidth("200px");
		codigo.setMaxLength(25);
		codigo.focus();
		codigo.addBlurListener(e -> saltaCodigo());
		binder.forField(codigo).withValidator(new StringLengthValidator(" De 1 a 25 ", 1, 25))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(ParametBBDD::getCodigo, ParametBBDD::setCodigo);

		descripcion = new TextField("Nombre");
		descripcion.setPlaceholder(" Nombre del parámetro ");
		descripcion.setWidth("400px");
		descripcion.setMaxLength(100);
		binder.forField(descripcion).withValidator(new StringLengthValidator(" De 1 a 99 ", 1, 100))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(ParametBBDD::getDescripcion, ParametBBDD::setDescripcion);

		valor = new TextArea("Valor");
		valor.setPlaceholder(" Valor del parámetro ");
		valor.setWidth("400px");
		valor.setHeight("100px");
		valor.setMaxLength(400);
		binder.forField(valor).withValidator(new StringLengthValidator(" De 1 a 400 ", 1, 400))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(ParametBBDD::getValor, ParametBBDD::setValor);

		binder.readBean(parametro);
		this.doActivaBotones(parametro.getId());
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(id, codigo, descripcion, valor);
	}

	public void saltaCodigo() {
		if (new ParametroDAO().getRegistroCodigo(codigo.getValue(), parametro.getId()) != null) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_REPETIDO);
			codigo.clear();
			codigo.focus();
		}
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		((PantallaPatrametros) this.getParent().getParent().getParent()).getContenedorGrid().setEnabled(true);
		((PantallaPatrametros) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmParametro.AYUDA_FRM_PARAMETROS));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		try {
			binder.writeBean(parametro);
			if (new ParametroDAO().grabaDatos(parametro) == false) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		if (parametro != null) {
			ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
					Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
						/**
						 * 
						 */
						private static final long serialVersionUID = 6169352858399108337L;

						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								borraElRegistro();

							}
						}
					});
		}
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new ParametroDAO().borraDatos(parametro))
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		return true;
	}

	/** The Constant AYUDA_FRM_PARAMETROS. */
	public static final String AYUDA_FRM_PARAMETROS = " <b>Pantalla de mantenimiento de parámetros  :\n  </b> <br>"
			+ Ayudas.AYUDA_BOTONES_ABCA + "<ul>"
			+ " <li><b>Id:</b>  Campo de identificación interna no modificable.</li>"
			+ "<li><b>Código:</b> Código de uso interno en la aplicación. </li>"
			+ "<li><b>Nombre:</b> Descripción del parámetro. </li>"
			+ "<li><b>Valor:</b> Valor que tiene asociado el  parámetro. </li>" + "</ul>  <hr>";

}
