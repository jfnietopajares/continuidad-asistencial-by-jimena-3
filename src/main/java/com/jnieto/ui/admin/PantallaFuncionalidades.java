package com.jnieto.ui.admin;

import java.util.ArrayList;

import com.jnieto.dao.FuncionalidadDAO;
import com.jnieto.dao.ServiciosDAO;
import com.jnieto.entity.Funcionalidad;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.PantallaMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;

/**
 * The Class PantallaFuncionalidades. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaFuncionalidades extends PantallaMaster {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -8589644160383892409L;

	/** The grid. */
	// Grid de datos
	public Grid<Funcionalidad> grid = new Grid<>();

	/** The funcionaliad. */
	// Beans
	private Funcionalidad funcionaliad = new Funcionalidad();

	/** The lista funcionalidades. */
	private ArrayList<Funcionalidad> listaFuncionalidades = new ArrayList<>();

	/**
	 * Instantiates a new pantalla funcionalidades.
	 */
	public PantallaFuncionalidades() {
		super();
		grid.setCaption(" Lista de funcionalidades ");
		grid.addColumn(Funcionalidad::getId).setCaption("Id");
		grid.addColumn(Funcionalidad::getDescripcion).setCaption("Descripción");
		// grid.setItems(new FuncionalidadDAO().getListaRegistros());
		grid.addItemClickListener(event -> selecciona());

		filtro.setPlaceholder(" texto a buscar  ");
		paginacion = new FuncionalidadDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
		contenedorGrid.addComponents(contenedorBotones, grid, filaPaginacion);
	}

	/**
	 * Selecciona.
	 */
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			funcionaliad = new FuncionalidadDAO().getRegistroId(grid.getSelectedItems().iterator().next().getId());
			doSubformulario(funcionaliad);
		}
	}

	/**
	 * Buscar click.
	 */
	public void buscarClick() {
		paginacion = new ServiciosDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();

		if (listaFuncionalidades.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.GRID_DATOS_NO_ENCONTRADOS_FILTRO + " " + filtro.getValue());
			filtro.clear();
			filtro.focus();
		} else {
			grid.setItems(listaFuncionalidades);
			if (listaFuncionalidades.size() == 1) {
				funcionaliad = new FuncionalidadDAO().getRegistroId(listaFuncionalidades.get(0).getId());
				doSubformulario(funcionaliad);
			}
		}
	}

	/**
	 * Anadir click.
	 */
	public void anadirClick() {
		funcionaliad = new Funcionalidad();
		doSubformulario(funcionaliad);
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(PantallaFuncionalidades.AYUDA_PANTALLA_FUNCIONALIDAD));
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		filtro.clear();
		filtro.focus();
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		contenedorGrid.setEnabled(true);
		paginacion = new FuncionalidadDAO().getPaginacionRegistros(filtro.getValue());
		setValoresGridBotones();
	}

	/**
	 * Cerrar click.
	 */
	public void cerrarClick() {
		this.removeAllComponents();
	}

	/**
	 * Do subformulario.
	 *
	 * @param funcionaliad the funcionaliad
	 */
	public void doSubformulario(Funcionalidad funcionaliad) {
		contenedorGrid.setEnabled(false);
		contenedorFormulario.setVisible(true);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmFuncionalidad(funcionaliad));
	}

	/**
	 * Sets the valores grid botones.
	 */
	public void setValoresGridBotones() {
		listaFuncionalidades = new FuncionalidadDAO().getListaRegistrosPaginados(filtro.getValue().toUpperCase(),
				paginacion);

		setValoresPgHeredados(listaFuncionalidades);
		if (listaFuncionalidades.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO + filtro.getValue());
		} else {
			this.getGrid().setItems(listaFuncionalidades);
			if (listaFuncionalidades.size() == 1) {
				funcionaliad = listaFuncionalidades.get(0);
				doSubformulario(funcionaliad);
			}
		}
	}

	/**
	 * Sets the valores pg heredados.
	 *
	 * @param listaFuncionalidades the new valores pg heredados
	 */
	public void setValoresPgHeredados(ArrayList<Funcionalidad> listaFuncionalidades) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();

		for (Funcionalidad funcionalidad : listaFuncionalidades) {
			listaValoresArrayList.add(funcionalidad.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	/**
	 * Gets the grid.
	 *
	 * @return the grid
	 */
	public Grid<Funcionalidad> getGrid() {
		return grid;
	}

	/**
	 * Sets the grid.
	 *
	 * @param grid the new grid
	 */
	public void setGrid(Grid<Funcionalidad> grid) {
		this.grid = grid;
	}

	/**
	 * Do control botones.
	 */
	@Override
	public void doControlBotones() {

	}

	/** The Constant AYUDA_PANTALLA_FUNCIONALIDAD. */
	public static final String AYUDA_PANTALLA_FUNCIONALIDAD = " <b>Pantalla de revisión  de funcionalidades  :\n  </b> <br>"
			+ " Permite buscar y seleccionar entre las funcionalidades actuales <br> "
			+ " por codigo o descripción  con cualquier coincidencia,  <br>"
			+ " para modificar sus valores o añadir nuevas funcionalidades<br><hr>" + Ayudas.AYUDA_BOTONES_BARCA;
}
