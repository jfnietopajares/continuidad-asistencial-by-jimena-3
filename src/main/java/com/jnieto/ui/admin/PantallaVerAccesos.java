package com.jnieto.ui.admin;

import java.time.LocalDate;
import java.util.ArrayList;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.AccesosDAO;
import com.jnieto.entity.Acceso;
import com.jnieto.entity.AccesoTipos;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.master.PantallaPaginacion;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class PantallaVerAccesos. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaVerAccesos extends PantallaPaginacion {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2816717123024115047L;

	private Grid<Acceso> grid = new Grid<>();

	public TextField usuario = null;
	public TextField numerohc = null;
	public DateField desde = null;
	public DateField hasta = null;

	public ComboBox<AccesoTipos> tipo = null;

	private HorizontalLayout filaBuscador = new HorizontalLayout();
	private VerticalLayout contenedorGrid = new VerticalLayout();

	private Button buscar, ayuda, cerrar;

	private ArrayList<Acceso> listaAccesos = new ArrayList<>();

	private Acceso acceso = null;

	public PantallaVerAccesos() {
		this.setMargin(false);
		this.setSizeFull();

		usuario = new ObjetosComunes().getUserid();
		usuario.addBlurListener(e -> clickBuscar());

		numerohc = new ObjetosComunes().getNumerohc("Historia", " historia ");
		numerohc.addBlurListener(e -> clickBuscar());

		desde = new ObjetosComunes().getFecha("Desde", " desde ");
		desde.setValue(LocalDate.now());

		hasta = new ObjetosComunes().getFecha("Hasta", " hasta ");
		hasta.setValue(LocalDate.now());

		tipo = new ObjetosComunes().getComboAccesosTipos(null);
		tipo.addBlurListener(e -> clickBuscar());

		buscar = new ObjetosComunes().getBotonBuscar();
		buscar.addClickListener(e -> clickBuscar());

		ayuda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "?", VaadinIcons.QUESTION,
				"Muestra ayuda de la pantalla");

		ayuda.addClickListener(e -> clickAyuda());

		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(e -> clickCerrar());

		paginacion = new AccesosDAO().getPaginacionRegistros(desde.getValue(), hasta.getValue(), "", "", 0);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
		grid.addColumn(Acceso::getFechaHora).setCaption("Fecha");
		grid.addColumn(Acceso::getTipoString).setCaption("Tipo");
		grid.addColumn(Acceso::getNombreUsuario).setCaption("Usuario");
		grid.addColumn(Acceso::getNhc).setCaption("Nhc");
		grid.addColumn(Acceso::getNombrePaciente).setCaption("Paciente");
		grid.addColumn(Acceso::getMotivo).setCaption("Motivo");
		grid.addItemClickListener(event -> selecciona());
		grid.setSizeFull();
		grid.asSingleSelect();
		grid.setHeightByRows(10);
		grid.setHeightMode(HeightMode.ROW);
		grid.setItems(listaAccesos);
		filaBuscador.addComponents(usuario, numerohc, desde, hasta, tipo, buscar, ayuda, cerrar);
		contenedorGrid.addComponents(grid, filaPaginacion);
		this.addComponents(filaBuscador, contenedorGrid);

	}

	public void clickBuscar() {
		String nhc = "", usr = "";
		int tipop = 0;
		if (!numerohc.getValue().isEmpty()) {
			nhc = numerohc.getValue();
		}
		if (!usuario.getValue().isEmpty()) {
			usr = usuario.getValue();
		}

		if (tipo.getSelectedItem().isPresent()) {
			tipop = tipo.getValue().getTipo();
		}
		paginacion = new AccesosDAO().getPaginacionRegistros(desde.getValue(), hasta.getValue(), usr, nhc, tipop);
		paginacion.setNumeroRegistrosPagina(Integer
				.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION_PACIENTES)));

		setValoresGridBotones();
	}

	public void clickAyuda() {
		new VentanaHtml(this.getUI(), new Label(PantallaVerAccesos.AYUDA_PANTALLA_VERACCESOS));
	}

	public void clickCerrar() {
		this.removeAllComponents();
	}

	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			acceso = grid.getSelectedItems().iterator().next();
			new VentanaHtml(this.getUI(), new Label(acceso.getHtml()), "Datos del acceso");
		}
	}

	@Override
	public void setValoresGridBotones() {
		String nhc = "", usr = "";
		int tipop = 0;
		if (!numerohc.getValue().isEmpty()) {
			nhc = numerohc.getValue();
		}
		if (!usuario.getValue().isEmpty()) {
			usr = usuario.getValue();
		}

		if (tipo.getSelectedItem().isPresent()) {
			tipop = tipo.getValue().getTipo();
		}
		listaAccesos = new AccesosDAO().getListaAccesosLight(desde.getValue(), hasta.getValue(), usr, nhc, tipop,
				paginacion);
		setValoresPgHeredados(listaAccesos);
		grid.setItems(listaAccesos);
		if (listaAccesos.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO);
		}
	}

	public void setValoresPgHeredados(ArrayList<Acceso> listaAccesos) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();
		for (Acceso acceso : listaAccesos) {
			listaValoresArrayList.add(acceso.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}

	public static final String AYUDA_PANTALLA_VERACCESOS = "<b>Pantalla de consulta de accesos. \n  </b> <br>"
			+ "Esta pantalla permite revisar los datos de la tabla de accesos.<br>"
			+ "Haciendo click en un fila se muestra una ventana con todos los datos del acceso.<br>"
			+ " Se pueden consultar por diferentes criterios:.<br>"
			+ "<ul> <li><b>Desde:</b> Desde la fecha de registro del aceso.</li>"
			+ "<li><b>Hasta:</b> Hasta la fecha de registro del acceso.</li>"
			+ "<li><b>Usuario:</b> Usuario que ha realizado el acceso al sistema o al dato </li>"
			+ "<li><b>Historia:</b> Filtra los acceso realizados a ese paciente.</li>"
			+ "<li><b>Tipo:</b> Filtra los acceso con el tipo elegido .</li>" + "</ul>  <hr> "
			+ " Descripción de botones:<br>" + "<ul> <li><b>Buscar:</b> Ejecuta la búsqueda según los criterios.</li>"
			+ "<li><b>?</b> Esta pantalla.</li>" + "<li><b>x</b> Cierra la ventana.</li>"
			+ "<li>Mediantes los botones de paginación puedes desplazar para ver diferentes registros.</li>"
			+ "</ul>  <hr> ";

}
