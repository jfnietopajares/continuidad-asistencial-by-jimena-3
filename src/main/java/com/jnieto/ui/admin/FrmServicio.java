package com.jnieto.ui.admin;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.dao.ServiciosDAO;
import com.jnieto.entity.Servicio;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmServicio. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmServicio extends FrmMaster {

	private static final long serialVersionUID = 3483451009429438747L;

	private Servicio servicio = null;

	Binder<Servicio> binder = new Binder<>();

	private TextField id = null;

	private TextField codigo = null;

	private TextField descripcion = null;

	/**
	 * Instantiates a new frm servicio.
	 *
	 * @param servicio the servicio
	 */
	public FrmServicio(Servicio servicio) {
		this.servicio = servicio;
		lbltitulo.setCaption("Formulario de Servicios.");

		id = new TextField("Id");
		id.setEnabled(false);
		id.setWidth("100px");
		binder.forField(id).bind(Servicio::getStringId, Servicio::setStringId);

		codigo = new TextField("Código");
		codigo.setMaxLength(4);
		codigo.setWidth("70px");
		codigo.focus();
		codigo.addBlurListener(e -> saltaCodigo());
		binder.forField(codigo).withValidator(new StringLengthValidator(" De 1 a 4 ", 1, 4))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Servicio::getCodigo, Servicio::setCodigo);

		descripcion = new TextField("Descripción");
		descripcion.setWidth("400px");
		binder.forField(descripcion).withValidator(new StringLengthValidator(" De 1 a 99 ", 1, 100))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Servicio::getDescripcion, Servicio::setDescripcion);

		binder.readBean(servicio);
		this.doActivaBotones(servicio.getId());
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(id, codigo, descripcion);
	}

	public void saltaCodigo() {
		if (new ServiciosDAO().getRegistroCodigo(codigo.getValue(), servicio.getId()) != null) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_REPETIDO);
			codigo.clear();
			codigo.focus();
		}
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		((PantallaServicios) this.getParent().getParent().getParent()).getContenedorGrid().setEnabled(true);
		((PantallaServicios) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmServicio.AYUDA_FRM_SERVICIOS));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		try {
			binder.writeBean(servicio);
			if (new ServiciosDAO().grabaDatos(servicio) == false) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		if (servicio != null) {
			ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
					Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
						/**
						 * 
						 */
						private static final long serialVersionUID = 6169352858399108337L;

						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								borraElRegistro();

							}
						}
					});
		}
	}

	/**
	 * Borra el registro.
	 */
	public void borraElRegistro() {
		if (new ServiciosDAO().borraDatos(servicio))
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		cerrarClick();
	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	@Override
	public void doActivaBotones(Long id) {
		if (id.equals(new Long(0))) {
			borrar.setEnabled(false);
		} else if (new ServiciosDAO().getReferenciasExternas(id)) {
			borrar.setEnabled(false);
			borrar.setDescription(NotificacionInfo.SQLREFERENCIASEXTERNAS);
		} else {
			borrar.setEnabled(true);
		}
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		return true;
	}

	/** The Constant AYUDA_FRM_SERVICIOS. */
	public static final String AYUDA_FRM_SERVICIOS = "<b>Pantalla de mantenimiento de servicios  :\n  </b> <br>"
			+ Ayudas.AYUDA_BOTONES_ABCA + "<ul> <li><b>Id:</b>  Campo de identificación interna no modificable.</li>"
			+ "<li><b>Código:</b> Código abreviado , oficial.</li>"
			+ "<li><b>Descripción:</b> Descripción del servicio .</li>" + "</ul>  <hr>";

}
