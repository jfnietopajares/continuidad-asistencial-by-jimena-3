package com.jnieto.ui.admin;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.dao.CentroDAO;
import com.jnieto.entity.Centro;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmCentro. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 * 
 */
public class FrmCentro extends FrmMaster {

	private static final long serialVersionUID = -5741523832354019786L;

	private Centro centro = null;

	private TextField id = null;

	private TextField descripcion = null;

	private TextField nemonico = null;

	private TextField codigo = null;

	private String codigoAnterior = "";

	Binder<Centro> binder = new Binder<>();

	/**
	 * Instantiates a new frm centro.
	 *
	 * @param centro the centro
	 */
	public FrmCentro(Centro centro) {
		super();
		this.centro = centro;

		this.lbltitulo.setCaption("Formulario de Centros");

		id = new TextField("Id");
		id.setEnabled(false);
		id.setWidth("100px");
		binder.forField(id).bind(Centro::getStringId, Centro::setStringId);

		codigo = new TextField("Código");
		codigo.setPlaceholder(" código nacional");
		codigo.setWidth("100px");
		codigo.setMaxLength(8);
		codigo.focus();
		codigo.addBlurListener(e -> saltaCodigo());
		binder.forField(codigo).withValidator(new StringLengthValidator(" De 1 a 8 caracteres ", 1, 8))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Centro::getCodigo, Centro::setCodigo);

		descripcion = new TextField("Nombre");
		descripcion.setPlaceholder(" Nombre del centro ");
		descripcion.setWidth("400px");
		descripcion.setMaxLength(100);
		binder.forField(descripcion).withValidator(new StringLengthValidator(" De 1 a 99 caracteres ", 1, 100))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(Centro::getDescripcion, Centro::setDescripcion);

		nemonico = new TextField("Nemónico");
		nemonico.setPlaceholder(" Nombre corto ");
		nemonico.setWidth("100px");
		nemonico.setMaxLength(10);
		binder.forField(nemonico).withValidator(new StringLengthValidator(" De 1 a 10 ", 1, 10))
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Centro::getNemonico, Centro::setNemonico);

		binder.readBean(centro);
		codigoAnterior = centro.getCodigo();
		this.doActivaBotones(centro.getId());
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(id, codigo, descripcion, nemonico);

	}

	/**
	 * Salta codigo.
	 * 
	 * Verifica que el códgio no esté repetido
	 */
	public void saltaCodigo() {
		if (new CentroDAO().getRegistroCodigo(codigo.getValue(), centro.getId()) != null) {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_REPETIDO + ":" + codigo.getValue());
			codigo.clear();
			codigo.focus();
			if (codigoAnterior != null)
				codigo.setValue(codigoAnterior);
		}
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		((PantallaCentros) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmCentro.AYUDA_FRM_CENTROS));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		try {
			binder.writeBean(centro);
			if (new CentroDAO().grabaDatos(centro) == false) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		if (centro != null) {
			ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
					Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
						/**
						 * 
						 */
						private static final long serialVersionUID = 6169352858399108337L;

						public void onClose(ConfirmDialog dialog) {
							if (dialog.isConfirmed()) {
								borraElRegistro();

							}
						}
					});
		}
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new CentroDAO().borraDatos(centro))
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	@Override
	public void doActivaBotones(Long id) {
		borrar.setEnabled(false);
		if (id.equals(new Long(0))) {
			borrar.setEnabled(false);
		} else if (new CentroDAO().getReferenciasExternas(id)) {
			borrar.setEnabled(false);
			borrar.setDescription(NotificacionInfo.SQLREFERENCIASEXTERNAS);
		} else {
			borrar.setEnabled(true);
		}
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		return true;
	}

	/** The Constant AYUDA_FRM_CENTROS. */
	public static final String AYUDA_FRM_CENTROS = "<b>Pantalla de mantenimiento de centros. \n  </b> <br>"
			+ " Descripción de campos:<br>"
			+ "<ul> <li><b>Id:</b>  Campo de identificación interna no modificable.</li>"
			+ "<li><b>Nombre:</b> Nombre  del Centro.</li>" + "<li><b>Codigo:</b> Código nacional único.</li>"
			+ "<li><b>Nemónico:</b> Nombre abreviado .</li>" + "</ul>  <hr> " + Ayudas.AYUDA_BOTONES_ABCA;

}
