package com.jnieto.ui;

import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.EpisodioDAO;
import com.jnieto.dao.HpHisDAO;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.ServiciosDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Cama;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Zona;
import com.jnieto.excepciones.ValidacionesExcepciones;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.jnieto.validators.ValidatorHoraMinuto;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class FrmEpisodio extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2754284931539809527L;

	private static final Logger logger = LogManager.getLogger(FrmEpisodio.class);

	private Episodio episodio;

	private Zona zona;

	private DateField fechaini = null;

	private TextField horaini = new TextField("Hora ini");

	private DateField fechafin = null;

	private ComboBox<Cama> comboCama = new ComboBox<Cama>();

	private TextField horafin = new TextField("Hora ini");

	private TextArea observaciones = new TextArea("Observaciones");

	private ComboBox<Servicio> comboServicio = null;

	private ComboBox<Centro> comboCentro = new ObjetosComunes().getComboCentros(null, Centro.CENTRO_DEFECTO);

	private ComboBox<Usuario> comboMedico = null;

	private TextField numerohc = new ObjetosComunes().getNumerohc("NHC", " nº hc");
	private TextField nombrePaciente = new TextField("Nombre");

	private Binder<Episodio> binder = new Binder<Episodio>();

	private ArrayList<Servicio> listaServicios = new ArrayList<Servicio>();

	private ArrayList<Usuario> listaMedicos = new ArrayList<Usuario>();

	public FrmEpisodio(Episodio epi, Zona zonaParam) {
		super();
		this.episodio = epi;
		this.zona = zonaParam;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6);
		lbltitulo.setCaption(episodio.getId().toString());

		if (episodio.getPaciente() == null) {
			numerohc.setEnabled(true);
			numerohc.focus();
			numerohc.addBlurListener(e -> saltaNumerohc());
			nombrePaciente.setEnabled(true);

		} else {
			lbltitulo.setValue(episodio.getPacienteApellidos());
			numerohc.setEnabled(false);
			numerohc.setValue(episodio.getPacienteNhc());
			nombrePaciente.setEnabled(false);
			nombrePaciente.setValue(episodio.getPacienteApellidos());
		}
		fila1.addComponents(numerohc, nombrePaciente);

		comboCentro.setCaption("Centro");
		binder.forField(comboCentro).asRequired().bind(Episodio::getCentro, Episodio::setCentro);
		comboCentro.addBlurListener(e -> cambiaCentro());
		comboServicio = new ObjetosComunes().getComboServicioCorto(Centro.CENTRO_DEFECTO, episodio.getServicio(), null,
				episodio.getDescripcionClase(episodio.getClaseLong()));

		comboServicio.setCaption("Serv.Peticionario");
		binder.forField(comboServicio).asRequired().bind(Episodio::getServicio, Episodio::setServicio);
		comboServicio.addBlurListener(e -> cambiaServicio());
		fila2.addComponents(comboCentro, comboServicio);
		fechaini = new ObjetosComunes().getFecha("F.Inicio", " fecha inicio");
		fechaini.setValue(LocalDate.now());
		binder.forField(fechaini).asRequired().bind(Episodio::getFinicio, Episodio::setFinicio);

		horaini = new ObjetosComunes().getHora("H.Ini.", "inicio");
		horaini.setValue(Utilidades.getHoraAcualString());
		binder.forField(horaini).asRequired().withValidator(new ValidatorHoraMinuto(horaini.getValue()))
				.bind(Episodio::getHinicioString, Episodio::setHinicio);

		fila3.addComponents(fechaini, horaini);

		comboCama = new ObjetosComunes().getComboCama(null, null, zona, "Cama/Sillón");
		binder.forField(comboCama).bind(Episodio::getCama, Episodio::setCama);

		if (zona.equals(Zona.ZONA_HDIA_ONCO) || zona.equals(Zona.ZONA_HDIA_MED) || zona.equals(Zona.ZONA_UCA)
				|| zona.equals(Zona.ZONA_TIPO_URPA))
			fila2.addComponent(comboCama);

		comboMedico = new ObjetosComunes().getComboMedicoServ(comboCentro.getSelectedItem().get(),
				comboServicio.getSelectedItem().get(), episodio.getUserid(), null);
		comboMedico.setCaption("Médico");

		fila4.addComponent(comboMedico);

		observaciones.setWidth("500px");
		observaciones.setHeight("45px");
		observaciones.setMaxLength(400);
		binder.forField(observaciones).bind(Episodio::getObservacion, Episodio::setObservacion);
		fila5.addComponent(observaciones);

		fechafin = new ObjetosComunes().getFecha("F.Fin", "fecha fin");
		fechafin.addBlurListener(e -> saltaFechaFin());
		fechafin.addFocusListener(e -> focusFechaFin());
		binder.forField(fechafin).bind(Episodio::getFfinal, Episodio::setFfinal);

		horafin = new ObjetosComunes().getHora("H.Fin.", " hora fin");
		binder.forField(horafin).withValidator(reTurnhorafin -> {
			if (reTurnhorafin.isEmpty() || reTurnhorafin.equals("0"))
				return true;
			else
				return new ValidatorHoraMinuto(reTurnhorafin).isValid(reTurnhorafin);
		}, " hora no válida").bind(Episodio::getHfinalString, Episodio::setHfinal);

		fila6.addComponents(fechafin, horafin);

		binder.readBean(episodio);
		// new NotificacionInfo(epi.getCama().getCama() + "----" +
		// comboCama.getValue().getCama(), true);
		if (fechaini.getValue() == null)
			fechaini.setValue(LocalDate.now());
		if (horafin.getValue().contentEquals("0"))
			horafin.clear();

		doControlCampos();
	}

	public void focusFechaFin() {
		if (fechafin.getValue() == null) {
			if (episodio.getClase().equals(Episodio.CLASE_HDIA)) {
				fechafin.setValue(fechaini.getValue());
				horafin.setValue(Utilidades.getHoraAcualString());
				horafin.focus();
			}
		}

	}

	public void saltaFechaFin() {
		if (fechafin.getValue() != null)
			if (horafin.getValue().isEmpty())
				horafin.setValue(Utilidades.getHoraAcualString());
	}

	public void doControlCampos() {
		comboCentro.setEnabled(false);
		comboServicio.setEnabled(false);
		fechaini.setEnabled(false);
		horaini.setEnabled(false);
		fechafin.setEnabled(false);
		horafin.setEnabled(false);
		comboCama.setEnabled(false);
		observaciones.setEnabled(false);
		if (episodio.getClase().equals(Episodio.CLASE_HDIA)) {
			comboCentro.setEnabled(true);
			comboServicio.setEnabled(true);
			fechaini.setEnabled(true);
			horaini.setEnabled(true);
			fechafin.setEnabled(true);
			horafin.setEnabled(true);
			comboCama.setEnabled(true);
			observaciones.setEnabled(false);
		}
	}

	public void saltaNumerohc() {
		if (numerohc.getValue() != null && !numerohc.getValue().isEmpty()) {
			Paciente paciente = new PacienteDAO().getPacientePorNhc(numerohc.getValue());
			if (paciente == null) {
				new NotificacionInfo("Paciente con nhc=" + numerohc.getValue() + " no encontrado.");
				numerohc.focus();
			} else {
				episodio.setPaciente(paciente);
				nombrePaciente.setValue(paciente.getApellidosNombre());
			}
		} else {
			new NotificacionInfo("El número de historia es obligatorio.");
			numerohc.focus();
		}
	}

	public void cambiaCentro() {
		listaServicios = new ServiciosDAO().getListaRegistos(comboCentro.getSelectedItem().get());
		comboServicio.setItems(listaServicios);
		if (listaServicios.size() > 0)
			comboServicio.setSelectedItem(listaServicios.get(0));
	}

	public void cambiaServicio() {

		listaMedicos = new UsuarioDAO().getListaUsuarios(comboCentro.getSelectedItem().get(),
				comboServicio.getSelectedItem().get());
		comboMedico.setItems(listaMedicos);
		if (listaMedicos.size() > 0)
			comboMedico.setSelectedItem(listaMedicos.get(0));
	}

	@Override
	public void cerrarClick() {

		MyUI.getCurrent().getWindows().iterator().next().close();

	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {

		try {
			binder.writeBean(episodio);
		} catch (ValidationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			binder.writeBean(episodio);
			// el sillón o la cama son obligatorio
			if (episodio.getClase().equals(Episodio.CLASE_HDIA)) {
				if (comboCama.getValue() == null) {
					comboCama.setComponentError(new UserError(NotificacionInfo.FORMULARIOCAMPOREQUERIDO));
					throw new ValidacionesExcepciones("Error de validación de campo");
				}
			}
			if (episodio.getUserid() == null || episodio.getUserid().getUserid().isEmpty()) {
				episodio.setUserid(((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME)));
			}
			if (new EpisodioDAO().grabaDatos(episodio) == true) {
				new NotificacionInfo(NotificacionInfo.DATO_GRABADO);
				if (episodio.getClase().equals(Episodio.CLASE_HDIA) && episodio.getFfinal() != null) {
					if (!new HpHisDAO().grabaEpisodioHdia(episodio)) {
						new NotificacionInfo("Error actualizanod HIS. Episodio=" + episodio.getId(), true);
						logger.error("Error actualizando episodio his. Episodio=" + episodio.getId());
					}
				}
				cerrarClick();
			} else {
				new NotificacionInfo(NotificacionInfo.DATO_ERROR);
			}

		} catch (Exception e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION + e.toString());
			logger.error(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION, e);

		}

	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

}
