package com.jnieto.ui;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Zona;
import com.jnieto.ui.hdiaonco.ConsolaHdiaOnco;
import com.jnieto.ui.hdiaonco.PantallaHadiOnco;
import com.jnieto.ui.master.VentanaFrm;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class PantallaMapaCamas extends VerticalLayout {
	/**
	* 
	*/
	private static final long serialVersionUID = 412249650109245189L;
	public Zona zona;
	public Centro centro;
	public Runnable hilo;
	public ExecutorService executor;
	public UI ui;
	public EpisodioClase claseEpi;
	private final String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	private static final Usuario usuario = ((Usuario) VaadinSession.getCurrent()
			.getAttribute(Constantes.SESSION_USERNAME));

	public PantallaMapaCamas(UI ui, Centro centro, Zona zona, EpisodioClase clase) {
		this.ui = ui;
		this.zona = zona;
		this.centro = centro;
		this.claseEpi = clase;
		hilo = new Loader(ui, this, centro, zona, basepath, clase, usuario);
		executor = Executors.newFixedThreadPool(1);
		executor.execute(hilo);
		ui.setPollInterval(1000);
// doPintaMapa();
	}

	public void finalize() {
		executor.shutdown();
		executor.shutdownNow();
	}

	public VerticalLayout getMapa() {
		return this;
	}

	class Loader implements Runnable {
		private UI ui;
		private VerticalLayout vlLayout;
		private Centro centro;
		private Zona zona;
		private String basepath;
		private EpisodioClase epiClase;
		private Usuario usuario;

		public Loader(UI ui, VerticalLayout vLayout, Centro centro, Zona zona, String basepath, EpisodioClase clase,
				Usuario usu) {
			this.ui = ui;
			this.vlLayout = vLayout;
			this.centro = centro;
			this.zona = zona;
			this.basepath = basepath;
			this.epiClase = clase;
			this.usuario = usu;
		}

		public void run() {
			try {
				while (true) {
					System.out.println("hilo" + LocalDateTime.now());
					doPintaMapaHilo(vlLayout);
					// ui.setContent(vlLayout);
					ui.setPollInterval(1000);
					Thread.sleep(40000);
				}
			} catch (InterruptedException e) {

			}
		}

		public void doPintaMapaHilo(VerticalLayout verticalLayout) {
			verticalLayout.removeAllComponents();
			ArrayList<Episodio> listaCamas = new EpisodioDAO().getCamasEpisodios(centro, zona, usuario);
			int contador = 0;
			HorizontalLayout fiLayout = null;
			for (Episodio episodio : listaCamas) {
				if (contador == 0) {
					fiLayout = new HorizontalLayout();
					fiLayout.setMargin(false);
					verticalLayout.addComponent(fiLayout);
				}
				Panel cajaCama = new CamaSillon(episodio, basepath);
				cajaCama.addClickListener(e -> clickCama(episodio));
				fiLayout.addComponent(cajaCama);
				contador++;
				if (contador == 3)
					contador = 0;
			}
		}
	}

	public void doPintaMapa() {
		this.removeAllComponents();
		ArrayList<Episodio> listaCamas = new EpisodioDAO().getCamasEpisodios(centro, zona, usuario);
		int contador = 0;
		HorizontalLayout fiLayout = null;
		for (Episodio episodio : listaCamas) {
			if (contador == 0) {
				fiLayout = new HorizontalLayout();
				fiLayout.setMargin(false);
// fiLayout.setSpacing(false);
				this.addComponent(fiLayout);
			}
			Panel cajaCama = new CamaSillon(episodio, basepath);
			cajaCama.addClickListener(e -> clickCama(episodio));
			fiLayout.addComponent(cajaCama);
			contador++;
			if (contador == 3)
				contador = 0;
		}
	}

	public void clickCama(Episodio episodio) {
		if (episodio.getPaciente() != null) {
			System.out.print(episodio.getClase().toString());
			System.out.print(episodio.getZona().toString());
			System.out.print(zona.ZONA_HDIA_ONCO.toString());
			if (episodio.getClase().equals(Episodio.CLASE_HDIA) && zona.equals(Zona.ZONA_HDIA_ONCO)) {
				TabSheet tabSheet = ((PantallaHadiOnco) this.getParent().getParent().getParent()).getTabsheet();
				VerticalLayout contenedorTabLayout = new VerticalLayout();
				contenedorTabLayout.addComponent(new ConsolaHdiaOnco(episodio.getPaciente(), episodio));
				tabSheet.addTab(contenedorTabLayout, episodio.getPaciente().getApellidosNombre()).setClosable(true);
				tabSheet.setSelectedTab(tabSheet.getTab(contenedorTabLayout));
			} else {
				new NotificacionInfo(" paciente");
			}

		} else {
			Episodio episodionew = new Episodio();
			episodionew.setClase(Episodio.CLASE_HDIA);
			episodionew.setFinicio(LocalDate.now());
			episodionew.setHinicio(Utilidades.getHoraAcualString());
			VentanaFrm ventanaFrm = new VentanaFrm(getUI(), new FrmEpisodio(episodionew, zona),
					"Programa tratamiento oncología" + "");
			// ventanaFrm.addCloseListener(e -> cargaListaPacientes());

		}
	}

}
