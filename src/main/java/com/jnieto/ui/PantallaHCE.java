package com.jnieto.ui;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.GacelaDAO;
import com.jnieto.dao.InformesDAO;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Canal;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Edad;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Funcionalidad;
import com.jnieto.entity.Informe;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroEvolutivoMedico;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.master.VentanaFrm;
import com.jnieto.utilidades.Constantes;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.ResourceReference;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class PantallaHCE extends HorizontalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6882910301987084906L;
	private Paciente paciente;
	private Centro centro;
	private Servicio servicio;

	private VerticalLayout columnaIzquierda = new VerticalLayout();
	private VerticalLayout columnaDerecha = new VerticalLayout();
	private VerticalLayout listaInformesLayout = new VerticalLayout();
	private HorizontalSplitPanel split = new HorizontalSplitPanel();

	private Label contenido = new Label();
	private Label pacienteLbl = new Label();

	private Grid<Informe> gridInformes = new Grid<Informe>();

	private Long[] listaCanales = { Canal.CANAL_LABORATORIO, Canal.CANAL_RADIOLOGIA, Canal.CANAL_ANATOMIA,
			Canal.CANAL_JIMENA };

	protected DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	private Usuario usuario = (Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);

	public PantallaHCE(Paciente pacienteParam, Centro centroParam, Servicio servicioParam) {
		this.setMargin(false);
		this.setSpacing(true);
		// this.setResponsive(true);
		this.setSizeFull();
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);
		this.addStyleName(MaterialTheme.LAYOUT_WELL);
		// this.addComponents(columnaIzquierda, columnaDerecha);
		this.addComponent(split);

		this.paciente = pacienteParam;
		this.servicio = servicioParam;
		this.centro = centroParam;

		split.addStyleNames(MaterialTheme.LAYOUT_COMPONENT_GROUP_FLAT);
		split.setMaxSplitPosition(300, Unit.PIXELS);
		split.setFirstComponent(columnaIzquierda);
		split.setSecondComponent(columnaDerecha);
		// split.setHeightUndefined();
		split.setSizeFull();

		columnaIzquierda.setMargin(false);
		columnaIzquierda.setSpacing(true);
		columnaIzquierda.setResponsive(true);
		columnaIzquierda.setDefaultComponentAlignment(Alignment.TOP_CENTER);
		columnaIzquierda.setWidth("300px");
		columnaIzquierda.addStyleName(MaterialTheme.LAYOUT_WELL);

		columnaDerecha.setMargin(false);
		columnaDerecha.setResponsive(true);
		columnaDerecha.setSpacing(true);
		columnaDerecha.addStyleName(MaterialTheme.LAYOUT_COMPONENT_GROUP_FLAT);
		columnaDerecha.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
		columnaDerecha.addStyleName(MaterialTheme.LAYOUT_WELL);
		// columnaDerecha.setWidth("900px");
		columnaDerecha.addComponent(contenido);
		columnaDerecha.setSizeFull();
		columnaDerecha.setHeightUndefined();
		// columnaCentro.setCaption("p3 ce");

		listaInformesLayout.setMargin(false);
		listaInformesLayout.setSpacing(false);
		listaInformesLayout.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
		// contenido.setWidth(900, Unit.PIXELS);

		pacienteLbl.setCaption("<h3><b>" + paciente.getApellidosNombre() + "&nbsp;("
				+ new Edad(paciente.getFnac()).getEdadAnos() + ")</b>" + "&nbsp;" + paciente.getNumerohc() + "</h3>"
				+ paciente.getEpisodioActual().getObservacion() + "<br>" + "Cama:"
				+ paciente.getEpisodioActual().getCamaCodigo() + "&nbsp;&nbsp;" + "Ingreso:"
				+ fechadma.format(paciente.getEpisodioActual().getFinicio()));
		pacienteLbl.setCaptionAsHtml(true);
		pacienteLbl.setWidth(890, Unit.PIXELS);

		columnaIzquierda.addComponent(pacienteLbl);

		Button evolutivosBt = getBoton();
		evolutivosBt.setCaption(new RegistroDAO().getTextoListaRegistrosPaciTipoEpi(paciente,
				Registro.REGISTRO_EVOLUTIVO, paciente.getEpisodioActual().getId()));
		if (evolutivosBt.getCaption().toUpperCase().indexOf("SIN EVOLUT") != -1) {
			// evolutivosBt.setEnabled(false);
			if (usuario.getTieneLaFuncionalidad(Funcionalidad.EVOLUTIVOS_FUNCIONALIDAD) == true) {
				evolutivosBt.setCaption("Añadir evolutivo");
				evolutivosBt.addClickListener(e -> clickevolutivosBtAnadir());
			}
		} else {
			evolutivosBt.addClickListener(e -> clickevolutivosBtVer());
			clickevolutivosBtVer();
		}
		columnaIzquierda.addComponent(evolutivosBt);

		HorizontalLayout hl = new HorizontalLayout();
		hl.setMargin(false);
		hl.setSpacing(false);
		Button comentariosButton = getBoton();
		comentariosButton.setCaption("Comentarios");
		comentariosButton.setWidth(50, Unit.PERCENTAGE);
		comentariosButton.addClickListener(e -> clickcomentariosButton());

		Button graficasButton = getBoton();
		graficasButton.setCaption("Gráficas");
		graficasButton.addClickListener(e -> clickgraficasButton());
		graficasButton.setWidth(50, Unit.PERCENTAGE);
		// url
		String url = "http://intranet.hnss.sacyl.es/php/paciente/graficasEnfermeria/grafica.php?llamada=JIMENA&comentarios=NO&numerohc="
				+ paciente.getNumerohc() + "&numicu=" + paciente.getEpisodioActual().getIcu();
		Resource res = new ExternalResource(url);
		final ResourceReference rr = ResourceReference.create(res, columnaDerecha, "Gráficas");
		graficasButton.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				getUI().getPage().open(rr.getURL(), "_blank");
			}
		});
		// si el episodio es de hospitalización añade los botones de comentarios y de
		// gráficas de enfermeria

		if (paciente.getEpisodioActual().getClaseLong().equals(Episodio.CLASE_HOSPITALIZACION.getId())) {
			hl.addComponents(comentariosButton, graficasButton);
			columnaIzquierda.addComponent(hl);
		}

		for (Long canal : listaCanales) {
			Button boton = getBoton();
			boton.setCaption(new InformesDAO().geTtextoBotonInfCanal(paciente, canal));
			boton.addClickListener(e -> clickBoton(canal));
			if (boton.getCaption().toUpperCase().indexOf("SIN INFORMES") != -1) {
				boton.setEnabled(false);
			}
			columnaIzquierda.addComponent(boton);
		}
		Button otrosInformesButton = getBoton();
		otrosInformesButton.setCaption("Otros informes");
		otrosInformesButton.addClickListener(e -> clickBoton(null));
		columnaIzquierda.addComponent(otrosInformesButton);

		// gridInformes.setWidth("300px");
		gridInformes.addColumn(Informe::getFechaHoraServcioDescrip).setCaption("Fecha    Sev    Descripción");
		// gridInformes.addColumn(Informe::getDescripcion20).setCaption("Des");
		gridInformes.addSelectionListener(e -> clickSeleccionaInforme());

		columnaIzquierda.addComponent(listaInformesLayout);

	}

	public void clickgraficasButton() {

	}

	public void clickcomentariosButton() {
		if (paciente.getEpisodioActual().getIcu() != null) {
			contenido.setCaption("<H2>Comentarios de enfermería.</H2>");
			contenido.setCaptionAsHtml(true);
			contenido.setValue(new GacelaDAO().getComentarios(paciente.getEpisodioActual().getIcu()));
			contenido.setContentMode(ContentMode.HTML);
			contenido.setSizeFull();
			columnaDerecha.removeAllComponents();
			columnaDerecha.addComponent(contenido);
			listaInformesLayout.removeAllComponents();
		} else {
			new NotificacionInfo(" Episodio sin icu" + paciente.getEpisodioActual().getId());
		}

	}

	public void clickevolutivosBtVer() {
		// contenido.setCaption("<H2>Evolutivos</H2>");
		contenido.setCaptionAsHtml(true);
		contenido.setValue(getHtmlEvolutivos());
		contenido.setContentMode(ContentMode.HTML);
		contenido.setSizeFull();
		Button nuevoEvolutivoBt = new Button("Nuevo evolutivo");
		nuevoEvolutivoBt.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		nuevoEvolutivoBt.addClickListener(e -> clickevolutivosBtAnadir());
		nuevoEvolutivoBt.setVisible(false);
		// Usuario usuario = (Usuario)
		// VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);
		// si el usuairo tiene la funcionalidad de evolutivo muestra el boton de nuevo
		if (usuario.getTieneLaFuncionalidad(Funcionalidad.EVOLUTIVOS_FUNCIONALIDAD) == true) {
			nuevoEvolutivoBt.setVisible(true);
		}
		columnaDerecha.removeAllComponents();
		columnaDerecha.addComponents(nuevoEvolutivoBt, contenido);
		listaInformesLayout.removeAllComponents();

	}

	public void clickevolutivosBtAnadir() {
		// columnaDerecha.removeAllComponents();
		RegistroEvolutivoMedico registroEvol = new RegistroEvolutivoMedico();
		registroEvol.setServicio(servicio);
		registroEvol.setEpisodio(paciente.getEpisodioActual());
		registroEvol.setPaciente(paciente);
		registroEvol.setCentro(centro);
		// columnaDerecha.addComponent(new FrmRegistroEvolutivoMedico(registroEvol));
		VentanaFrm frmevol = new VentanaFrm(this.getUI(), new FrmRegistroEvolutivoMedico(registroEvol),
				registroEvol.getDescripcion());
		frmevol.addCloseListener(e -> cierraVentanaEvolutivo());
	}

	public void cierraVentanaEvolutivo() {
		clickevolutivosBtVer();
	}

	public void clickSeleccionaInforme() {
		if (gridInformes.getSelectedItems().size() > 0) {

			Informe informe = gridInformes.getSelectedItems().iterator().next();

			informe.setListaCampos(new InformesDAO().getListaCamposInformeso(informe.getId()));
			contenido.setCaption(null);
			columnaDerecha.removeAllComponents();
			if (informe.getTipobin() == 1) {

				FileResource resourcePdf = new FileResource(new File(
						VaadinService.getCurrent().getBaseDirectory().getAbsolutePath() + "/WEB-INF/images/pdf.png"));

				Button pdfButton = getBoton();
				pdfButton.setIcon(resourcePdf);
				pdfButton.setCaption(informe.getDescripcion());
				pdfButton.addClickListener(e -> clickPdf(informe));
				columnaDerecha.addComponents(pdfButton, contenido);

			} else {
				columnaDerecha.addComponents(contenido);
			}

			if (informe.getCanal().equals(Canal.CANAL_LABORATORIO)) {
				contenido.setValue(informe.getHtmlCabecera() + informe.getHtmlCampos_i());
			} else if (informe.getCanal().equals(Canal.CANAL_ANATOMIA)) {
				contenido.setValue(informe.getHtmlCabecera() + informe.getHtmlCampos_i());
			} else if (informe.getCanal().equals(Canal.CANAL_JIMENA)) {
				contenido.setValue(informe.getHtmlCabecera() + informe.getHtmlCampos_i());
			} else if (informe.getCanal().equals(Canal.CANAL_RADIOLOGIA)) {
				contenido.setValue(informe.getHtmlCabecera() + informe.getHtmlCampos_i());
			} else {
				contenido.setValue("");
			}
			// no tiene campos pintamos el pdf
			if (informe.getListaCampos().size() == 0) {
				if (informe.getTipobin() == 1) {
					clickPdf(informe);
				}
			}
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	public void clickPdf(Informe informe) {

		Embedded pdf = new Embedded("", new FileResource(informe.getFilePdf()));
		pdf.setMimeType("application/pdf");
		pdf.setSizeFull();
		pdf.setWidthUndefined();
		pdf.setType(Embedded.TYPE_BROWSER);
		pdf.setWidth("800px");
		pdf.setHeight("800px");
		columnaDerecha.removeAllComponents();
		columnaDerecha.addComponent(pdf);

		/*
		 * File file = informe.getFilePdf(); String url = informe.getUrlFilePdf();
		 * Resource res = new ExternalResource(url); final ResourceReference rr =
		 * ResourceReference.create(res, columnaDerecha, "Informe");
		 * getUI().getPage().open(rr.getURL(), "_blank");
		 */
	}

	/*
	 * public void verPdf() { Informe informe = new
	 * InformesDAO().getRegistroId(gridInformes.getSelectedItems().iterator().next()
	 * .getId()); contenido.setValue(informe.getHtmlCampos_i()); }
	 */

	public void clickBoton(Long canal) {
		ArrayList<Informe> listaInformes = new ArrayList<Informe>();
		if (canal != null) {
			listaInformes = new InformesDAO().getListaInformesPaciCanalNumero(paciente, canal, 5);
		} else {
			listaInformes = new InformesDAO().getListaInformesPaciOtrosCanalesNumero(paciente, 5);
		}
		gridInformes.setItems(listaInformes);
		listaInformesLayout.removeAllComponents();
		listaInformesLayout.addComponent(gridInformes);
		if (listaInformes.size() > 0) {
			if (listaInformes.get(0).getPaciente() != null) {
				gridInformes.select(listaInformes.get(0));
				clickSeleccionaInforme();
			}
		}
	}

	public String getHtmlEvolutivos() {
		String cadena = "";
		ArrayList<Registro> listaEvolutivos = new RegistroDAO().getListaRegistrosPaciTipoEpi(paciente,
				Registro.REGISTRO_EVOLUTIVO, paciente.getEpisodioActual().getId());
		for (Registro registro : listaEvolutivos) {
			if (registro != null) {
				cadena = cadena.concat(registro.getHtmlCampos() + "<hr>");
			}
		}
		return cadena;
	}

	public PantallaHCE(Component... children) {
		super(children);
	}

	public Button getBoton() {
		Button boton = new Button();
		boton.setCaptionAsHtml(true);
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		boton.setWidth("300px");
		return boton;
	}
}
