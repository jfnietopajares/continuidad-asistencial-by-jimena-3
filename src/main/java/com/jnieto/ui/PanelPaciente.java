package com.jnieto.ui;

import java.time.format.DateTimeFormatter;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.entity.Edad;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Usuario;
import com.jnieto.utilidades.Constantes;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;

/**
 * The Class PanelPaciente.
 */
public final class PanelPaciente extends Panel {

	/**
	* 
	*/
	private static final long serialVersionUID = -9168771043458391227L;

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	private final Label pacientelbl = new Label();

	/**
	 * Instantiates a new panel paciente.
	 *
	 * @param paciente the paciente
	 */
	public PanelPaciente(Paciente paciente) {
		super();
		this.setCaption("<b>" + paciente.getApellidosNombre() + "</b>&nbsp;&nbsp");

		if (paciente.getFnac() != null) {
			this.setCaption(this.getCaption() + "(" + new Edad(paciente.getFnac()).getEdadAnos() + ")");
		}
		this.setCaptionAsHtml(true);
		this.addStyleName(MaterialTheme.FORMLAYOUT_LIGHT);
		// this.setWidth("550px");
		// this.setHeight("300px");
//		pacientelbl.setSizeUndefined();
		pacientelbl.setContentMode(ContentMode.HTML);
		if (paciente != null) {

			String contenidoString = "";

			/*
			 * contenidoString = "<h3><hr><b>Paciente:</b> " +
			 * paciente.getApellidosNombre(); if (paciente.getFnac() != null) {
			 * contenidoString = contenidoString.concat(" (" + new
			 * Edad(paciente.getFnac()).getEdadUnidades() + ")"); } contenidoString =
			 * contenidoString.concat("<br><hr> ");
			 */

			if (((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
					.getEstado() == Usuario.USUARIO_ESTADOADMINISTRADOR) {
				contenidoString = contenidoString.concat("<b>Id:</b>" + paciente.getId() + "  ");
			}
			contenidoString = contenidoString.concat("<b>Historia:</b>&nbsp;" + paciente.getNumerohc() + " <br> ");
			contenidoString = contenidoString
					.concat("<b>Teléfono:</b>&nbsp;" + paciente.getTelefono() + " &nbsp;&nbsp;  ");
			contenidoString = contenidoString.concat("<b>Móvil:</b>&nbsp;" + paciente.getMovil() + " <br>  ");
			contenidoString = contenidoString.concat("<b>Dni: </b>&nbsp;" + paciente.getDni()) + "&nbsp;&nbsp;";
			contenidoString = contenidoString
					.concat("<b>Naci: </b>&nbsp;" + fechadma.format(paciente.getFnac()) + " <br>  ");
			contenidoString = contenidoString.concat("<hr>");
			if (paciente.getEpisodioActual() != null) {
				contenidoString = contenidoString
						.concat(Episodio.getDescripcionClase(paciente.getEpisodioActual().getClaseLong()));

				if (paciente.getEpisodioActual().getServicio() != null) {
					contenidoString = contenidoString
							.concat("<b>Servicio:</b>" + paciente.getEpisodioActual().getServicio().getCodigo() + "  ");
				} else {
					contenidoString = contenidoString.concat("<b>Servicio:</b>???  ");

				}
				if (paciente.getEpisodioActual().getFinicio() != null) {
					contenidoString = contenidoString.concat("&nbsp;&nbsp;&nbsp;<b>Fecha inicio:</b>&nbsp;"
							+ fechadma.format(paciente.getEpisodioActual().getFinicio()) + " <br> ");
				} else
					contenidoString = contenidoString.concat("<b>Fecha inicio:</b> ???  <br> ");

				if (paciente.getEpisodioActual().getUserid() != null) {
					contenidoString = contenidoString.concat("<b>Médico:</b>&nbsp; "
							+ paciente.getEpisodioActual().getUserid().getApellidosNombre() + " <br> ");
				} else {
					contenidoString = contenidoString.concat("<b>Médico:</b>???   <br> ");
				}
				if (paciente.getEpisodioActual().getObservacion() != null
						&& !paciente.getEpisodioActual().getObservacion().isEmpty()) {
					contenidoString = contenidoString
							.concat("<b>Diag:</b>" + paciente.getEpisodioActual().getObservacion());
				}
			}

			if (paciente.getProcesoActual() != null) {
				if (((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
						.getEstado() == Usuario.USUARIO_ESTADOADMINISTRADOR) {
					if (paciente.getProcesoActual().getId() != null) {
						contenidoString = contenidoString
								.concat("<b>IdProceso:</b>" + paciente.getProcesoActual().getId() + "  ");
					} else {
						contenidoString = contenidoString.concat("<b>Proceso:</b> ?? ");
					}
				}
				if (paciente.getProcesoActual().getServicio() != null) {
					contenidoString = contenidoString
							.concat("<b>Servicio:</b>" + paciente.getProcesoActual().getServicio().getCodigo() + "  ");
				} else {
					contenidoString = contenidoString.concat("<b>Servicio:</b>  ");

				}
				if (paciente.getProcesoActual().getFechaini() != null) {
					contenidoString = contenidoString.concat(
							"<b>Fecha:</b>" + fechadma.format(paciente.getProcesoActual().getFechaini()) + "  ");
				} else
					contenidoString = contenidoString.concat("<b>Fecha:</b>  ");

				contenidoString = contenidoString.concat("<br> ");

				if (paciente.getProcesoActual().getSubambito().equals(Proceso.SUBAMBITO_PARTOS)) {
					if (paciente.getPais() != null)
						contenidoString = contenidoString.concat("<b>Nacionalidad:</b>" + paciente.getPais() + "  ");
					else
						contenidoString = contenidoString.concat("<b>Nacionalidad:</b>??  ");

					contenidoString = contenidoString.concat("<b>Número :</b>" + "." + "  ");
				}

			}
			contenidoString = contenidoString.concat("<b> </b>" + "" + " <br> ");
			contenidoString = contenidoString.concat("</h3> ");

			pacientelbl.setValue(contenidoString);
			this.setContent(pacientelbl);
		}

	}

}
