package com.jnieto.ui;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.entity.Reports;
import com.jnieto.entity.Usuario;
import com.jnieto.reports.ColonCsv;
import com.jnieto.reports.ColonExcel;
import com.jnieto.reports.ColonHtml;
import com.jnieto.reports.ColonPdf;
import com.jnieto.reports.MamaCsv;
import com.jnieto.reports.MamaExcel;
import com.jnieto.reports.MamaHtml;
import com.jnieto.reports.MamaPdf;
import com.jnieto.reports.OxigenoAltasBajasPdf;
import com.jnieto.reports.PaliativosCsv;
import com.jnieto.reports.PaliativosExcel;
import com.jnieto.reports.PaliativosHtml;
import com.jnieto.reports.PaliativosPdf;
import com.jnieto.utilidades.Constantes;
// import com.vaadin.addon.spreadsheet.Spreadsheet;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class PantallaReports extends HorizontalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4040850700330804849L;
	private final String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	private VerticalLayout contenedorSeleccion = new VerticalLayout();
	private HorizontalLayout filaFechas = new HorizontalLayout();
	private HorizontalLayout filaTipo = new HorizontalLayout();
	private VerticalLayout contenerorReport = new VerticalLayout();

	private DateField desde = null;
	private DateField hasta = null;

	private Button enviar = null;

	private CheckBox excelBox = new CheckBox("");
	private CheckBox pdfBox = new CheckBox("");
	private CheckBox csvBox = new CheckBox("");
	private CheckBox htmlBox = new CheckBox("");

	private ArrayList<Reports> listaArrayList = new ArrayList<>();
	RadioButtonGroup<Reports> opcionButtonGroup = null;

	public PantallaReports() {
		try {

			FileResource resourceExcel = new FileResource(new File(basepath + "/WEB-INF/images/excel.png"));
			FileResource resourcePdf = new FileResource(new File(basepath + "/WEB-INF/images/pdf.png"));
			FileResource resourceCsv = new FileResource(new File(basepath + "/WEB-INF/images/csv.png"));
			FileResource resourceHtml = new FileResource(new File(basepath + "/WEB-INF/images/html.png"));
			/*
			 * Image imageExcel = new Image("", resourceExcel); Image imagePdf = new
			 * Image("", resourcePdf); Image imageCsv = new Image("", resourceCsv); Image
			 * imageHtml = new Image("", resourceHtml);
			 */
			excelBox.setIcon(resourceExcel);
			pdfBox.setIcon(resourcePdf);
			csvBox.setIcon(resourceCsv);
			htmlBox.setIcon(resourceHtml);

			this.setMargin(false);
			contenedorSeleccion.setMargin(false);
			contenedorSeleccion.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);
			filaFechas.setMargin(false);
			filaTipo.setMargin(false);

			contenerorReport.setMargin(false);
			contenerorReport.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);

			desde = new ObjetosComunes().getFecha("Desde", "");
			desde.setValue(LocalDate.now());

			hasta = new ObjetosComunes().getFecha("Hasta", "");
			hasta.setValue(LocalDate.now());

			enviar = new ObjetosComunes().getBotonCalcular();
			enviar.addClickListener(event -> clickEnviar());

			listaArrayList = new Reports()
					.getReportsUser((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME));

			opcionButtonGroup = new RadioButtonGroup<Reports>("Selecciona tipo de listado.", listaArrayList);
			opcionButtonGroup.setItemCaptionGenerator(Reports::getDescripcion);
			opcionButtonGroup.addSelectionListener(e -> selecciona());
			opcionButtonGroup.addContextClickListener(e -> selecciona());

			filaFechas.addComponents(desde, hasta);
			filaTipo.addComponents(excelBox, pdfBox, htmlBox, csvBox);
			contenedorSeleccion.addComponents(filaFechas, filaTipo, opcionButtonGroup);
			this.addComponents(contenedorSeleccion, contenerorReport);
		} catch (Exception e) {
			new NotificacionInfo(e.getMessage());
		}
	}

	/**
	 * Selecciona. Ejecuta el listado en función de la opcion 1 Listado de casos de
	 * mama 2 Listado casos colon
	 */
	public void selecciona() {
		Reports reports;
		FileResource resourceFicheroExcel;
		Link link;
		FileResource resourceFicheroCsv;

		if (excelBox.isEmpty() && pdfBox.isEmpty() && htmlBox.isEmpty() && csvBox.isEmpty()) {
			new NotificacionInfo(" Debes elegir tipos de salida excel o pdf ");
		} else {
			// directorioString=this.getParent().getParent(
			contenerorReport.removeAllComponents();
			reports = opcionButtonGroup.getValue();
			switch (reports.getId()) {
			case 1: // mama
				if (excelBox.getValue()) {
					MamaExcel mExcel = new MamaExcel(desde.getValue(), hasta.getValue());
					String namefileString = mExcel.getUrl();
					resourceFicheroExcel = new FileResource(new File(mExcel.getFilename()));
					link = new Link("Descarga el fichero: " + namefileString, resourceFicheroExcel);
					contenerorReport.addComponent(link);
					// UI.getCurrent().getPage().open(resourceFicheroExcel, "a_new", true);
				}
				if (pdfBox.getValue()) {
					File fpdf = new MamaPdf(desde.getValue(), hasta.getValue()).getFile();
					Embedded pdf = new Embedded("", new FileResource(fpdf));
					pdf.setMimeType("application/pdf");
					pdf.setType(Embedded.TYPE_BROWSER);
					pdf.setWidth("750px");
					pdf.setHeight("610px");
					contenerorReport.addComponent(pdf);
				}
				if (htmlBox.getValue()) {
					String htmlString = new MamaHtml(desde.getValue(), hasta.getValue()).getTablaHtmlMama();
					Label lblContenidoLabel = new Label(htmlString);
					lblContenidoLabel.setContentMode(ContentMode.HTML);
					contenerorReport.addComponent(lblContenidoLabel);
				}
				if (csvBox.getValue()) {
					// String ficheroString = new MamaCsv(desde.getValue(),
					// hasta.getValue()).getNombreFichero();
					MamaCsv mamaCsv = new MamaCsv(desde.getValue(), hasta.getValue());
					resourceFicheroCsv = new FileResource(new File(mamaCsv.getNombreFichero()));
					link = new Link("Descarga el fichero csv:" + mamaCsv.getUrl(), resourceFicheroCsv);
					contenerorReport.addComponent(link);
					UI.getCurrent().getPage().open(resourceFicheroCsv, "a_new", true);
				}
				break;
			case 2: // casos colon
				if (excelBox.getValue()) {
					ColonExcel mExcel = new ColonExcel(desde.getValue(), hasta.getValue());
					String namefileString = mExcel.getUrl();
					resourceFicheroExcel = new FileResource(new File(mExcel.getFilename()));
					link = new Link("Descarga el fichero: " + namefileString, resourceFicheroExcel);
					contenerorReport.addComponent(link);
				}
				if (pdfBox.getValue()) {
					File fpdf = new ColonPdf(desde.getValue(), hasta.getValue()).getFile();
					Embedded pdf = new Embedded("", new FileResource(fpdf));
					pdf.setMimeType("application/pdf");
					pdf.setType(Embedded.TYPE_BROWSER);
					pdf.setWidth("750px");
					pdf.setHeight("610px");
					contenerorReport.addComponent(pdf);
				}
				if (htmlBox.getValue()) {
					String htmlString = new ColonHtml(desde.getValue(), hasta.getValue()).getTablaHtmlColon();
					Label lblContenidoLabel = new Label(htmlString);
					lblContenidoLabel.setContentMode(ContentMode.HTML);
					contenerorReport.addComponent(lblContenidoLabel);
				}
				if (csvBox.getValue()) {
					ColonCsv coloncsv = new ColonCsv(desde.getValue(), hasta.getValue());
					resourceFicheroCsv = new FileResource(new File(coloncsv.getNombreFichero()));
					link = new Link("Descarga el fichero csv:" + coloncsv.getNombreFichero(), resourceFicheroCsv);
					contenerorReport.addComponent(link);
					UI.getCurrent().getPage().open(resourceFicheroCsv, "a_new", true);
				}
				break;
			case 10: // Paliativos
				if (excelBox.getValue()) {
					String namefileString = new PaliativosExcel(desde.getValue(), hasta.getValue()).getFilename();
					resourceFicheroExcel = new FileResource(new File(basepath + namefileString));
					link = new Link("Descarga el fichero: " + namefileString, resourceFicheroExcel);
					contenerorReport.addComponent(link);
					// UI.getCurrent().getPage().open(resourceFicheroExcel, "a_new", true);
				}
				if (pdfBox.getValue()) {
					File fpdf = new PaliativosPdf(desde.getValue(), hasta.getValue()).getFile();
					Embedded pdf = new Embedded("", new FileResource(fpdf));
					pdf.setMimeType("application/pdf");
					pdf.setType(Embedded.TYPE_BROWSER);
					pdf.setWidth("750px");
					pdf.setHeight("610px");
					contenerorReport.addComponent(pdf);
				}
				if (htmlBox.getValue()) {
					String htmlString = new PaliativosHtml(desde.getValue(), hasta.getValue()).getTablaHtmlMama();
					Label lblContenidoLabel = new Label(htmlString);
					lblContenidoLabel.setContentMode(ContentMode.HTML);
					contenerorReport.addComponent(lblContenidoLabel);
				}
				if (csvBox.getValue()) {
					String ficheroString = new PaliativosCsv(desde.getValue(), hasta.getValue()).getNombreFichero();
					resourceFicheroCsv = new FileResource(new File(ficheroString));
					link = new Link("Descarga el fichero csv: " + ficheroString, resourceFicheroCsv);
					contenerorReport.addComponent(link);
					UI.getCurrent().getPage().open(resourceFicheroCsv, "a_new", true);
				}
				break;

			case 20: // mail altas bajas oxigeno
				File fpdf = new OxigenoAltasBajasPdf(desde.getValue(), hasta.getValue()).getFile();
				Embedded pdf = new Embedded("", new FileResource(fpdf));
				pdf.setMimeType("application/pdf");
				pdf.setType(Embedded.TYPE_BROWSER);
				pdf.setWidth("750px");
				pdf.setHeight("610px");
				contenerorReport.addComponent(pdf);

				break;
			}
			// opcionButtonGroup.r;
			opcionButtonGroup.setItems(listaArrayList);
			// opcionButtonGroup.setSelectedItem(new Reports(0));
		}
	}

	public void clickEnviar() {

	}
}
