package com.jnieto.ui;

import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Zona;
import com.jnieto.ui.master.ConsolaMaster;

public class ConsolaHdiaMed extends ConsolaMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3312476414950339302L;
	private PanelPaciente cajaPaciente;
	private CajaEpisodio cajaEpisodio;
	private CajaEnfCtes cajaEnfCtes;

	public ConsolaHdiaMed(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
		zona = Zona.ZONA_HDIA_MED;
		if (paciente == null) {
			new NotificacionInfo("Sin elegir paciente");
			return;
		}
		doConsolaHdia();
	}

	public void doConsolaHdia() {
		cajaPaciente = new PanelPaciente(paciente);
		cajaEpisodio = new CajaEpisodio(paciente, Episodio.CLASE_HDIA, zona);
		fila1.addComponent(cajaPaciente);
		fila2.addComponent(cajaEpisodio);
		episodio = cajaEpisodio.getEpisodio();
		if (episodio != null) {
			cajaEnfCtes = new CajaEnfCtes(paciente, episodio);
			fila1.addComponent(cajaEnfCtes);
		} else {

		}

	}

	public ConsolaHdiaMed(Paciente paciente, Long subambito) {
		super(paciente, subambito);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void refrescar() {
		// TODO Auto-generated method stub

	}

}
