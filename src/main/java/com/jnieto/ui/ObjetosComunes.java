package com.jnieto.ui;

import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.AgendaDAO;
import com.jnieto.dao.CamaDAO;
import com.jnieto.dao.CentroDAO;
import com.jnieto.dao.GruposCatalogoDAO;
import com.jnieto.dao.ServiciosDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.dao.ZonaDAO;
import com.jnieto.entity.AccesoTipos;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Cama;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.MensajesEstados;
import com.jnieto.entity.MensajesTipos;
import com.jnieto.entity.RegistroOxi;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Zona;
import com.jnieto.utilidades.Constantes;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.themes.ValoTheme;

/**
 * The Class ObjetosComunes.
 * 
 * *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class ObjetosComunes {

	public static String NOMBRE_BOTON_MAMA = "MAMA";

	public static String NOMBRE_BOTON_COLON = "COLON";

	public static String NOMBRE_BOTON_PALIATIVOS = "PALIATIVOS";

	public static String NOMBRE_BOTON_OXIGENOTERAPIA = "OXIGENO";

	public static String NOMBRE_BOTON_PARTOS = "PARTOS";

	public static String NOMBRE_BOTON_HDIAMED = "H.D.MÉDICO";

	/**
	 * Instantiates a new objetos comunes.
	 */
	public ObjetosComunes() {

	}

	public Button getBoton(String tipo, String ancho, String caption, Resource icono, String descripcion) {
		Button boton = new Button();
		boton.setWidthUndefined();
		if (tipo.equals(Constantes.BOTON_TIPO_LIGERO)) {
			// boton.setHeight(ancho);
			boton.setCaption(caption);
			boton.setHeight("20px");
			boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		} else {
			boton.setIcon(icono);
			boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);

			boton.setHeight("30px");
		}
		boton.setDescription(descripcion);
		return boton;
	}

	/**
	 * Gets the boton buscar.
	 *
	 * @return the boton buscar
	 */
	public Button getBotonBuscar() {
		Button boton = new Button();
		// boton.setCaption("Buscar");
		boton.setIcon(VaadinIcons.SEARCH);
		// boton.setWidth("95px");
		boton.setClickShortcut(KeyCode.ENTER);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Busca datos referenciados en el formulario actual");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton borrar.
	 *
	 * @return the boton borrar
	 */
	public Button getBotonBorrar() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.MINUS);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Borra de la base de datos la ficha actual");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton grabar.
	 *
	 * @return the boton grabar
	 */
	public Button getBotonGrabar() {
		Button boton = new Button();
		// boton.setCaption("Grabar");
		boton.setIcon(VaadinIcons.RECORDS);
		boton.setIcon(VaadinIcons.CHECK);
		// boton.setWidth("95px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Almacena en base de datos la información actual.");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton conectar.
	 *
	 * @return the boton conectar
	 */
	public Button getBotonConectar() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.CHECK);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Conectar.");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton limpiar.
	 *
	 * @return the boton limpiar
	 */
	public Button getBotonLimpiar() {
		Button boton = new Button();
		// boton.setCaption("Limpiar");
		boton.setIcon(VaadinIcons.COMPILE);
		// boton.setWidth("95px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription(
				"Borra los datos de los campos del formulario. No modifica información en la base de datos");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton terapias.
	 *
	 * @return the boton terapias
	 */
	public Button getBotonTerapias() {
		Button boton = new Button();
		boton.setCaption("Terpias");
		boton.setIcon(VaadinIcons.PILLS);
		boton.setWidth("100px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Permite gestionar las terapias del paciente y/o proceso actual ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton procesos.
	 *
	 * @return the boton procesos
	 */
	public Button getBotonProcesos() {
		Button boton = new Button();
		// boton.setCaption("Procesos");
		boton.setIcon(VaadinIcons.FILE_PROCESS);
		boton.setWidth("110px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Permite gestionar los procesos del paciente y/o proceso actual ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton incidencias.
	 *
	 * @return the boton incidencias
	 */
	public Button getBotonIncidencias() {
		Button boton = new Button();
		// boton.setCaption("Incidencias");
		boton.setIcon(VaadinIcons.EXCLAMATION);
		boton.setWidth("130px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Permite gestionar las inicidencias del paciente  ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton regristro.
	 *
	 * @return the boton regristro
	 */
	public Button getBotonRegristro() {
		Button boton = new Button();
		// boton.setCaption("Incidencias");
		boton.setIcon(VaadinIcons.CLIPBOARD);
		// boton.setWidth("130px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Registro ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton enviar.
	 *
	 * @return the boton enviar
	 */
	public Button getBotonEnviar() {
		Button boton = new Button();
		boton.setCaption("Enviar");
		boton.setIcon(VaadinIcons.MAILBOX);
		boton.setWidth("120px");
		boton.setHeight("30px");
		boton.setDescription("Genera los datos del mail y los envía ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton listar.
	 *
	 * @return the boton listar
	 */
	public Button getBotonListar() {
		Button boton = new Button();
		boton.setCaption("Enviar");
		boton.setIcon(VaadinIcons.MAILBOX);
		boton.setWidth("120px");
		boton.setHeight("30px");
		boton.setDescription("Genera los datos y muestra el listado");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton calcular.
	 *
	 * @return the boton calcular
	 */
	public Button getBotonCalcular() {
		Button boton = new Button();
		boton.setCaption("Calcular");
		boton.setIcon(VaadinIcons.CALC);
		boton.setWidth("120px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Calcula los indicadores");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonJimena() {
		Button boton = new Button();
		boton.setCaption("Jimena");
		boton.setWidth("120px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Acceso a Jimena");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonPartos() {
		Button boton = new Button();
		boton.setCaption(NOMBRE_BOTON_PARTOS);
		boton.setWidth("120px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Consola partos");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonHdia() {
		Button boton = new Button();
		boton.setCaption(NOMBRE_BOTON_HDIAMED);
		boton.setWidth("120px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Consola hdia médico");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton anterior.
	 *
	 * @return the boton anterior
	 */
	public Button getBotonAnterior() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.BACKWARDS);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton primero.
	 *
	 * @return the boton primero
	 */
	public Button getBotonPrimero() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.FAST_BACKWARD);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton siguiente.
	 *
	 * @return the boton siguiente
	 */
	public Button getBotonSiguiente() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.FORWARD);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton ultimo.
	 *
	 * @return the boton ultimo
	 */
	public Button getBotonUltimo() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.FAST_FORWARD);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonUpload() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.UPLOAD);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Adjunta informe al proceso");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the botonr registros.
	 *
	 * @return the botonr registros
	 */
	public Button getBotonrRegistros() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.RECORDS);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton proceso mama.
	 *
	 * @return the boton proceso mama
	 */
	public Button getBotonProcesoMama() {
		Button boton = new Button(ObjetosComunes.NOMBRE_BOTON_MAMA);
		// boton.setIcon(VaadinIcons.FEMALE);
		// boton.setWidth("400px");
		boton.setCaption(NOMBRE_BOTON_MAMA);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Gestiona un proceso de mama para el paciente ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonProcesoColon() {
		Button boton = new Button(ObjetosComunes.NOMBRE_BOTON_COLON);
		boton.setCaption(NOMBRE_BOTON_COLON);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Gestiona un proceso de colon para el paciente ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonProcesoPaliativos() {
		Button boton = new Button(ObjetosComunes.NOMBRE_BOTON_PALIATIVOS);
		boton.setCaption(NOMBRE_BOTON_PALIATIVOS);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Gestiona un proceso de paliativos para el paciente ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonProcesoOxigeno() {
		Button boton = new Button(ObjetosComunes.NOMBRE_BOTON_OXIGENOTERAPIA);
		boton.setCaption(NOMBRE_BOTON_OXIGENOTERAPIA);
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Gestiona un proceso de oxigenoterapia.");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	/**
	 * Gets the boton cerrar.
	 *
	 * @return the boton cerrar
	 */
	public Button getBotonCerrar() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.CLOSE_SMALL);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Cierra formulario actual ");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonCalendarioVerCitas() {
		Button boton = new Button();
		boton.setVisible(true);
		boton.setCaption("Citados");
		boton.setHeight("30px");
		boton.setIcon(VaadinIcons.CALENDAR);
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonMapaCamas() {
		Button boton = new Button();
		boton.setVisible(true);
		boton.setCaption("Mapa");
		boton.setHeight("30px");
		boton.setIcon(VaadinIcons.MAP_MARKER);
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public Button getBotonVerInforme() {
		Button boton = new Button();
		boton.setIcon(VaadinIcons.CLIPBOARD_PULSE);
		// boton.setWidth("400px");
		boton.setWidthUndefined();
		boton.setHeight("30px");
		boton.setDescription("Ver informe asocidado");
		boton.addStyleName(MaterialTheme.BUTTON_PRIMARY + " " + MaterialTheme.BUTTON_ROUND);
		return boton;
	}

	public ComboBox<Cama> getComboCama(Cama cama, Cama camadefecto, Zona zona, String texto) {
		ComboBox<Cama> combo = new ComboBox<>();
		ArrayList<Cama> listaCamas = new CamaDAO().getListaCamas(zona);
		combo.setCaption(texto);
		combo.setItems(listaCamas);
		combo.setWidth("120px");
		combo.setItemCaptionGenerator(Cama::getCama);
		if (cama != null) {
			combo.setSelectedItem(cama);
		} else if (listaCamas.size() == 1) {
			combo.setSelectedItem(listaCamas.get(0));
		} else if (cama != null) {
			combo.setSelectedItem(cama);
		} else
			combo.setSelectedItem(listaCamas.get(0));

		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return combo;
	}

	public ComboBox<Agenda> getComboAgenda(Centro centro, Servicio servicio, Agenda seleccionada, Agenda defecto) {
		ComboBox<Agenda> combo = new ComboBox<Agenda>();
		combo.setVisible(true);
		ArrayList<Agenda> listaAgendas = new AgendaDAO().getListaAgendas(centro, servicio);
		combo.setWidth("150px");
		combo.setItemCaptionGenerator(Agenda::getDescripcion);
		combo.setItems(listaAgendas);
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		if (seleccionada != null) {
			combo.setSelectedItem(defecto);
		} else if (defecto != null) {
			combo.setSelectedItem(defecto);
		} else if (listaAgendas.size() == 1) {
			combo.setSelectedItem(listaAgendas.get(0));
		} else
			combo.setSelectedItem(listaAgendas.get(0));

		return combo;
	}

	/**
	 * Gets the combo servicio.
	 *
	 * @param servicio the servicio
	 * @return the comobo servicio
	 */
	public ComboBox<Servicio> getComboServicio(Servicio servicio, Servicio defecto) {
		ComboBox<Servicio> combo = new ComboBox<>();
		ArrayList<Servicio> listaServicios = new ServiciosDAO().getListaRegistos();
		combo.setCaption("Servicio");
		combo.setItems(listaServicios);
		// combo.setWidth("200px");
		combo.setItemCaptionGenerator(Servicio::getDescripcion);
		if (servicio != null) {
			combo.setSelectedItem(servicio);
		} else if (listaServicios.size() == 1) {
			combo.setSelectedItem(listaServicios.get(0));
		} else if (defecto != null) {
			combo.setSelectedItem(defecto);
		} else
			combo.setSelectedItem(listaServicios.get(0));

		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return combo;
	}

	public ComboBox<Servicio> getComboServicioCorto(Centro centro, Servicio servicio, Servicio defecto, String ambito) {
		ComboBox<Servicio> combo = new ComboBox<>();
		combo.setResponsive(true);
		ArrayList<Servicio> listaServicios = new ServiciosDAO().getListaRegistos(centro, ambito);
		combo.setItems(listaServicios);
		// combo.setWidth("80px");
		combo.setItemCaptionGenerator(Servicio::getCodigo);
		if (servicio != null) {
			combo.setSelectedItem(servicio);
		} else if (listaServicios.size() == 1) {
			combo.setSelectedItem(listaServicios.get(0));
		} else if (defecto != null) {
			combo.setSelectedItem(defecto);
		} else
			combo.setSelectedItem(listaServicios.get(0));

		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);

		return combo;
	}

	public ComboBox<Centro> getComboCentros(Centro centro, Centro defecto) {
		ComboBox<Centro> combo = new ComboBox<>();
		combo.setResponsive(true);
		ArrayList<Centro> listaCentros = new CentroDAO().getListaRegistrosAreaHosCep();
		combo.setItems(listaCentros);
		// combo.setWidth("120px");
		combo.setItemCaptionGenerator(Centro::getNemonico);
		if (centro != null)
			combo.setSelectedItem(centro);
		else if (listaCentros.size() == 1)
			combo.setSelectedItem(listaCentros.get(0));
		else if (defecto != null)
			combo.setSelectedItem(defecto);
		else
			combo.setSelectedItem(listaCentros.get(0));

		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return combo;
	}

	public ComboBox<Usuario> getComboMedicoServ(Centro centro, Servicio servicio, Usuario usuario, Usuario defecto) {
		ComboBox<Usuario> combo = new ComboBox<>();
		ArrayList<Usuario> listaMedicos = new UsuarioDAO().getListaUsuarios(centro, servicio);
		combo.setItems(listaMedicos);
		combo.setWidth("300px");
		combo.setItemCaptionGenerator(Usuario::getApellidosNombre);
		if (listaMedicos != null)
			combo.setItems(listaMedicos);

		if (listaMedicos.size() == 1)
			combo.setSelectedItem(listaMedicos.get(0));
		else if (listaMedicos.size() > 1) {
			if (defecto != null)
				combo.setSelectedItem(defecto);
			else
				combo.setSelectedItem(listaMedicos.get(0));
		}
		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return combo;
	}

	public ComboBox<MensajesEstados> getComboMensajesEstados(MensajesEstados msgstd) {
		ComboBox<MensajesEstados> combo = new ComboBox<>();
		ArrayList<MensajesEstados> lista = MensajesEstados.LISTAMENSAJESESTADOS;
		combo.setCaption("Estado");
		combo.setItems(lista);
		combo.setWidth("180px");
		combo.setItemCaptionGenerator(MensajesEstados::getDescripcion);
		combo.setEmptySelectionAllowed(true);
		if (msgstd != null)
			combo.setSelectedItem(msgstd);
		else if (lista.size() == 1)
			combo.setSelectedItem(lista.get(0));

		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<EpisodioClase> getComboEpisodioClase(EpisodioClase clase, Centro centro) {
		ComboBox<EpisodioClase> combo = new ComboBox<>();
		combo.setResponsive(true);
		ArrayList<EpisodioClase> lista = Episodio.getListaclases(centro);
		combo.setItems(lista);
		combo.setWidth("80px");
		combo.setItemCaptionGenerator(EpisodioClase::getDescripcion);
		combo.setEmptySelectionAllowed(true);
		if (clase != null) {
			combo.setSelectedItem(clase);
		} else if (lista.size() == 1) {
			combo.setSelectedItem(lista.get(0));
		} else {
			combo.setSelectedItem(Episodio.CLASE_HOSPITALIZACION);
		}
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<Zona> getComboZona(Zona zonaValor, Zona zonaDefecto, int tipo, Centro centro, Servicio servicio) {
		ComboBox<Zona> combo = new ComboBox<>();
		combo.setResponsive(true);
		ArrayList<Zona> lista = new ZonaDAO().getListaRegistrosTipo(tipo, centro, servicio);
		lista.add(new Zona());
		combo.setEmptySelectionCaption("");
		combo.setItems(lista);
		combo.setItemCaptionGenerator(Zona::getZona);
		combo.setEmptySelectionAllowed(true);
		if (zonaValor != null) {
			combo.setSelectedItem(zonaValor);
		} else if (lista.size() == 1) {
			combo.setSelectedItem(lista.get(0));
		} else {
			combo.setSelectedItem(null);
		}
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		combo.setEmptySelectionAllowed(false);
		combo.setDescription("zona");
		return combo;
	}

	public ComboBox<MensajesTipos> getComboMensajesTipos(MensajesTipos msgstd) {
		ComboBox<MensajesTipos> combo = new ComboBox<>();
		ArrayList<MensajesTipos> lista = MensajesTipos.LISTAMENSAJESTIPOS;
		combo.setCaption("Tipo");
		combo.setItems(lista);
		combo.setWidth("100px");
		combo.setItemCaptionGenerator(MensajesTipos::getDescripcion);
		combo.setEmptySelectionAllowed(true);
		if (msgstd != null)
			combo.setSelectedItem(msgstd);
		else if (lista.size() == 1)
			combo.setSelectedItem(lista.get(0));

		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<AccesoTipos> getComboAccesosTipos(AccesoTipos acs) {
		ComboBox<AccesoTipos> combo = new ComboBox<>();
		ArrayList<AccesoTipos> lista = AccesoTipos.LISTACCESOSTIPOS;
		combo.setCaption("Tipo");
		combo.setItems(lista);
		combo.setWidth("200px");
		combo.setItemCaptionGenerator(AccesoTipos::getDescripcion);
		combo.setEmptySelectionAllowed(true);
		if (acs != null)
			combo.setSelectedItem(acs);
		else if (lista.size() == 1)
			combo.setSelectedItem(lista.get(0));

		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<Centro> getCreaComboCentro(Centro centro) {
		ComboBox<Centro> combo = new ComboBox<>();
		ArrayList<Centro> listaCentros = new CentroDAO().getListaRegistros();
		combo.setCaption("Centro");
		combo.setItems(listaCentros);
		combo.setItemCaptionGenerator(Centro::getNemonico);
		combo.setEmptySelectionAllowed(false);
		if (centro != null) {
			combo.setSelectedItem(centro);
		} else if (listaCentros.size() == 1)
			combo.setSelectedItem(listaCentros.get(0));
		else
			combo.setSelectedItem(Centro.CENTRO_DEFECTO);

		combo.setWidth("150px");
		combo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return combo;
	}

	public ComboBox<String> getcreaComboTipoFlujo(String oxiTipoFluo) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Tipo Flujo");
		combo.setWidth("120px");
		// combo.setItems("Contínuo", "Pulsos");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroOxi.GRUPOSC_O2_TIPOS_FLUJOS));
		if (oxiTipoFluo == null)
			combo.setValue(RegistroOxi.TIPO_FLUJO_DEFECTO);
		else
			combo.setValue(oxiTipoFluo);
		combo.setEmptySelectionAllowed(false);

		return combo;
	}

	public ComboBox<String> docreaComboTipoPrescripcion(String tipo) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Elige Tipo");
		combo.setWidth("150px");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroOxi.GRUPOSC_O2_TIPOS_PRESCRIPCION));
		if (tipo == null)
			combo.setValue(RegistroOxi.TIPO_PRESCPCION_DEFECTO);
		else
			combo.setValue(tipo);
		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<String> docreaComboDuracion(String duracion) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Elige duracion");
		combo.setWidth("195px");
		// combo.setItems("3 meses", "1 año", "Definitiva");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroOxi.GRUPOSC_O2_DURACION_PRESCIPCION));
		if (duracion == null)
			combo.setValue(RegistroOxi.DURACION_DEFECTO);
		else
			combo.setValue(duracion);
		combo.setEmptySelectionAllowed(false);
		return combo;
	}

	public ComboBox<String> docreaComboInterfase(String oxiInterfase) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption("Interfase");
		combo.setWidth("150px");
		// combo.setItems("Contínuo", "Pulsos");
		combo.setItems(new GruposCatalogoDAO().getValoresPorGrupo(RegistroOxi.GRUPOSC_O2_TIPO_INTERFASE));
		if (oxiInterfase == null)
			combo.setValue(RegistroOxi.TIPO_INTERFASE_DEFECTO);
		else
			combo.setValue(oxiInterfase);
		combo.setEmptySelectionAllowed(false);

		return combo;
	}

	public ComboBox<String> getComboNumero(String captacion, int ini, int fin, String valor, String ancho) {
		ComboBox<String> combo = new ComboBox<String>();
		combo.setCaption(captacion);
		combo.setWidth(ancho);
		ArrayList<String> listaArrayList = new ArrayList<String>();
		for (int i = ini; i <= fin; i++) {
			listaArrayList.add(Integer.toString(i));
		}
		combo.setItems(listaArrayList);
		combo.setEmptySelectionAllowed(false);
		combo.addStyleName(MaterialTheme.COMBOBOX_SMALL);
		return combo;
	}

	/**
	 * Gets the fecha.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the fecha
	 */
	// un campo tipo fecha
	public DateField getFecha(String textocap, String textoplace) {
		DateField campo = new DateField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setDateFormat("dd/MM/yyyy");
		campo.setIcon(VaadinIcons.CALENDAR_USER);
		campo.setWidth("140px");
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	public TextField getHora(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setIcon(VaadinIcons.CLOCK);
		campo.setWidth("60px");
		campo.setMaxLength(5);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the userid.
	 *
	 * @return the userid
	 */
	// user id
	public TextField getUserid() {
		TextField userid = new TextField();
		userid.setCaption("Usuario");
		userid.setPlaceholder(" id usuario ");
		userid.setIcon(VaadinIcons.USER);
		userid.setWidth("100px");
		userid.setMaxLength(10);
		userid.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return userid;
	}

	/**
	 * Gets the ape nombre.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the ape nombre
	 */
	// nombre o apellidos
	public TextField getApeNombre(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		// campo.setIcon(VaadinIcons.USER);
		campo.setWidth("180px");
		campo.setMaxLength(30);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the dni.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the dni
	 */
	// dni
	public TextField getDni(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(9);
		campo.setWidth("95px");
		campo.setIcon(VaadinIcons.USER_CARD);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the numerohc.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the numerohc
	 */
	// numero historia
	public TextField getNumerohc(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(9);
		campo.setWidth("90px");
		campo.setIcon(VaadinIcons.HEALTH_CARD);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the mail.
	 *
	 * @return the mail
	 */
	// mail
	public TextField getMail() {
		TextField campo = new TextField();
		campo.setCaption("Mail");
		campo.setPlaceholder(" mail ");
		campo.setIcon(VaadinIcons.MAILBOX);
		campo.setWidth("130px");
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the sexo.
	 *
	 * @return the sexo
	 */
//sexo
	public RadioButtonGroup<String> getSexo() {
		RadioButtonGroup<String> campo = new RadioButtonGroup<>(null, Constantes.LISTASEXOS);
		campo.setCaption("Sexo");
		campo.addStyleName(ValoTheme.OPTIONGROUP_HORIZONTAL);
		campo.setSelectedItem("Mujer");
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		// campo.setIcon(VaadinIcons.PHONE_LANDLINE);
		return campo;
	}

	/**
	 * Gets the telefono.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the telefono
	 */
	// teléfono
	public TextField getTelefono(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(12);
		campo.setWidth("95px");
		campo.setIcon(VaadinIcons.PHONE_LANDLINE);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the movil.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the movil
	 */
	// movil
	public TextField getMovil(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(12);
		campo.setWidth("95px");
		campo.setIcon(VaadinIcons.MOBILE);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the domicilio.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the domicilio
	 */
	public TextField getDomicilio(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(200);
		campo.setWidth("200px");
		campo.setIcon(VaadinIcons.ROAD);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);

		return campo;
	}

	/**
	 * Gets the codigo postal.
	 *
	 * @param textocap   the textocap
	 * @param textoplace the textoplace
	 * @return the codigo postal
	 */
	public TextField getCodigoPostal(String textocap, String textoplace) {
		TextField campo = new TextField();
		campo.setCaption(textocap);
		campo.setPlaceholder(textoplace);
		campo.setMaxLength(5);
		campo.setWidth("70px");
		campo.setIcon(VaadinIcons.ENVELOPE);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);

		return campo;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	// passwd
	public PasswordField getPassword() {
		PasswordField campo = new PasswordField();
		campo.setCaption("Clave");
		campo.setPlaceholder(" clave ");
		campo.setIcon(VaadinIcons.KEY);
		campo.setWidth("110px");
		campo.setMaxLength(60);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;
	}

	/**
	 * Gets the usuario activo.
	 *
	 * @return the usuario activo
	 */
	// usuario activo
	public CheckBox getUsuarioActivo() {
		CheckBox campo = new CheckBox("Activo", true);
		campo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		return campo;

	}

	/**
	 * Gets the filtro.
	 *
	 * @return the filtro
	 */
	public TextField getFiltro() {
		TextField filtro = new TextField();
		filtro.setCursorPosition(0);
		filtro.setMaxLength(100);
		filtro.setWidth("150px");
		return filtro;
	}

}
