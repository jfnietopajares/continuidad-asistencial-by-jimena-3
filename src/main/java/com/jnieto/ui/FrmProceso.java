package com.jnieto.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.dialogs.ConfirmDialog;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.InformesDAO;
import com.jnieto.dao.ProcesoDAO;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Servicio;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.ui.master.VentanaUpload;
import com.jnieto.ui.master.VentanaVerPdf;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.server.UserError;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class FrmRegistroProceso. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmProceso extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9215935108571046832L;

	private static final Logger logger = LogManager.getLogger(FrmProceso.class);

	private DateField fechaini = null;

	private ComboBox<Servicio> servicioSelect = null;

	private TextField subservicio = new TextField("Subservicio");

	private TextField origen = new TextField("Origen");

	private ComboBox<Centro> centroSelect = null;

	private TextField motivo = new TextField("Motivo");

	private TextField diagnostico = new TextField("Diagnostico");

	private TextField observaciones = new TextField("Observaciones");

	private TextField motivo_baja = new TextField("Motivo Baja");

	private DateField fechafin = null;

	private VerticalLayout filacampos = new VerticalLayout();

	private Button upload = null;

	private Button informe = null;

	public Proceso proceso = new Proceso();

	Binder<Proceso> binder = new Binder<>();

	/**
	 * Instantiates a new frm registro proceso.
	 *
	 * @param proceso the proceso.
	 */

	public FrmProceso(Proceso proceso) {
		super();
		this.proceso = proceso;
		this.setWidthUndefined();
		this.setHeightUndefined();
		if (proceso.getId().equals(new Long(0))) {
			proceso.setFechaini(LocalDate.now());
		}
		lbltitulo.setCaption("Nº: " + proceso.getId());

		// filacampos.setMargin(false);

		upload = new ObjetosComunes().getBotonUpload();
		upload.addClickListener(event -> uploadClick());

		informe = new ObjetosComunes().getBotonVerInforme();
		informe.addClickListener(e -> clickVerInformePdf());

		if (proceso.getIdInformeAsociado() > 0) {
			informe.setVisible(true);
		} else {
			informe.setVisible(false);
		}

		subservicio.setVisible(false);

		fechaini = new ObjetosComunes().getFecha("Fecha inicio", "");
		fechaini.setValue(LocalDate.now());
		binder.forField(fechaini).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Proceso::getFechaini,
				Proceso::setFechaini);

		motivo.setWidth("350px");
		motivo.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(motivo).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Proceso::getMotivo,
				Proceso::setMotivo);
		motivo.focus();

		origen.setWidth("300px");
		origen.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(origen).bind(Proceso::getOrigen, Proceso::setOrigen);

		diagnostico.setWidth("300px");
		diagnostico.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(diagnostico).bind(Proceso::getDiagnostico, Proceso::setDiagnostico);

		observaciones.setWidth("500px");
		observaciones.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(Proceso::getObservaciones, Proceso::setObservaciones);

		motivo_baja.setWidth("400px");
		motivo_baja.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(motivo_baja).bind(Proceso::getMotivo_baja, Proceso::setMotivo_baja);

		fechafin = new ObjetosComunes().getFecha("Fecha fin", "");

		binder.forField(fechafin).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaini.getValue());
		}, "Mayor o igual que fecha inicio").bind(Proceso::getFechafin, Proceso::setFechafin);

		centroSelect = new ObjetosComunes().getCreaComboCentro(proceso.getCentro());
		binder.forField(centroSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Proceso::getCentro,
				Proceso::setCentro);

		servicioSelect = new ObjetosComunes().getComboServicio(proceso.getServicio(),
				Proceso.getServicioDefecto(proceso.getSubambito()));
		binder.forField(servicioSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Proceso::getServicio,
				Proceso::setServicio);

		binder.readBean(proceso);
		this.doActivaBotones();

		fila1.addComponents(fechaini, centroSelect, informe);
		fila2.addComponents(servicioSelect, motivo);
		fila3.addComponents(origen, diagnostico);
		fila4.addComponents();
		fila5.addComponents(fechafin, motivo_baja);
		fila6.addComponent(observaciones);
		// contenedorCampos.addComponents(fila1, fila2, fila3, fila6, fila5);
		contenedorBotones.removeAllComponents();
		contenedorBotones.addComponents(lbltitulo, grabar, borrar, upload, ayuda, cerrar);
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila6, fila5);
		// this.addComponents(contenedorBotones, contenedorCampos);
	}

	public void clickVerInformePdf() {
		new VentanaVerPdf(this.getUI(), proceso.getIdInformeAsociado(), VentanaVerPdf.TIPO_VERPDF_INFORME);
	}

	public void uploadClick() {
		new VentanaUpload(this.getUI(), proceso);

		proceso.setIdInformeAsociado(new InformesDAO().getIdInformeProceso(proceso.getPaciente(), proceso.getId()));

		if (proceso.getIdInformeAsociado() > 0) {
			informe.setVisible(true);
		} else {
			informe.setVisible(false);
		}
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		/*
		 * for (Window window : MyUI.getCurrent().getWindows()) {
		 * System.out.print(window.getCaption()); } Sy
		 * Sstem.out.print(MyUI.getCurrent().getWindows().iterator().next().getClass().
		 * getName());
		 */

		MyUI.getCurrent().getWindows().iterator().next().close();

		try {
			if (MyUI.getCurrent().getWindows() != null) {
				if (MyUI.getCurrent().getWindows().iterator() != null) {
					if (MyUI.getCurrent().getWindows().iterator().next() != null) {
						if (MyUI.getCurrent().getWindows().iterator().next().getClass().getName()
								.equals("com.jnieto.ui.VentanaFrm")) {
							MyUI.getCurrent().getWindows().iterator().next().close();
							// ((CajaProcesos) this.getParent()).refrescarClick();
						}
					}
				}
			} else {
				((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
			}
		} catch (NoSuchElementException e) {
			new NotificacionInfo("Error en elemento de pantalla." + e.getMessage());
			logger.error("Error en elemento de pantalla.", e);
			// ((PantallaProcesos)
			// this.getParent().getParent().getParent()).refrescarClick();
		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		if (proceso != null) {
			if (new ProcesoDAO().getNumeroRegistroProceso(proceso) == 0) {
				ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO,
						Constantes.CONFIRMACION_BORRADO_MENSAJE, Constantes.CONFIRMACION_BOTONSI,
						Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {

							private static final long serialVersionUID = 6169352858399108337L;

							public void onClose(ConfirmDialog dialog) {
								if (dialog.isConfirmed()) {
									borraElRegistro();
								}
							}
						});

			} else {
				new NotificacionInfo(NotificacionInfo.SQLREGISTROSERRORBORRADO);
			}
			cerrarClick();
		}
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new ProcesoDAO().borraDatos(proceso)) {
			new NotificacionInfo(NotificacionInfo.DATO_BORRADO);
		} else {
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		}
		cerrarClick();
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		if (this.doValidaFormulario() == true) {
			try {
				binder.writeBean(proceso);
				ProcesoDAO procesoDAO = new ProcesoDAO();
				if (procesoDAO.grabaDatos(proceso) == true) {
					new NotificacionInfo(NotificacionInfo.DATO_GRABADO);
					cerrarClick();
				} else {
					new NotificacionInfo(NotificacionInfo.DATO_ERROR);
				}
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			}
		}
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(FrmProceso.AYUDA_FRM_PROCESO));
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;

		// fechaini no se puede solapar con otros procesos ya cerrados.
		if (new ProcesoDAO().getprocesosSolapados(proceso, fechaini.getValue())) {
			valido = false;
			fechaini.setComponentError(new UserError(Proceso.AVISO_PROCESO_SOLAPADO));
			new NotificacionInfo(Proceso.AVISO_PROCESO_SOLAPADO);
		} else {
			fechaini.setComponentError(null);
		}

		if (!motivo_baja.isEmpty() && fechafin.isEmpty()) {
			new NotificacionInfo(Proceso.AVISO_PROCESO_MOTIVO_FECHA);
			valido = false;
			fechafin.setComponentError(new UserError(Proceso.AVISO_PROCESO_MOTIVO_FECHA));
		} else {
			fechafin.setComponentError(null);
		}
		if (motivo_baja.isEmpty() && !fechafin.isEmpty()) {
			new NotificacionInfo(Proceso.AVISO_PROCESO_MOTIVO_FECHA);
			valido = false;
			motivo_baja.setComponentError(new UserError(Proceso.AVISO_PROCESO_MOTIVO_FECHA));
		} else {
			motivo_baja.setComponentError(null);
		}
		return valido;
	}

	/**
	 * Do control botones. El botón de borrar está activo cuando se está editan un
	 * registro no cuando se añade.
	 * 
	 * Los procesos cerrados sólo se pueden editar o borrar si no han pasado el
	 * límite de dias que indica la constante DIAS_MODIFICACION_PROCESOS_CERRADOS
	 * 
	 * @param proceso the proceso
	 */
	public void doActivaBotones() {
		if (proceso.getId().equals(new Long(0))) {
			borrar.setEnabled(false);
		} else {
			upload.setEnabled(true);
			if (new ProcesoDAO().getNumeroRegistroProceso(proceso) > 0) {
				borrar.setEnabled(false);
			} else {
				borrar.setEnabled(true);
			}
		}

		if (proceso.getMotivo_baja() != null && !proceso.getMotivo_baja().isEmpty()) {
			LocalDate date = LocalDate.now();
			System.out.print(fechafin.getValue());
			long dias = ChronoUnit.DAYS.between(fechafin.getValue(), date);
			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				filacampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
				upload.setEnabled(false);
			}
		}
	}

	/** The Constant AYUDA_FRM_PROCESO. */
	public static final String AYUDA_FRM_PROCESO = " <b>Pantalla de mantenimiento de procesos  :\n  </b> <br>"
			+ Ayudas.AYUDA_BOTONES_ABCA + "<ul> " + "<li><b>Fecha inicio:</b>  Fecha de inicio del proceso.</li>"
			+ "<li><b>Centro:</b>  Centro al que está asociado el proceso.</li>"
			+ "<li><b>Servicio:</b>  Servicio al que está asociado el proceso.</li>"
			+ "<li><b>Motivo:</b>  Motivo asistencial que origina el inicio del proceso.</li>"
			+ "<li><b>Origen:</b>  Origen del proceso.</li>" + "<li><b>Observaciones:</b>  Observaciones .</li>"
			+ "<li><b>Fecha fin:</b>  Fecha fin del proceso .</li>"
			+ "<li><b>Motivo fin:</b>  Motivo fin del proceso .</li>" + " <hr>";

	public Proceso getProceso() {
		return proceso;
	}

	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
	}

}
