package com.jnieto.ui.mama;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.vaadin.dialogs.ConfirmDialog;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroColon;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.PantallaProcesos;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.ui.master.VentanaVerPdf;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroColono.
 * 
 * @author JuanNieto
 */
public class FrmRegistroColon extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6411347327340582726L;

	private DateField fechaAnalitica = null;

	private TextField resultado = new TextField();

	private DateField fechaCitaColonoscopia = null;

	private DateField fechaInformeColonoscopia = null;

	private DateField fechaInformeAnatomia = null;

	protected TextField idInformeColonoscopia = new TextField("Id Inf.Colon");

	protected TextField idInformeAnatomia = new TextField("Id. Inf.Ana");

	protected TextArea observaciones = new TextArea("Comentario");

	private Button informe = null;

	private RegistroColon registroColon = null;

	private Binder<RegistroColon> binder = new Binder<>();

	/**
	 * Instantiates a new frm registro colon.
	 */
	public FrmRegistroColon(RegistroColon registroColonParam) {
		super();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila12, fila3, fila4);
		this.registroColon = registroColonParam;

		if (registroColon == null) {
			new NotificacionInfo(NotificacionInfo.ERROR_REGISTRO_ES_NULO, true);
		} else if (registroColon.getPaciente() == null) {
			new NotificacionInfo(NotificacionInfo.ERROR_PACIENTE_ES_NUlO, true);
		} else if (registroColon.getPaciente().getId() == 0) {
			new NotificacionInfo(NotificacionInfo.ERROR_PACIENTE_ES_NUlO, true);
		} else if (registroColon.getProblema() == null) {
			new NotificacionInfo(NotificacionInfo.ERROR_PROCESO_ES_NULO, true);
		} else if (registroColon.getProblema().getId() == 0) {
			new NotificacionInfo(NotificacionInfo.ERROR_PROCESO_ES_NULO, true);
		} else {

			lbltitulo.setCaption(
					"Formulario Colon " + registroColon.getProblema().getId() + "/" + registroColon.getId());

			fechaAnalitica = new ObjetosComunes().getFecha("Fecha Analítica ", " fecha análisi TSOH ");

			binder.forField(fechaAnalitica)
					.withValidator(returnDate -> !returnDate.isBefore(registroColon.getProblema().getFechaini()),
							" Mayor inico proceso")
					.bind(RegistroColon::getAnaliticaDate, RegistroColon::setAnalitica);

			resultado.setCaption("Resultado TSH");
			resultado.setPlaceholder(" valor TSOH S ");
			resultado.setWidth("50px");
			resultado.setMaxLength(5);
			resultado.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
			binder.forField(resultado).withValidator(numero -> numero.matches("[0-9]*"), " sólo números  ")
					.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 1000),
							" de 1 a 9999 ")
					.bind(RegistroColon::getResultadoString, RegistroColon::setResultado);

			informe = new ObjetosComunes().getBotonVerInforme();
			informe.addClickListener(e -> clickVerInformePdf());

			if (registroColon.getProblema().getIdInformeAsociado() > 0) {
				informe.setVisible(true);
			} else {
				informe.setVisible(false);
			}

			fechaCitaColonoscopia = new ObjetosComunes().getFecha("Fecha cita colono ", " f. cita colono  ");

			binder.forField(fechaCitaColonoscopia).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaAnalitica.getValue());
			}, "Mayor o igual que fecha ap").bind(RegistroColon::getCitaColonoDate, RegistroColon::setCitaColono);

			fechaInformeColonoscopia = new ObjetosComunes().getFecha("Fecha informe colono ", " f. inf colono  ");
			binder.forField(fechaInformeColonoscopia).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaCitaColonoscopia.getValue());
			}, "Mayor o igual que fecha cita ").bind(RegistroColon::getInformeColonoDate,
					RegistroColon::setInformeColono);

			idInformeColonoscopia.setCaption("Id Inf Colono ");
			idInformeColonoscopia.setPlaceholder(" id inf colono ");
			idInformeColonoscopia.setWidth("100px");
			idInformeColonoscopia.setMaxLength(10);
			idInformeColonoscopia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
			binder.forField(idInformeColonoscopia).bind(RegistroColon::getIdAnatoValor, RegistroColon::setIdAnato);

			fechaInformeAnatomia = new ObjetosComunes().getFecha("Fecha inf anatomía  ", " f. inf anatomía  ");
			binder.forField(fechaInformeAnatomia).withValidator(returnDate -> {
				if (returnDate == null)
					return true;
				else
					return !returnDate.isBefore(fechaCitaColonoscopia.getValue());
			}, "Mayor o igual que fecha informe colonoscopia ").bind(RegistroColon::getInformeAnatoDate,
					RegistroColon::setInformeAnato);

			idInformeAnatomia.setCaption("Id Inf Anatom ");
			idInformeAnatomia.setPlaceholder(" id inf anato ");
			idInformeAnatomia.setWidth("100px");
			idInformeAnatomia.setMaxLength(10);
			idInformeAnatomia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
			binder.forField(idInformeAnatomia).bind(RegistroColon::getIdAnatoValor, RegistroColon::setIdAnato);

			observaciones.setCaption("Observaciones");
			observaciones.setPlaceholder(" observaciones al registro de colon ");
			observaciones.setWidth("400px");
			observaciones.setHeight("100px");
			observaciones.setMaxLength(400);
			observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
			binder.forField(observaciones).bind(RegistroColon::getObservacionesValor, RegistroColon::setObservaciones);

			binder.readBean(registroColon);
			doActivaBotones(registroColon.getId());

			fila1.addComponents(fechaAnalitica, resultado, informe);
			fila2.addComponents(fechaCitaColonoscopia, fechaInformeColonoscopia, idInformeColonoscopia);
			fila3.addComponents(fechaInformeAnatomia, idInformeAnatomia);
			fila4.addComponents(observaciones);
			// this.addComponents(contenedorBotones, fila1, fila2, fila3, fila4);
			doActivaBotones();

		}
	}

	public void clickVerInformePdf() {
		new VentanaVerPdf(this.getUI(), registroColon.getProblema().getIdInformeAsociado(),
				VentanaVerPdf.TIPO_VERPDF_INFORME);
	}

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();

	}

	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	@Override
	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroColon);
				if (!new RegistroDAO().grabaDatos(registroColon)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();

			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			} finally {

			}
		}
	}

	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroColon)) {
			registroColon = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;

		/*
		 * if (fechaAnalitica.isEmpty()) { fechaAnalitica.setComponentError(null);
		 * valido = false; fechaAnalitica .setComponentError(new
		 * UserError(Mensajes.FORMULARIOCAMPOREQUERIDO + fechaAnalitica.getCaption()));
		 * } else { fechaAnalitica.setComponentError(null); } if
		 * (!fechaCitaColonoscopia.isEmpty() && !fechaAnalitica.isEmpty()) { if
		 * (!fechaCitaColonoscopia.getValue().isAfter(fechaAnalitica.getValue())) {
		 * valido = false; fechaCitaColonoscopia .setComponentError(new
		 * UserError(Mensajes.FORMULARIOFECHAMENOR + fechaAnalitica.getValue())); } }
		 * else { fechaCitaColonoscopia.setComponentError(null); } if
		 * (!fechaInformeColonoscopia.isEmpty() && !fechaCitaColonoscopia.isEmpty()) {
		 * if
		 * (!fechaInformeColonoscopia.getValue().isAfter(fechaCitaColonoscopia.getValue(
		 * ))) { valido = false; fechaCitaColonoscopia.setComponentError( new
		 * UserError(Mensajes.FORMULARIOFECHAMENOR + fechaCitaColonoscopia.getValue()));
		 * } } else { fechaInformeColonoscopia.setComponentError(null); } if
		 * (!fechaInformeAnatomia.isEmpty() && !fechaInformeColonoscopia.isEmpty()) { if
		 * (!fechaInformeAnatomia.getValue().isAfter(fechaInformeColonoscopia.getValue()
		 * )) { valido = false; fechaInformeAnatomia.setComponentError( new
		 * UserError(Mensajes.FORMULARIOFECHAMENOR +
		 * fechaInformeColonoscopia.getValue())); } } else {
		 * fechaInformeAnatomia.setComponentError(null); }
		 */
		return valido;
	}

	@Override
	public void doActivaBotones(Long id) {
		if (id.compareTo(new Long(0)) > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}
		if (resultado.getValue().isEmpty()) {
			resultado.focus();
		}
		// comprueba que tenga fecha de tratamiento
		if (registroColon.getInformeAnatoDate() != null) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroColon.getInformeAnatoDate(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				fila1.setEnabled(false);
				fila2.setEnabled(false);
				fila3.setEnabled(false);
				fila4.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
		// comprueba que el procesos asociado no esté cerrado
		if (registroColon.getProblema().getFechafin() != null) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroColon.getProblema().getFechafin(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				fila1.setEnabled(false);
				fila2.setEnabled(false);
				fila3.setEnabled(false);
				fila4.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);

			}
		}

	}

	public String getAyudaHtml() {
		String textoHtml = " <b>Pantalla de mantenimiento de casos de colon  :\n  </b> <br>" + "<ul> "
				+ "<li><b>Fecha Analítica:</b>  Fecha de resultados de TSOH del laboratorio.</li>"
				+ "<li><b>Resultado:</b> Valo numérico de la prueba TSOH.  </li>"
				+ "<li><b>Cita colonoscopia:</b> Fecha de cita para realización de la colonoscopia.  </li>"
				+ "<li><b>Informe colonoscopia:</b> Fecha del informe de la colonosocopia.  </li>"
				+ "<li><b>Id Informe colonoscopia:</b> Identificador  del informe de la colonosocopia.  </li>"
				+ "<li><b>Fecha Informe de anatomía:</b> Fecha del informe de anatomía.</li>"
				+ "<li><b>Id  Informe de anatomía:</b> Identificador del informe de anatomía.   </li>" + "</ul> "
				+ " <hr>" + Ayudas.AYUDA_BOTONES_ABCA;
		return textoHtml;
	}
}
