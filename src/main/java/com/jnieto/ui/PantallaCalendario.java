package com.jnieto.ui;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Servicio;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class PantallaCalendario extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4151253609116909292L;
	private LocalDate fecha;
	private int mes;
	private DayOfWeek diaSemana;
	private int ano;
	private int dia;

	private Centro centro;
	private Servicio servicio;
	private Agenda agenda;

	public PantallaCalendario(LocalDate feDate, Centro centro, Servicio servicio, Agenda agenda) {
		this.centro = centro;
		this.servicio = servicio;
		this.agenda = agenda;
		mes = feDate.getMonth().getValue();
		ano = feDate.getYear();
		dia = 1;

		String str = ano + "-";
		if (mes < 10)
			str = str.concat("0" + mes + "-01");
		else
			str = str.concat(mes + "-01");

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		fecha = LocalDate.parse(str, formatter);
		diaSemana = fecha.getDayOfWeek();
		doCalendario();
	}

	public void doCalendario() {
		for (int contadorMes = 0; contadorMes < 3; contadorMes++) {
			mes = fecha.getMonthValue();
			ano = fecha.getYear();
			dia = 1;
			this.addComponent(getCabecera(mes, ano));
			while (mes == fecha.getMonth().getValue()) {
				this.addComponent(getFilaSemana(diaSemana.getValue(), mes, ano));
				diaSemana = fecha.getDayOfWeek();
			}
		}

	}

	public HorizontalLayout getCabecera(int mes, int ano) {
		HorizontalLayout fila = new HorizontalLayout();
		String cadena = "<b>Mes y año:</b>" + mes + "/" + ano;
		if (this.servicio != null)
			cadena = cadena.concat("   <b>Servicio:</b>" + this.servicio.getCodigo());

		if (this.agenda != null)
			cadena = cadena.concat("<b> Agenda:</b>" + this.agenda.getDescripcion());
		else
			cadena = cadena.concat(" Todas las agendas");

		Label texto = new Label(cadena);
		texto.setCaptionAsHtml(true);
		texto.setContentMode(ContentMode.HTML);
		texto.addStyleName(ValoTheme.LABEL_H4);
		fila.addComponent(texto);
		return fila;
	}

	public HorizontalLayout getFilaSemana(int diaSemanaInicial, int mes, int ano) {
		HorizontalLayout fila = new HorizontalLayout();
		fila.setMargin(false);
		fila.setSpacing(true);
		fila.addStyleName(MaterialTheme.LAYOUT_WELL);
		int i = 0;
		if (diaSemanaInicial > 1) {
			do {
				fila.addComponent(getCajadia(0, i));
				i++;
			} while (i < (diaSemanaInicial - 1));
		}
		while (i < 7) {
			if (this.getDia() <= getDiasDelMes(mes, ano)) {
				fila.addComponent(getCajadia(dia, fecha.getDayOfWeek().ordinal()));
				this.setDia(this.getDia() + 1);
				this.setFecha(this.getFecha().plusDays(1));
			} else {
				fila.addComponent(getCajadia(0, i));
			}
			i++;
		}
		return fila;
	}

	public VerticalLayout getCajadia(int dia, int columna) {
		VerticalLayout cajaDia = new VerticalLayout();
		cajaDia.setMargin(false);
		cajaDia.setSpacing(true);
		cajaDia.addStyleName(MaterialTheme.LAYOUT_WELL);
		cajaDia.setWidth(14, Unit.PERCENTAGE);
		cajaDia.setWidth("170px");
		// jaDia.setComponentAlignment(Alinment.TOP_CENTER);
		Label nombreDia = new Label();
		// nombreDia.addStyleName(ValoTheme.LABEL_H4);
		Label numeroDia = new Label();
		numeroDia.setContentMode(ContentMode.HTML);
		// numeroDia.addStyleName(ValoTheme.LABEL_LARGE);
		Label textoDia = new Label();
		textoDia.setCaptionAsHtml(true);
		textoDia.setContentMode(ContentMode.HTML);
		if (dia == 0) {
			nombreDia.setValue(getNombreDia(columna));
			numeroDia.setValue("");
		} else {
			nombreDia.setValue(getNombreDia(columna));
			numeroDia.setValue("<div id=\"dia\" style=\"text-align:center\"><b>" + Integer.toString(dia) + "</b></div");
			textoDia.setValue(contenidoDia(fecha));
		}
		cajaDia.addComponents(nombreDia, numeroDia, textoDia);
		return cajaDia;
	}

	public PantallaCalendario(Component... children) {
		super(children);
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public DayOfWeek getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(DayOfWeek diaSemana) {
		this.diaSemana = diaSemana;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}

	public String getNombreDia(int dias) {
		String nombreDia = "";
		switch (dias) {
		case 0:
			nombreDia = "Lunes";
			break;
		case 1:
			nombreDia = "Martes";
			break;
		case 2:
			nombreDia = "Miercoles";
			break;
		case 3:
			nombreDia = "Jueves";
			break;
		case 4:
			nombreDia = "Viernes";
			break;
		case 5:
			nombreDia = "Sabado";
			break;
		case 6:
			nombreDia = "Domingo";
			break;
		default:
			nombreDia = "??";
			break;
		}
		return nombreDia;
	}

	public int getDiasDelMes(int mes, int ano) {
		int numeroDias = 0;
		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
			numeroDias = 31;
		else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
			numeroDias = 30;
		else if (mes == 2) {
			if ((ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0)))
				numeroDias = 29;
			else
				numeroDias = 28;
		}
		return numeroDias;
	}

	public String contenidoDia(LocalDate fecha) {
		String texto = "";
		ArrayList<Episodio> listaCitados = new ArrayList<Episodio>();

		listaCitados = new EpisodioDAO().getListaEpisodiosCitas(Episodio.CLASE_CONSULTAS.getId(), centro, servicio,
				null, null, null, fecha);
		for (Episodio epi : listaCitados) {
			String descripcion = epi.getPrestacion().getDescripcion();
			if (descripcion.length() > 18)
				descripcion = descripcion.substring(0, 17);

			texto = texto.concat("<b>" + descripcion + "</b>: " + epi.getCitasDadas() + "<br>");
		}
		return texto;
	}
}
