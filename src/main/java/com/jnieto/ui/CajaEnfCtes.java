package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.RegistroEnfCtesDAO;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroEnfConstantes;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaEnfCtes extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4350799388886661499L;

	private RegistroEnfConstantes registro;
	private ArrayList<RegistroEnfConstantes> listaRegistros = new ArrayList<>();
	private Grid<RegistroEnfConstantes> grid = new Grid<RegistroEnfConstantes>();

	public CajaEnfCtes(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		lbltitulo.setCaption("Constantes");
		contenedorGrid.addComponent(grid);
		doGrid();
	}

	public CajaEnfCtes(Paciente paciente, Episodio episodio) {
		super(paciente, episodio);
		lbltitulo.setCaption("Constantes");
		contenedorGrid.addComponent(grid);
		doGrid();
	}

	public void doGrid() {
		listaRegistros = new RegistroEnfCtesDAO().getListaRegistros(desde, hasta, paciente);
		grid.setSizeUndefined();
		grid.setHeightByRows(5);
		grid.addColumn(RegistroEnfConstantes::getFechaHora).setCaption("Fecha");
		grid.addColumn(RegistroEnfConstantes::getPesoString).setCaption("Peso");
		grid.addColumn(RegistroEnfConstantes::getTallaSrting).setCaption("Talla");
		grid.addColumn(RegistroEnfConstantes::getTasString).setCaption("Tas");
		grid.addColumn(RegistroEnfConstantes::getTadString).setCaption("Tad");
		grid.addColumn(RegistroEnfConstantes::getTemperaturaString).setCaption("Tª");
		grid.addColumn(RegistroEnfConstantes::getFreCardiacaString).setCaption("Fc");
		grid.addColumn(RegistroEnfConstantes::getFreRespiratorioString).setCaption("Fr");
		grid.setItems(listaRegistros);
		grid.addSelectionListener(event -> seleccionaClick());
		grid.setWidth("650px");

	}

	public void seleccionaClick() {
		if (grid.getSelectedItems().size() > 0) {
			registro = grid.getSelectedItems().iterator().next();
			FrmRegistroEnfConstantes frc = new FrmRegistroEnfConstantes(registro);
			VentanaFrm vFrm = new VentanaFrm(this.getUI(), frc, registro.getDescripcion());
			vFrm.addCloseListener(e -> cerrarVentana());
		}
	}

	public void cerrarVentana() {
		refrescarClick();
	}

	@Override
	public void nuevoClick() {
		registro = new RegistroEnfConstantes();
		registro.setPaciente(paciente);
		registro.setEpisodio(paciente.getEpisodioActual());
		registro.setCentro(paciente.getEpisodioActual().getCentro());
		registro.setServicio(paciente.getEpisodioActual().getServicio());
		registro.setCanal(paciente.getEpisodioActual().getCanal());
		FrmRegistroEnfConstantes frc = new FrmRegistroEnfConstantes(registro);
		new VentanaFrm(this.getUI(), frc, registro.getDescripcion());
	}

	@Override
	public void refrescarClick() {
		listaRegistros = new RegistroEnfCtesDAO().getListaRegistros(desde, hasta, paciente);
		grid.setItems(listaRegistros);
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void editarClick() {
		// TODO Auto-generated method stub

	}

}
