package com.jnieto.ui.quirofano;

import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.RegistroQuiSegEntrada;
import com.jnieto.entity.RegistroQuiSegPausa;
import com.jnieto.entity.RegistroQuiSegPre;
import com.jnieto.entity.RegistroQuiSegSalida;
import com.jnieto.entity.Zona;
import com.jnieto.ui.CajaEpisodio;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.PanelPaciente;
import com.jnieto.ui.master.CajaMasterRegistro;
import com.jnieto.ui.master.ConsolaMaster;

public class ConsolaQuirofano extends ConsolaMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6401582774382670710L;

	private PanelPaciente cajaPaciente;
	private CajaEpisodio cajaEpisodio;
	private CajaMasterRegistro cajaCheckPrequi;
	private CajaMasterRegistro cajaCheckEntrada;
	private CajaMasterRegistro cajaCheckPausa;
	private CajaMasterRegistro cajaCheckSalida;

	public static String NOMBREPACKAGE = "com.jnieto.ui.quirofano.";

	public ConsolaQuirofano(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
		if (paciente == null) {
			new NotificacionInfo("Sin elegir paciente");
			return;
		}
		zona = Zona.ZONA_QUIROHNSS;
		cajaPaciente = new PanelPaciente(paciente);
		cajaEpisodio = new CajaEpisodio(paciente, Episodio.CLASE_HDIA, zona);
		fila1.addComponent(cajaPaciente);
		fila2.addComponent(cajaEpisodio);
		episodio = cajaEpisodio.getEpisodio();
		if (episodio != null) {
			RegistroQuiSegPre registroQuiSegPre = (RegistroQuiSegPre) new RegistroDAO().getRegistroPacienteEpisodio(
					episodio.getPaciente(), episodio, RegistroQuiSegPre.PLANTILLLA_EDITOR_QUISEGPRE);
			if (registroQuiSegPre == null) {
				registroQuiSegPre = new RegistroQuiSegPre();
				registroQuiSegPre.setServicio(episodio.getServicio());
				registroQuiSegPre.setCentro(episodio.getCentro());
				registroQuiSegPre.setPaciente(paciente);
				registroQuiSegPre.setEpisodio(episodio);
			}
			cajaCheckPrequi = new CajaMasterRegistro(registroQuiSegPre, "FrmRegistroQuiSegPre", "RegistroQuiSegPre",
					NOMBREPACKAGE + "FrmRegistroQuiSegPre");
			fila1.addComponent(cajaCheckPrequi);
//--
			RegistroQuiSegEntrada registroQuiSegEntrada = (RegistroQuiSegEntrada) new RegistroDAO()
					.getRegistroPacienteEpisodio(episodio.getPaciente(), episodio,
							RegistroQuiSegEntrada.PLANTILLLA_EDITOR_QUISEGENTRA);
			if (registroQuiSegEntrada == null) {
				registroQuiSegEntrada = new RegistroQuiSegEntrada();
				registroQuiSegEntrada.setServicio(episodio.getServicio());
				registroQuiSegEntrada.setCentro(episodio.getCentro());
				registroQuiSegEntrada.setPaciente(paciente);
				registroQuiSegEntrada.setEpisodio(episodio);
			}
			cajaCheckEntrada = new CajaMasterRegistro(registroQuiSegEntrada, "FrmRegistroQuiSegEntrada",
					"RegistroQuiSegEntrada", NOMBREPACKAGE + "FrmRegistroQuiSegEntrada");
			fila1.addComponent(cajaCheckEntrada);

			RegistroQuiSegPausa registroQuiSegPausa = (RegistroQuiSegPausa) new RegistroDAO()
					.getRegistroPacienteEpisodio(episodio.getPaciente(), episodio,
							RegistroQuiSegPausa.PLANTILLLA_EDITOR_QUISEGPAUSA);
			if (registroQuiSegPausa == null) {
				registroQuiSegPausa = new RegistroQuiSegPausa();
				registroQuiSegPausa.setServicio(episodio.getServicio());
				registroQuiSegPausa.setCentro(episodio.getCentro());
				registroQuiSegPausa.setPaciente(paciente);
				registroQuiSegPausa.setEpisodio(episodio);
			}
			cajaCheckPausa = new CajaMasterRegistro(registroQuiSegPausa, "FrmRegistroQuiSegPausa",
					"RegistroQuiSegPausa", NOMBREPACKAGE + "FrmRegistroQuiSegPausa");
			fila2.addComponent(cajaCheckPausa);

			RegistroQuiSegSalida registroQuiSegSalida = (RegistroQuiSegSalida) new RegistroDAO()
					.getRegistroPacienteEpisodio(episodio.getPaciente(), episodio,
							RegistroQuiSegSalida.PLANTILLLA_EDITOR_QUISEGSALIDA);
			if (registroQuiSegSalida == null) {
				registroQuiSegSalida = new RegistroQuiSegSalida();
				registroQuiSegSalida.setServicio(episodio.getServicio());
				registroQuiSegSalida.setCentro(episodio.getCentro());
				registroQuiSegSalida.setPaciente(paciente);
				registroQuiSegSalida.setEpisodio(episodio);
			}
			cajaCheckSalida = new CajaMasterRegistro(registroQuiSegSalida, "FrmRegistroQuiSegSalida",
					"RegistroQuiSegSalida", NOMBREPACKAGE + "FrmRegistroQuiSegSalida");
			fila2.addComponent(cajaCheckSalida);
		} else {

		}

	}

	@Override
	public void refrescar() {
		// TODO Auto-generated method stub

	}

}
