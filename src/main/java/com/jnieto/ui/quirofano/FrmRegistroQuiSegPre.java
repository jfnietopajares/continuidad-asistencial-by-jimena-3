package com.jnieto.ui.quirofano;

import java.time.LocalDate;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroQuiSegPre;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;

public class FrmRegistroQuiSegPre extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3257517034600111824L;

	private DateField fechaCheck;
	private TextField horaCheck;
	private TextField auxiliar = new TextField("Auxiliar");
	private CheckBox aspirador = new CheckBox("Aspirador");
	private CheckBox bolsapresion = new CheckBox("Bolsa de presión");
	private CheckBox aparatosred = new CheckBox("Aparatos en red");
	private CheckBox intubacion = new CheckBox("Intubación");
	private CheckBox medicacion = new CheckBox("Medicación");

	private Binder<RegistroQuiSegPre> binder = new Binder<RegistroQuiSegPre>();
	private RegistroQuiSegPre registroQuiSegPre;

	public FrmRegistroQuiSegPre(Registro registroCheck) {
		registroQuiSegPre = (RegistroQuiSegPre) registroCheck;

		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7);

		fechaCheck = new ObjetosComunes().getFecha("Fecha", "fecha");
		fechaCheck.setValue(LocalDate.now());
		binder.forField(fechaCheck).bind(RegistroQuiSegPre::getFechaCheckDate, RegistroQuiSegPre::setFechaCheck);

		horaCheck = new ObjetosComunes().getHora("Hora", "");
		binder.forField(horaCheck).bind(RegistroQuiSegPre::getHoraCheckString, RegistroQuiSegPre::setHoraCheck);

		fila1.addComponents(fechaCheck, horaCheck);

		auxiliar.setWidth("200px");
		auxiliar.addStyleNames(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(auxiliar).bind(RegistroQuiSegPre::getAuxiliarString, RegistroQuiSegPre::setAuxiliar);
		fila2.addComponent(auxiliar);

		aspirador.setCaption(registroQuiSegPre.getAspirador().getDescripcion());
		binder.forField(aspirador).bind(RegistroQuiSegPre::getAspiradorBoolean, RegistroQuiSegPre::setAspirador);
		fila3.addComponent(aspirador);

		bolsapresion.setCaption(registroQuiSegPre.getBolsapresion().getDescripcion());
		binder.forField(bolsapresion).bind(RegistroQuiSegPre::getBolsapresionBoolean,
				RegistroQuiSegPre::setBolsapresion);
		fila4.addComponent(bolsapresion);

		aparatosred.setCaption(registroQuiSegPre.getAparatosred().getDescripcion());
		aparatosred.setDescription(registroQuiSegPre.getAparatosred().getTextoAyuda());
		binder.forField(aparatosred).bind(RegistroQuiSegPre::getAparatosredBoolean, RegistroQuiSegPre::setAparatosred);
		fila5.addComponent(aparatosred);

		intubacion.setCaption(registroQuiSegPre.getIntubacion().getDescripcion());
		intubacion.setDescription(registroQuiSegPre.getIntubacion().getTextoAyuda());
		binder.forField(intubacion).bind(RegistroQuiSegPre::getIntubacionBoolean, RegistroQuiSegPre::setIntubacion);
		fila6.addComponent(intubacion);

		medicacion.setCaption(registroQuiSegPre.getMedicacion().getDescripcion());
		medicacion.setDescription(registroQuiSegPre.getMedicacion().getTextoAyuda());
		binder.forField(medicacion).bind(RegistroQuiSegPre::getMedicacionBoolean, RegistroQuiSegPre::setMedicacion);
		fila7.addComponent(medicacion);
		binder.readBean(registroQuiSegPre);
		doActivaBotones(registroQuiSegPre);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroQuiSegPre);
			if (!new RegistroDAO().grabaDatos(registroQuiSegPre)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registro != null) {
			super.doActivaBotones(registro);
		}

	}
}
