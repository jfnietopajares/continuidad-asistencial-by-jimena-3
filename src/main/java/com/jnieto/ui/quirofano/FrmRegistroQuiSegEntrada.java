package com.jnieto.ui.quirofano;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroQuiSegEntrada;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;

public class FrmRegistroQuiSegEntrada extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8298827128229673199L;

	private CheckBox identidad = new CheckBox();
	private CheckBox procedimiento = new CheckBox();
	private CheckBox localizacion = new CheckBox();
	private CheckBox localizacionNoproce = new CheckBox();
	private CheckBox localizacionOkcorr = new CheckBox();
	private CheckBox alergia = new CheckBox();
	private CheckBox alergiaOkcorre = new CheckBox();

	private CheckBox antibioti60min = new CheckBox();
	private CheckBox antibiotiNoporce = new CheckBox();
	private CheckBox antibiotiOkcorre = new CheckBox();

	private CheckBox tep_tvp = new CheckBox();
	private CheckBox tep_tvpNoproce = new CheckBox();
	private CheckBox tep_tvpOkcorre = new CheckBox();

	private CheckBox implante = new CheckBox();
	private CheckBox implanteNoproce = new CheckBox();
	private CheckBox implanteOkcorre = new CheckBox();

	private CheckBox intuDificil = new CheckBox();
	private CheckBox intuDificilNoproce = new CheckBox();
	private CheckBox pulsioxi = new CheckBox();

	private CheckBox riesgoAspira = new CheckBox();
	private CheckBox hemoderiv = new CheckBox();
	private CheckBox hemoderivNoproce = new CheckBox();
	private CheckBox hemoderivOkcorre = new CheckBox();

	private TextField observaciones = new TextField("Observaciones");

	private RegistroQuiSegEntrada registroEntrada = new RegistroQuiSegEntrada();
	private Binder<RegistroQuiSegEntrada> binder = new Binder<RegistroQuiSegEntrada>();

	public FrmRegistroQuiSegEntrada(Registro registroParam) {
		this.registroEntrada = (RegistroQuiSegEntrada) registroParam;
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7, fila8, fila9);

		identidad.setCaption(registroEntrada.getIdentidad().getDescripcion());
		identidad.setDescription(registroEntrada.getIdentidad().getTextoAyuda());
		binder.forField(identidad).bind(RegistroQuiSegEntrada::getIdentidadBoolean,
				RegistroQuiSegEntrada::setIdentidad);

		procedimiento.setCaption(registroEntrada.getProcedimiento().getDescripcion());
		procedimiento.setDescription(registroEntrada.getProcedimiento().getTextoAyuda());
		binder.forField(procedimiento).bind(RegistroQuiSegEntrada::getProcedimientoBoolean,
				RegistroQuiSegEntrada::setProcedimiento);
		fila1.addComponents(identidad, procedimiento);

		localizacion.setCaption(registroEntrada.getLocalizacion().getDescripcion());
		localizacion.setDescription(registroEntrada.getLocalizacion().getTextoAyuda());
		binder.forField(localizacion).bind(RegistroQuiSegEntrada::getLocalizacionBoolean,
				RegistroQuiSegEntrada::setLocalizacion);

		localizacionNoproce.setCaption(registroEntrada.getLocalizacionNoproce().getDescripcion());
		localizacionNoproce.setDescription(registroEntrada.getLocalizacionNoproce().getTextoAyuda());
		binder.forField(localizacionNoproce).bind(RegistroQuiSegEntrada::getLocalizacionNoproceBoolean,
				RegistroQuiSegEntrada::setLocalizacionNoproce);

		localizacionOkcorr.setCaption(registroEntrada.getLocalizacionOkcorr().getDescripcion());
		localizacionOkcorr.setDescription(registroEntrada.getLocalizacionOkcorr().getTextoAyuda());
		binder.forField(localizacionOkcorr).bind(RegistroQuiSegEntrada::getLocalizacionOkcorrBoolean,
				RegistroQuiSegEntrada::setLocalizacionOkcorr);
		fila2.addComponents(localizacion, localizacionNoproce, localizacionOkcorr);

		alergia.setCaption(registroEntrada.getAlergia().getDescripcion());
		alergia.setDescription(registroEntrada.getAlergia().getTextoAyuda());
		binder.forField(alergia).bind(RegistroQuiSegEntrada::getAlergiaBoolean, RegistroQuiSegEntrada::setAlergia);

		alergiaOkcorre.setCaption(registroEntrada.getAlergiaOkcorre().getDescripcion());
		alergiaOkcorre.setDescription(registroEntrada.getAlergiaOkcorre().getTextoAyuda());
		binder.forField(alergiaOkcorre).bind(RegistroQuiSegEntrada::getAlergiaOkcorreBoolean,
				RegistroQuiSegEntrada::setAlergiaOkcorre);
		fila3.addComponents(alergia, alergiaOkcorre);

		antibioti60min.setCaption(registroEntrada.getAntibioti60min().getDescripcion());
		antibioti60min.setDescription(registroEntrada.getAntibioti60min().getTextoAyuda());
		binder.forField(antibioti60min).bind(RegistroQuiSegEntrada::getAntibioti60minBoolean,
				RegistroQuiSegEntrada::setAntibioti60min);

		antibiotiNoporce.setCaption(registroEntrada.getAntibiotiNoporce().getDescripcion());
		antibiotiNoporce.setDescription(registroEntrada.getAntibiotiNoporce().getTextoAyuda());
		binder.forField(antibiotiNoporce).bind(RegistroQuiSegEntrada::getAntibiotiNoporceBoolean,
				RegistroQuiSegEntrada::setAntibiotiNoporce);

		antibiotiOkcorre.setCaption(registroEntrada.getAntibiotiOkcorre().getDescripcion());
		antibiotiOkcorre.setDescription(registroEntrada.getAntibiotiOkcorre().getTextoAyuda());
		binder.forField(antibiotiOkcorre).bind(RegistroQuiSegEntrada::getAntibiotiOkcorreBoolean,
				RegistroQuiSegEntrada::setAntibiotiOkcorre);

		fila4.addComponents(antibioti60min, antibiotiNoporce, antibiotiOkcorre);

		tep_tvp.setCaption(registroEntrada.getTep_tvp().getDescripcion());
		tep_tvp.setDescription(registroEntrada.getTep_tvp().getTextoAyuda());
		binder.forField(tep_tvp).bind(RegistroQuiSegEntrada::getTep_tvpBoolean, RegistroQuiSegEntrada::setTep_tvp);

		tep_tvpNoproce.setCaption(registroEntrada.getTep_tvp().getDescripcion());
		tep_tvpNoproce.setDescription(registroEntrada.getTep_tvp().getTextoAyuda());
		binder.forField(tep_tvpNoproce).bind(RegistroQuiSegEntrada::getTep_tvpNoproceBoolean,
				RegistroQuiSegEntrada::setTep_tvpNoproce);

		tep_tvpOkcorre.setCaption(registroEntrada.getTep_tvpOkcorre().getDescripcion());
		tep_tvpOkcorre.setDescription(registroEntrada.getTep_tvpOkcorre().getTextoAyuda());
		binder.forField(tep_tvpOkcorre).bind(RegistroQuiSegEntrada::getTep_tvpOkcorreBoolean,
				RegistroQuiSegEntrada::setTep_tvpOkcorre);

		fila5.addComponents(tep_tvp, tep_tvpNoproce, tep_tvpOkcorre);

		implante.setCaption(registroEntrada.getImplante().getDescripcion());
		implante.setDescription(registroEntrada.getImplante().getTextoAyuda());
		binder.forField(implante).bind(RegistroQuiSegEntrada::getImplanteBoolean, RegistroQuiSegEntrada::setImplante);

		implanteNoproce.setCaption(registroEntrada.getImplanteNoproce().getDescripcion());
		implanteNoproce.setDescription(registroEntrada.getImplanteNoproce().getTextoAyuda());
		binder.forField(implanteNoproce).bind(RegistroQuiSegEntrada::getImplanteNoproceBoolean,
				RegistroQuiSegEntrada::setImplanteNoproce);

		implanteOkcorre.setCaption(registroEntrada.getImplanteOkcorre().getDescripcion());
		implanteOkcorre.setDescription(registroEntrada.getImplanteOkcorre().getTextoAyuda());
		binder.forField(implanteOkcorre).bind(RegistroQuiSegEntrada::getImplanteOkcorreBoolean,
				RegistroQuiSegEntrada::setImplanteOkcorre);
		fila6.addComponents(implante, implanteNoproce, implanteOkcorre);

		intuDificil.setCaption(registroEntrada.getIntuDificil().getDescripcion());
		intuDificil.setDescription(registroEntrada.getIntuDificil().getTextoAyuda());
		binder.forField(intuDificil).bind(RegistroQuiSegEntrada::getIntuDificilBoolean,
				RegistroQuiSegEntrada::setIntuDificil);

		intuDificilNoproce.setCaption(registroEntrada.getIntuDificilNoproce().getDescripcion());
		intuDificilNoproce.setDescription(registroEntrada.getIntuDificilNoproce().getTextoAyuda());
		binder.forField(intuDificilNoproce).bind(RegistroQuiSegEntrada::getIntuDificilNoproceBoolean,
				RegistroQuiSegEntrada::setIntuDificilNoproce);

		pulsioxi.setCaption(registroEntrada.getPulsioxi().getDescripcion());
		pulsioxi.setDescription(registroEntrada.getPulsioxi().getTextoAyuda());
		binder.forField(pulsioxi).bind(RegistroQuiSegEntrada::getPulsioxiBollean, RegistroQuiSegEntrada::setPulsioxi);

		fila7.addComponents(intuDificil, intuDificilNoproce, pulsioxi);

		riesgoAspira.setCaption(registroEntrada.getRiesgoAspira().getDescripcion());
		riesgoAspira.setDescription(registroEntrada.getRiesgoAspira().getTextoAyuda());
		binder.forField(riesgoAspira).bind(RegistroQuiSegEntrada::getRiesgoAspiraBoolean,
				RegistroQuiSegEntrada::setRiesgoAspira);

		hemoderiv.setCaption(registroEntrada.getHemoderiv().getDescripcion());
		hemoderiv.setDescription(registroEntrada.getHemoderiv().getTextoAyuda());
		binder.forField(hemoderiv).bind(RegistroQuiSegEntrada::getHemoderivBoolean,
				RegistroQuiSegEntrada::setHemoderiv);

		hemoderivNoproce.setCaption(registroEntrada.getHemoderivNoproce().getDescripcion());
		hemoderivNoproce.setDescription(registroEntrada.getHemoderivNoproce().getTextoAyuda());
		binder.forField(hemoderivNoproce).bind(RegistroQuiSegEntrada::getHemoderivNoproceBoolean,
				RegistroQuiSegEntrada::setHemoderivNoproce);

		hemoderivOkcorre.setCaption(registroEntrada.getHemoderivOkcorre().getDescripcion());
		hemoderivOkcorre.setDescription(registroEntrada.getHemoderivOkcorre().getTextoAyuda());
		binder.forField(hemoderivOkcorre).bind(RegistroQuiSegEntrada::getHemoderivOkcorreBoolean,
				RegistroQuiSegEntrada::setHemoderivOkcorre);

		fila8.addComponents(riesgoAspira, hemoderiv, hemoderivNoproce, hemoderivOkcorre);

		observaciones.setWidth("400px");
		observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(RegistroQuiSegEntrada::getObservacionesStgring,
				RegistroQuiSegEntrada::setObservaciones);
		fila9.addComponents(observaciones);

		binder.readBean(registroEntrada);
		doActivaBotones(registroEntrada);

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroEntrada);
			if (!new RegistroDAO().grabaDatos(registroEntrada)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
