package com.jnieto.ui.quirofano;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroQuiSegPausa;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextArea;

public class FrmRegistroQuiSegPausa extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3588874732183199973L;

	private CheckBox procePosicion = new CheckBox();
	private CheckBox pasosCriticos = new CheckBox();
	private CheckBox imagenes = new CheckBox();
	private CheckBox viaAerea = new CheckBox();
	private CheckBox pasosCriticosAnes = new CheckBox();
	private CheckBox glucema = new CheckBox();
	private CheckBox glucemaNoProce = new CheckBox();
	private CheckBox hiptermia = new CheckBox();
	private CheckBox hiptermiaNoProce = new CheckBox();
	private CheckBox instrumental = new CheckBox();
	private CheckBox instrumentalOkcorre = new CheckBox();
	private CheckBox equipos = new CheckBox();
	private CheckBox equiposOkCoree = new CheckBox();
	private TextArea observaciones = new TextArea("Observaciones.");

	private Binder<RegistroQuiSegPausa> binder = new Binder<RegistroQuiSegPausa>();
	private RegistroQuiSegPausa registroQuiSegPausa;

	public FrmRegistroQuiSegPausa(Registro registroParam) {
		registroQuiSegPausa = (RegistroQuiSegPausa) registroParam;

		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7, fila8);

		procePosicion.setCaption(registroQuiSegPausa.getProcePosicion().getDescripcion());
		procePosicion.setDescription(registroQuiSegPausa.getProcePosicion().getTextoAyuda());
		binder.forField(procePosicion).bind(RegistroQuiSegPausa::getProcePosicionBoolean,
				RegistroQuiSegPausa::setProcePosicion);
		fila1.addComponent(procePosicion);

		pasosCriticos.setCaption(registroQuiSegPausa.getPasosCriticos().getDescripcion());
		pasosCriticos.setDescription(registroQuiSegPausa.getPasosCriticos().getTextoAyuda());
		binder.forField(pasosCriticos).bind(RegistroQuiSegPausa::getPasosCriticosBoolean,
				RegistroQuiSegPausa::setPasosCriticos);
		fila1.addComponent(pasosCriticos);

		imagenes.setCaption(registroQuiSegPausa.getImagenes().getDescripcion());
		imagenes.setDescription(registroQuiSegPausa.getImagenes().getTextoAyuda());
		binder.forField(imagenes).bind(RegistroQuiSegPausa::getImagenesBoolean, RegistroQuiSegPausa::setImagenes);
		fila2.addComponent(imagenes);

		viaAerea.setCaption(registroQuiSegPausa.getViaAerea().getDescripcion());
		viaAerea.setDescription(registroQuiSegPausa.getViaAerea().getTextoAyuda());
		binder.forField(viaAerea).bind(RegistroQuiSegPausa::getViaAereaBoolean, RegistroQuiSegPausa::setViaAerea);
		fila2.addComponent(viaAerea);

		pasosCriticosAnes.setCaption(registroQuiSegPausa.getPasosCriticosAnes().getDescripcion());
		pasosCriticosAnes.setDescription(registroQuiSegPausa.getPasosCriticosAnes().getTextoAyuda());
		binder.forField(pasosCriticosAnes).bind(RegistroQuiSegPausa::getPasosCriticosAnesBoolean,
				RegistroQuiSegPausa::setPasosCriticosAnes);
		fila3.addComponent(pasosCriticosAnes);

		glucema.setCaption(registroQuiSegPausa.getGlucema().getDescripcion());
		glucema.setDescription(registroQuiSegPausa.getGlucema().getTextoAyuda());
		binder.forField(glucema).bind(RegistroQuiSegPausa::getGlucemaNoProceBoolean,
				RegistroQuiSegPausa::setGlucemaNoProce);
		fila4.addComponent(glucema);

		glucemaNoProce.setCaption(registroQuiSegPausa.getGlucemaNoProce().getDescripcion());
		glucemaNoProce.setDescription(registroQuiSegPausa.getGlucemaNoProce().getTextoAyuda());
		binder.forField(glucemaNoProce).bind(RegistroQuiSegPausa::getGlucemaNoProceBoolean,
				RegistroQuiSegPausa::setGlucemaNoProce);
		fila4.addComponent(glucemaNoProce);

		hiptermia.setCaption(registroQuiSegPausa.getHiptermia().getDescripcion());
		hiptermia.setDescription(registroQuiSegPausa.getHiptermia().getTextoAyuda());
		binder.forField(hiptermia).bind(RegistroQuiSegPausa::getHiptermiaBoolean, RegistroQuiSegPausa::setHiptermia);
		fila5.addComponent(hiptermia);

		hiptermiaNoProce.setCaption(registroQuiSegPausa.getHiptermiaNoProce().getDescripcion());
		hiptermiaNoProce.setDescription(registroQuiSegPausa.getHiptermiaNoProce().getTextoAyuda());
		binder.forField(hiptermiaNoProce).bind(RegistroQuiSegPausa::getHiptermiaNoProceBoolean,
				RegistroQuiSegPausa::setHiptermiaNoProce);
		fila5.addComponent(hiptermiaNoProce);

		instrumental.setCaption(registroQuiSegPausa.getInstrumental().getDescripcion());
		instrumental.setDescription(registroQuiSegPausa.getInstrumental().getTextoAyuda());
		binder.forField(instrumental).bind(RegistroQuiSegPausa::getInstrumentalBoolean,
				RegistroQuiSegPausa::setInstrumental);
		fila6.addComponent(instrumental);

		instrumentalOkcorre.setCaption(registroQuiSegPausa.getInstrumentalOkcorre().getDescripcion());
		instrumentalOkcorre.setDescription(registroQuiSegPausa.getInstrumentalOkcorre().getTextoAyuda());
		binder.forField(instrumentalOkcorre).bind(RegistroQuiSegPausa::getInstrumentalOkcorreBoolean,
				RegistroQuiSegPausa::setInstrumentalOkcorre);
		fila6.addComponent(instrumentalOkcorre);

		equipos.setCaption(registroQuiSegPausa.getEquipos().getDescripcion());
		equipos.setDescription(registroQuiSegPausa.getEquipos().getTextoAyuda());
		binder.forField(equipos).bind(RegistroQuiSegPausa::getEquiposBoolean, RegistroQuiSegPausa::setEquipos);
		fila7.addComponent(equipos);

		equiposOkCoree.setCaption(registroQuiSegPausa.getEquiposOkCoree().getDescripcion());
		equiposOkCoree.setDescription(registroQuiSegPausa.getEquiposOkCoree().getTextoAyuda());
		binder.forField(equiposOkCoree).bind(RegistroQuiSegPausa::getEquiposOkCoreeBoolean,
				RegistroQuiSegPausa::setEquiposOkCoree);
		fila7.addComponent(equiposOkCoree);

		observaciones.setWidth("500px");
		observaciones.setHeight("100px");
		observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(RegistroQuiSegPausa::getObservacionesString,
				RegistroQuiSegPausa::setObservaciones);

		fila8.addComponents(observaciones);

		binder.readBean(registroQuiSegPausa);
		doActivaBotones(registroQuiSegPausa);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroQuiSegPausa);
			if (!new RegistroDAO().grabaDatos(registroQuiSegPausa)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registro != null) {
			super.doActivaBotones(registro);
		}

	}
}
