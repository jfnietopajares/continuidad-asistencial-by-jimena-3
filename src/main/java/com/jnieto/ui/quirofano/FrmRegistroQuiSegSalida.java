package com.jnieto.ui.quirofano;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroQuiSegSalida;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextArea;

public class FrmRegistroQuiSegSalida extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4736091580276444975L;
	private CheckBox nombreProce = new CheckBox();
	private CheckBox contaje = new CheckBox();
	private CheckBox glucemiaRealizado = new CheckBox();
	private CheckBox glucemiaNoproce = new CheckBox();
	private CheckBox hipotermiaRealizado = new CheckBox();
	private CheckBox hipotermiaNoproce = new CheckBox();
	private CheckBox identiMuestras = new CheckBox();
	private CheckBox identiMuestrasNoproce = new CheckBox();
	private CheckBox profiTrombo = new CheckBox();
	private CheckBox profiTromboNoproce = new CheckBox();
	private CheckBox profiAntibi = new CheckBox();
	private CheckBox profiAntibioNoproce = new CheckBox();
	private CheckBox pasosCritRecu = new CheckBox();
	private TextArea observaciones = new TextArea("Observaciones.");
	private CheckBox checkFinalizado = new CheckBox();

	private Binder<RegistroQuiSegSalida> binder = new Binder<RegistroQuiSegSalida>();
	private RegistroQuiSegSalida registroQuiSegSalida;

	public FrmRegistroQuiSegSalida(Registro registroParama) {
		// este casting es para que funcione la clase cajaMasterREgistro que usa
		// refleccion
		this.registroQuiSegSalida = (RegistroQuiSegSalida) registroParama;
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7, fila8, fila9, fila10);

		nombreProce.setCaption(registroQuiSegSalida.getNombreProce().getDescripcion());
		nombreProce.setDescription(registroQuiSegSalida.getNombreProce().getTextoAyuda());
		binder.forField(nombreProce).bind(RegistroQuiSegSalida::getNombreProceBoolean,
				RegistroQuiSegSalida::setNombreProce);
		fila1.addComponent(nombreProce);

		contaje.setCaption(registroQuiSegSalida.getContaje().getDescripcion());
		contaje.setDescription(registroQuiSegSalida.getContaje().getTextoAyuda());
		binder.forField(contaje).bind(RegistroQuiSegSalida::getContajeBoolean, RegistroQuiSegSalida::setContaje);
		fila2.addComponent(contaje);

		glucemiaRealizado.setCaption(registroQuiSegSalida.getGlucemiaRealizado().getDescripcion());
		glucemiaRealizado.setDescription(registroQuiSegSalida.getGlucemiaRealizado().getTextoAyuda());
		binder.forField(glucemiaRealizado).bind(RegistroQuiSegSalida::getGlucemiaRealizadoBoolean,
				RegistroQuiSegSalida::setGlucemiaRealizado);

		glucemiaNoproce.setCaption(registroQuiSegSalida.getGlucemiaNoproce().getDescripcion());
		glucemiaNoproce.setDescription(registroQuiSegSalida.getGlucemiaNoproce().getTextoAyuda());
		binder.forField(glucemiaNoproce).bind(RegistroQuiSegSalida::getGlucemiaNoproceBoolean,
				RegistroQuiSegSalida::setGlucemiaNoproce);
		fila3.addComponents(glucemiaRealizado, glucemiaNoproce);

		hipotermiaRealizado.setCaption(registroQuiSegSalida.getHipotermiaRealizado().getDescripcion());
		hipotermiaRealizado.setDescription(registroQuiSegSalida.getHipotermiaRealizado().getTextoAyuda());
		binder.forField(hipotermiaRealizado).bind(RegistroQuiSegSalida::getHipotermiaRealizadoBoolean,
				RegistroQuiSegSalida::setHipotermiaRealizado);

		hipotermiaNoproce.setCaption(registroQuiSegSalida.getHipotermiaNoproce().getDescripcion());
		hipotermiaNoproce.setDescription(registroQuiSegSalida.getHipotermiaNoproce().getTextoAyuda());
		binder.forField(hipotermiaNoproce).bind(RegistroQuiSegSalida::getHipotermiaNoproceBoolean,
				RegistroQuiSegSalida::setHipotermiaNoproce);

		fila4.addComponents(hipotermiaRealizado, hipotermiaNoproce);

		identiMuestras.setCaption(registroQuiSegSalida.getIdentiMuestras().getDescripcion());
		identiMuestras.setDescription(registroQuiSegSalida.getIdentiMuestras().getTextoAyuda());
		binder.forField(identiMuestras).bind(RegistroQuiSegSalida::getIdentiMuestrasBoolean,
				RegistroQuiSegSalida::setIdentiMuestras);

		identiMuestrasNoproce.setCaption(registroQuiSegSalida.getIdentiMuestrasNoproce().getDescripcion());
		identiMuestrasNoproce.setDescription(registroQuiSegSalida.getIdentiMuestrasNoproce().getTextoAyuda());
		binder.forField(identiMuestrasNoproce).bind(RegistroQuiSegSalida::getIdentiMuestrasNoproceBoolean,
				RegistroQuiSegSalida::setIdentiMuestrasNoproce);
		fila5.addComponents(identiMuestras, identiMuestrasNoproce);

		profiTrombo.setCaption(registroQuiSegSalida.getProfiTrombo().getDescripcion());
		profiTrombo.setDescription(registroQuiSegSalida.getProfiTrombo().getTextoAyuda());
		binder.forField(profiTrombo).bind(RegistroQuiSegSalida::getProfiTromboBoolean,
				RegistroQuiSegSalida::setProfiTrombo);

		profiTromboNoproce.setCaption(registroQuiSegSalida.getProfiTromboNoproce().getDescripcion());
		profiTromboNoproce.setDescription(registroQuiSegSalida.getProfiTromboNoproce().getTextoAyuda());
		binder.forField(profiTromboNoproce).bind(RegistroQuiSegSalida::getProfiAntibioNoproceBoolean,
				RegistroQuiSegSalida::setProfiAntibioNoproce);
		fila6.addComponents(profiTrombo, profiTromboNoproce);

		profiAntibi.setCaption(registroQuiSegSalida.getProfiAntibi().getDescripcion());
		profiAntibi.setDescription(registroQuiSegSalida.getProfiAntibi().getTextoAyuda());
		binder.forField(profiAntibi).bind(RegistroQuiSegSalida::getProfiAntibiBoolean,
				RegistroQuiSegSalida::setProfiAntibi);

		profiAntibioNoproce.setCaption(registroQuiSegSalida.getProfiAntibioNoproce().getDescripcion());
		profiAntibioNoproce.setDescription(registroQuiSegSalida.getProfiAntibioNoproce().getTextoAyuda());
		binder.forField(profiAntibioNoproce).bind(RegistroQuiSegSalida::getProfiAntibioNoproceBoolean,
				RegistroQuiSegSalida::setProfiAntibioNoproce);
		fila7.addComponents(profiAntibi, profiAntibioNoproce);

		pasosCritRecu.setCaption(registroQuiSegSalida.getPasosCritRecu().getDescripcion());
		pasosCritRecu.setDescription(registroQuiSegSalida.getPasosCritRecu().getTextoAyuda());
		binder.forField(pasosCritRecu).bind(RegistroQuiSegSalida::getPasosCritRecuBoolean,
				RegistroQuiSegSalida::setPasosCritRecu);
		fila8.addComponents(pasosCritRecu);

		observaciones.setWidth("500px");
		observaciones.setHeight("100px");
		observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(RegistroQuiSegSalida::getObservacionesString,
				RegistroQuiSegSalida::setObservaciones);
		fila9.addComponents(observaciones);

		checkFinalizado.setCaption(registroQuiSegSalida.getCheckFinalizado().getDescripcion());
		checkFinalizado.setDescription(registroQuiSegSalida.getCheckFinalizado().getTextoAyuda());
		binder.forField(checkFinalizado).bind(RegistroQuiSegSalida::getCheckFinalizadoBoolean,
				RegistroQuiSegSalida::setCheckFinalizado);
		fila10.addComponents(checkFinalizado);

		binder.readBean(registroQuiSegSalida);
		doActivaBotones(registroQuiSegSalida);

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroQuiSegSalida);
			if (!new RegistroDAO().grabaDatos(registroQuiSegSalida)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registro != null) {
			super.doActivaBotones(registro);
		}

	}
}
