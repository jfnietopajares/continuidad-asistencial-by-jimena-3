package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.RegistroPartoConstantes;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.validators.ValidatorDecimal;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoConstantes extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7217320145681938131L;
	private RegistroPartoConstantes registroConstantes = new RegistroPartoConstantes();

	private TextField temperatura = new TextField("Tª");
	private TextField tas = new TextField("Tas");
	private TextField tad = new TextField("Tad");
	private TextField fc = new TextField("Fc");

	private ComboBox<String> fiebreMaterna;
	private ComboBox<String> registroCaridiotocografico;
	private TextField interpretacioRC = new TextField("Interpretación R.Card.");
	private ComboBox<String> phFetal;
	private TextField phFetalValor = new TextField("ph Valor");

	private Binder<RegistroPartoConstantes> binder = new Binder<RegistroPartoConstantes>();

	public FrmRegistroPartoConstantes(RegistroPartoConstantes registro) {
		super();
		this.registroConstantes = registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4);
		lbltitulo.setCaption(registroConstantes.getProblema().getId() + "/" + registroConstantes.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		temperatura.setWidth("60px");
		temperatura.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(temperatura)
				.withValidator(new ValidatorDecimal(temperatura.getValue(),
						registroConstantes.VAR_PARTO_CTE_T.getValorInferior(),
						registroConstantes.VAR_PARTO_CTE_T.getValorSuperior()))
				.bind(RegistroPartoConstantes::getTemperaturaString, RegistroPartoConstantes::setTemperatura);

		fc.setWidth("60px");
		fc.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(fc).withValidator(new ValidatorEntero(fc.getValue(), 20, 300))
				.bind(RegistroPartoConstantes::getFcString, RegistroPartoConstantes::setFc);

		tas.setWidth("60px");
		tas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tas).withValidator(new ValidatorEntero(tas.getValue(), 5, 300))
				.bind(RegistroPartoConstantes::getTasString, RegistroPartoConstantes::setTas);

		tad.setWidth("60px");
		tad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tad).withValidator(new ValidatorEntero(tad.getValue(), 10, 300))
				.bind(RegistroPartoConstantes::getTadString, RegistroPartoConstantes::setTad);

		fiebreMaterna = new ObjetosClinicos().getComoSiNo("Fiebre Materna", "");
		binder.forField(fiebreMaterna).bind(RegistroPartoConstantes::getFiebreMaternaString,
				RegistroPartoConstantes::setFiebreMaterna);

		fila1.addComponents(fc, tas, tad, temperatura, fiebreMaterna);

		registroCaridiotocografico = new ObjetosClinicos().getComboString("Tipo Reg.Car.", "",
				ObjetosClinicos.PARTOS_CTE_TIPO_CARDIOTOGRAFO, "200px");
		binder.forField(registroCaridiotocografico).bind(RegistroPartoConstantes::getRegistroCaridiotocograficoString,
				RegistroPartoConstantes::setRegistroCaridiotocografico);

		interpretacioRC.setWidth("400px");
		interpretacioRC.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(interpretacioRC).bind(RegistroPartoConstantes::getInterpretacioRCString,
				RegistroPartoConstantes::setInterpretacioRC);
		fila3.addComponents(registroCaridiotocografico, interpretacioRC);
		phFetal = new ObjetosClinicos().getComoSiNo("ph Fetal", "");
		binder.forField(phFetal).bind(RegistroPartoConstantes::getPhFetalString, RegistroPartoConstantes::setPhFetal);

		phFetalValor.setWidth("300px");
		phFetalValor.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(phFetalValor).bind(RegistroPartoConstantes::getPhFetalValorString,
				RegistroPartoConstantes::setPhFetalValor);
		fila4.addComponents(phFetal, phFetalValor);

		binder.readBean(registroConstantes);
		doActivaBotones(registroConstantes);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroConstantes);
			if (!new RegistroDAO().grabaDatos(registroConstantes)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
