package com.jnieto.ui.partos;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.jnieto.continuidad.MyUI;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.utilidades.Parametros;

public abstract class FrmRegistroParto extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4149505635614619480L;

	public FrmRegistroParto() {
		super();
	}

	@Override
	public void cerrarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void ayudaClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void grabarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

	public void doActivaBotones(Registro registro) {

		if (registro.getProblema().getMotivo_baja() != null && !registro.getProblema().getMotivo_baja().isEmpty()) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registro.getProblema().getFechafin(), date);
			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

}
