package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.RegistroPartoRecienNacido;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Parametros;
import com.jnieto.utilidades.Utilidades;
import com.jnieto.validators.ValidatorEntero;
import com.jnieto.validators.ValidatorHoraMinuto;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.RadioButtonGroup;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoRecienNacido extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5252816955285469186L;

	private TextField numerohc;
	private String numerohcAnterior;
	private DateField fechaNacimiento;
	private TextField horaNacimiento;
	private RadioButtonGroup<String> sexo = null;

	private TextField apgar1Fc = new TextField("Fc");
	private TextField apgar1Respira = new TextField("Respira");
	private TextField apgar1Tono = new TextField("Tono");
	private TextField apgar1Reflejos = new TextField("Reflejos");
	private TextField apgar1Color = new TextField("Color");
	private TextField apgar1Puntuacion = new TextField("Apgar");

	private TextField apgar5Fc = new TextField("Fc");
	private TextField apgar5Respira = new TextField("Respira");
	private TextField apgar5Tono = new TextField("Tono");
	private TextField apgar5Reflejos = new TextField("Reflejos");
	private TextField apgar5Color = new TextField("Color");
	private TextField apgar5Puntuacion = new TextField("Apgar");

	private TextField apgar10Fc = new TextField("Fc");
	private TextField apgar10Respira = new TextField("Respira");
	private TextField apgar10Tono = new TextField("Tono");
	private TextField apgar10Reflejos = new TextField("Reflejos");
	private TextField apgar10Color = new TextField("Color");
	private TextField apgar10Puntuacion = new TextField("Apgar");

	private TextField cordonArterial = new TextField("Cord.Arterial");
	private TextField cordonVenoso = new TextField("Cord.Venoso");
	private ComboBox<String> reanimacion;
	private ComboBox<String> pinzaCordon;

	private TextField peso = new TextField("Peso (gr)");
	private TextField talla = new TextField("Talla (cm)");
	private TextField pCefalico = new TextField("P.Cefálico (cm)");

	private ComboBox<String> pielConPiel;
	private ComboBox<String> lactanciaParitorio;
	private ComboBox<String> profilaxSBG;
	private ComboBox<String> mortalidad;
	private ComboBox<String> ingresNeonatos;

	private RegistroPartoRecienNacido registroRecienNacido;

	private Binder<RegistroPartoRecienNacido> binder = new Binder<RegistroPartoRecienNacido>();

	public FrmRegistroPartoRecienNacido(RegistroPartoRecienNacido registro) {
		super();
		this.registroRecienNacido = (RegistroPartoRecienNacido) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7);

		this.fila2.setCaption("Apgar minuto 1 ");
		this.fila2.addStyleName(MaterialTheme.LAYOUT_WELL);
		this.fila3.setCaption("Apgar minuto 5");
		this.fila3.addStyleName(MaterialTheme.LAYOUT_WELL);
		this.fila4.setCaption("Apgar minuto 10");
		this.fila4.addStyleName(MaterialTheme.LAYOUT_WELL);
		this.fila5.setCaption("Cordón umbilical");
		this.fila5.addStyleName(MaterialTheme.LAYOUT_WELL);
		lbltitulo.setCaption(registroRecienNacido.getProblema().getId() + "/" + registroRecienNacido.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		numerohc = new ObjetosComunes().getNumerohc("Nº Historia", " n historia ");
		numerohc.addBlurListener(e -> saltaNumeroHc());
		binder.forField(numerohc).bind(RegistroPartoRecienNacido::getNumerohcString,
				RegistroPartoRecienNacido::setNumerohc);

		fechaNacimiento = new ObjetosComunes().getFecha("F.Nacimiento", " f. nacimiento ");
		binder.forField(fechaNacimiento).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroPartoRecienNacido::getFechaNacimientoDate, RegistroPartoRecienNacido::setFechaNacimiento);

		horaNacimiento = new ObjetosComunes().getHora("Hora naci.", "hora");
		horaNacimiento.setValue(Utilidades.getHoraAcualString());
		binder.forField(horaNacimiento).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.withValidator(new ValidatorHoraMinuto(horaNacimiento.getValue()))
				.bind(RegistroPartoRecienNacido::getHoraNacimientoString, RegistroPartoRecienNacido::setHoraNacimiento);

		sexo = new ObjetosComunes().getSexo();
		binder.forField(sexo).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroPartoRecienNacido::getSexoString, RegistroPartoRecienNacido::setSexo);

		peso.setWidth("50px");
		peso.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(peso).withValidator(new ValidatorEntero(peso.getValue(), 250, 9000))
				.bind(RegistroPartoRecienNacido::getPesostring, RegistroPartoRecienNacido::setPeso);

		talla.setWidth("50px");
		talla.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(talla).withValidator(new ValidatorEntero(talla.getValue(), 40, 800))
				.bind(RegistroPartoRecienNacido::getTallaString, RegistroPartoRecienNacido::setTalla);

		pCefalico.setWidth("50px");
		pCefalico.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(pCefalico).withValidator(new ValidatorEntero(pCefalico.getValue(), 40, 900))
				.bind(RegistroPartoRecienNacido::getpCefalicoString, RegistroPartoRecienNacido::setpCefalico);

		fila1.addComponents(numerohc, fechaNacimiento, horaNacimiento, sexo, peso, talla, pCefalico);

		apgar1Fc.setWidth("60px");
		apgar1Fc.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Fc).bind(RegistroPartoRecienNacido::getApgar1FcString,
				RegistroPartoRecienNacido::setApgar1Fc);

		apgar1Respira.setWidth("60px");
		apgar1Respira.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Respira).bind(RegistroPartoRecienNacido::getApgar1RespiraString,
				RegistroPartoRecienNacido::setApgar1Respira);

		apgar1Tono.setWidth("60px");
		apgar1Tono.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Tono).bind(RegistroPartoRecienNacido::getApgar1TonoString,
				RegistroPartoRecienNacido::setApgar1Tono);

		apgar1Reflejos.setWidth("60px");
		apgar1Reflejos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Reflejos).bind(RegistroPartoRecienNacido::getApgar1ReflejosString,
				RegistroPartoRecienNacido::setApgar1Reflejos);

		apgar1Color.setWidth("60px");
		apgar1Color.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Color).bind(RegistroPartoRecienNacido::getApgar1ColorString,
				RegistroPartoRecienNacido::setApgar1Color);

		apgar1Puntuacion.setWidth("60px");
		apgar1Puntuacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar1Puntuacion).bind(RegistroPartoRecienNacido::getApgar1PuntuacionString,
				RegistroPartoRecienNacido::setApgar1Puntuacion);

		fila2.addComponents(apgar1Fc, apgar1Respira, apgar1Tono, apgar1Reflejos, apgar1Color, apgar1Puntuacion);

		apgar5Fc.setWidth("60px");
		apgar5Fc.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Fc).bind(RegistroPartoRecienNacido::getApgar5FcString,
				RegistroPartoRecienNacido::setApgar5Fc);

		apgar5Respira.setWidth("60px");
		apgar5Respira.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Respira).bind(RegistroPartoRecienNacido::getApgar5RespiraString,
				RegistroPartoRecienNacido::setApgar5Respira);

		apgar5Tono.setWidth("60px");
		apgar5Tono.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Tono).bind(RegistroPartoRecienNacido::getApgar5TonoString,
				RegistroPartoRecienNacido::setApgar5Tono);

		apgar5Reflejos.setWidth("60px");
		apgar5Reflejos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Reflejos).bind(RegistroPartoRecienNacido::getApgar5ReflejosString,
				RegistroPartoRecienNacido::setApgar5Reflejos);

		apgar5Color.setWidth("60px");
		apgar5Color.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Color).bind(RegistroPartoRecienNacido::getApgar5ColorString,
				RegistroPartoRecienNacido::setApgar5Color);

		apgar5Puntuacion.setWidth("60px");
		apgar5Puntuacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar5Puntuacion).bind(RegistroPartoRecienNacido::getApgar5PuntuacionString,
				RegistroPartoRecienNacido::setApgar5Puntuacion);

		fila3.addComponents(apgar5Fc, apgar5Respira, apgar5Tono, apgar5Reflejos, apgar5Color, apgar5Puntuacion);

		apgar10Fc.setWidth("60px");
		apgar10Fc.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Fc).bind(RegistroPartoRecienNacido::getApgar10FcString,
				RegistroPartoRecienNacido::setApgar10Fc);

		apgar10Respira.setWidth("60px");
		apgar10Respira.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Respira).bind(RegistroPartoRecienNacido::getApgar10RespiraString,
				RegistroPartoRecienNacido::setApgar10Respira);

		apgar10Tono.setWidth("60px");
		apgar10Tono.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Tono).bind(RegistroPartoRecienNacido::getApgar10TonoString,
				RegistroPartoRecienNacido::setApgar10Tono);

		apgar10Reflejos.setWidth("60px");
		apgar10Reflejos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Reflejos).bind(RegistroPartoRecienNacido::getApgar10ReflejosString,
				RegistroPartoRecienNacido::setApgar10Reflejos);

		apgar10Color.setWidth("60px");
		apgar10Color.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Color).bind(RegistroPartoRecienNacido::getApgar10ColorString,
				RegistroPartoRecienNacido::setApgar10Color);

		apgar10Puntuacion.setWidth("60px");
		apgar10Puntuacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(apgar10Puntuacion).bind(RegistroPartoRecienNacido::getApgar10PuntuacionString,
				RegistroPartoRecienNacido::setApgar10Puntuacion);

		fila4.addComponents(apgar10Fc, apgar10Respira, apgar10Tono, apgar10Reflejos, apgar10Color, apgar10Puntuacion);

		cordonArterial.setWidth("100px");
		cordonArterial.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(cordonArterial).bind(RegistroPartoRecienNacido::getCordonArterialString,
				RegistroPartoRecienNacido::setCordonVenoso);

		cordonVenoso.setWidth("100px");
		cordonVenoso.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(cordonVenoso).bind(RegistroPartoRecienNacido::getCordonVenosoString,
				RegistroPartoRecienNacido::setCordonVenoso);

		reanimacion = new ObjetosClinicos().getComboString("Reanimiación", "", ObjetosClinicos.PARTOS_RN_REANIMIACION,
				"200px");
		binder.forField(reanimacion).bind(RegistroPartoRecienNacido::getReanimacionString,
				RegistroPartoRecienNacido::setReanimacion);

		pinzaCordon = new ObjetosClinicos().getComboString("Pizamiento", "", ObjetosClinicos.PARTOS_RN_CORDON, "200px");
		binder.forField(pinzaCordon).bind(RegistroPartoRecienNacido::getPinzaCordonString,
				RegistroPartoRecienNacido::setPinzaCordon);

		fila5.addComponents(cordonArterial, cordonVenoso, pinzaCordon, reanimacion);

		// fila6.addComponents(peso, talla, pCefalico);

		pielConPiel = new ObjetosClinicos().getComboString("Piel con piel", "", ObjetosClinicos.PARTOS_RN_PIELPIEL,
				"200px");
		binder.forField(pielConPiel).bind(RegistroPartoRecienNacido::getPielConPielSring,
				RegistroPartoRecienNacido::setPielConPiel);

		lactanciaParitorio = new ObjetosClinicos().getComoSiNo("Lactancia paritorio ", "");
		binder.forField(lactanciaParitorio).bind(RegistroPartoRecienNacido::getLactanciaParitorioString,
				RegistroPartoRecienNacido::setLactanciaParitorio);

		profilaxSBG = new ObjetosClinicos().getComboString("Profilaxis SGB ", "",
				ObjetosClinicos.PARTOS_RN_PROFILAXISSBG, "200px");
		binder.forField(profilaxSBG).bind(RegistroPartoRecienNacido::getProfilaxSBGString,
				RegistroPartoRecienNacido::setProfilaxSBG);

		mortalidad = new ObjetosClinicos().getComoSiNo("Mortalidad  ", "");
		binder.forField(mortalidad).bind(RegistroPartoRecienNacido::getMortalidadString,
				RegistroPartoRecienNacido::setMortalidad);

		ingresNeonatos = new ObjetosClinicos().getComoSiNo("Ingreso Neontados  ", "");
		binder.forField(ingresNeonatos).bind(RegistroPartoRecienNacido::getIngresNeonatosString,
				RegistroPartoRecienNacido::setIngresNeonatos);

		fila7.addComponents(pielConPiel, lactanciaParitorio, profilaxSBG, mortalidad, ingresNeonatos);

		binder.readBean(registroRecienNacido);
		numerohcAnterior = numerohc.getValue();
		this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones(registroRecienNacido);
	}

	public void saltaNumeroHc() {
		if (!numerohc.getValue().isEmpty()) {
			if (Utilidades.isNumeric(numerohc.getValue())) {
				int nhc = Integer.parseInt(numerohc.getValue());
				if (nhc > Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_MAXIMO_NUMERO_HISTORIA))) {
					new NotificacionInfo("El número de histororia debe ser menor de "
							+ MyUI.objParametros.get(Parametros.KEY_MAXIMO_NUMERO_HISTORIA));
					numerohc.clear();
					numerohc.focus();
				} else {
					Paciente hijoPaciente = new PacienteDAO().getPacientePorNhc(numerohc.getValue());
					if (hijoPaciente != null) {

					} else {
						new NotificacionInfo("Ese hay paciente para esa  historia  ");
						numerohc.clear();
						numerohc.focus();
					}
				}
			} else {

			}
		}
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroRecienNacido);
			if (!new RegistroDAO().grabaDatos(registroRecienNacido)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				// hay que duplicar el registro para asociar los datos a la historia del hijo
				Paciente hijoPaciente = new PacienteDAO().getPacientePorNhc(numerohc.getValue());
				RegistroPartoRecienNacido registroHijoNacido = registroRecienNacido;
				registroHijoNacido.setPaciente(hijoPaciente);
				registroHijoNacido.setProblema(null);
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void doActivaBotones() {
		super.doActivaBotones(registroRecienNacido);

	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
