package com.jnieto.ui.partos;

import java.util.ArrayList;

import com.jnieto.dao.RegistroPartoAlivioDolorDAO;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroPartoAlivioDolor;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaPartoAlivioDolor extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8631839194401710370L;
	private RegistroPartoAlivioDolor registro;
	private ArrayList<RegistroPartoAlivioDolor> listaRegistros = new ArrayList<>();

	private Grid<RegistroPartoAlivioDolor> grid = new Grid<RegistroPartoAlivioDolor>();

//	private Grid<?> grid;

	public CajaPartoAlivioDolor() {
	}

	public CajaPartoAlivioDolor(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
	}

	public CajaPartoAlivioDolor(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		lbltitulo.setCaption("Partos Alivio Dolor");
		// grid.setSizeUndefined();
		// grid.setHeightByRows(5);
		// grid.setHeaderRowHeight(10);
		// grid.setHeightMode(HeightMode.ROW);
		// grid.setHeightByRows(5);

//		grid = new Grid<T>();

		grid.addColumn(RegistroPartoAlivioDolor::getFechaHora).setCaption("Fecha");
		grid.addColumn(RegistroPartoAlivioDolor::getFarmacologicoString).setCaption("Farma");
		// grid.setHeaderVisible(false);
		// grid.removeHeaderRow(0);
		// grid.setHeaderRowHeight(0);
		grid.setBodyRowHeight(35);
		grid.setHeight("300px");
		contenedorGrid.addComponent(grid);
		refrescarClick();
	}

	public CajaPartoAlivioDolor(Paciente paciente, EpisodioClase clase, Long subambito) {
		super(paciente, clase, subambito);
	}

	@Override
	public void nuevoClick() {
		RegistroPartoAlivioDolor registro = new RegistroPartoAlivioDolor();
		registro.setPaciente(paciente);
		registro.setProblema(proceso);
		VentanaFrm frmDolorFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoAlivioDolor(registro),
				registro.getDescripcion());
		frmDolorFrm.addCloseListener(e -> cierraVentana());
	}

	public void cierraVentana() {
		refrescarClick();
	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			RegistroPartoAlivioDolor registro = grid.getSelectedItems().iterator().next();
			VentanaFrm frmDolorFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoAlivioDolor(registro),
					registro.getDescripcion());
			frmDolorFrm.addCloseListener(e -> cierraVentana());
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}

	}

	@Override
	public void refrescarClick() {
		listaRegistros = new RegistroPartoAlivioDolorDAO().getListaRegistros(proceso);
		contenedorBotones.setEnabled(true);
		if (listaRegistros.size() > 0) {
			grid.setItems(listaRegistros);
		} else {
			nuevo.setEnabled(true);
		}
		nuevo.setEnabled(true);
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {

	}

}
