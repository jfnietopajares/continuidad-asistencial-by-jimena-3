package com.jnieto.ui.partos;

import java.time.LocalDate;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoIngreso;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoIngreso extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3985323932490463020L;
	private DateField fechaIngreso;
	private TextField horaIngreso;
	private TextField observador = new TextField("Observador");
	private ComboBox<String> tipoIngreso;
	private ComboBox<String> situacion;
	private ComboBox<String> presentacion;
	private ComboBox<String> bishop = new ObjetosComunes().getComboNumero("Test de Bishop", 0, 10, "", "60px");

	private RegistroPartoIngreso registroIngreso;

	private Binder<RegistroPartoIngreso> binder = new Binder<RegistroPartoIngreso>();

	public FrmRegistroPartoIngreso(Registro registro) {
		super();
		this.registroIngreso = (RegistroPartoIngreso) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		// this.fila3.setCaption("Test de Bishop ");
		// fila3.addStyleName(MaterialTheme.LAYOUT_WELL);

		lbltitulo.setCaption(registroIngreso.getProblema().getId() + "/" + registroIngreso.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		fechaIngreso = new ObjetosComunes().getFecha("Fecha ingreso", " fecha ingreso");
		binder.forField(fechaIngreso)
				.withValidator(returnDate -> !returnDate.isBefore(registroIngreso.getProblema().getFechaini()),
						" Mayor o inicio proceso")

				.bind(RegistroPartoIngreso::getFechaIngresoDate, RegistroPartoIngreso::setFechaIngreso);

		horaIngreso = new ObjetosComunes().getHora("Hora ingreso", "hora");
		horaIngreso.setValue(Utilidades.getHoraAcualString());
		binder.forField(horaIngreso).bind(RegistroPartoIngreso::getHoraIngresoString,
				RegistroPartoIngreso::setHoraIngreso);

		observador.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		observador.setMaxLength(100);
		binder.forField(observador).bind(RegistroPartoIngreso::getObservadorString,
				RegistroPartoIngreso::setObservador);

		fila1.addComponents(fechaIngreso, horaIngreso, observador);

		tipoIngreso = new ObjetosClinicos().getComboString("Tipo ingreso", "", ObjetosClinicos.PARTOS_TIPOS_INGRESO,
				"200px");
		binder.forField(tipoIngreso).bind(RegistroPartoIngreso::getTipoIngresoString,
				RegistroPartoIngreso::setTipoIngreso);

		situacion = new ObjetosClinicos().getComboString("Situación", "", ObjetosClinicos.PARTOS_TIPOS_SITUACION,
				"200px");
		binder.forField(situacion).bind(RegistroPartoIngreso::getSituacionString, RegistroPartoIngreso::setSituacion);

		presentacion = new ObjetosClinicos().getComboString("Presentación", "",
				ObjetosClinicos.PARTOS_TIPOS_PRESENTACION, "200px");
		binder.forField(presentacion).bind(RegistroPartoIngreso::getPresentacionString,
				RegistroPartoIngreso::setPresentacion);

		fila2.addComponents(tipoIngreso, situacion, presentacion);

		binder.forField(bishop).bind(RegistroPartoIngreso::getBishopString, RegistroPartoIngreso::setBishop);

		fila3.addComponent(bishop);

		binder.readBean(registroIngreso);
		if (fechaIngreso.isEmpty())
			fechaIngreso.setValue(LocalDate.now());

		if (horaIngreso.isEmpty())
			horaIngreso.setValue(Utilidades.getHoraAcualString());
		this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones(registroIngreso);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroIngreso);
			if (!new RegistroDAO().grabaDatos(registroIngreso)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
