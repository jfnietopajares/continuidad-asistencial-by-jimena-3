package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoInduccion;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoInduccion extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4392150392842063582L;

	private RegistroPartoInduccion registroInduccion = new RegistroPartoInduccion();

	private TextField patologiaMaterna = new TextField("Patología materna");
	private ComboBox<String> patologiaFetalCir;
	private TextField patologiaFetalPeg = new TextField("Patología fetal PEG");

	private CheckBox roturaBolasmas12 = new CheckBox("Rotura bolsa mayor 12 horas");
	private CheckBox alteracionLiqAmni = new CheckBox("Líquido amniótico alterado");
	private CheckBox gestacionFetalProlongada = new CheckBox("Gestación fetal prolongada");
	private TextField otrasIndicacionesInduccion = new TextField("Otras indicaciones");
	private ComboBox<String> maduracioncervical = new ComboBox<String>("Maduración cervical");
	private TextField horas = new TextField("Horas");
	private ComboBox<String> preparacionOxiticica = new ComboBox<String>("Prep.Oxitócica");
	private TextField evolucion = new TextField("Evolución");

	Binder<RegistroPartoInduccion> binder = new Binder<RegistroPartoInduccion>();

	public FrmRegistroPartoInduccion(Registro registro) {
		super();
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5);
		this.registroInduccion = (RegistroPartoInduccion) registro;
		lbltitulo.setCaption(registroInduccion.getProblema().getId() + "/" + registroInduccion.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		patologiaMaterna.setWidth("200px");
		patologiaMaterna.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(patologiaMaterna).bind(RegistroPartoInduccion::getPatologiaMaternaString,
				RegistroPartoInduccion::setPatologiaMaterna);

		patologiaFetalCir = new ObjetosClinicos().getComboString("Pato.Fetal CIR", "",
				ObjetosClinicos.PARTOS_INDU_PATOLOGIAFETAL, "100px");
		binder.forField(patologiaFetalCir).bind(RegistroPartoInduccion::getPatologiaFetalCirString,
				RegistroPartoInduccion::setPatologiaFetalCir);

		patologiaFetalPeg.setWidth("100px");
		patologiaFetalPeg.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(patologiaFetalPeg).bind(RegistroPartoInduccion::getPatologiaFetalPegString,
				RegistroPartoInduccion::setPatologiaFetalPeg);

		fila1.addComponents(patologiaMaterna, patologiaFetalCir, patologiaFetalPeg);

		binder.forField(roturaBolasmas12).bind(RegistroPartoInduccion::getRoturaBolasmas12Boolean,
				RegistroPartoInduccion::setRoturaBolasmas12);

		binder.forField(gestacionFetalProlongada).bind(RegistroPartoInduccion::getGestacionFetalProlongadaBoolean,
				RegistroPartoInduccion::setGestacionFetalProlongada);

		binder.forField(alteracionLiqAmni).bind(RegistroPartoInduccion::getAlteracionLiqAmniBoolean,
				RegistroPartoInduccion::setAlteracionLiqAmni);

		fila2.addComponents(roturaBolasmas12, gestacionFetalProlongada, alteracionLiqAmni);

		otrasIndicacionesInduccion.setWidth("600px");
		otrasIndicacionesInduccion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(otrasIndicacionesInduccion).bind(RegistroPartoInduccion::getOtrasIndicacionesInduccionString,
				RegistroPartoInduccion::setOtrasIndicacionesInduccion);
		fila3.addComponents(otrasIndicacionesInduccion);

		maduracioncervical = new ObjetosClinicos().getComoSiNo("Maduración cervical", "");
		binder.forField(maduracioncervical).bind(RegistroPartoInduccion::getMaduracioncervicalString,
				RegistroPartoInduccion::setMaduracioncervical);

		horas.setWidth("50px");
		horas.setMaxLength(3);
		horas.setPlaceholder("horas");
		horas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(horas).bind(RegistroPartoInduccion::getHorasString, RegistroPartoInduccion::setHoras);

		preparacionOxiticica = new ObjetosClinicos().getComoSiNo("Estimulación oxitócica", "");
		binder.forField(preparacionOxiticica).bind(RegistroPartoInduccion::getPreparacionOxiticicaString,
				RegistroPartoInduccion::setPreparacionOxiticica);

		fila4.addComponents(maduracioncervical, horas, preparacionOxiticica);

		evolucion.setWidth("600px");
		evolucion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(evolucion).bind(RegistroPartoInduccion::getEvolucionString,
				RegistroPartoInduccion::setEvolucion);
		fila5.addComponents(evolucion);

		binder.readBean(registroInduccion);
		doActivaBotones(registroInduccion);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();

	}

	@Override
	public void ayudaClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroInduccion);
			if (!new RegistroDAO().grabaDatos(registroInduccion)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

}
