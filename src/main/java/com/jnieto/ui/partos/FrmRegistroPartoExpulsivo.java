package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoExpulsivo;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.validators.ValidatorDecimal;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoExpulsivo extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5493389044079368282L;

	private TextField horas = new TextField("Horas");
	private ComboBox<String> partoTipo;
	private ComboBox<String> phFetal;
	private TextField temperatura = new TextField("Tª");
	private TextField interpretacioRC = new TextField("Interpretación R.Card.");
	private ComboBox<String> indicacionCesarea;

	private RegistroPartoExpulsivo registroExpulsivo;

	private Binder<RegistroPartoExpulsivo> binder = new Binder<RegistroPartoExpulsivo>();

	public FrmRegistroPartoExpulsivo(Registro registro) {
		super();
		this.registroExpulsivo = (RegistroPartoExpulsivo) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		lbltitulo.setCaption(registroExpulsivo.getProblema().getId() + "/" + registroExpulsivo.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		horas.setWidth("90px");
		horas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(horas).withValidator(new ValidatorEntero(horas.getValue(), 0, 300))
				.bind(RegistroPartoExpulsivo::getHorasString, RegistroPartoExpulsivo::setHoras);

		partoTipo = new ObjetosClinicos().getComboString("Parto", "", ObjetosClinicos.PARTOS_EXPUL_TIPOPARTO, "300px");
		binder.forField(partoTipo).bind(RegistroPartoExpulsivo::getPartoTipoString,
				RegistroPartoExpulsivo::setPartoTipo);

		fila1.addComponents(horas, partoTipo);

		phFetal = new ObjetosClinicos().getComoSiNo("pH Fetal", "");
		binder.forField(phFetal).bind(RegistroPartoExpulsivo::getPhFetalString, RegistroPartoExpulsivo::setPhFetal);

		temperatura.setWidth("60px");
		temperatura.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(temperatura)
				.withValidator(new ValidatorDecimal(temperatura.getValue(),
						registroExpulsivo.VAR_PARTO_EXPUL_T.getValorInferior(),
						registroExpulsivo.VAR_PARTO_EXPUL_T.getValorSuperior()))
				.bind(RegistroPartoExpulsivo::getTemperaturaString, RegistroPartoExpulsivo::setTemperatura);

		fila2.addComponents(phFetal, temperatura);

		interpretacioRC.setWidth("400px");
		interpretacioRC.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(interpretacioRC).bind(RegistroPartoExpulsivo::getInterpretacioRCString,
				RegistroPartoExpulsivo::setInterpretacioRC);
		indicacionCesarea = new ObjetosClinicos().getComboString("Inidcación Cesárea", "",
				ObjetosClinicos.PARTOS_EXPUL_INDICACIONCESAREA, "300px");
		binder.forField(indicacionCesarea).bind(RegistroPartoExpulsivo::getIndicacionCesareaString,
				RegistroPartoExpulsivo::setIndicacionCesarea);
		fila3.addComponents(interpretacioRC, indicacionCesarea);

		binder.readBean(registroExpulsivo);
		doActivaBotones(registroExpulsivo);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();

	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroExpulsivo);
			if (!new RegistroDAO().grabaDatos(registroExpulsivo)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}

	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
