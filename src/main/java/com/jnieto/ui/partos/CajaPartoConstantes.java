package com.jnieto.ui.partos;

import java.util.ArrayList;

import com.jnieto.dao.RegistroPartoConstantesDAO;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroPartoConstantes;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaPartoConstantes extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = -441831275155738813L;
	private RegistroPartoConstantes registro;
	private ArrayList<RegistroPartoConstantes> listaRegistros = new ArrayList<>();
	private Grid<RegistroPartoConstantes> grid = new Grid<RegistroPartoConstantes>();

	public CajaPartoConstantes() {
	}

	public CajaPartoConstantes(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
	}

	public CajaPartoConstantes(Paciente paciente, Long subambito) {
		super(paciente, subambito);
	}

	public CajaPartoConstantes(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		lbltitulo.setCaption("Partos Constantes ");
		grid.addColumn(RegistroPartoConstantes::getFechaHora).setCaption("Fecha");
		grid.addColumn(RegistroPartoConstantes::getTadString).setCaption("TAS");
		grid.addColumn(RegistroPartoConstantes::getTadString).setCaption("TAD");
		// grid.setHeaderVisible(false);
		// grid.removeHeaderRow(0);
		// grid.setHeaderRowHeight(0);
		grid.setBodyRowHeight(35);
		grid.setHeight("300px");
		contenedorGrid.addComponent(grid);
		refrescarClick();
	}

	public CajaPartoConstantes(Paciente paciente, EpisodioClase clase, Long subambito) {
		super(paciente, clase, subambito);
	}

	@Override
	public void nuevoClick() {
		RegistroPartoConstantes registro = new RegistroPartoConstantes();
		registro.setPaciente(paciente);
		registro.setProblema(proceso);
		VentanaFrm frmCtesFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoConstantes(registro),
				registro.getDescripcion());
		frmCtesFrm.addCloseListener(e -> cierraVentana());
		refrescarClick();

	}

	public void cierraVentana() {
		refrescarClick();
	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			RegistroPartoConstantes registro = grid.getSelectedItems().iterator().next();
			VentanaFrm frmDolorFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoConstantes(registro),
					registro.getDescripcion());
			frmDolorFrm.addCloseListener(e -> cierraVentana());
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	@Override
	public void refrescarClick() {
		listaRegistros = new RegistroPartoConstantesDAO().getListaRegistros(proceso);
		contenedorBotones.setEnabled(true);
		if (listaRegistros.size() > 0) {
			grid.setItems(listaRegistros);
		} else {
			nuevo.setEnabled(true);
		}
		nuevo.setEnabled(true);
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {

	}

}
