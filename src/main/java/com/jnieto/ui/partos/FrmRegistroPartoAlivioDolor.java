package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.RegistroPartoAlivioDolor;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextArea;

public class FrmRegistroPartoAlivioDolor extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1388322773793914543L;
	/*
	 * private HorizontalLayout fila1 = new HorizontalLayout(); private
	 * HorizontalLayout fila2 = new HorizontalLayout(); private HorizontalLayout
	 * fila3 = new HorizontalLayout(); private HorizontalLayout fila4 = new
	 * HorizontalLayout();
	 */RegistroPartoAlivioDolor registroDolor = new RegistroPartoAlivioDolor();

	private ComboBox<String> farmacologico = new ComboBox<String>();
	private CheckBox movimiento = new CheckBox("Movimiento");
	private CheckBox calorLocal = new CheckBox("Calor Local");
	private CheckBox masaje = new CheckBox("Masaje");
	private CheckBox pelota = new CheckBox("Pelota");
	private CheckBox ducha = new CheckBox("Ducha");
	private TextArea otros = new TextArea("Otros");

	Binder<RegistroPartoAlivioDolor> binder = new Binder<RegistroPartoAlivioDolor>();

	public FrmRegistroPartoAlivioDolor(RegistroPartoAlivioDolor registro) {
		super();
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		this.registroDolor = registro;
		/*
		 * this.fila1.setMargin(false); this.fila2.setMargin(false);
		 * this.fila3.setMargin(false); this.fila4.setMargin(false);
		 * contenedorCampos.addComponents(fila1, fila2, fila3, fila4);
		 */
		lbltitulo.setCaption(registroDolor.getProblema().getId() + "/" + registroDolor.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		farmacologico = new ObjetosClinicos().getComboString("Tto Farmacológico.", "",
				ObjetosClinicos.PARTOS_DOLOR_FARMACOLOGICO, "300px");
		binder.forField(farmacologico).bind(RegistroPartoAlivioDolor::getFarmacologicoString,
				RegistroPartoAlivioDolor::setFarmacologico);
		fila1.addComponent(farmacologico);
		binder.forField(movimiento).bind(RegistroPartoAlivioDolor::getMovimientoBolean,
				RegistroPartoAlivioDolor::setMovimiento);
		binder.forField(calorLocal).bind(RegistroPartoAlivioDolor::getCalorLocalBoolean,
				RegistroPartoAlivioDolor::setCalorLocal);

		binder.forField(masaje).bind(RegistroPartoAlivioDolor::getMasajeBoolean, RegistroPartoAlivioDolor::setMasaje);
		binder.forField(pelota).bind(RegistroPartoAlivioDolor::getPelotaBoolean, RegistroPartoAlivioDolor::setPelota);
		binder.forField(ducha).bind(RegistroPartoAlivioDolor::getDuchaBoolean, RegistroPartoAlivioDolor::setDucha);
		fila2.addComponents(movimiento, calorLocal, masaje, pelota, ducha);

		otros.setWidth("500px");
		otros.setHeight("100px");
		otros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(otros).bind(RegistroPartoAlivioDolor::getOtrosString, RegistroPartoAlivioDolor::setOtros);
		fila3.addComponent(otros);

		binder.readBean(registroDolor);
		doActivaBotones(registroDolor);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroDolor);
			if (!new RegistroDAO().grabaDatos(registroDolor)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
