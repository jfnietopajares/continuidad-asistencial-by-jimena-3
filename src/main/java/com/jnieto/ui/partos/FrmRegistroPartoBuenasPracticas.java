package com.jnieto.ui.partos;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoBuenasPracticas;
import com.jnieto.ui.NotificacionInfo;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;

public class FrmRegistroPartoBuenasPracticas extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3574288642762571854L;

	private CheckBox protocoloAcogida = new CheckBox("Protocolo de acogida");
	private CheckBox planParto = new CheckBox("Plan de parto");
	private CheckBox acompanamiento = new CheckBox("Elección de compañamiento y aseroramiento");
	private CheckBox moviDilatacion = new CheckBox("Movimiento durante la dilatación");
	private CheckBox ingestaLiq = new CheckBox("Ingesta de liquidos");
	private CheckBox eleccPostura = new CheckBox("Elección postura expulsivo");
	private CheckBox donaCordon = new CheckBox("Donación sangre cordón");
	private CheckBox buenasRN = new CheckBox("Buenas prácticas del recién nacido");

	private RegistroPartoBuenasPracticas registroBuenaspracticas = new RegistroPartoBuenasPracticas();
	private Binder<RegistroPartoBuenasPracticas> binder = new Binder<RegistroPartoBuenasPracticas>();

	public FrmRegistroPartoBuenasPracticas(Registro registro) {
		super();
		this.registroBuenaspracticas = (RegistroPartoBuenasPracticas) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		lbltitulo.setCaption(registroBuenaspracticas.getProblema().getId() + "/" + registroBuenaspracticas.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);
		binder.forField(protocoloAcogida).bind(RegistroPartoBuenasPracticas::getProtocoloAcogidaBoolean,
				RegistroPartoBuenasPracticas::setProtocoloAcogida);
		binder.forField(planParto).bind(RegistroPartoBuenasPracticas::getPlanPartoBoolean,
				RegistroPartoBuenasPracticas::setPlanParto);
		binder.forField(acompanamiento).bind(RegistroPartoBuenasPracticas::getAcompanamientoBoolean,
				RegistroPartoBuenasPracticas::setAcompanamiento);

		fila1.addComponents(protocoloAcogida, planParto, acompanamiento);
		binder.forField(moviDilatacion).bind(RegistroPartoBuenasPracticas::getMoviDilatacionBoolean,
				RegistroPartoBuenasPracticas::setMoviDilatacion);
		binder.forField(ingestaLiq).bind(RegistroPartoBuenasPracticas::getIngestaLiqBoolean,
				RegistroPartoBuenasPracticas::setIngestaLiq);
		binder.forField(eleccPostura).bind(RegistroPartoBuenasPracticas::getEleccPosturaBoolean,
				RegistroPartoBuenasPracticas::setEleccPostura);

		fila2.addComponents(moviDilatacion, ingestaLiq, eleccPostura);
		binder.forField(donaCordon).bind(RegistroPartoBuenasPracticas::getDonaCordonBoolean,
				RegistroPartoBuenasPracticas::setDonaCordon);
		binder.forField(buenasRN).bind(RegistroPartoBuenasPracticas::getBuenasRNBoolean,
				RegistroPartoBuenasPracticas::setBuenasRN);
		fila3.addComponents(donaCordon, buenasRN);
		binder.readBean(registroBuenaspracticas);
		doActivaBotones(registroBuenaspracticas);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroBuenaspracticas);
			if (!new RegistroDAO().grabaDatos(registroBuenaspracticas)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
