package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.RegistroPartoMedicacion;
import com.jnieto.ui.NotificacionInfo;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoMedicacion extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3915885361048276139L;

	private CheckBox analgesia = new CheckBox("Analgesia");
	private CheckBox analgesiaAntihemetico = new CheckBox("Analgesia Antihemético");
	private CheckBox sbg = new CheckBox("Protocolo SNG");
	private CheckBox ram = new CheckBox("Protcolo RAM");;
	private CheckBox diabetes = new CheckBox("Protocolo Diabetes");;
	private CheckBox preeclampsia = new CheckBox("Protocolo Preclampsia");;
	private CheckBox cesarea = new CheckBox("Protocolo Cesárea");;
	private TextField otraMedicacion = new TextField("Otra medicación");

	private RegistroPartoMedicacion registroMedicacion = new RegistroPartoMedicacion();

	private Binder<RegistroPartoMedicacion> binder = new Binder<RegistroPartoMedicacion>();

	public FrmRegistroPartoMedicacion(RegistroPartoMedicacion registro) {
		super();
		this.registroMedicacion = registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		lbltitulo.setCaption(registroMedicacion.getProblema().getId() + "/" + registroMedicacion.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		binder.forField(analgesia).bind(RegistroPartoMedicacion::getAnalgesiaBoolean,
				RegistroPartoMedicacion::setAnalgesia);
		binder.forField(analgesiaAntihemetico).bind(RegistroPartoMedicacion::getAnalgesiaAntihemeticoBoolean,
				RegistroPartoMedicacion::setAnalgesiaAntihemetico);

		binder.forField(sbg).bind(RegistroPartoMedicacion::getSbgBoolean, RegistroPartoMedicacion::setSbg);
		binder.forField(ram).bind(RegistroPartoMedicacion::getRamBoolean, RegistroPartoMedicacion::setRam);
		fila1.addComponents(analgesia, analgesiaAntihemetico, sbg, ram);

		binder.forField(diabetes).bind(RegistroPartoMedicacion::getDiabetesBoolean,
				RegistroPartoMedicacion::setDiabetes);
		binder.forField(preeclampsia).bind(RegistroPartoMedicacion::getPreeclampsiBooleana,
				RegistroPartoMedicacion::setPreeclampsia);
		binder.forField(cesarea).bind(RegistroPartoMedicacion::getCesareaBoolean, RegistroPartoMedicacion::setCesarea);
		fila2.addComponents(diabetes, preeclampsia, cesarea);

		otraMedicacion.setWidth("600px");
		// otraMedicacion.setHeight("100px");
		otraMedicacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(otraMedicacion).bind(RegistroPartoMedicacion::getOtraMedicacionString,
				RegistroPartoMedicacion::setOtraMedicacion);

		fila3.addComponent(otraMedicacion);
		binder.readBean(registroMedicacion);
		doActivaBotones(registroMedicacion);

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroMedicacion);
			if (!new RegistroDAO().grabaDatos(registroMedicacion)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void borraElRegistro() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean doValidaFormulario() {
		// TODO Auto-generated method stub
		return false;
	}

}
