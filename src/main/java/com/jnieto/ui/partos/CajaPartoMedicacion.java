package com.jnieto.ui.partos;

import java.util.ArrayList;

import com.jnieto.dao.RegistroPartoMedicacionDAO;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroPartoMedicacion;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaPartoMedicacion extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1286808639837542950L;

	private RegistroPartoMedicacion registro;
	private ArrayList<RegistroPartoMedicacion> listaRegistros = new ArrayList<>();
	private Grid<RegistroPartoMedicacion> grid = new Grid<RegistroPartoMedicacion>();

	public CajaPartoMedicacion() {
	}

	public CajaPartoMedicacion(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
	}

	public CajaPartoMedicacion(Paciente paciente, Long subambito) {
		super(paciente, subambito);
	}

	public CajaPartoMedicacion(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		lbltitulo.setCaption("Partos Medicación ");
		grid.addColumn(RegistroPartoMedicacion::getFechaHora).setCaption("Fecha");
		grid.addColumn(RegistroPartoMedicacion::getAnalgesiaString).setCaption("Anal");
		grid.addColumn(RegistroPartoMedicacion::getAnalgesiaAntihemeticoString).setCaption("AnalAH");
		// grid.setHeaderVisible(false);
		// grid.removeHeaderRow(0);
		// grid.setHeaderRowHeight(0);
		grid.setBodyRowHeight(35);
		grid.setHeight("300px");
		contenedorGrid.addComponent(grid);
		refrescarClick();
	}

	public CajaPartoMedicacion(Paciente paciente, EpisodioClase clase, Long subambito) {
		super(paciente, clase, subambito);
	}

	@Override
	public void nuevoClick() {
		RegistroPartoMedicacion registro = new RegistroPartoMedicacion();
		registro.setPaciente(paciente);
		registro.setProblema(proceso);
		VentanaFrm frmCtesFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoMedicacion(registro),
				registro.getDescripcion());
		frmCtesFrm.addCloseListener(e -> cierraVentana());
		refrescarClick();

	}

	public void cierraVentana() {
		refrescarClick();
	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			RegistroPartoMedicacion registro = grid.getSelectedItems().iterator().next();
			VentanaFrm frmDolorFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoMedicacion(registro),
					registro.getDescripcion());
			frmDolorFrm.addCloseListener(e -> cierraVentana());
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	@Override
	public void refrescarClick() {
		listaRegistros = new RegistroPartoMedicacionDAO().getListaRegistros(proceso);
		contenedorBotones.setEnabled(true);
		if (listaRegistros.size() > 0) {
			grid.setItems(listaRegistros);
		} else {
			nuevo.setEnabled(true);
		}
		nuevo.setEnabled(true);
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {

	}

}
