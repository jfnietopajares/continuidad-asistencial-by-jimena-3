package com.jnieto.ui.partos;

import java.util.ArrayList;

import com.jnieto.dao.RegistroPartoRecienNacidoDAO;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroPartoRecienNacido;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaPartoRecienNacido extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1188094409578117056L;

	private ArrayList<RegistroPartoRecienNacido> listaRegistros = new ArrayList<>();

	private Grid<RegistroPartoRecienNacido> grid = new Grid<RegistroPartoRecienNacido>();

	public CajaPartoRecienNacido() {
	}

	public CajaPartoRecienNacido(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
	}

	public CajaPartoRecienNacido(Paciente paciente, Long subambito) {
		super(paciente, subambito);
	}

	public CajaPartoRecienNacido(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		lbltitulo.setCaption("Partos Recién Nacidos ");
		grid.addColumn(RegistroPartoRecienNacido::getIdString).setCaption("Id");
		grid.addColumn(RegistroPartoRecienNacido::getFechaHoraNacimiento).setCaption("Fecha Naci");
		grid.addColumn(RegistroPartoRecienNacido::getPesostring).setCaption("Peso");
		grid.addColumn(RegistroPartoRecienNacido::getTallaString).setCaption("Talla");
		// grid.setHeaderVisible(false);
		grid.setBodyRowHeight(35);
		grid.setHeight("300px");
		contenedorGrid.addComponent(grid);
		refrescarClick();
	}

	public CajaPartoRecienNacido(Paciente paciente, EpisodioClase clase, Long subambito) {
		super(paciente, clase, subambito);
	}

	@Override
	public void nuevoClick() {
		RegistroPartoRecienNacido registro = new RegistroPartoRecienNacido();
		registro.setPaciente(paciente);
		registro.setProblema(proceso);
		VentanaFrm frmCtesFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoRecienNacido(registro),
				registro.getDescripcion());
		frmCtesFrm.addCloseListener(e -> cierraVentana());
		refrescarClick();
	}

	public void cierraVentana() {
		refrescarClick();
	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			RegistroPartoRecienNacido registro = grid.getSelectedItems().iterator().next();
			VentanaFrm frmDolorFrm = new VentanaFrm(this.getUI(), new FrmRegistroPartoRecienNacido(registro),
					registro.getDescripcion());
			frmDolorFrm.addCloseListener(e -> cierraVentana());
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	@Override
	public void refrescarClick() {
		listaRegistros = new RegistroPartoRecienNacidoDAO().getListaRegistros(proceso);
		contenedorBotones.setEnabled(true);
		if (listaRegistros.size() > 0) {
			grid.setItems(listaRegistros);
			grid.select(listaRegistros.get(0));
		}
		nuevo.setEnabled(true);
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {

	}

}
