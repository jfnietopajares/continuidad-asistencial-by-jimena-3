package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoPuerperio;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.TextField;

public class FrmRegistroPartoPuerperio extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4311484761384201032L;
	private TextField analgesia = new TextField("Analgesia");
	private TextField tas = new TextField("Tas");
	private TextField tad = new TextField("Tad");
	private TextField fc = new TextField("Fc");
	private TextField diuresis = new TextField("Diuresis");

	private TextField sangrado = new TextField("Sangrado");
	private TextField alturaFondoUtero = new TextField("Altura fondo uterino");
	private TextField dolor = new TextField("Dolor");
	private TextField medicacion = new TextField("Medicación");

	private TextField lactanciaMaterna = new TextField("Lactancia materna");
	private TextField pielConPiel = new TextField("Piel con Piel");

	private RegistroPartoPuerperio registroPuerperio = new RegistroPartoPuerperio();

	private Binder<RegistroPartoPuerperio> binder = new Binder<RegistroPartoPuerperio>();

	public FrmRegistroPartoPuerperio(Registro registro) {
		super();
		this.registroPuerperio = (RegistroPartoPuerperio) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4);
		lbltitulo.setCaption(registroPuerperio.getProblema().getId() + "/" + registroPuerperio.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		analgesia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		analgesia.setMaxLength(60);
		binder.forField(analgesia).bind(RegistroPartoPuerperio::getAnalgesiaString,
				RegistroPartoPuerperio::setAnalgesia);

		tas.setWidth("60px");
		tas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tas).withValidator(new ValidatorEntero(tas.getValue(), 5, 300))
				.bind(RegistroPartoPuerperio::getTasString, RegistroPartoPuerperio::setTas);

		tad.setWidth("60px");
		tad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tad).withValidator(new ValidatorEntero(tad.getValue(), 10, 300))
				.bind(RegistroPartoPuerperio::getTadString, RegistroPartoPuerperio::setTad);

		fc.setWidth("60px");
		fc.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(fc).withValidator(new ValidatorEntero(fc.getValue(), 20, 300))
				.bind(RegistroPartoPuerperio::getFcString, RegistroPartoPuerperio::setFc);

		diuresis.setWidth("100px");
		diuresis.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(diuresis).withValidator(new ValidatorEntero(fc.getValue(), 0, 3000))
				.bind(RegistroPartoPuerperio::getDiuresisString, RegistroPartoPuerperio::setDiuresis);
		fila1.addComponents(analgesia, tas, tad, fc, diuresis);

		sangrado.setWidth("450px");
		sangrado.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sangrado).bind(RegistroPartoPuerperio::getSangradoString, RegistroPartoPuerperio::setSangrado);

		alturaFondoUtero.setWidth("450px");
		alturaFondoUtero.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(alturaFondoUtero).bind(RegistroPartoPuerperio::getAlturaFondoUteroString,
				RegistroPartoPuerperio::setAlturaFondoUtero);

		fila2.addComponents(sangrado, alturaFondoUtero);
		dolor.setWidth("450px");
		dolor.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(dolor).bind(RegistroPartoPuerperio::getDolorString, RegistroPartoPuerperio::setDolor);

		medicacion.setWidth("450px");
		medicacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(medicacion).bind(RegistroPartoPuerperio::getMedicacionString,
				RegistroPartoPuerperio::setMedicacion);

		fila3.addComponents(dolor, medicacion);
		lactanciaMaterna.setWidth("450px");
		lactanciaMaterna.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(lactanciaMaterna).bind(RegistroPartoPuerperio::getLactanciaMaternaString,
				RegistroPartoPuerperio::setLactanciaMaterna);

		pielConPiel.setWidth("450px");
		pielConPiel.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(pielConPiel).bind(RegistroPartoPuerperio::getPielConPielString,
				RegistroPartoPuerperio::setPielConPiel);

		fila4.addComponents(lactanciaMaterna, pielConPiel);

		binder.readBean(registroPuerperio);
		doActivaBotones(registroPuerperio);

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroPuerperio);
			if (!new RegistroDAO().grabaDatos(registroPuerperio)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
