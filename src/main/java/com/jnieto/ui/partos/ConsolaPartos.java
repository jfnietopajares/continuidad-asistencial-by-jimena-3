package com.jnieto.ui.partos;

import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoAlumbramiento;
import com.jnieto.entity.RegistroPartoBuenasPracticas;
import com.jnieto.entity.RegistroPartoExpulsivo;
import com.jnieto.entity.RegistroPartoGestacion;
import com.jnieto.entity.RegistroPartoInduccion;
import com.jnieto.entity.RegistroPartoIngreso;
import com.jnieto.entity.RegistroPartoPuerperio;
import com.jnieto.ui.CajaProcesos;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.PanelPaciente;
import com.jnieto.ui.master.CajaMasterRegistro;
import com.jnieto.ui.master.ConsolaMaster;

public class ConsolaPartos extends ConsolaMaster {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5012787581646174921L;
	private Registro registroGestacion = new Registro();
	private Registro registroIngreso = new Registro();
	private Registro registroInduccion = new Registro();
	private Registro registroExpulsivo = new Registro();
	private Registro registroAlumbramiento = new Registro();
	private Registro registroPuerperio = new Registro();
	private Registro registroBuenaspracticas = new Registro();

//	private Proceso proceso = new Proceso();

	private PanelPaciente cajaPaciente;
	private CajaProcesos cajaProcesos;

	private CajaMasterRegistro cajaGestacion = new CajaMasterRegistro();
	private CajaMasterRegistro cajaIngreso = new CajaMasterRegistro();
	private CajaMasterRegistro cajaInduccion = new CajaMasterRegistro();
	private CajaPartoAlivioDolor cajaDolor = new CajaPartoAlivioDolor();
	private CajaPartoConstantes cajaCtes = new CajaPartoConstantes();
	private CajaMasterRegistro cajaExpulsivo = new CajaMasterRegistro();
	private CajaMasterRegistro cajaAlumbramiento = new CajaMasterRegistro();
	private CajaPartoMedicacion cajaMedicacion = new CajaPartoMedicacion();
	private CajaMasterRegistro cajaPuerperio = new CajaMasterRegistro();
	private CajaMasterRegistro cajaBuenasPracticas = new CajaMasterRegistro();
	private CajaPartoRecienNacido cajaRecienNacido = new CajaPartoRecienNacido();

	public static String NOMBREPACKAGE = "com.jnieto.ui.partos.";

	public ConsolaPartos(Paciente pacientePara) {
		super(pacientePara, pacientePara.getProcesoActual());

		if (paciente == null) {
			new NotificacionInfo("Sin elegir paciente");
			return;
		}
		cajaPaciente = new PanelPaciente(paciente);
		cajaProcesos = new CajaProcesos(paciente, Proceso.SUBAMBITO_PARTOS);
		cajaProcesos.addLayoutClickListener(e -> cambiaProceso());

		cajaGestacion.setEnabled(false);
		cajaIngreso.setEnabled(false);
		cajaInduccion.setEnabled(false);
		cajaExpulsivo.setEnabled(false);
		cajaAlumbramiento.setEnabled(false);
		cajaPuerperio.setEnabled(false);
		cajaBuenasPracticas.setEnabled(false);
		// grid
		cajaDolor.setEnabled(false);
		cajaCtes.setEnabled(false);
		cajaMedicacion.setEnabled(false);
		cajaRecienNacido.setEnabled(false);

		fila1.addComponents(cajaPaciente, cajaGestacion, cajaIngreso);
		fila2.addComponents(cajaProcesos, cajaInduccion, cajaDolor);
		fila3.addComponents(cajaCtes, cajaExpulsivo, cajaAlumbramiento);
		fila4.addComponents(cajaMedicacion, cajaPuerperio, cajaBuenasPracticas);
		fila5.addComponents(cajaRecienNacido);

		proceso = cajaProcesos.getProceso();
		if (proceso != null) {
			doConsolaPartos();
		}
	}

	/**
	 * Cada vez que hace click en la cajaProcesos comprueba si ha seleccionado un
	 * proceso diferente en cuyo caso refresca todas las cajas de la consola con la
	 * información asociada al nuevo proceso
	 * 
	 */
	public void cambiaProceso() {

		if (!cajaProcesos.getProceso().equals(proceso)) {
			proceso = cajaProcesos.getProceso();
			// if (proceso != null) {
			paciente.setProcesoActual(proceso);
			doIniciaCajas();
			doConsolaPartos();
			// }
		}
	}

	public void doConsolaPartos() {

		cajaPaciente = new PanelPaciente(paciente);

		registroGestacion = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoGestacion.PLANTILLLA_EDITOR_PAR_GESTACION);

		if (registroGestacion == null) {
			registroGestacion = new RegistroPartoGestacion();
			registroGestacion.setPaciente(this.paciente);
			registroGestacion.setProblema(proceso);
		}
		cajaGestacion = new CajaMasterRegistro((Registro) registroGestacion, "FrmRegistroPartoGestacion",
				"RegistroPartoGestacion", NOMBREPACKAGE + "FrmRegistroPartoGestacion");

		registroIngreso = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoIngreso.PLANTILLLA_EDITOR_PAR_INGRESO);

		if (registroIngreso == null) {
			registroIngreso = new RegistroPartoIngreso();
			registroIngreso.setPaciente(this.paciente);
			registroIngreso.setProblema(this.proceso);
		}
		cajaIngreso = new CajaMasterRegistro(registroIngreso, "FrmRegistroPartoIngreso", "RegistroPartoIngreso",
				NOMBREPACKAGE + "FrmRegistroPartoIngreso");

		registroInduccion = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoInduccion.PLANTILLLA_EDITOR_PAR_INDUCCION);
		if (registroInduccion == null) {
			registroInduccion = new RegistroPartoInduccion();
			registroInduccion.setPaciente(this.paciente);
			registroInduccion.setProblema(this.proceso);
		}
		cajaInduccion = new CajaMasterRegistro(registroInduccion, "FrmRegistroPartoInduccion", "RegistroPartoInduccion",
				NOMBREPACKAGE + "FrmRegistroPartoInduccion");

		registroExpulsivo = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoExpulsivo.PLANTILLLA_EDITOR_PAR_EXPULSIVO);
		if (registroExpulsivo == null) {
			registroExpulsivo = new RegistroPartoExpulsivo();
			registroExpulsivo.setPaciente(this.paciente);
			registroExpulsivo.setProblema(this.proceso);
		}
		cajaExpulsivo = new CajaMasterRegistro(registroExpulsivo, "FrmRegistroPartoExpulsivo", "RegistroPartoExpulsivo",
				NOMBREPACKAGE + "FrmRegistroPartoExpulsivo");

		registroAlumbramiento = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoAlumbramiento.PLANTILLLA_EDITOR_PAR_ALUMBRAMIENTO);
		if (registroAlumbramiento == null) {
			registroAlumbramiento = new RegistroPartoAlumbramiento();
			registroAlumbramiento.setPaciente(this.paciente);
			registroAlumbramiento.setProblema(this.proceso);
		}
		cajaAlumbramiento = new CajaMasterRegistro(registroAlumbramiento, "FrmRegistroPartoAlumbramiento",
				"RegistroPartoAlumbramiento", NOMBREPACKAGE + "FrmRegistroPartoAlumbramiento");

		registroPuerperio = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoPuerperio.PLANTILLLA_EDITOR_PAR_PUERPERIO);
		if (registroPuerperio == null) {
			registroPuerperio = new RegistroPartoPuerperio();
			registroPuerperio.setPaciente(this.paciente);
			registroPuerperio.setProblema(this.proceso);
		}
		cajaPuerperio = new CajaMasterRegistro(registroPuerperio, "FrmRegistroPartoPuerperio", "RegistroPartoPuerperio",
				NOMBREPACKAGE + "FrmRegistroPartoPuerperio");

		registroBuenaspracticas = new RegistroDAO().getLisRegistro(paciente, proceso,
				RegistroPartoBuenasPracticas.PLANTILLLA_EDITOR_PAR_BUENASPRACTICAS);
		if (registroBuenaspracticas == null) {
			registroBuenaspracticas = new RegistroPartoBuenasPracticas();
			registroBuenaspracticas.setPaciente(this.paciente);
			registroBuenaspracticas.setProblema(this.proceso);
		}
		cajaBuenasPracticas = new CajaMasterRegistro(registroBuenaspracticas, "FrmRegistroPartoBuenasPracticas",
				"RegistroPartoBuenasPracticas", NOMBREPACKAGE + "FrmRegistroPartoBuenasPracticas");

		// cajas GRID
		cajaDolor = new CajaPartoAlivioDolor(paciente, proceso);
		cajaCtes = new CajaPartoConstantes(paciente, proceso);
		cajaMedicacion = new CajaPartoMedicacion(paciente, proceso);
		cajaRecienNacido = new CajaPartoRecienNacido(paciente, proceso);

		fila1.removeAllComponents();
		fila2.removeAllComponents();
		fila3.removeAllComponents();
		fila4.removeAllComponents();
		fila5.removeAllComponents();

		fila1.addComponents(cajaPaciente, cajaGestacion, cajaIngreso);
		fila2.addComponents(cajaProcesos, cajaInduccion, cajaDolor);
		fila3.addComponents(cajaCtes, cajaExpulsivo, cajaAlumbramiento);
		fila4.addComponents(cajaMedicacion, cajaPuerperio, cajaBuenasPracticas);
		fila5.addComponents(cajaRecienNacido);

	}

	@Override
	public void refrescar() {
		doConsolaPartos();
	}

	public Proceso getProceso() {
		return proceso;
	}

	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
		doConsolaPartos();
	}

	public void doIniciaCajas() {
		cajaGestacion = new CajaMasterRegistro();
		cajaIngreso = new CajaMasterRegistro();
		cajaInduccion = new CajaMasterRegistro();
		cajaDolor = new CajaPartoAlivioDolor();
		cajaCtes = new CajaPartoConstantes();
		cajaExpulsivo = new CajaMasterRegistro();
		cajaAlumbramiento = new CajaMasterRegistro();
		cajaMedicacion = new CajaPartoMedicacion();
		cajaPuerperio = new CajaMasterRegistro();
		cajaBuenasPracticas = new CajaMasterRegistro();
		cajaRecienNacido = new CajaPartoRecienNacido();

	}
}
