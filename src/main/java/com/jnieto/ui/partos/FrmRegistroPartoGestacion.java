package com.jnieto.ui.partos;

import java.time.Period;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Edad;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoGestacion;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class FrmRegistroPartoGestacion extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4412454493885015440L;

	private VerticalLayout contenerdorEnfermedadMaterna = new VerticalLayout();
	private VerticalLayout contenerdorPatologiaFetal = new VerticalLayout();
	private TextField edad = new TextField("Edad");
	private TextField edadGestacional = new TextField("E.Gestacional");
	private DateField fechaUltimaRegla;
	private DateField fechaprobableParto;

	private TextField paridad = new TextField("Paridad");
	private ComboBox<String> comoboGrupoABO;
	private TextField alergia = new TextField("Alergia");
	private ComboBox<String> eco20semanas;

	private CheckBox patoMatNo = new CheckBox("No", true);
	private CheckBox patoMatTrombofilia = new CheckBox("Trombofilia", true);
//	private CheckBox patoMatDiabetesGestional = new CheckBox("Diabetes gestacional", true);
	private CheckBox TVP = new CheckBox("TVP", true);
	private CheckBox DMPG = new CheckBox("DMPG", true);
	private CheckBox DMG = new CheckBox("DMG", true);
	private CheckBox EII = new CheckBox("EII", true);
	private CheckBox reumatogicas = new CheckBox("Reuma.", true);
	private CheckBox cancer = new CheckBox("Cáncer", true);
	private CheckBox IPuterinas_95 = new CheckBox("IPuterinas>95", true);
	private CheckBox AR = new CheckBox("AR", true);
	private CheckBox HTA = new CheckBox("HTA", true);
	private CheckBox APPE = new CheckBox("APPE", true);
	private CheckBox Epilepsia = new CheckBox("Epilepsia", true);
	private CheckBox fetomuerto = new CheckBox("Feto Muero", true);
	private TextField patoMatOtros = new TextField("");

	private CheckBox patoFetoNo = new CheckBox("No", true);
	private CheckBox patoFetoDefCremiento = new CheckBox("Cremiento", true);
	private CheckBox patoFetoAlteracionLiq = new CheckBox("Líquido Amiótico", true);
	private CheckBox patoFetoMalformacion = new CheckBox("Malformación", true);
	private CheckBox patoFetoHidramnios = new CheckBox("Hidramnios", true);
	private CheckBox patoFetoOligoamnios = new CheckBox("Oligoamnios", true);
	private TextField patoFetoOtros = new TextField("");

	private ComboBox<String> sero1TRubeola = new ObjetosClinicos().getComboString("Rubeola 1ºT", "",
			ObjetosClinicos.PARTOS_GESTACION_SEROLO_RUBEOLA, "110px");
	private TextField sero1Tsifilis = new TextField("Sífilis");
	private TextField sero1TToxo = new TextField("Toxiplasmosis");
	private ComboBox<String> sero1TVHB = new ObjetosClinicos().getComboString("VHB 1ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");
	private ComboBox<String> sero1TVHC = new ObjetosClinicos().getComboString("VHC 1ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");
	private ComboBox<String> sero1TVIH = new ObjetosClinicos().getComboString("VIH 1ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");

	private ComboBox<String> sero3TRubeola = new ObjetosClinicos().getComboString("Rubeola 3ºT", "",
			ObjetosClinicos.PARTOS_GESTACION_SEROLO_RUBEOLA, "100px");
	private TextField sero3Tsifilis = new TextField("Sífilis");
	private TextField sero3TToxo = new TextField("Toxiplasmosis");
	private ComboBox<String> sero3TVHB = new ObjetosClinicos().getComboString("VHB 3ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");
	private ComboBox<String> sero3TVHC = new ObjetosClinicos().getComboString("VHC 3ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");
	private ComboBox<String> sero3TVIH = new ObjetosClinicos().getComboString("VIH 3ºT", "",
			ObjetosClinicos.POSITIVO_NEGATIVO, "110px");

	private DateField esteGrupoBFecha;
	private ComboBox<String> esteGrupoBValor;

	private ComboBox<String> tecReproAsist;
	private ComboBox<String> gemelar;
	private TextField numeroFetos = new TextField("N.Fetos");

	private RegistroPartoGestacion registroGestacion = new RegistroPartoGestacion();

	Binder<RegistroPartoGestacion> binder = new Binder<RegistroPartoGestacion>();

	public FrmRegistroPartoGestacion(Registro registro) {
		super();
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		HorizontalLayout fila31 = new HorizontalLayout();
		HorizontalLayout fila32 = new HorizontalLayout();
		fila31.setMargin(false);
		fila32.setMargin(false);
		fila3.setMargin(false);
		fila31.setSpacing(false);
		fila32.setSpacing(false);
		fila3.setSpacing(false);

		contenerdorEnfermedadMaterna.setMargin(false);
		contenerdorEnfermedadMaterna.setSpacing(false);
		contenerdorEnfermedadMaterna.setCaption("Enfermedad del la madre");
		contenerdorEnfermedadMaterna.addStyleName(MaterialTheme.PANEL_WELL);
		contenerdorEnfermedadMaterna.addComponents(fila31, fila32, fila3);

		HorizontalLayout fila41 = new HorizontalLayout();
		HorizontalLayout fila42 = new HorizontalLayout();
		fila41.setMargin(false);
		fila42.setMargin(false);
		fila4.setMargin(false);
		fila41.setSpacing(false);
		fila42.setSpacing(false);

		contenerdorPatologiaFetal.setMargin(false);
		contenerdorPatologiaFetal.setSpacing(false);
		contenerdorPatologiaFetal.setCaption("Patología del feto");
		contenerdorPatologiaFetal.addStyleName(MaterialTheme.PANEL_WELL);
		contenerdorPatologiaFetal.addComponents(fila4, fila41, fila42);

		contenedorCampos.addComponents(fila1, fila2, contenerdorEnfermedadMaterna, contenerdorPatologiaFetal, fila5,
				fila6, fila7);
		this.registroGestacion = (RegistroPartoGestacion) registro;
		// this.fila3.setCaption("Patología Materna");
		this.fila3.addStyleName(MaterialTheme.PANEL_WELL);
		fila31.addStyleName(MaterialTheme.PANEL_WELL);
		fila32.addStyleName(MaterialTheme.PANEL_WELL);

		this.fila4.addStyleName(MaterialTheme.PANEL_WELL);
		this.fila5.setCaption("Serología 1º trimestre");
		this.fila5.addStyleName(MaterialTheme.PANEL_WELL);
		this.fila6.setCaption("Serología 3º trimestre");
		this.fila6.addStyleName(MaterialTheme.PANEL_WELL);
		lbltitulo.setCaption(registroGestacion.getProblema().getId() + "/" + registroGestacion.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		edad.setWidth("40px");
		edad.setMaxLength(3);
		edad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(edad).withValidator(new ValidatorEntero(edad.getValue(), 10, 60))
				.bind(RegistroPartoGestacion::getEdadString, RegistroPartoGestacion::setEdad);

		edadGestacional.setWidth("60px");
		edadGestacional.setMaxLength(5);
		edadGestacional.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(edadGestacional).bind(RegistroPartoGestacion::getEdadGestacionalString,
				RegistroPartoGestacion::setEdadGestacional);

		fechaUltimaRegla = new ObjetosComunes().getFecha("FUR", "fecha u regla");
		fechaUltimaRegla.addBlurListener(e -> saltaFUR());
		binder.forField(fechaUltimaRegla).bind(RegistroPartoGestacion::getFechaUltimaReglaDate,
				RegistroPartoGestacion::setFechaUltimaRegla);

		fechaprobableParto = new ObjetosComunes().getFecha("FPP", "fecha p. parto");
		binder.forField(fechaprobableParto).bind(RegistroPartoGestacion::getFechaprobablePartoDate,
				RegistroPartoGestacion::setFechaprobableParto);

		paridad.setWidth("150px");
		paridad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(paridad).bind(RegistroPartoGestacion::getParidadString, RegistroPartoGestacion::setParidad);

		comoboGrupoABO = new ObjetosClinicos().getComboString("Grupo ABO", registroGestacion.getGrupoABOString(),
				ObjetosClinicos.GRUPO_ABO, "80px");
		binder.forField(comoboGrupoABO).bind(RegistroPartoGestacion::getGrupoABOString,
				RegistroPartoGestacion::setGrupoABO);
		fila1.addComponents(edad, edadGestacional, fechaUltimaRegla, fechaprobableParto, paridad, comoboGrupoABO);

		alergia.setWidth("340px");
		alergia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(alergia).bind(RegistroPartoGestacion::getAlergiaString, RegistroPartoGestacion::setAlergia);

		eco20semanas = new ObjetosClinicos().getComboString("Eco 20 sem", registroGestacion.getEco20semanasString(),
				ObjetosClinicos.PARTOS_GESTA_ECO20SEMANAS, "150px");
		binder.forField(eco20semanas).bind(RegistroPartoGestacion::getEco20semanasString,
				RegistroPartoGestacion::setEco20semanas);

		fila2.addComponents(alergia, eco20semanas);

		// patoMatNo.setValue(registroGestacion.getPatoMatNoBoolean());
		patoMatNo.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoMatNo.addValueChangeListener(e -> saltaPatoMatNo());
		binder.forField(patoMatNo).bind(RegistroPartoGestacion::getPatoMatNoBoolean,
				RegistroPartoGestacion::setPatoMatNo);

		patoMatTrombofilia.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoMatTrombofilia.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(patoMatTrombofilia).bind(RegistroPartoGestacion::getPatoMatTrombofiliaBoolean,
				RegistroPartoGestacion::setPatoMatTrombofilia);

		// patoMatDiabetesGestional.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		// binder.forField(patoMatDiabetesGestional).bind(RegistroPartoGestacion::getPatoMatDiabetesGestionalBoolean,
		// RegistroPartoGestacion::setPatoMatDiabetesGestional);

		TVP.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		TVP.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(TVP).bind(RegistroPartoGestacion::getTVPBoolean, RegistroPartoGestacion::setTVP);

		DMPG.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		DMPG.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(DMPG).bind(RegistroPartoGestacion::getDMPGBooelan, RegistroPartoGestacion::setDMPG);

		DMG.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		DMG.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(DMG).bind(RegistroPartoGestacion::getDMGBooelan, RegistroPartoGestacion::setDMG);

		EII.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		EII.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(EII).bind(RegistroPartoGestacion::getEIIBoolean, RegistroPartoGestacion::setEII);

		reumatogicas.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		reumatogicas.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(reumatogicas).bind(RegistroPartoGestacion::getReumatogicasBoolean,
				RegistroPartoGestacion::setReumatogicas);

		cancer.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		cancer.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(cancer).bind(RegistroPartoGestacion::getCancerBoolean, RegistroPartoGestacion::setCancer);

		IPuterinas_95.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		IPuterinas_95.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(IPuterinas_95).bind(RegistroPartoGestacion::getIPuterinas_95Boolean,
				RegistroPartoGestacion::setIPuterinas_95);

		fila31.addComponents(patoMatNo, patoMatTrombofilia, TVP, DMPG, DMG, EII, reumatogicas, cancer, IPuterinas_95);

		AR.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		AR.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(AR).bind(RegistroPartoGestacion::getARBoolean, RegistroPartoGestacion::setAR);

		HTA.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		HTA.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(HTA).bind(RegistroPartoGestacion::getHTABoolean, RegistroPartoGestacion::setHTA);

		APPE.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		APPE.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(APPE).bind(RegistroPartoGestacion::getAPPEBoolean, RegistroPartoGestacion::setAPPE);

		Epilepsia.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		Epilepsia.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(Epilepsia).bind(RegistroPartoGestacion::getEpilepsiaBoolean,
				RegistroPartoGestacion::setEpilepsia);

		fetomuerto.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		fetomuerto.addValueChangeListener(e -> saltaPatoMatCheck());
		binder.forField(fetomuerto).bind(RegistroPartoGestacion::getFetomuertoBoolean,
				RegistroPartoGestacion::setFetomuerto);
		patoMatOtros.setCaption("Otras patologías de la madre");
		patoMatOtros.setWidth("300px");
		fetomuerto.addValueChangeListener(e -> saltaPatoMatCheck());
		patoMatOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(patoMatOtros).bind(RegistroPartoGestacion::getPatoMatOtrosString,
				RegistroPartoGestacion::setPatoMatOtros);

		fila32.addComponents(AR, HTA, APPE, Epilepsia, fetomuerto, patoMatOtros);

		// fila3.addComponents(patoMatOtros);

		patoFetoNo.setValue(registroGestacion.getPatoFetoNoBoolean());
		patoFetoNo.addValueChangeListener(e -> saltaPatoFetoNo());
		patoFetoNo.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(patoFetoNo).bind(RegistroPartoGestacion::getPatoFetoNoBoolean,
				RegistroPartoGestacion::setPatoFetoNo);

		patoFetoDefCremiento.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoFetoDefCremiento.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoDefCremiento).bind(RegistroPartoGestacion::getPatoFetoDefCremientoBoolean,
				RegistroPartoGestacion::setPatoFetoDefCremiento);

		patoFetoAlteracionLiq.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoFetoAlteracionLiq.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoAlteracionLiq).bind(RegistroPartoGestacion::getPatoFetoAlteracionLiqBoolean,
				RegistroPartoGestacion::setPatoFetoAlteracionLiq);

		patoFetoMalformacion.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoFetoMalformacion.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoMalformacion).bind(RegistroPartoGestacion::getPatoFetoMalformacionBoolean,
				RegistroPartoGestacion::setPatoFetoMalformacion);

		patoFetoHidramnios.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoFetoHidramnios.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoHidramnios).bind(RegistroPartoGestacion::getPatoFetoHidramniosoBoolean,
				RegistroPartoGestacion::setPatoFetoHidramnios);

		patoFetoOligoamnios.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		patoFetoOligoamnios.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoOligoamnios).bind(RegistroPartoGestacion::getPatoFetoOligoamniosolBoolean,
				RegistroPartoGestacion::setPatoFetoOligoamnios);

		fila4.addComponents(patoFetoNo, patoFetoDefCremiento, patoFetoAlteracionLiq, patoFetoMalformacion,
				patoFetoHidramnios, patoFetoOligoamnios);
		patoFetoOtros = new TextField("Otras patologías del feto");
		patoFetoOtros.setWidth("350px");
		patoFetoOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		patoFetoOtros.addValueChangeListener(e -> saltaPatoFetoCheck());
		binder.forField(patoFetoOtros).bind(RegistroPartoGestacion::getPatoFetoOtrosString,
				RegistroPartoGestacion::setPatoFetoOtros);
		fila41.addComponents(patoFetoOtros);

		// sero1TRubeola.setWidth("100px");
		// sero1TRubeola.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1TRubeola).bind(RegistroPartoGestacion::getSero1TRubeolaString,
				RegistroPartoGestacion::setSero1TRubeola);

		sero1Tsifilis.setWidth("100px");
		sero1Tsifilis.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1Tsifilis).bind(RegistroPartoGestacion::getSero1TsifilisString,
				RegistroPartoGestacion::setSero1Tsifilis);

		sero1TToxo.setWidth("100px");
		sero1TToxo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1TToxo).bind(RegistroPartoGestacion::getSero1TToxoString,
				RegistroPartoGestacion::setSero1TToxo);

		// sero1TVHB.setWidth("100px");
		// sero1TVHB.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1TVHB).bind(RegistroPartoGestacion::getSero1TVHBString,
				RegistroPartoGestacion::setSero1TVHB);

		// sero1TVHC.setWidth("100px");
		// sero1TVHC.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1TVHC).bind(RegistroPartoGestacion::getSero1TVHCString,
				RegistroPartoGestacion::setSero1TVHC);

		// sero1TVIH.setWidth("100px");
		// sero1TVIH.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero1TVIH).bind(RegistroPartoGestacion::getSero1TVIHString,
				RegistroPartoGestacion::setSero1TVIH);
		fila5.addComponents(sero1TRubeola, sero1Tsifilis, sero1TToxo, sero1TVHB, sero1TVHC, sero1TVIH);

		// sero3TRubeola.setWidth("100px");
		// sero3TRubeola.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3TRubeola).bind(RegistroPartoGestacion::getSero3TRubeolaString,
				RegistroPartoGestacion::setSero3TRubeola);

		sero3Tsifilis.setWidth("100px");
		sero3Tsifilis.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3Tsifilis).bind(RegistroPartoGestacion::getSero3TsifilisString,
				RegistroPartoGestacion::setSero3Tsifilis);

		sero3TToxo.setWidth("100px");
		sero3TToxo.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3TToxo).bind(RegistroPartoGestacion::getSero3TToxoString,
				RegistroPartoGestacion::setSero3TToxo);

		// sero3TVHB.setWidth("100px");
		// sero3TVHB.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3TVHB).bind(RegistroPartoGestacion::getSero3TVHBString,
				RegistroPartoGestacion::setSero3TVHB);

		// sero3TVHC.setWidth("100px");
		// sero3TVHC.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3TVHC).bind(RegistroPartoGestacion::getSero3TVHCString,
				RegistroPartoGestacion::setSero3TVHC);

		// sero3TVIH.setWidth("100px");
		// sero3TVIH.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sero3TVIH).bind(RegistroPartoGestacion::getSero3TVIHString,
				RegistroPartoGestacion::setSero3TVIH);
		fila6.addComponents(sero3TRubeola, sero3Tsifilis, sero3TToxo, sero3TVHB, sero3TVHC, sero3TVIH);

		esteGrupoBFecha = new ObjetosComunes().getFecha("Fecha SGB", " fecha SGB");
		binder.forField(esteGrupoBFecha).bind(RegistroPartoGestacion::getEsteGrupoBFechaDate,
				RegistroPartoGestacion::setEsteGrupoBFecha);

		esteGrupoBValor = new ObjetosClinicos().getComboString("Valor SGB", "", ObjetosClinicos.PARTOS_GESTA_VALORSGB,
				"150px");
		binder.forField(esteGrupoBValor).bind(RegistroPartoGestacion::getEsteGrupoBValorString,
				RegistroPartoGestacion::setEsteGrupoBValor);

		tecReproAsist = new ObjetosClinicos().getComboString("TRA", "", ObjetosClinicos.PARTOS_GESTA_TECREPROASIST,
				"200px");
		binder.forField(tecReproAsist).bind(RegistroPartoGestacion::getTecReproAsistString,
				RegistroPartoGestacion::setTecReproAsist);

		gemelar = new ObjetosClinicos().getComoSiNo("Gemelar", "");
		binder.forField(gemelar).bind(RegistroPartoGestacion::getGemelarString, RegistroPartoGestacion::setGemelar);

		numeroFetos.setWidth("50px");
		numeroFetos.setMaxLength(1);
		numeroFetos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(numeroFetos).withValidator(new ValidatorEntero(numeroFetos.getValue(), 1, 8))
				.bind(RegistroPartoGestacion::getNumeroFetosString, RegistroPartoGestacion::setNumeroFetos);
		fila7.addComponents(esteGrupoBFecha, esteGrupoBValor, tecReproAsist, gemelar, numeroFetos);

		binder.readBean(registroGestacion);
		if (paridad.getValue().isEmpty())
			paridad.setValue("G  P  Pp  Ab  ");

		if (edad.getValue().isEmpty()) {
			edad.setValue(Integer.toString(new Edad(registroGestacion.getPaciente().getFnac()).getEdadAnos()));
		}

		// this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones(registroGestacion);
	}

	public void saltaFUR() {
		if (fechaprobableParto.isEmpty()) {
			fechaprobableParto.setValue(fechaUltimaRegla.getValue().plus(Period.ofDays(294)));
		}
	}

	public void saltaPatoMatNo() {
		if (patoMatNo.getValue() == true) {
			patoMatTrombofilia.setValue(false);
			patoMatTrombofilia.setValue(false);
			TVP.setValue(false);
			DMPG.setValue(false);
			DMG.setValue(false);
			EII.setValue(false);
			reumatogicas.setValue(false);
			cancer.setValue(false);
			IPuterinas_95.setValue(false);
			AR.setValue(false);
			HTA.setValue(false);
			APPE.setValue(false);
			Epilepsia.setValue(false);
			fetomuerto.setValue(false);
			patoMatOtros.clear();
			patoFetoNo.focus();
		}
	}

	public void saltaPatoMatCheck() {
		if (patoMatTrombofilia.getValue() == true || TVP.getValue() == true || DMPG.getValue() == true
				|| DMG.getValue() == true || EII.getValue() == true || reumatogicas.getValue() == true
				|| cancer.getValue() == true || IPuterinas_95.getValue() == true || AR.getValue() == true
				|| HTA.getValue() == true || APPE.getValue() == true || Epilepsia.getValue() == true
				|| fetomuerto.getValue() == true || !patoMatOtros.isEmpty()) {
			patoMatNo.setValue(false);
		}
	}

	public void saltaPatoFetoNo() {
		if (patoFetoNo.getValue() == true) {
			patoFetoAlteracionLiq.setValue(false);
			patoFetoDefCremiento.setValue(false);
			patoFetoMalformacion.setValue(false);
			patoFetoHidramnios.setValue(false);
			patoFetoOligoamnios.setValue(false);
			patoFetoOtros.clear();
			sero1TRubeola.focus();
		}
	}

	public void saltaPatoFetoCheck() {
		if (patoFetoAlteracionLiq.getValue() == true || patoFetoDefCremiento.getValue() == true
				|| patoFetoMalformacion.getValue() == true || patoFetoHidramnios.getValue() == true
				|| patoFetoOligoamnios.getValue() == true || !patoFetoOtros.isEmpty()) {
			patoFetoNo.setValue(false);
		}
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroGestacion);
			if (!new RegistroDAO().grabaDatos(registroGestacion)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
				// new NotificacionInfo(this.getParent().getParent().getClass().getTypeName());
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
