package com.jnieto.ui.partos;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoAlumbramiento;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;

public class FrmRegistroPartoAlumbramiento extends FrmRegistroParto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1965236078345018404L;

	private ComboBox<String> tipoParto;
	private ComboBox<String> placenta;
	private ComboBox<String> membranasFetales;
	private ComboBox<String> cordonVasos;
	private ComboBox<String> cordonInsercion;
	private ComboBox<String> cordonDonacion;
	private ComboBox<String> hemorragiaCantidad;
	private ComboBox<String> hemorragiaSingos;
	private ComboBox<String> hemorragiaTratamiento;

	private RegistroPartoAlumbramiento registroAlumbramiento = new RegistroPartoAlumbramiento();

	private Binder<RegistroPartoAlumbramiento> binder = new Binder<RegistroPartoAlumbramiento>();

	public FrmRegistroPartoAlumbramiento(Registro registro) {
		super();
		this.registroAlumbramiento = (RegistroPartoAlumbramiento) registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3);
		this.fila2.setCaption("Cordón ");
		fila2.addStyleName(MaterialTheme.LAYOUT_WELL);
		this.fila3.setCaption("Hemorragia ");
		fila3.addStyleName(MaterialTheme.LAYOUT_WELL);

		lbltitulo.setCaption(registroAlumbramiento.getProblema().getId() + "/" + registroAlumbramiento.getId());
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		tipoParto = new ObjetosClinicos().getComboString("Tipo parto", "", ObjetosClinicos.PARTOS_ALUMBRA_TIPOPARTO,
				"120px");
		binder.forField(tipoParto).bind(RegistroPartoAlumbramiento::getTipoPartoString,
				RegistroPartoAlumbramiento::setTipoParto);

		placenta = new ObjetosClinicos().getComboString("Placenta", "", ObjetosClinicos.PARTOS_ALUMBRA_PLACENTA,
				"120px");
		binder.forField(placenta).bind(RegistroPartoAlumbramiento::getPlacentaString,
				RegistroPartoAlumbramiento::setPlacenta);

		membranasFetales = new ObjetosClinicos().getComboString("Membranas", "",
				ObjetosClinicos.PARTOS_ALUMBRA_MEMBRANAS, "120px");
		binder.forField(membranasFetales).bind(RegistroPartoAlumbramiento::getMembranasFetalesString,
				RegistroPartoAlumbramiento::setMembranasFetales);
		fila1.addComponents(tipoParto, placenta, membranasFetales);

		cordonVasos = new ObjetosClinicos().getComboString("Cordón vasos", "",
				ObjetosClinicos.PARTOS_ALUMBRA_CORDONVASOS, "120px");
		binder.forField(cordonVasos).bind(RegistroPartoAlumbramiento::getCordonVasosString,
				RegistroPartoAlumbramiento::setCordonVasos);

		cordonInsercion = new ObjetosClinicos().getComboString("Cordón inserción", "",
				ObjetosClinicos.PARTOS_ALUMBRA_CORDONINSERCION, "120px");
		binder.forField(cordonInsercion).bind(RegistroPartoAlumbramiento::getCordonInsercionString,
				RegistroPartoAlumbramiento::setCordonInsercion);

		cordonDonacion = new ObjetosClinicos().getComboString("Donación", "",
				ObjetosClinicos.PARTOS_ALUMBRA_CORDONDONACION, "120px");
		binder.forField(cordonDonacion).bind(RegistroPartoAlumbramiento::getCordonInsercionString,
				RegistroPartoAlumbramiento::setCordonInsercion);

		fila2.addComponents(cordonVasos, cordonInsercion, cordonDonacion);

		hemorragiaCantidad = new ObjetosClinicos().getComboString("Cantidad", "",
				ObjetosClinicos.PARTOS_ALUMBRA_HEMORRAGIAVOLUMEN, "120px");
		binder.forField(hemorragiaCantidad).bind(RegistroPartoAlumbramiento::getHemorragiaCantidadString,
				RegistroPartoAlumbramiento::setHemorragiaCantidad);

		hemorragiaSingos = new ObjetosClinicos().getComoSiNo(" Singos clínicos", "");
		binder.forField(hemorragiaSingos).bind(RegistroPartoAlumbramiento::getHemorragiaSingosString,
				RegistroPartoAlumbramiento::setHemorragiaSingos);

		hemorragiaTratamiento = new ObjetosClinicos().getComboString("Tratamiento", "",
				ObjetosClinicos.PARTOS_ALUMBRA_HEMORRAGIATRATO, "120px");
		binder.forField(hemorragiaTratamiento).bind(RegistroPartoAlumbramiento::getHemorragiaSingosString,
				RegistroPartoAlumbramiento::setHemorragiaSingos);

		fila3.addComponents(hemorragiaCantidad, hemorragiaSingos, hemorragiaTratamiento);
		binder.readBean(registroAlumbramiento);
		doActivaBotones(registroAlumbramiento);

	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();

	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroAlumbramiento);
			if (!new RegistroDAO().grabaDatos(registroAlumbramiento)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
