package com.jnieto.ui;

import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.PeticionesDao;
import com.jnieto.entity.Peticion;
import com.jnieto.entity.Variable;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class PanelPeticion. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PanelPeticion extends CustomComponent {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6337431831377063299L;

	/**
	 * Instantiates a new panel peticion.
	 *
	 * @param peticion the peticion
	 */
	public PanelPeticion(Peticion peticion) {
		Panel panel = new Panel("Detalle Peticion." + peticion.getId());
		panel.addStyleName(MaterialTheme.CARD_HOVERABLE);
		VerticalLayout panelContenido = new VerticalLayout();
		// Set the size as undefined at all levels
		panelContenido.setSizeUndefined();
		panelContenido.setMargin(false);
		panel.setSizeUndefined();
		setSizeUndefined();
		// The composition root MUST be set
		Label fecha = new Label();
		if (peticion.getServicio() != null)
			fecha.setValue(
					"Fecha" + peticion.getFecha().toString() + " Servicio:" + peticion.getServicio().getCodigo());
		else
			fecha.setValue("Fecha" + peticion.getFecha().toString() + " Servicio: ?");

		panelContenido.addComponent(fecha);

		Label paciente = new Label();
		paciente.setValue(peticion.getPaciente().getNumerohc() + " " + peticion.getPaciente().getApellidosNombre());
		panelContenido.addComponent(paciente);

		if (peticion.getUserid() != null) {
			Label medico = new Label();
			medico.setValue(peticion.getUserid().getApellidosNombre());
			panelContenido.addComponent(medico);
		}
		ArrayList<Variable> listaVariables = new PeticionesDao().getVariablesPeticion(peticion.getId());
		for (Variable v : listaVariables) {
			Label etiqueta = new Label();
			etiqueta.setContentMode(ContentMode.HTML);
			etiqueta.setValue("<b>" + v.getDescripcion() + "</b>: " + v.getValor());
			panelContenido.addComponent(etiqueta);
		}

		panel.setContent(panelContenido);
		setCompositionRoot(panel);
	}

	/**
	 * Instantiates a new panel peticion.
	 *
	 * @param compositionRoot the composition root
	 */
	public PanelPeticion(Component compositionRoot) {
		super(compositionRoot);

	}

}
