package com.jnieto.ui;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.dao.ProcesoDAO;
import com.jnieto.dao.RegistroColonDAO;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.dao.RegistroMamaDAO;
import com.jnieto.dao.RegistroO2IncidenciaDao;
import com.jnieto.dao.RegistroOxiAerosolDAO;
import com.jnieto.dao.RegistroOxiDomiDAO;
import com.jnieto.dao.RegistroOxiHipercapmiaDAO;
import com.jnieto.dao.RegistroOxiMapneaDAO;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroColon;
import com.jnieto.entity.RegistroMama;
import com.jnieto.entity.RegistroO2Incidencia;
import com.jnieto.entity.RegistroOxiAerosol;
import com.jnieto.entity.RegistroOxiDomi;
import com.jnieto.entity.RegistroOxiHipercapnia;
import com.jnieto.entity.RegistroOxiMapnea;
import com.jnieto.ui.mama.FrmRegistroColon;
import com.jnieto.ui.mama.FrmRegistroMama;
import com.jnieto.ui.master.PantallaMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.ui.oxigeno.FrmRegistroOxiAerosol;
import com.jnieto.ui.oxigeno.FrmRegistroOxiDomi;
import com.jnieto.ui.oxigeno.FrmRegistroOxiHipercapmia;
import com.jnieto.ui.oxigeno.FrmRegistroOxiIncidencia;
import com.jnieto.ui.oxigeno.FrmRegistroOxiMapne;
import com.vaadin.ui.Button;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.renderers.LocalDateRenderer;

/**
 * The Class PantallaProcesos. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaProcesos extends PantallaMaster {

	private static final long serialVersionUID = 1L;

	protected Paciente paciente;

	protected Long subambito;

	protected Proceso proceso;

	protected Grid<Proceso> grid = new Grid<>();

	protected Grid<Registro> gridRegistros = new Grid<>();

	protected ArrayList<Registro> listaRegistros;

	protected Button botonRegistro, botonProceso;

	protected ArrayList<Proceso> listaProcesos;

	protected HorizontalLayout filaBotonesO2 = new HorizontalLayout();

	protected Button botonAerosol = null;

	protected Button botonIncidencias = null;

	protected Button botonApnea = null;

	protected Button botonDomiciliaria = null;

	protected Button botonHipercapnia = null;

	protected Registro registro = new Registro();
	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	/**
	 * Instantiates a new pantalla procesos.
	 *
	 * @param paciente  the paciente
	 * @param subambito the subambito
	 */
	public PantallaProcesos(Paciente paciente, Long subambito, Registro registro) {
		super(paciente);
		this.setMargin(false);
		filaBotonesO2.setMargin(false);
		contenedorDatosPaciente.setVisible(true);
		this.paciente = paciente;
		this.subambito = subambito;
		this.registro = registro;

		contenedorGrid.addStyleName(MaterialTheme.CARD_HOVERABLE);

		listaRegistros = new RegistroDAO().getListaRegistrosLight(subambito, paciente);

		gridRegistros.addColumn(Registro::getFechaHora).setCaption("Fecha");
		gridRegistros.addColumn(Registro::getDescripcion).setCaption("Descripcion");
		gridRegistros.setItems(listaRegistros);
		gridRegistros.setHeightByRows(4);
		gridRegistros.setWidth("700px");
		gridRegistros.asSingleSelect();
		gridRegistros.addItemClickListener(e -> seleccionaRegistro());

		contenedorDatosPaciente.addComponent(gridRegistros);

		listaProcesos = new ProcesoDAO().getListaProcesosPacienteSubambito(paciente, subambito);
		grid.addColumn(Proceso::getFechaini, new LocalDateRenderer("dd/MM/yyyy")).setCaption("Inicio");
		grid.addColumn(Proceso::getMotivo).setCaption("Motivo");
		grid.addColumn(Proceso::getFechafin, new LocalDateRenderer("dd/MM/yyyy")).setCaption("Fin");
		grid.setItems(listaProcesos);
		grid.asSingleSelect();
		grid.addItemClickListener(event -> selecciona());
		grid.setStyleGenerator(e -> getEstilo(e));
		this.seleccionaValorInicial();

		botonRegistro = new ObjetosComunes().getBotonrRegistros();
		botonRegistro.setEnabled(false);
		botonRegistro.addClickListener(event -> clicEditaRegistro(new Long(0)));

		botonProceso = new ObjetosComunes().getBotonProcesos();
		botonProceso.setEnabled(false);
		botonProceso.addClickListener(event -> clickEditaProceso());

		botonAerosol = new ObjetosComunes().getBotonrRegistros();
		botonAerosol.setCaption("Ae");
		botonAerosol.setDescription("Añade / edita un registro de aerosol terapia.");
		botonAerosol.addClickListener(event -> clicEditaRegistroAerosol(new Long(0)));

		botonApnea = new ObjetosComunes().getBotonrRegistros();
		botonApnea.setCaption("Ma");
		botonApnea.setDescription("Añade / edita un registro de monitorización de apnea.");
		botonApnea.addClickListener(event -> clicEditaRegistroMapnea(new Long(0)));

		botonDomiciliaria = new ObjetosComunes().getBotonrRegistros();
		botonDomiciliaria.setCaption("Do");
		botonDomiciliaria.setDescription("Añade / edita un registro de oxigenoterapia domiciliaria.");
		botonDomiciliaria.addClickListener(event -> clicEditaRegistroDomiciliaria(new Long(0)));

		botonHipercapnia = new ObjetosComunes().getBotonrRegistros();
		botonHipercapnia.setCaption("Hi");
		botonHipercapnia.setDescription("Añade / edita un registro de equipo hipercapmia.");
		botonHipercapnia.addClickListener(event -> clicEditaEquipoApoyo(new Long(0)));

		botonIncidencias = new ObjetosComunes().getBotonIncidencias();
		botonIncidencias.setCaption("");
		botonIncidencias.addClickListener(event -> cliclEditaIncidencias(new Long(0)));

		contenedorBotones.removeAllComponents();
		contenedorBotones.addComponents(new Label(Proceso.getDescripcionSubambito(subambito)), anadir, refrescar,
				botonProceso, botonRegistro, ayuda, cerrar);

		filaBotonesO2.addComponents(botonAerosol, botonApnea, botonDomiciliaria, botonHipercapnia, botonIncidencias);
		doControlBotones();
		contenedorGrid.addComponents(contenedorBotones, filaBotonesO2, grid, filaPaginacion);
		doAbrirFormulario();

	}

	private String getEstilo(Proceso proceso) {
		if (proceso.getFechafin() != null) {
			return "RED";
		}
		return null;
	}

	/**
	 * Buscar click.
	 */
	@Override
	public void buscarClick() {
	}

	/**
	 * Anadir click.
	 */
	@Override
	public void anadirClick() {
		FrmProceso registroProblema = new FrmProceso(
				new Proceso(new Long(0), paciente, this.subambito));
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(registroProblema);
		contenedorFormulario.setVisible(true);
	}

	/**
	 * Refrescar click.
	 */
	@Override
	public void refrescarClick() {
		contenedorGrid.setEnabled(true);
		gridRegistros.setEnabled(true);
		listaProcesos = new ProcesoDAO().getListaProcesosPacienteSubambito(paciente, this.subambito);
		grid.setItems(listaProcesos);
		if (listaProcesos.size() > 0) {
			grid.select(listaProcesos.get(0));
			this.proceso = listaProcesos.get(0);
			selecciona();
		}
		listaRegistros = new RegistroDAO().getListaRegistrosLight(subambito, paciente);
		gridRegistros.setItems(listaRegistros);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.setVisible(false);
		doControlBotones();
	}

	/**
	 * Selecciona.
	 */
	@Override
	public void selecciona() {
		if (grid.getSelectedItems().size() == 1) {
			doControlBotones();
		}
	}

	/**
	 * Click edita proceso.
	 */
	public void clickEditaProceso() {
		if (grid.getSelectedItems().size() == 1) {
			contenedorGrid.setEnabled(false);
			proceso = grid.getSelectedItems().iterator().next();
			new AccesoControlador().doAccesoUsuarioProceso(proceso);
			contenedorFormulario.removeAllComponents();
			contenedorFormulario.addComponent(new FrmProceso(proceso));
			contenedorFormulario.setVisible(true);
		} else {
			new NotificacionInfo(NotificacionInfo.PROCESO_PACIENTE_SIN_ELEIGR_PROCESO);
		}
	}

	/**
	 * Clic edita registro.
	 */
	private void clicEditaRegistro(Long idRegistroElegido) {
		Long idregistro;
		if (grid.getSelectedItems().size() == 1) {
			proceso = grid.getSelectedItems().iterator().next();
			if (proceso.getId() > 0) {
				contenedorGrid.setEnabled(false);
				gridRegistros.setEnabled(false);
				if (subambito == Proceso.SUBAMBITO_MAMA) {
					RegistroMama registromama;
					if (idRegistroElegido.equals(new Long(0))) {
						idregistro = new RegistroDAO().getRegistroPacienteProceso(paciente, proceso,
								RegistroMama.PLANTILLLA_EDITOR_REGISTROMAMA);
					} else {
						idregistro = idRegistroElegido;
					}
					if (idregistro == 0) {
						registromama = new RegistroMama(new Long(0));
						registromama.setPaciente(paciente);
						registromama.setProblema(proceso);
						registromama.setFechaap(proceso.getFechaini());
					} else {
						registromama = new RegistroMamaDAO().getRegistroId(idregistro);
						new AccesoControlador().doAccesoUsuarioRegistro(registromama);
					}
					contenedorFormulario.removeAllComponents();
					contenedorFormulario.addComponent(new FrmRegistroMama(registromama));
					contenedorFormulario.setVisible(true);
				} else if (subambito == Proceso.SUBAMBITO_COLON) {
					RegistroColon registrocolon;
					idregistro = new RegistroDAO().getRegistroPacienteProceso(paciente, proceso,
							RegistroColon.PLANTILLLA_EDITOR_REGISTROCOLON);
					if (idregistro == 0) {
						registrocolon = new RegistroColon();
						registrocolon.setPaciente(paciente);
						registrocolon.setProblema(proceso);
						registrocolon.setAnalitica(proceso.getFechaini());
					} else {
						registrocolon = new RegistroColonDAO().getRegistroId(idregistro);
						new AccesoControlador().doAccesoUsuarioRegistro(registrocolon);
					}
					contenedorFormulario.removeAllComponents();
					contenedorFormulario.addComponent(new FrmRegistroColon(registrocolon));
					contenedorFormulario.setVisible(true);
				}
			} else {
				new NotificacionInfo(
						NotificacionInfo.PROCESO_ERROR_SUBAMBITO + "  " + Proceso.SUBAMBITO_MAMA + " " + subambito);
			}
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	public void clicEditaRegistroMama(Long idRegistroElegido) {
		contenedorGrid.setEnabled(false);
		RegistroMama registromama = new RegistroMamaDAO().getRegistroId(idRegistroElegido);
		new AccesoControlador().doAccesoUsuarioRegistro(registromama);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroMama(registromama));
		contenedorFormulario.setVisible(true);
	}

	public void clicEditaRegistroColon(Long idRegistroElegido) {
		contenedorGrid.setEnabled(false);
		RegistroColon registrocolon = new RegistroColonDAO().getRegistroId(idRegistroElegido);
		new AccesoControlador().doAccesoUsuarioRegistro(registrocolon);
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroColon(registrocolon));
		contenedorFormulario.setVisible(true);
	}

	public void clicEditaRegistroAerosol(Long idRegistroElegido) {
		Long idregistro = new Long(0);
		RegistroOxiAerosol registroaerosolAerosol = null;
		if (idRegistroElegido.equals(new Long(0))) {
			if (grid.getSelectedItems().size() > 0) {
				contenedorGrid.setEnabled(false);
				proceso = grid.getSelectedItems().iterator().next();
				idregistro = new RegistroDAO().getRegistroPacienteProcesoO2(paciente, proceso,
						RegistroOxiAerosol.PLANTILLLA_EDITOR_REGISTROAEROSOL);
			} else {
				new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
				return;
			}
		} else {
			idregistro = idRegistroElegido;
		}

		if (idregistro.equals(new Long(0))) {
			registroaerosolAerosol = new RegistroOxiAerosol();
			registroaerosolAerosol.setPaciente(paciente);
			registroaerosolAerosol.setProblema(proceso);
			registroaerosolAerosol.setFechaInicio(proceso.getFechaini());
		} else {
			registroaerosolAerosol = new RegistroOxiAerosolDAO().getRegistroId(idregistro);
			new AccesoControlador().doAccesoUsuarioRegistro(registroaerosolAerosol);
		}
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroOxiAerosol(registroaerosolAerosol));
		contenedorFormulario.setVisible(true);

	}

	public void clicEditaRegistroMapnea(Long idRegistroElegido) {
		Long idregistro = new Long(0);
		RegistroOxiMapnea registroMapnea = null;
		if (idRegistroElegido.equals(new Long(0))) {
			if (grid.getSelectedItems().size() > 0) {
				contenedorGrid.setEnabled(false);
				proceso = grid.getSelectedItems().iterator().next();
				idregistro = new RegistroDAO().getRegistroPacienteProcesoO2(paciente, proceso,
						RegistroOxiMapnea.PLANTILLLA_EDITOR_REGISTROMAPNEA);
			} else {
				new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
				return;
			}
		} else {
			idregistro = idRegistroElegido;
		}

		if (idregistro.equals(new Long(0))) {
			registroMapnea = new RegistroOxiMapnea();
			registroMapnea.setPaciente(paciente);
			registroMapnea.setProblema(proceso);
			registroMapnea.setFechaInicio(proceso.getFechaini());
		} else {
			registroMapnea = new RegistroOxiMapneaDAO().getRegistroId(idregistro);
			new AccesoControlador().doAccesoUsuarioRegistro(registroMapnea);
		}
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroOxiMapne(registroMapnea));
		contenedorFormulario.setVisible(true);
	}

	public void clicEditaRegistroDomiciliaria(Long idRegistroElegido) {

		Long idregistro = new Long(0);
		RegistroOxiDomi registroDomi = null;
		if (idRegistroElegido.equals(new Long(0))) {
			if (grid.getSelectedItems().size() > 0) {
				contenedorGrid.setEnabled(false);
				proceso = grid.getSelectedItems().iterator().next();
				idregistro = new RegistroDAO().getRegistroPacienteProcesoO2(paciente, proceso,
						RegistroOxiDomi.PLANTILLLA_EDITOR_REGISTRODOMI);
			} else {
				new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
				return;
			}
		} else {
			idregistro = idRegistroElegido;
		}

		if (idregistro.equals(new Long(0))) {
			registroDomi = new RegistroOxiDomi();
			registroDomi.setPaciente(paciente);
			registroDomi.setProblema(proceso);
			registroDomi.setFechaInicio(proceso.getFechaini());
		} else {
			registroDomi = new RegistroOxiDomiDAO().getRegistroId(idregistro);
			new AccesoControlador().doAccesoUsuarioRegistro(registroDomi);

		}
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroOxiDomi(registroDomi));
		contenedorFormulario.setVisible(true);
	}

	public void clicEditaEquipoApoyo(Long idRegistroElegido) {

		Long idregistro = new Long(0);
		RegistroOxiHipercapnia registroEapoyo = null;
		if (idRegistroElegido.equals(new Long(0))) {
			if (grid.getSelectedItems().size() > 0) {
				contenedorGrid.setEnabled(false);
				proceso = grid.getSelectedItems().iterator().next();
				idregistro = new RegistroDAO().getRegistroPacienteProcesoO2(paciente, proceso,
						RegistroOxiHipercapnia.PLANTILLLA_EDITOR_REGISTROEQPOYO);
			} else {
				new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
				return;
			}
		} else {
			idregistro = idRegistroElegido;
		}
		if (idregistro.equals(new Long(0))) {
			registroEapoyo = new RegistroOxiHipercapnia();
			registroEapoyo.setPaciente(paciente);
			registroEapoyo.setProblema(proceso);
			registroEapoyo.setFechaInicio(proceso.getFechaini());
		} else {
			registroEapoyo = new RegistroOxiHipercapmiaDAO().getRegistroId(idregistro);
			new AccesoControlador().doAccesoUsuarioRegistro(registroEapoyo);
		}
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroOxiHipercapmia(registroEapoyo));
		contenedorFormulario.setVisible(true);
	}

	public void cliclEditaIncidencias(Long idRegistroElegido) {

		Long idregistro = new Long(0);
		RegistroO2Incidencia registroIncidencia = null;
		if (idRegistroElegido.equals(new Long(0))) {
			if (grid.getSelectedItems().size() > 0) {
				contenedorGrid.setEnabled(false);
				proceso = grid.getSelectedItems().iterator().next();
				idregistro = new RegistroDAO().getRegistroPacienteProcesoO2(paciente, proceso,
						RegistroO2Incidencia.PLANTILLLA_EDITOR_REGISTROINCIDENCIA);
			} else {
				new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
				return;
			}
		} else {
			idregistro = idRegistroElegido;
		}

		if (idregistro == 0) {
			registroIncidencia = new RegistroO2Incidencia();
			registroIncidencia.setPaciente(paciente);
			registroIncidencia.setProblema(proceso);
			registroIncidencia.setFechaInicio(LocalDate.now());
		} else {
			registroIncidencia = new RegistroO2IncidenciaDao().getRegistroId(idregistro);
			new AccesoControlador().doAccesoUsuarioRegistro(registroIncidencia);
		}
		contenedorFormulario.removeAllComponents();
		contenedorFormulario.addComponent(new FrmRegistroOxiIncidencia(registroIncidencia));
		contenedorFormulario.setVisible(true);
	}

	public void seleccionaRegistro() {
		if (gridRegistros.getSelectedItems().size() == 1) {
			Registro registro = gridRegistros.getSelectedItems().iterator().next();
			if (registro.getProblema().getSubambito().equals(Proceso.SUBAMBITO_MAMA)) {
				this.clicEditaRegistroMama(registro.getId());
			} else if (registro.getProblema().getSubambito().equals(Proceso.SUBAMBITO_COLON)) {
				this.clicEditaRegistroColon(registro.getId());
			} else if (registro.getProblema().getSubambito().equals(Proceso.SUBAMBITO_OXIGENO)) {
				if (registro.getPlantilla_editor().equals(RegistroOxiAerosol.PLANTILLLA_EDITOR_REGISTROAEROSOL)) {
					clicEditaRegistroAerosol(registro.getId());
				} else if (registro.getPlantilla_editor().equals(RegistroOxiMapnea.PLANTILLLA_EDITOR_REGISTROMAPNEA)) {
					clicEditaRegistroMapnea(registro.getId());
				} else if (registro.getPlantilla_editor().equals(RegistroOxiDomi.PLANTILLLA_EDITOR_REGISTRODOMI)) {
					clicEditaRegistroDomiciliaria(registro.getId());
				} else if (registro.getPlantilla_editor()
						.equals(RegistroOxiHipercapnia.PLANTILLLA_EDITOR_REGISTROEQPOYO)) {
					clicEditaEquipoApoyo(registro.getId());
				} else if (registro.getPlantilla_editor()
						.equals(RegistroO2Incidencia.PLANTILLLA_EDITOR_REGISTROINCIDENCIA)) {
					cliclEditaIncidencias(registro.getId());
				}
			}
		}
	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();

	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(getAyudaHtml()));
	}

	/**
	 * Selecciona valor inicial.
	 */
	public void seleccionaValorInicial() {
		if (grid.getSelectedItems().size() == 1) {
			Set<Proceso> selected = grid.getSelectedItems();
			Iterator<Proceso> it = selected.iterator();
			if (it.hasNext()) {
				proceso = new ProcesoDAO().getRegistroId(it.next().getId(), null);
			}
		}
	}

	/**
	 * Gets the descripcion grid.
	 *
	 * @return the descripcion grid
	 */
	public String getDescripcionGrid() {
		String texto = "";
		if (this.subambito == Proceso.SUBAMBITO_MAMA) {
			texto = texto.concat("  Mama ");
		}
		return texto;
	}

	/**
	 * Gets the grid.
	 *
	 * @return the grid
	 */
	public Grid<Proceso> getGrid() {
		return grid;
	}

	/**
	 * Sets the grid.
	 *
	 * @param grid the new grid
	 */
	public void setGrid(Grid<Proceso> grid) {
		this.grid = grid;
	}

	/**
	 * Sets the valores grid botones.
	 */
	@Override
	public void setValoresGridBotones() {

	}

	/**
	 * Do control botones. Si el paciente tiene algún proceso del mismo tipo abierto
	 * no se puede añadir nuevos el botón de añadir no está activo
	 * 
	 * Si hay proceso seleccionado de la lista se activan los botones de editar
	 * proceso, registro y los de oxígeno
	 * 
	 * Si el ámbito es paliativos no se activo el botón de registros, no tiene
	 * registro de datos asociado.
	 * 
	 * Si el ámbito es oxígeno se activa la fila de botones de oxígeno
	 * 
	 * 
	 */
	@Override
	public void doControlBotones() {
		if (new ProcesoDAO().getTodosLosProcesosCerrados(this.paciente.getId(), subambito) == true) {
			anadir.setEnabled(true);
			anadir.setDescription("Añade un nuevo proceso al paciente");
		} else {
			anadir.setEnabled(false);
			anadir.setDescription("Este paciente tiene proceso abierto de este tipo.");
		}
		if (listaProcesos.size() > 0) {
			grid.select(listaProcesos.get(0));
			this.proceso = listaProcesos.get(0);
		}

		if (grid.getSelectedItems().size() == 1) {
			botonProceso.setEnabled(true);
			botonRegistro.setEnabled(true);
			botonAerosol.setEnabled(true);
			botonApnea.setEnabled(true);
			botonIncidencias.setEnabled(true);
			botonDomiciliaria.setEnabled(true);
			botonHipercapnia.setEnabled(true);
		} else {
			botonProceso.setEnabled(false);
			botonRegistro.setEnabled(false);
			botonAerosol.setEnabled(false);
			botonApnea.setEnabled(false);
			botonIncidencias.setEnabled(false);
			botonDomiciliaria.setEnabled(false);
			botonHipercapnia.setEnabled(false);
		}

		if (subambito.equals(Proceso.SUBAMBITO_PALIATIVOS)) {
			botonRegistro.setEnabled(false);
			gridRegistros.setVisible(false);
			filaBotonesO2.setVisible(false);
		} else {
			if (subambito.equals(Proceso.SUBAMBITO_OXIGENO)) {
				botonRegistro.setVisible(false);
				filaBotonesO2.setVisible(true);
			} else {
				botonRegistro.setVisible(true);
				filaBotonesO2.setVisible(false);
			}
		}

	}

	public void doAbrirFormulario() {
		if (registro != null) {
			if (registro instanceof RegistroOxiAerosol) {
				contenedorFormulario.removeAllComponents();
				contenedorFormulario.addComponent(new FrmRegistroOxiAerosol((RegistroOxiAerosol) registro));
				contenedorFormulario.setVisible(true);
			}

			if (registro instanceof RegistroOxiDomi) {
				contenedorFormulario.removeAllComponents();
				contenedorFormulario.addComponent(new FrmRegistroOxiDomi((RegistroOxiDomi) registro));
				contenedorFormulario.setVisible(true);
			}
		}
	}

	public String getAyudaHtml() {
		String htmlAyudaString = " <b>Pantalla de mantenimiento de procesos  del  paciente:\n  </b> <br>"
				+ "Descripción de botones: \n  <br>"
				+ "<ul> <li><b>+</b>  Crea un proceso nuevo siempre que no haya ningún otro abierto  </li>"
				+ "<li><b>Refrescar</b>  Refresca la pantalla </li>"
				+ "<li><b>Proceso</b>  Edita el proceso seleccionado </li>"
				+ "<li><b>Registro</b>  Crea o modifica un registro </li>" + "<li><b>?</b>  Este texto   </li>"
				+ "</ul>  <hr>" + " Haciento click en una fila de la tabla de procesos se puede modificar y/o borrar";
		return htmlAyudaString;
	}

	public Proceso getProceso() {
		return proceso;
	}

	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}
