package com.jnieto.ui;

import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Zona;
import com.jnieto.ui.master.ConsolaMaster;

public class ConsolaUCA extends ConsolaMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5572225210547808664L;
	private PanelPaciente cajaPaciente;
	private CajaEnfCtes cajaEnfCtes;
	private CajaEpisodio cajaEpisodio;

	public ConsolaUCA(Paciente paciente, EpisodioClase clase) {

		super(paciente, clase);
		zona = Zona.ZONA_UCA;
		cajaPaciente = new PanelPaciente(paciente);
		cajaEnfCtes = new CajaEnfCtes(paciente, paciente.getEpisodioActual());
		cajaEpisodio = new CajaEpisodio(paciente, clase, zona);
		fila1.addComponents(cajaPaciente, cajaEnfCtes);
		fila2.addComponent(cajaEpisodio);
	}

	@Override
	public void refrescar() {

	}

}
