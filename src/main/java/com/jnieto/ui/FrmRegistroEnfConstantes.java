package com.jnieto.ui;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroEnfCtesDAO;
import com.jnieto.entity.RegistroEnfConstantes;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.validators.ValidatorDecimal;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.ui.TextField;

public class FrmRegistroEnfConstantes extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6854455232308167032L;

	private TextField peso = new TextField("Peso (Kg)");
	private TextField talla = new TextField("Talla (Cm)");
	private TextField imc = new TextField("IMC");
	private TextField tas = new TextField("Tas");
	private TextField tad = new TextField("Tad");
	private TextField temperatura = new TextField("Tª");
	private TextField freCardiaca = new TextField("FC");
	private TextField satOxigeno = new TextField("SaTO2");
	private TextField freRespiratorio = new TextField("Fr");
	private TextField eva = new TextField("EVA");
	/*
	 * private HorizontalLayout fila1 = new HorizontalLayout(); private
	 * HorizontalLayout fila2 = new HorizontalLayout(); private HorizontalLayout
	 * fila3 = new HorizontalLayout(); private HorizontalLayout fila4 = new
	 * HorizontalLayout();
	 */
	private RegistroEnfConstantes registro = new RegistroEnfConstantes();

	Binder<RegistroEnfConstantes> binder = new Binder<RegistroEnfConstantes>();

	public FrmRegistroEnfConstantes(RegistroEnfConstantes registro) {
		super();
		this.registro = registro;
		this.setWidthUndefined();
		this.setHeightUndefined();
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2);
		lbltitulo.setCaption("Constantes: " + registro.getEpisodio().getId() + "/" + registro.getId());

		peso.setWidth("60px");
		binder.forField(peso)
				.withValidator(new ValidatorDecimal(peso.getValue(), registro.VAR_ENF_CTES_PESO.getValorInferior(),
						registro.VAR_ENF_CTES_PESO.getValorSuperior()))
				.bind(RegistroEnfConstantes::getPesoString, RegistroEnfConstantes::setPeso);
		peso.addBlurListener(e -> cambiaPesoTalla());

		talla.setWidth("60px");
		binder.forField(talla)
				.withValidator(new ValidatorDecimal(peso.getValue(), registro.VAR_ENF_CTES_TALLA.getValorInferior(),
						registro.VAR_ENF_CTES_TALLA.getValorSuperior()))
				.bind(RegistroEnfConstantes::getTallaSrting, RegistroEnfConstantes::setTalla);
		talla.addBlurListener(e -> cambiaPesoTalla());

		imc.setWidth("60px");
		binder.forField(imc).bind(RegistroEnfConstantes::getImcString, RegistroEnfConstantes::setImc);

		temperatura.setWidth("60px");
		binder.forField(temperatura)
				.withValidator(new ValidatorDecimal(temperatura.getValue(), registro.VAR_ENF_CTES_T.getValorInferior(),
						registro.VAR_ENF_CTES_T.getValorSuperior()))
				.bind(RegistroEnfConstantes::getTemperaturaString, RegistroEnfConstantes::setTemperatura);

		freCardiaca.setWidth("60px");
		binder.forField(freCardiaca).withValidator(new ValidatorEntero(freCardiaca.getValue(), 20, 300))
				.bind(RegistroEnfConstantes::getFreCardiacaString, RegistroEnfConstantes::setFreCardiaca);

		freRespiratorio.setWidth("60px");
		binder.forField(freRespiratorio).withValidator(new ValidatorEntero(freRespiratorio.getValue(), 10, 300))
				.bind(RegistroEnfConstantes::getFreRespiratorioString, RegistroEnfConstantes::setFreRespiratorio);

		fila1.addComponents(peso, talla, imc, temperatura, freCardiaca, freRespiratorio);

		tas.setWidth("60px");
		binder.forField(tas).withValidator(new ValidatorEntero(tas.getValue(), 5, 300))
				.bind(RegistroEnfConstantes::getTasString, RegistroEnfConstantes::setTas);

		tad.setWidth("60px");
		binder.forField(tad).withValidator(new ValidatorEntero(tad.getValue(), 10, 300))
				.bind(RegistroEnfConstantes::getTadString, RegistroEnfConstantes::setTad);

		satOxigeno.setWidth("60px");
		binder.forField(satOxigeno).withValidator(new ValidatorEntero(satOxigeno.getValue(), 50, 100))
				.bind(RegistroEnfConstantes::getSatOxigenoString, RegistroEnfConstantes::setSatOxigeno);

		fila2.addComponents(tas, tad, satOxigeno, eva);
		eva.setWidth("40px");
		binder.forField(eva).withValidator(new ValidatorEntero(eva.getValue(), 0, 10))
				.bind(RegistroEnfConstantes::getEvaString, RegistroEnfConstantes::setEva);

		try {

			binder.readBean(registro);
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.addComponents(contenedorBotones, contenedorCampos);
	}

	public void cambiaPesoTalla() {
		Double imcDouble = null;
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		DecimalFormat formateador = new DecimalFormat("####.#", otherSymbols);
		try {
			if (peso.getValue() != null && !peso.getValue().isEmpty() && talla.getValue() != null
					&& !talla.getValue().isEmpty()) {
				Double pesoDouble = Double.parseDouble(peso.getValue());
				Double tallaDouble = Double.parseDouble(talla.getValue());
				imcDouble = pesoDouble / tallaDouble * 100;
				imc.setValue(formateador.format(imcDouble));
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registro);
			new RegistroEnfCtesDAO().actualizaDatos(registro);
			MyUI.getCurrent().getWindows().iterator().next().close();
		} catch (Exception e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

}
