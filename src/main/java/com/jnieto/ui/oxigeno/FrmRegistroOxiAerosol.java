package com.jnieto.ui.oxigeno;

import org.vaadin.dialogs.ConfirmDialog;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.RegistroOxiAerosol;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.PantallaProcesos;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The Class FrmRegistroOxiAerosol. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroOxiAerosol extends FrmRegistroOxi {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7101735315012462598L;

	private TextField sesionesDia = new TextField("Sesiones Día");
	private TextField numeroDias = new TextField("Número  Días");

	RegistroOxiAerosol registroOxiAerosol = new RegistroOxiAerosol();

	Binder<RegistroOxiAerosol> binder = new Binder<RegistroOxiAerosol>();

	public FrmRegistroOxiAerosol(RegistroOxiAerosol param) {
		super();
		this.setMargin(false);
		this.registroOxiAerosol = param;

		lbltitulo.setCaption(registroOxiAerosol.getVariableTerapia().getValor() + " "
				+ registroOxiAerosol.getProblema().getId() + "/" + registroOxiAerosol.getId());
		lbltitulo.setVisible(true);

		binder.forField(centroSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiAerosol::getCentro, RegistroOxiAerosol::setCentro);

		binder.forField(servicioSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiAerosol::getServicio, RegistroOxiAerosol::setServicio);

		modalidadesSelect = this.getComboModalidades(registroOxiAerosol.getVariableTerapia().getValor(), "");
		binder.forField(modalidadesSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiAerosol::getModalidadString, RegistroOxiAerosol::setModalidad);

		binder.forField(pruebaDiagnostica).bind(RegistroOxiAerosol::getPruebaDiagnosticaString,
				RegistroOxiAerosol::setPruebaDiagnostica);

		binder.forField(resultadosPrueba).bind(RegistroOxiAerosol::getResultadosPruebaString,
				RegistroOxiAerosol::setResultadosPrueba);

		binder.forField(tipoPrescripcionSelest).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiAerosol::getTipoPrescripcionString, RegistroOxiAerosol::setTipoPrescripcion);
		tipoPrescripcionSelest.addBlurListener(e -> cambiaTipoPrescrip());

		binder.forField(fechaInicio).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.withValidator(returnDate -> !returnDate.isBefore(registroOxiAerosol.getProblema().getFechaini()),
						" Mayor inicio proceso")
				.bind(RegistroOxiAerosol::getFechaInicio, RegistroOxiAerosol::setFechaInicio);

		binder.forField(duracionSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroOxiAerosol::getDuracionString, RegistroOxiAerosol::setDuracion);

		binder.forField(observaciones).bind(RegistroOxiAerosol::getObservacionesString,
				RegistroOxiAerosol::setObservaciones);

		binder.forField(fechaBaja).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaInicio.getValue());
		}, "Mayor o igual que fecha inicio ").bind(RegistroOxiAerosol::getFechaBaja, RegistroOxiAerosol::setFechaBaja);

		binder.forField(motivoBaja).bind(RegistroOxiAerosol::getMotivoBajaString, RegistroOxiAerosol::setMotivoBaja);

		sesionesDia.setWidth("80px");
		sesionesDia.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(sesionesDia).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 25), " de 0 a 4 ")
				.bind(RegistroOxiAerosol::getSesionesDiaString, RegistroOxiAerosol::setSesionesDia);

		numeroDias.setWidth("80px");
		numeroDias.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(numeroDias).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.withValidator(numero -> numero.matches("[0-9]*"), " sólo números")
				.withValidator(numero -> (Integer.parseInt(numero) > 0 && Integer.parseInt(numero) < 999),
						" de 1 a 999 ")
				.bind(RegistroOxiAerosol::getNumeroDiasString, RegistroOxiAerosol::setNumeroDias);

		binder.readBean(registroOxiAerosol);

		fila1.addComponents(centroSelect, servicioSelect, modalidadesSelect);
		fila2.addComponents(pruebaDiagnostica, resultadosPrueba, farmaco);
		fila3.addComponents(fechaInicio, tipoPrescripcionSelest, duracionSelect);
		fila4.addComponents(sesionesDia, numeroDias, interfase);
		fila5.addComponent(observaciones);
		fila6.addComponents(fechaBaja, motivoBaja);

		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6);
		// filaObservaciones.addComponents();
		// filaBaja.addComponents(fechaBaja, motivoBaja);

		// this.contenedorCampos.addComponents(filaCentrosServicio,
		// filaPruebaResultados, filaInicioTipoDuraciçon,
		// filaObservaciones, filaBaja);

		// this.addComponents(contenedorBotones, contenedorCampos);
		doActivaBotones();
	}

	/**
	 * Haz activa botones. El botón de borrar sólo se activa si se edita un registro
	 * y no se activa si es nuevo Si el registro tiene fecha de fina anterior a diez
	 * dias no se puede borrar Si el proceso asociado al registro está cerrado con
	 * una fecha anterior a diez dias no se puede borrar ni modificar
	 */
	@Override
	public void doActivaBotones() {
		super.doActivaBotones(registroOxiAerosol);

	}

	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroOxiAerosol);
				if (!new RegistroDAO().grabaDatos(registroOxiAerosol)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();
			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			}
		} else {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroOxiAerosol)) {
			registroOxiAerosol = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);
		cerrarClick();
	}

	@Override
	public boolean doValidaFormulario() {
		boolean valido = true;
		if (!super.doValidaFormulario()) {
			valido = false;
		}

		return valido;
	}

	@Override
	public String getAyudaHtml() {
		String ayudaHtml = super.getAyudaHtml() + "<ul> " + "<li><b>Sesiones:</b> Número de sesiones diarias.</li>"
				+ "<li><b>Número de días:</b>Número de días.</li>" + "</ul>" + Ayudas.AYUDA_BOTONES_ABCA;
		return ayudaHtml;
	}
}
