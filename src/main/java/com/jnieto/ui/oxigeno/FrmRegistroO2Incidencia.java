package com.jnieto.ui.oxigeno;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroO2Incidencia;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.PantallaProcesos;
import com.jnieto.ui.UtilidadesUi;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.utilidades.Ayudas;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;

/**
 * The Class FrmRegistroIncidencia. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class FrmRegistroO2Incidencia extends FrmMaster {

	private static final long serialVersionUID = -3411150200312190415L;

	private DateField fechaini = null;

	private ComboBox<String> tipoIncidenciaSelect = null;

	protected TextArea observaciones = new TextArea();

	private DateField fechafin = null;

	private HorizontalLayout fila1 = new HorizontalLayout();

	private HorizontalLayout fila2 = new HorizontalLayout();

	private HorizontalLayout fila3 = new HorizontalLayout();

	private Binder<RegistroO2Incidencia> binder = new Binder<>();

	private RegistroO2Incidencia registroIncidencia = new RegistroO2Incidencia();

	public FrmRegistroO2Incidencia(RegistroO2Incidencia parainci) {
		this.registroIncidencia = parainci;
		this.setSizeFull();
		this.setMargin(false);
		fila1.setMargin(false);
		fila2.setMargin(false);
		fila3.setMargin(false);
		lbltitulo.setCaption(
				"Incidencia Oxigeno " + registroIncidencia.getProblema().getId() + "/" + registroIncidencia.getId());

		fechaini = new ObjetosComunes().getFecha("Fecha Inicio ", " inicio incidencia");
		binder.forField(fechaini).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.withValidator(returnDate -> !returnDate.isBefore(registroIncidencia.getProblema().getFechaini()),
						" Mayor inicio proceso")
				.bind(RegistroO2Incidencia::getFechaInicioDate, RegistroO2Incidencia::setFechaInicio);

		fechafin = new ObjetosComunes().getFecha("Fecha fin ", " fin incidencia");
		binder.forField(fechafin).withValidator(returnDate -> {
			if (returnDate == null)
				return true;
			else
				return !returnDate.isBefore(fechaini.getValue());
		}, "Mayor o igual que fecha inicio ").bind(RegistroO2Incidencia::getFechaFinDate,
				RegistroO2Incidencia::setFechaFin);

		observaciones.setCaption("Observaciones");
		observaciones.setWidth("400px");
		observaciones.setHeight("100px");
		observaciones.setMaxLength(400);
		observaciones.setPlaceholder(" observaciones al registro de mama");
		observaciones.setMaxLength(200);
		binder.forField(observaciones).bind(RegistroO2Incidencia::getObservacionesString,
				RegistroO2Incidencia::setObservaciones);

		tipoIncidenciaSelect = new UtilidadesUi().getComboTipoIncidencia("");
		binder.forField(tipoIncidenciaSelect).asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO)
				.bind(RegistroO2Incidencia::getTipoString, RegistroO2Incidencia::setTipo);

		binder.readBean(registroIncidencia);

		doActivaBotones(registroIncidencia.getId());

		fila1.addComponents(fechaini, fechafin);
		fila2.addComponent(tipoIncidenciaSelect);
		fila3.addComponents(observaciones);
		this.addComponents(contenedorBotones, fila1, fila2, fila3);

	}

	/**
	 * Cerrar click.
	 */
	@Override
	public void cerrarClick() {
		this.removeAllComponents();
		this.setVisible(false);
		((PantallaProcesos) this.getParent().getParent().getParent()).refrescarClick();
	}

	/**
	 * Ayuda click.
	 */
	@Override
	public void ayudaClick() {
		new VentanaHtml(this.getUI(), new Label(this.getAyudaHtml()));
	}

	/**
	 * Grabar click.
	 */
	@Override
	public void grabarClick() {
		if (doValidaFormulario() == true) {
			try {
				binder.writeBean(registroIncidencia);
				if (!new RegistroDAO().grabaDatos(registroIncidencia)) {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);

				} else {
					new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				}
				this.cerrarClick();

			} catch (ValidationException e) {
				new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
			} finally {

			}
		}
	}

	/**
	 * Borrar click.
	 */
	@Override
	public void borrarClick() {
		ConfirmDialog.show(this.getUI(), Constantes.CONFIRMACION_TITULO, Constantes.CONFIRMACION_BORRADO_MENSAJE,
				Constantes.CONFIRMACION_BOTONSI, Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 6169352858399108337L;

					public void onClose(ConfirmDialog dialog) {
						if (dialog.isConfirmed()) {
							borraElRegistro();
						}
					}
				});
		this.cerrarClick();
	}

	/**
	 * Borra el registro.
	 */
	@Override
	public void borraElRegistro() {
		if (new RegistroDAO().doBorrarRegistro(registroIncidencia)) {
			registroIncidencia = null;
			new NotificacionInfo(NotificacionInfo.FORMULARIO_DATO_BORRADO);
		} else
			new NotificacionInfo(NotificacionInfo.FORMULARIO_ERROR_DATO_BORRADO);

		cerrarClick();
	}

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	public boolean doValidaFormulario() {
		boolean valido = true;
		/*
		 * if (fechaini.isEmpty()) { new
		 * NotificacionInfo(Constantes.AVISO_DATO_OBIGATORIO); valido = false;
		 * fechaini.setComponentError(new UserError(Constantes.AVISO_DATO_OBIGATORIO));
		 * }
		 */

		/*
		 * if (tipoIncidenciaSelect.isEmpty()) { new
		 * NotificacionInfo(Constantes.AVISO_DATO_OBIGATORIO); valido = false;
		 * tipoIncidenciaSelect.setComponentError(new
		 * UserError(Constantes.AVISO_DATO_OBIGATORIO)); }
		 * 
		 */
		/*
		 * if (!fechafin.isEmpty()) { if
		 * (!fechafin.getValue().isAfter(fechaini.getValue())) { new
		 * NotificacionInfo(ProcesoNew.AVISO_PROCESO_FIN_MENOR_INICIO); valido = false;
		 * fechafin.setComponentError(new
		 * UserError(ProcesoNew.AVISO_PROCESO_FIN_MENOR_INICIO)); } } else {
		 * fechafin.setComponentError(null); }
		 */
		return valido;
	}

	@Override
	public void doActivaBotones(Long id) {
		if (id.compareTo(new Long(0)) > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}

		if (registroIncidencia.getFechaFinDate() != null) {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registroIncidencia.getFechaFinDate(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				fila1.setEnabled(false);
				fila2.setEnabled(false);
				fila3.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
	}

	public String getAyudaHtml() {
		String textoHtml = " <b>Pantalla de registro de incidencias de oxigenoterapia :\n  </b> <br>" + "<ul> "
				+ "<li><b>Fecha inicio:</b>  ??????.</li>" + " <hr> " + Ayudas.AYUDA_BOTONES_ABCA;
		return textoHtml;
	}
}
