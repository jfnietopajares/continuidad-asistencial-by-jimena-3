package com.jnieto.ui.oxigeno;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.PeticionesDao;
import com.jnieto.dao.ProcesoDAO;
import com.jnieto.dao.RegistroOxiAerosolDAO;
import com.jnieto.dao.RegistroOxiDomiDAO;
import com.jnieto.entity.Peticion;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroOxiDomi;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.PanelPeticion;
import com.jnieto.ui.PantallaProcesos;
import com.jnieto.ui.master.PantallaPaginacion;
import com.jnieto.utilidades.Parametros;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.LocalDateRenderer;

/**
 * The Class PantallaPeticiones. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PantallaPeticiones extends PantallaPaginacion {

	private static final long serialVersionUID = 8974866440805080123L;

	private VerticalLayout columnaPeticiones = new VerticalLayout();

	private HorizontalLayout filafechas = new HorizontalLayout();

	private DateField desde = null;

	private DateField hasta = null;

	private Grid<Peticion> grid = new Grid<>();

	private Peticion peticion = new Peticion();

	ArrayList<Peticion> listaPeticiones = new ArrayList<>();

	TabSheet tabsheet = new TabSheet();

	DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");

	/**
	 * Instantiates a new pantalla peticiones.
	 */
	public PantallaPeticiones() {
		super();
		this.addComponent(tabsheet);
		tabsheet.setStyleName(MaterialTheme.TABSHEET_ICONS_ON_TOP);

		this.setMargin(false);
		columnaPeticiones.setMargin(false);
		filafechas.setMargin(false);
		desde = new ObjetosComunes().getFecha("Desde", " desde fecha");
		desde.setValue(LocalDate.now());
		desde.addValueChangeListener(event -> doCargaListaDatos());

		hasta = new ObjetosComunes().getFecha("Hasta", " hasta fecha");
		hasta.setValue(LocalDate.now());
		hasta.addValueChangeListener(event -> doCargaListaDatos());
		filafechas.addComponents(desde, hasta);
		/*
		 * ArrayList<Peticion> listaPeticiones = new
		 * PeticionesDao().getListaPeticionesPendientes(desde.getValue(),
		 * hasta.getValue()); if (listaPeticiones.size() > 0)
		 * grid.select(listaPeticiones.get(0));
		 */
		grid.setCaption(" Lista de peticiones pendientes   ");
		grid.addColumn(Peticion::getId).setCaption("Id");
		grid.addColumn(peticion -> "Procesa", new ButtonRenderer<Object>(clickEvent -> seleccionaPeticion()));
		grid.addColumn(peticion -> "Ver", new ButtonRenderer<Object>(clickEvent -> ver()));

		grid.addColumn(Peticion::getVolante).setCaption("Volante");
		grid.addColumn(Peticion::getFecha, new LocalDateRenderer("dd/MM/yyyy")).setCaption("Fecha");
		grid.addColumn(Peticion::getNombrePaciente).setCaption("Paciente");
		grid.setStyleGenerator(e -> getEstilo(e));
		// grid.setItems(listaPeticiones);
//		grid.addItemClickListener(event -> seleccionaPeticion());	
		grid.setWidth("900px");
		grid.asSingleSelect();

		columnaPeticiones.addComponents(filafechas, grid, filaPaginacion);
		tabsheet.addTab(columnaPeticiones, "Peticiones");

		paginacion = new PeticionesDao().getPaginacionPaciente(desde.getValue(), hasta.getValue());
		// paginacion.setNumeroRegistrosPagina(PagiLisReg.NUMERO_REGISTROS_PAGINACION);
		paginacion.setNumeroRegistrosPagina(
				Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION)));
		setValoresGridBotones();
		// this.addComponents(columnaPeticiones, columnaDetalle);

	}

	private String getEstilo(Peticion peti) {
		if (peti.getEstado() == Peticion.ESTADO_PETICIONES_PROCESADAS) {
			return "RED";
		}
		return null;
	}

	public void ver() {
		if (grid.getSelectedItems().size() == 1) {
			PanelPeticion panel = new PanelPeticion(grid.getSelectedItems().iterator().next());
			peticion = grid.getSelectedItems().iterator().next();
			VerticalLayout contenedorTabLayout = new VerticalLayout();
			contenedorTabLayout.addComponent(panel);
			tabsheet.addTab(contenedorTabLayout, peticion.getPaciente().getApellidosNombre()).setClosable(true);
			tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
		}
	}

	/**
	 * Do carga lista datos.
	 */
	public void doCargaListaDatos() {
		paginacion = new PeticionesDao().getPaginacionPaciente(desde.getValue(), hasta.getValue());
		paginacion.setNumeroRegistrosPagina(
				Integer.parseInt((String) MyUI.objParametros.get(Parametros.KEY_NUMERO_REGISTROS_PAGINACION)));
		setValoresGridBotones();
	}

	/**
	 * Selecciona peticion.
	 */
	public void seleccionaPeticion() {
		Registro regi = null;

		if (grid.getSelectedItems().size() == 1) {
			Peticion peticion = new Peticion();
			peticion = grid.getSelectedItems().iterator().next();
			// revisar si el paciente tiene proceso abierto
			Proceso proceso = new ProcesoDAO().getProcesoPaciSub(peticion.getPaciente().getId(),
					Proceso.SUBAMBITO_OXIGENO);
			if (proceso == null) {
				proceso = doCreaProcesoO2(peticion);
				regi = doHazRegistro(proceso, peticion);
			}

			VerticalLayout contenedorTabLayout = new VerticalLayout();
			contenedorTabLayout
					.addComponent(new PantallaProcesos(peticion.getPaciente(), Proceso.SUBAMBITO_OXIGENO, regi));
			tabsheet.addTab(contenedorTabLayout, peticion.getPaciente().getApellidosNombre()).setClosable(true);
			tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
		}
	}

	public Proceso doCreaProcesoO2(Peticion peticion) {
		Proceso proceso = new Proceso();
		proceso.setPaciente(peticion.getPaciente());
		proceso.setSubambito(Proceso.SUBAMBITO_OXIGENO);
		proceso.setServicio(peticion.getServicio());
		proceso.setFechaini(peticion.getFecha());
		proceso.setHoraini(peticion.getHora());
		proceso.setOrigen("Petición facultativa.");
		proceso.setMotivo(peticion.getMotivo());
		proceso.setOrigen("Proceso automático.");
		// proceso.setDiagnostico(peticion.getD);
		Long idLong = new ProcesoDAO().getInsertaDatos(proceso);
		if (idLong > 0) {
			proceso.setId(idLong);
			return proceso;
		} else
			return null;
	}

	public Registro doHazRegistro(Proceso proceso, Peticion peticion) {
		String volante = Long.toString(peticion.getVolante());
		switch (volante) {
		// aerosol
		case "588941950":
			return new RegistroOxiAerosolDAO().getDePeticion(peticion, proceso);

		}
		if (esBolanteDe(peticion.getVolante(), "OXIGENOTERAPIA")) {
			RegistroOxiDomi oxiDomi = new RegistroOxiDomiDAO().getDePeticion(peticion, proceso);
			return oxiDomi;
		}

		return null;
	}

	public Boolean esBolanteDe(Long volante, String tipo) {
		if (tipo.equals("OXIGENOTERAPIA")) {
			String[] vStrings = Peticion.PLANTILLAS_PETICIONES_OXIGENO.split(",");
			for (String v : vStrings) {
				Long vplantillaLong = Long.parseLong(v);
				if (vplantillaLong.equals(volante)) {
					return true;
				}
			}
		} else if (tipo.equals("OXIGENOTERAPIA")) {
			String[] vStrings = Peticion.PLANTILLAS_PETICIONES_OXIGENO.split(",");
			for (String v : vStrings) {
				Long vplantillaLong = Long.parseLong(v);
				if (vplantillaLong.equals(volante)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Gets the peticion.
	 *
	 * @return the peticion
	 */
	public Peticion getPeticion() {
		return peticion;
	}

	/**
	 * Sets the peticion.
	 *
	 * @param peticion the new peticion
	 */
	public void setPeticion(Peticion peticion) {
		this.peticion = peticion;
		// this.peticion =new PeticionesDao().getPorId(peticion);
	}

	@Override
	public void setValoresGridBotones() {
		listaPeticiones = new PeticionesDao().getListaPeticionesPendientes(desde.getValue(), hasta.getValue(),
				paginacion);
		setValoresPgHeredados(listaPeticiones);
		if (listaPeticiones.isEmpty()) {
			new NotificacionInfo(NotificacionInfo.FORMULARIONOHAYDATOSPARACRITERIO);
		} else {
			grid.setItems(listaPeticiones);
			if (listaPeticiones.size() == 1) {

			}
		}

	}

	public void setValoresPgHeredados(ArrayList<Peticion> listaPeticiones) {
		ArrayList<Integer> listaValoresArrayList = new ArrayList<>();

		for (Peticion peti : listaPeticiones) {
			listaValoresArrayList.add(peti.getNumeroOrden());
		}
		setBotonesPaginacion(listaValoresArrayList);
	}
}
