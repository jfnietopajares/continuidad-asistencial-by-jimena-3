package com.jnieto.ui.master;

import org.apache.commons.codec.digest.DigestUtils;

import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Constantes;
import com.vaadin.data.Binder;
import com.vaadin.server.UserError;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * The Class VentanaCambioClave.
 */
public class VentanaCambioClave extends Window {

	private static final long serialVersionUID = -4464150461869006944L;

	private Window subWindow = null;

	private FormLayout formulario = null;

	private UI ui = null;

	private PasswordField claveAntigua = null;

	private PasswordField clavenueva1 = null;

	private PasswordField clavenueva2 = null;

	private Button aceptar = null;

	private Usuario usuario = null;

	Binder<Usuario> binder = new Binder<>();

	/**
	 * Instantiates a new ventana cambio clave.
	 *
	 * @param ui Muestra una ventana emergente en modo modal para que el usuario
	 *           pueda cambiar su contraseña
	 */
	public VentanaCambioClave(UI ui) {
		this.ui = ui;
		this.usuario = ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME));
		this.ui = ui;
		subWindow = new Window("Cambio de clave de " + usuario.getApellidosNombre());
		subWindow.setWidth("540px");
		subWindow.setHeight("350px");
		subWindow.center();
		subWindow.setModal(true);
		subWindow.isResizeLazy();

		// Crea un layout y lo añade a la ventana
		formulario = new FormLayout();
		formulario.setMargin(true);
		formulario.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		subWindow.setContent(formulario);

		claveAntigua = new ObjetosComunes().getPassword();
		claveAntigua.setCaption("Clave actual");
		claveAntigua.setPlaceholder(" Clave actual");
		claveAntigua.addBlurListener(e -> doValidaClaveAntigua());
		claveAntigua.setCursorPosition(0);
		claveAntigua.focus();

		clavenueva1 = new ObjetosComunes().getPassword();
		clavenueva1.setCaption("Clave nueva");
		clavenueva1.setPlaceholder(" Clave nueva ");
		clavenueva1.addBlurListener(e -> doValidaClaveNueva1());
		binder.forField(clavenueva1)
				.withValidator(numero -> numero.matches("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,16}$"),
						" mínimo 6 dígitos un número una mayúscula  una minúscula   ")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getPassword, Usuario::setPassword);

		clavenueva2 = new ObjetosComunes().getPassword();
		clavenueva2.setCaption("Confirma clave nueva");
		clavenueva2.setPlaceholder(" confirma la clave nueva ");
		binder.forField(clavenueva2)
				.withValidator(numero -> numero.matches("^(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{6,16}$"),
						" mínimo 6 dígitos un número una mayúscula  una minúscula   ")
				.asRequired(NotificacionInfo.FORMULARIOCAMPOREQUERIDO).bind(Usuario::getPassword, Usuario::setPassword);
		clavenueva2.addBlurListener(e -> doVerificaClavesIguales());

		aceptar = new ObjetosComunes().getBotonGrabar();
		aceptar.addClickListener(e -> doRegistraCambios());
		formulario.addComponents(claveAntigua, clavenueva1, clavenueva2, aceptar);
		// Abrir en el UI de la aplicación
		this.ui.addWindow(subWindow);
	}

	/**
	 * Do registra cambios.
	 */
	public void doRegistraCambios() {
		if (doVerificaClavesIguales() && doValidaClaveAntigua() && doValidaClaveNueva1()) {
			usuario.setPasswordhash(DigestUtils.sha1Hex(clavenueva1.getValue().trim()));
			if (new UsuarioDAO().doActualizaClave(usuario)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				VaadinSession.getCurrent().setAttribute(Constantes.SESSION_USERNAME, usuario);
				subWindow.close();
			}
		} else {
			new NotificacionInfo(NotificacionInfo.AVISO_DATO_NO_VALIDO);
			clavenueva2.focus();
		}
	}

	public boolean doValidaClaveNueva1() {
		if (!clavenueva1.isEmpty()) {
			if (claveAntigua.getValue().trim().equals(clavenueva1.getValue().trim())) {
				clavenueva1.setComponentError(new UserError(Usuario.LONGIN_CLAVE_ANTIGUA_IGUAL_NUEVA));
				new NotificacionInfo(Usuario.LONGIN_CLAVE_ANTIGUA_IGUAL_NUEVA);
				clavenueva1.setCursorPosition(0);
				clavenueva1.clear();
				clavenueva1.focus();
			} else {
				clavenueva1.setComponentError(null);
				return true;
			}
		}
		return false;
	}

	/**
	 * Do verifica claves iguales.
	 * 
	 * Comprueba que las claves nuevas coincidan
	 * 
	 * Comprueba que la clave nueva no se igual a la antigua
	 * 
	 */
	public boolean doVerificaClavesIguales() {
		if (!clavenueva2.isEmpty()) {
			if (!clavenueva1.getValue().trim().equals(clavenueva2.getValue().trim())) {
				new NotificacionInfo(NotificacionInfo.CAMBIO_CLAVE_NO_COINCIDEN);
				clavenueva2.setComponentError(new UserError(NotificacionInfo.CAMBIO_CLAVE_NO_COINCIDEN));
				clavenueva2.setCursorPosition(0);
				clavenueva2.clear();
				clavenueva2.focus();
			} else {
				clavenueva2.setComponentError(null);
				return true;
			}
		}
		return false;
	}

	/**
	 * Do valida clave antigua.
	 */
	public boolean doValidaClaveAntigua() {
		// String claveAlmacenada;
		// claveAlmacenada = usuario.getPasswordhash().trim();
		if (!claveAntigua.isEmpty()) {
			if (!DigestUtils.sha1Hex(claveAntigua.getValue().trim()).equals(usuario.getPasswordhash().trim())) {
				claveAntigua.setComponentError(new UserError(Usuario.LONGIN_CLAVE_INCORRECTA));
				claveAntigua.setCursorPosition(0);
				claveAntigua.setValue("");
				claveAntigua.focus();
			} else {
				claveAntigua.setComponentError(null);
				return true;
			}
		}
		return false;
	}
}
