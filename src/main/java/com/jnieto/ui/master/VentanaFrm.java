package com.jnieto.ui.master;

import com.github.appreciated.material.MaterialTheme;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class VentanaFrm extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3553450551393827863L;

	public VentanaFrm() {
	}

	public VentanaFrm(String caption) {
		super(caption);
	}

	public VentanaFrm(UI ui, VerticalLayout vt, String captacion) {
		super("", vt);
		// this.setWidth("440px");
		// this.setClosable(false);
		vt.setMargin(false);
		vt.setSpacing(false);
		this.setStyleName(MaterialTheme.WINDOW_TOP_TOOLBAR);
		this.setWidth(vt.getWidth(), vt.getWidthUnits());
		this.setHeightUndefined();
		// this.setHeight(vt.getHeight(), vt.getHeightUnits());
		this.center();
		this.setModal(true);
		this.isResizeLazy();
		this.isResizable();
		this.setCaption(captacion);
		this.setCaptionAsHtml(true);
		ui.addWindow(this);

		/*
		 * Window subWindow = new Window(); subWindow.setWidth("440px");
		 * subWindow.center(); subWindow.setModal(true); subWindow.setHeightUndefined();
		 * subWindow.setContent(vt); ui.addWindow(subWindow);
		 */
	}

	public VentanaFrm(String caption, Component content) {
		super(caption, content);
	}

}
