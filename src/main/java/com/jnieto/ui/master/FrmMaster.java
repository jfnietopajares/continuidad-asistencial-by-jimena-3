package com.jnieto.ui.master;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class Frm_Master. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public abstract class FrmMaster extends VerticalLayout {

	private static final long serialVersionUID = 1814307515922589664L;

	protected HorizontalLayout contenedorBotones = new HorizontalLayout();
	protected VerticalLayout contenedorCampos = new VerticalLayout();
	protected HorizontalLayout fila1 = new HorizontalLayout();
	protected HorizontalLayout fila2 = new HorizontalLayout();
	protected HorizontalLayout fila3 = new HorizontalLayout();
	protected HorizontalLayout fila4 = new HorizontalLayout();
	protected HorizontalLayout fila5 = new HorizontalLayout();
	protected HorizontalLayout fila6 = new HorizontalLayout();
	protected HorizontalLayout fila7 = new HorizontalLayout();
	protected HorizontalLayout fila8 = new HorizontalLayout();
	protected HorizontalLayout fila9 = new HorizontalLayout();
	protected HorizontalLayout fila10 = new HorizontalLayout();
	protected HorizontalLayout fila11 = new HorizontalLayout();
	protected HorizontalLayout fila12 = new HorizontalLayout();
	protected HorizontalLayout fila13 = new HorizontalLayout();
	protected HorizontalLayout fila14 = new HorizontalLayout();
	protected HorizontalLayout fila15 = new HorizontalLayout();
	protected HorizontalLayout fila16 = new HorizontalLayout();
	protected HorizontalLayout fila17 = new HorizontalLayout();
	protected HorizontalLayout fila18 = new HorizontalLayout();

	protected Button grabar, borrar, cerrar, ayuda;

	protected Label lbltitulo = null;

	/**
	 * Instantiates a new frm master.
	 */
	public FrmMaster() {
		this.setSizeUndefined();
		this.setMargin(false);
		this.addStyleName(MaterialTheme.CARD_HOVERABLE);
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);
		this.setWidthUndefined();
		this.setHeightUndefined();
		this.fila1.setMargin(false);
		this.fila2.setMargin(false);
		this.fila3.setMargin(false);
		this.fila4.setMargin(false);
		this.fila5.setMargin(false);
		this.fila6.setMargin(false);
		this.fila7.setMargin(false);
		this.fila8.setMargin(false);
		this.fila9.setMargin(false);
		this.fila10.setMargin(false);
		this.fila11.setMargin(false);
		this.fila12.setMargin(false);
		this.fila13.setMargin(false);
		this.fila14.setMargin(false);
		this.fila15.setMargin(false);
		this.fila16.setMargin(false);
		this.fila17.setMargin(false);
		this.fila18.setMargin(false);
		/*
		 * this.fila1.setSpacing(false); this.fila2.setSpacing(false);
		 * this.fila3.setSpacing(false); this.fila4.setSpacing(false);
		 * this.fila5.setSpacing(false); this.fila6.setSpacing(false);
		 * this.fila7.setSpacing(false); this.fila8.setSpacing(false);
		 * this.fila9.setSpacing(false); this.fila10.setSpacing(false);
		 */

		contenedorBotones.setMargin(false);
		contenedorBotones.setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

		contenedorCampos.setMargin(false);
		contenedorCampos.addStyleName(MaterialTheme.CARD_HOVERABLE);
		contenedorCampos.setDefaultComponentAlignment(Alignment.TOP_LEFT);
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila5, fila6, fila7, fila8, fila9, fila10,
				fila11, fila12, fila13, fila14, fila15, fila16, fila17, fila18);

		lbltitulo = new Label();
		lbltitulo.setContentMode(ContentMode.HTML);

		grabar = new ObjetosComunes().getBotonGrabar();
		grabar.addClickListener(event -> grabarClick());
		borrar = new ObjetosComunes().getBotonBorrar();
		borrar.addClickListener(event -> borrarClick());
		borrar.setEnabled(false);
		cerrar = new ObjetosComunes().getBotonCerrar();
		cerrar.addClickListener(event -> cerrarClick());
		// ayuda = new ObjetosComunes().getBotonAyuda(Constantes.BOTON_TIPO_NORMAL);
		ayuda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "~", VaadinIcons.QUESTION,
				"Muestra ayuda de la pantalla");

		ayuda.addClickListener(event -> ayudaClick());

		contenedorBotones.addComponents(lbltitulo, grabar, borrar, ayuda, cerrar);
		this.addComponents(contenedorBotones, contenedorCampos);

	}

	/**
	 * Do control botones.
	 *
	 * @param id the id
	 */
	public void doActivaBotones(Long id) {
		if (id > 0) {
			borrar.setEnabled(true);
		} else {
			borrar.setEnabled(false);
		}
	}

	public void doActivaBotones(Registro registro) {
		if (registro != null) {
			if (registro.getId() > 0) {
				borrar.setEnabled(true);
			} else {
				borrar.setEnabled(false);
			}
		} else {
			borrar.setEnabled(false);
		}
		/**
		 * si el registro esta asociado a un problema y ell problema está cerrado y han
		 * pasado más de los días definidos el contenedor de campos de desactiva para
		 * que no se modifique
		 */
		if (registro.getProblema() != null && registro.getProblema().getMotivo_baja() != null
				&& registro.getProblema().getMotivo_baja() != "") {
			LocalDate date = LocalDate.now();
			long dias = ChronoUnit.DAYS.between(registro.getProblema().getFechafin(), date);

			if (dias > Integer.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
				new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
				contenedorCampos.setEnabled(false);
				borrar.setEnabled(false);
				grabar.setEnabled(false);
			}
		}
		/**
		 * si el registro esta asociado a un episodio y el episodio está cerrado y han
		 * pasado más de los días definidos el contenedor de campos de desactiva para
		 * que no se modifique
		 */
		if (registro.getEpisodio() != null) {
			if (registro.getEpisodio().getFfinal() != null) {
				LocalDate date = LocalDate.now();
				long dias = ChronoUnit.DAYS.between(registro.getEpisodio().getFfinal(), date);
				if (dias > Integer
						.parseInt(MyUI.objParametros.getProperty(Parametros.KEY_DIAS_MODIFICACION_PROCESOS))) {
					new NotificacionInfo(Proceso.AVISO_PROCESO_CERRADO);
					contenedorCampos.setEnabled(false);
					borrar.setEnabled(false);
					grabar.setEnabled(false);
				}
			}
		}
	}

	public void doActivaBotones() {

	}

	/**
	 * Cerrar click.
	 */
	public abstract void cerrarClick();

	/**
	 * Ayuda click.
	 */
	public abstract void ayudaClick();

	/**
	 * Grabar click.
	 */
	public abstract void grabarClick();

	/**
	 * Borrar click.
	 */
	public abstract void borrarClick();

	/**
	 * Borra el registro.
	 */
	public abstract void borraElRegistro();

	/**
	 * Do valida formulario.
	 *
	 * @return true, if successful
	 */
	public abstract boolean doValidaFormulario();

	/**
	 * Gets the contenedor botones.
	 *
	 * @return the contenedor botones
	 */
	public HorizontalLayout getContenedorBotones() {
		return contenedorBotones;
	}

	/**
	 * Sets the contenedor botones.
	 *
	 * @param contenedorBotones the new contenedor botones
	 */
	public void setContenedorBotones(HorizontalLayout contenedorBotones) {
		this.contenedorBotones = contenedorBotones;
	}

	/**
	 * Gets the grabar.
	 *
	 * @return the grabar
	 */
	public Button getGrabar() {
		return grabar;
	}

	/**
	 * Sets the grabar.
	 *
	 * @param grabar the new grabar
	 */
	public void setGrabar(Button grabar) {
		this.grabar = grabar;
	}

	/**
	 * Gets the borrar.
	 *
	 * @return the borrar
	 */
	public Button getBorrar() {
		return borrar;
	}

	/**
	 * Sets the borrar.
	 *
	 * @param borrar the new borrar
	 */
	public void setBorrar(Button borrar) {
		this.borrar = borrar;
	}

	/**
	 * Gets the cerrar.
	 *
	 * @return the cerrar
	 */
	public Button getCerrar() {
		return cerrar;
	}

	/**
	 * Sets the cerrar.
	 *
	 * @param cerrar the new cerrar
	 */
	public void setCerrar(Button cerrar) {
		this.cerrar = cerrar;
	}

	/**
	 * Gets the ayuda.
	 *
	 * @return the ayuda
	 */
	public Button getAyuda() {
		return ayuda;
	}

	/**
	 * Sets the ayuda.
	 *
	 * @param ayuda the new ayuda
	 */
	public void setAyuda(Button ayuda) {
		this.ayuda = ayuda;
	}

	/**
	 * Gets the lbltitulo.
	 *
	 * @return the lbltitulo
	 */
	public Label getLbltitulo() {
		return lbltitulo;
	}

	/**
	 * Sets the lbltitulo.
	 *
	 * @param lbltitulo the new lbltitulo
	 */
	public void setLbltitulo(Label lbltitulo) {
		this.lbltitulo = lbltitulo;
	}

	public String getAyudaHtml() {
		return "";
	}
}
