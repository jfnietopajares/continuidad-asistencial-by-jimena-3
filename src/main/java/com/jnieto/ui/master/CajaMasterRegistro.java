package com.jnieto.ui.master;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Campos_r;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroPartoGestacion;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Variable;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Constantes;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 * The Class CajaMasterRegistro.
 */
public class CajaMasterRegistro extends VerticalLayout {

	private static final long serialVersionUID = -4116135528681951953L;

	private static final Logger logger = LogManager.getLogger(CajaMasterRegistro.class);

	private HorizontalLayout filaBotones = new HorizontalLayout();

	private Label lbltitulo = new Label();

	private Label lblcontenido = new Label();

	private Grid<Campos_r> grid = new Grid<Campos_r>();

	private Grid<Variable> gridVariable = new Grid<Variable>();

	private Registro registro = new Registro();

	private Button nuevo, refrescar, ayuda;

	private String nombreDeClaseFrm = null;

	private String nombreDeClaseFrmPkg = null;

	private String nombreDeClaseRegistro = null;

	/**
	 * Instantiates a new caja master registro.
	 *
	 * @param registroPara      El registro de datos que sirve de parámetro en la
	 *                          llamada del formulario. El formulario hace casting
	 *                          al tipo de registro concreto
	 * @param formularioClass   El nombre de la clase de tipo formulario que se debe
	 *                          instanciar cuando el usuario pulsa el botón de
	 *                          añadir o editar.
	 * @param tipoRegistroClass El tipo de registro, tipo
	 */
	public CajaMasterRegistro(Registro registroPara, String formularioClass, String tipoRegistroClass,
			String nombreDeClaseFrmPkg) {
		this.setMargin(false);
		// this.setHeight("300px");
		this.registro = registroPara;
		/// this.nombreDeClaseFrmPkg = "com.jnieto.ui." + formularioClass;
		this.nombreDeClaseFrmPkg = nombreDeClaseFrmPkg;
		this.nombreDeClaseFrm = formularioClass;
		this.nombreDeClaseRegistro = tipoRegistroClass;
		this.addStyleName(MaterialTheme.CARD_HOVERABLE);
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);
		// this.setHeight("600px");
		filaBotones.setMargin(false);
		filaBotones.setHeight("10px");

		lblcontenido.setContentMode(ContentMode.HTML);
		lbltitulo.setCaptionAsHtml(true);
		setValorLabel();
		/*
		 * if (registro != null) { if (((Usuario)
		 * VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
		 * .getEstado() == Usuario.USUARIO_ESTADOADMINISTRADOR) { if
		 * (registro.getProblema() != null) { lbltitulo.setCaption("idproble" +
		 * registro.getProblema().getId() + " " + registro.getDescripcion() + ":" +
		 * registro.getId() + " " + registro.getFechaHora()); } else if
		 * (registro.getEpisodio() != null) {
		 * 
		 * } else { lbltitulo.setCaption("Epi? Prb?" + registro.getDescripcion() + ":" +
		 * registro.getId() + " " + registro.getFechaHora()); } } else { lbltitulo
		 * .setCaption(registro.getDescripcion() + ":" + registro.getId() + " " +
		 * registro.getFechaHora()); } } else { lbltitulo.setCaption(new
		 * RegistroPartoGestacion().getDescripcion()); }
		 */

		nuevo = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "+", VaadinIcons.PLUS,
				"Añade un nuevo dato.");

		nuevo.addClickListener(e -> clickNuevo());
		refrescar = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "?", VaadinIcons.REFRESH,
				"Actualiza tabla de datos.");

		refrescar.addClickListener(event -> refrescarClick());

//		ayuda = new ObjetosComunes().getBotonAyuda(Constantes.BOTON_TIPO_LIGERO);
		ayuda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "~", VaadinIcons.QUESTION,
				"Muestra ayuda de la pantalla");

		ayuda.addClickListener(event -> ayudaClick());

		filaBotones.addComponents(lbltitulo, nuevo, refrescar, ayuda);
		// grid.removeHeaderRow(0);
		grid.setHeaderRowHeight(0);
		grid.setHeight("150px");
		// grid.addColumn(Campos_r::getRegistro).clearExpandRatio().getAssistiveCaption();
		grid.addColumn(Campos_r::getDescripyDato).clearExpandRatio().getAssistiveCaption();

		// grid.addColumn(Campos_r::getDato);
		/*
		 * for (int i = 0; i < grid.getHeaderRowCount(); i++) { grid.removeHeaderRow(i);
		 * }
		 */

		// grid.removeHeaderRow(grid.getHeaderRow());
		// while (grid.getHeaderRowCount() > 0) { grid.removeHeaderRow(0); }

		// grid.setHeaderVisible(false);
		// grid.removeHeaderRow(0);
		// grid.setHeaderRowHeight(0);
		grid.setBodyRowHeight(35);
		// grid.getH

		gridVariable.setHeaderRowHeight(0);
		gridVariable.setHeight("300px");
		gridVariable.setBodyRowHeight(35);
		gridVariable.addColumn(Variable::getDescripcion).setCaption("Variable").setHidable(true).clearExpandRatio()
				.getAssistiveCaption();
		gridVariable.addColumn(Variable::getDatoTipoFormato).setCaption("Valor").clearExpandRatio()
				.getAssistiveCaption();

		this.addComponents(filaBotones, gridVariable);
		refrescarClick();
	}

	/**
	 * Click nuevo.
	 */
	public void clickNuevo() {
		VentanaFrm vfmFrm = null;

		/*
		 * switch (nombreDeClaseFrm) { case "FrmRegistroPartoGestacion": vfmFrm = new
		 * VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoGestacion((RegistroPartoGestacion) registro)); break; case
		 * "FrmRegistroPartoIngreso": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoIngreso((RegistroPartoIngreso) registro)); break; case
		 * "FrmRegistroPartoInduccion": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoInduccion((RegistroPartoInduccion) registro)); break; case
		 * "FrmRegistroPartoExpulsivo": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoExpulsivo((RegistroPartoExpulsivo) registro)); break; case
		 * "FrmRegistroPartoAlumbramiento": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoAlumbramiento((RegistroPartoAlumbramiento) registro)); break;
		 * case "FrmRegistroPartoPuerperio": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoPuerperio((RegistroPartoPuerperio) registro)); break; case
		 * "FrmRegistroPartoBuenasPracticas": vfmFrm = new VentanaFrm(this.getUI(), new
		 * FrmRegistroPartoBuenasPracticas((RegistroPartoBuenasPracticas) registro));
		 * break; } vfmFrm.addCloseListener(e -> cierraVentana());
		 */

		Constructor<?> constructor;
		try {
			/**
			 * constructor para la clase que hay que instanciar
			 */

			constructor = Class.forName(nombreDeClaseFrmPkg).getConstructor(Registro.class);
			// Class<?> registroClase = Class.forName(nombreDeClaseRegistro);

			// constructor =
			// Class.forName(nombreDeClaseFrmPkg).getConstructor(registroClase);

			/**
			 * clase instanciada / * /** hace casting a VerticalLayout por que todos los
			 * formularios de VentanaFrm son tipo VerticalLayout
			 */

			// Object frmObject = constructor.newInstance(registroClase.newInstance());

			Object frmObject = constructor.newInstance(this.registro);
			vfmFrm = new VentanaFrm(this.getUI(), (VerticalLayout) frmObject,
					(String) (registro.getDescripcion() + " " + registro.getPaciente().getApellidosNombre()));
			vfmFrm.addCloseListener(e -> cierraVentana());
		} catch (NoSuchMethodException | SecurityException | ClassNotFoundException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException e1) {

			logger.error("Error en llamada clase frm=" + nombreDeClaseFrmPkg + " clase reg=" + nombreDeClaseRegistro,
					e1);
		}

	}

	/**
	 * Cierra ventana.
	 */
	public void cierraVentana() {
		refrescarClick();
	}

	/**
	 * Refrescar click.
	 */
	public void refrescarClick() {
		/*
		 * Cuando se graba un registro se crea una nueva tupla y se marca la antigua con
		 * el campo estado = 5 por eso es necesario recuperar de la base de datos el
		 * nuevo registro y actualizar el atributo en esta clase
		 * 
		 */
		if (registro != null) {
			lblcontenido.setValue("");
			if (registro.getPaciente() != null && registro.getProblema() != null) {
				Registro registro1 = new RegistroDAO().getLisRegistro(registro.getPaciente(), registro.getProblema(),
						registro.getPlantilla_editor());
				if (registro1 != null) {
					if (registro1.getListaCampos() != null) {
						grid.setItems(registro1.getListaCampos());
						grid.recalculateColumnWidths();
						gridVariable.setItems(registro1.getListaVariablesConValor());
						gridVariable.recalculateColumnWidths();
					}
					this.registro = registro1;
					setValorLabel();
					/*
					 * lbltitulo.setCaption( registro.getDescripcion() + ":" + registro.getId() +
					 * " " + registro.getFechaHora());
					 */
				}
			} else if (registro.getPaciente() != null && registro.getEpisodio() != null) {

				Registro registro1 = new RegistroDAO().getRegistroPacienteEpisodio(registro.getPaciente(),
						registro.getEpisodio(), registro.getPlantilla_editor());
				if (registro1 != null) {
					if (registro1.getListaCampos() != null) {
						grid.setItems(registro1.getListaCampos());
						grid.recalculateColumnWidths();
						gridVariable.setItems(registro1.getListaVariablesConValor());
						gridVariable.recalculateColumnWidths();
					}
					this.registro = registro1;
					lbltitulo.setCaption(
							registro.getDescripcion() + ":" + registro.getId() + " " + registro.getFechaHora());
				}
			}
		}
	}

	/**
	 * Ayuda click.
	 */
	public void ayudaClick() {

	}

	/**
	 * Instantiates a new caja master registro.
	 *
	 * @param children the children
	 */
	public CajaMasterRegistro(Component... children) {
		super(children);
	}

	/**
	 * Click ver registro completo.
	 */
	public void clickVerRegistroCompleto() {

	};

	public void setValorLabel() {
		if (registro != null) {
			if (((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
					.getEstado() == Usuario.USUARIO_ESTADOADMINISTRADOR) {
				if (registro.getProblema() != null) {
					lbltitulo.setCaption("idproble" + registro.getProblema().getId() + " " + registro.getDescripcion()
							+ ":" + registro.getId() + " " + registro.getFechaHora());
				} else if (registro.getEpisodio() != null) {

				} else {
					lbltitulo.setCaption("Epi? Prb?" + registro.getDescripcion() + ":" + registro.getId() + " "
							+ registro.getFechaHora());
				}
			} else {
				lbltitulo.setCaption(registro.getDescripcion() + registro.getFechaHora());
			}
		} else {
			lbltitulo.setCaption(new RegistroPartoGestacion().getDescripcion());
		}
	}
}
