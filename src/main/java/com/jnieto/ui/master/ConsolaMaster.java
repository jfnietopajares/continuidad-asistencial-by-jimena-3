package com.jnieto.ui.master;

import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Zona;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

public abstract class ConsolaMaster extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6164891377349315585L;
	protected Paciente paciente;
	protected Proceso proceso;
	protected Episodio episodio;
	protected EpisodioClase clase;
	protected Long subambito;
	protected Zona zona;

	protected HorizontalLayout fila1 = new HorizontalLayout();
	protected HorizontalLayout fila2 = new HorizontalLayout();
	protected HorizontalLayout fila3 = new HorizontalLayout();
	protected HorizontalLayout fila4 = new HorizontalLayout();
	protected HorizontalLayout fila5 = new HorizontalLayout();

	public ConsolaMaster(Paciente paciente, EpisodioClase clase) {
		this.paciente = paciente;
		this.clase = clase;
		doConsola();
	}

	public ConsolaMaster(Paciente paciente, Long subambito) {
		this.paciente = paciente;
		this.subambito = subambito;
		doConsola();
	}

	public ConsolaMaster(Paciente paciente, Proceso proceso) {
		this.paciente = paciente;
		this.proceso = proceso;
		doConsola();
	}

	public ConsolaMaster(Paciente paciente, Episodio episodio) {
		this.paciente = paciente;
		this.episodio = episodio;
		doConsola();
	}

	public ConsolaMaster(Paciente paciente, Episodio episodio, Proceso proceso) {
		this.paciente = paciente;
		this.episodio = episodio;
		this.proceso = proceso;
		doConsola();
	}

	public void doConsola() {
		fila1.setMargin(false);
		fila2.setMargin(false);
		fila3.setMargin(false);
		fila4.setMargin(false);
		fila5.setMargin(false);
		this.addComponents(fila1, fila2, fila3, fila4, fila5);
	}

	public ConsolaMaster(Component... children) {
		super(children);
	}

	public abstract void refrescar();

}
