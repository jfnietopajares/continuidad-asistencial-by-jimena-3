package com.jnieto.ui.master;

import java.time.LocalDate;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Zona;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.utilidades.Constantes;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public abstract class CajaMasterGrid extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6301298745930439790L;

	protected HorizontalLayout contenedorGrid = new HorizontalLayout();

	protected HorizontalLayout contenedorBotones = new HorizontalLayout();

	protected Button nuevo, editar, refrescar, cerrar, ayuda;

	protected Paciente paciente = new Paciente();

	protected EpisodioClase clase = new EpisodioClase();

	protected Long subambito;

	protected Proceso proceso = new Proceso();

	protected Episodio episodio = new Episodio();

	protected Zona zona = new Zona();

	protected LocalDate desde = null;

	protected LocalDate hasta = null;

	protected Label lbltitulo = new Label();

	public CajaMasterGrid() {

	}

	public CajaMasterGrid(Paciente paciente, EpisodioClase clase) {
		this.paciente = paciente;
		this.clase = clase;
		doCaja();
	}

	public CajaMasterGrid(Paciente paciente, EpisodioClase clase, Zona zona) {
		this.paciente = paciente;
		this.clase = clase;
		this.zona = zona;
		doCaja();
	}

	public CajaMasterGrid(Paciente paciente, Long subambito) {
		this.paciente = paciente;
		this.subambito = subambito;
		doCaja();
	}

	public CajaMasterGrid(Paciente paciente, Proceso proceso) {
		this.paciente = paciente;
		this.proceso = proceso;
		doCaja();
	}

	public CajaMasterGrid(Paciente paciente, Episodio episodio) {
		this.paciente = paciente;
		this.episodio = episodio;
		doCaja();
	}

	public CajaMasterGrid(Paciente paciente, EpisodioClase clase, Long subambito) {
		this.paciente = paciente;
		this.clase = clase;
		this.subambito = subambito;
		doCaja();
	}

	public void doCaja() {
		this.setMargin(false);
		this.addStyleName(MaterialTheme.CARD_HOVERABLE);
		this.setDefaultComponentAlignment(Alignment.TOP_LEFT);

		lbltitulo.setContentMode(ContentMode.HTML);
		lbltitulo.setHeight("20px");
		contenedorGrid.setMargin(false);
		contenedorBotones.setMargin(false);
		contenedorBotones.setHeight("15px");

		// nuevo = new ObjetosComunes().getBotonNuevo(Constantes.BOTON_TIPO_NORMAL);
		// nuevo = new ObjetosComunes().getBotonNuevo(Constantes.BOTON_TIPO_LIGERO);
		nuevo = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "+", VaadinIcons.PLUS,
				"Añade un nuevo dato.");
		nuevo.addClickListener(event -> nuevoClick());

		// editar = new ObjetosComunes().getBotonEditar(Constantes.BOTON_TIPO_LIGERO);
		editar = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "√", VaadinIcons.CHECK,
				"Edita el dato seleccionado.");

		editar.addClickListener(event -> editarClick());

		// refrescar = new
		// ObjetosComunes().getBotonRefrescar(Constantes.BOTON_TIPO_LIGERO);
		refrescar = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "~", VaadinIcons.REFRESH,
				"Actualiza tabla de datos.");

		refrescar.addClickListener(event -> refrescarClick());

		// ayuda = new ObjetosComunes().getBotonAyuda(Constantes.BOTON_TIPO_LIGERO);
		ayuda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_LIGERO, "40px", "?", VaadinIcons.QUESTION,
				"Muestra ayuda de la pantalla");

		ayuda.addClickListener(event -> ayudaClick());

		contenedorBotones.addComponents(lbltitulo, nuevo, editar, refrescar, ayuda);
		this.addComponents(contenedorBotones, contenedorGrid);
	}

	public abstract void nuevoClick();

	public abstract void editarClick();

	public abstract void refrescarClick();

	public abstract void ayudaClick();

	public abstract void seleccionaClick();

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Long getSubambito() {
		return subambito;
	}

	public void setSubambito(Long subambito) {
		this.subambito = subambito;
	}

	public EpisodioClase getClase() {
		return clase;
	}

	public void setClase(EpisodioClase clase) {
		this.clase = clase;
	}

	public Proceso getProceso() {
		return proceso;
	}

	public void setProceso(Proceso proceso) {
		this.proceso = proceso;
	}

	public Episodio getEpisodio() {
		return episodio;
	}

	public void setEpisodio(Episodio episodio) {
		this.episodio = episodio;
	}

}
