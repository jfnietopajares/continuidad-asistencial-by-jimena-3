package com.jnieto.ui.master;

import java.io.File;
import java.time.LocalDate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.controlador.Uploaderr;
import com.jnieto.dao.InformesDAO;
import com.jnieto.entity.Informe;
import com.jnieto.entity.Proceso;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.PanelPaciente;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.ProgressListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Window;

/**
 * The Class VentanaUpload.
 */
public class VentanaUpload extends Window implements SucceededListener, FailedListener, ProgressListener {

	private static final long serialVersionUID = 6992582432390213459L;

	private static final Logger logger = LogManager.getLogger(VentanaUpload.class);

	public static final String TIPOPDF = "application/pdf";

	private UI ui;

	private Window subWindow;

	private FormLayout contenedorVentada = new FormLayout();
	private Label texto = new Label();

	private String nombre = "Adjuntar ficheros";

	private Proceso proceso;

	Upload upload1;
	Uploaderr receiver;
	Upload upload;
	String filenameString;

	public VentanaUpload(UI ui, Proceso proceso) {
		this.proceso = proceso;
		this.ui = ui;

		// Crea una ventana modal
		subWindow = new Window(nombre);
		subWindow.setWidth("800px");
		subWindow.setHeight("600px");
		subWindow.center();
		subWindow.setModal(true);
		subWindow.isResizeLazy();

		// Crea un layout y lo añade a la ventana
		contenedorVentada = new FormLayout();
		contenedorVentada.setMargin(true);
		contenedorVentada.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
		subWindow.setContent(contenedorVentada);
		contenedorVentada.addComponents(texto);
		// Abrir en el UI de la aplicación
		ui.addWindow(subWindow);
		contenedorVentada.addComponent(new PanelPaciente(proceso.getPaciente()));

		receiver = new Uploaderr();

		upload = new Upload(null, receiver);
		upload.setImmediateMode(true);
		upload.setButtonCaption("Selecciona fichero");
		upload.setIcon(VaadinIcons.UPLOAD);
		upload.addProgressListener(this);
		upload.addSucceededListener(this);
		upload.addFailedListener(this);
		contenedorVentada.addComponents(upload);
	}

	public VentanaUpload(String caption) {
		super(caption);
	}

	public VentanaUpload(String caption, Component content) {
		super(caption, content);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {

		File file = receiver.getFile();
		contenedorVentada.removeAllComponents();
		contenedorVentada.addComponent(new PanelPaciente(proceso.getPaciente()));
		contenedorVentada.addComponents(upload);

		if (event.getMIMEType().equals(VentanaUpload.TIPOPDF)) {

			Button grabar = new ObjetosComunes().getBotonGrabar();
			grabar.addClickListener(e -> clicGrabar(file));
			grabar.setCaption("Confirma grabación ");
			contenedorVentada.addComponent(grabar);

			Embedded pdf = new Embedded("", new FileResource(file));
			pdf.setMimeType("application/pdf");
			pdf.setType(Embedded.TYPE_BROWSER);
			pdf.setHeight("600px");
			pdf.setWidth("400px");
			contenedorVentada.addComponent(pdf);
		} else {
			new NotificacionInfo(NotificacionInfo.ERRORTIPOFICHERONOVALIDO);
		}

	}

	@Override
	public void updateProgress(long readBytes, long contentLength) {
		logger.info("UPLOAD Status: " + readBytes + "----/" + contentLength);

	}

	@Override
	public void uploadFailed(FailedEvent event) {
		new NotificacionInfo(" Fallo en la carga del fichero ");
	}

	public void clicGrabar(File file) {
		Informe informe = new Informe();
		informe.setPaciente(proceso.getPaciente());
		informe.setProblema(proceso);
		informe.setServicio(proceso.getServicio());
		informe.setDescripcion(informe.getDescripcionSubambito(proceso.getSubambito()));
		informe.setFicheroInformeFile(file);
		informe.setTipo_documento(Informe.getTipoDocumentoSubambito(proceso.getSubambito()));
		informe.setFecha(LocalDate.now());
		informe.setHora(Utilidades.getHoraNumeroAcual());

		Long idLong = new InformesDAO().getIdInformeDesTipo(proceso.getPaciente(), informe.getDescripcion(),
				informe.getTipo_documento(), proceso.getId());

		if (idLong.equals(new Long(0))) {

			if (!new InformesDAO().insertaDatos(informe)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				subWindow.close();
			}

		} else {
			informe.setId(idLong);
			if (!new InformesDAO().actualizaFichero(informe)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
				subWindow.close();
			}
		}
	}

}
