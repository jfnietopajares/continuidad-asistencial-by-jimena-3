package com.jnieto.ui.hdiaonco;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroOncoEvolutivo;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.master.FrmMaster;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.TextArea;

public class FrmRegistroOncoEvolutivo extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7650487402023385593L;

	private TextArea observaciones = new TextArea("Observaciones");

	private RegistroOncoEvolutivo registroEvolutivo;

	private Binder<RegistroOncoEvolutivo> binder = new Binder<RegistroOncoEvolutivo>();

	public FrmRegistroOncoEvolutivo(Registro registro) {
		registroEvolutivo = (RegistroOncoEvolutivo) registro;
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1);

		observaciones.setWidth("600px");
		observaciones.setHeight("100px");
		observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(RegistroOncoEvolutivo::getObservacionesString,
				RegistroOncoEvolutivo::setObservaciones);
		fila1.addComponent(observaciones);

		binder.readBean(registroEvolutivo);
		doActivaBotones(registroEvolutivo);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroEvolutivo);
			if (!new RegistroDAO().grabaDatos(registroEvolutivo)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registroEvolutivo != null) {
			super.doActivaBotones(registroEvolutivo);
		}

	}
}
