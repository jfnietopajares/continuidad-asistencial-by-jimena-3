package com.jnieto.ui.hdiaonco;

import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.RegistroOncoCuidados;
import com.jnieto.entity.RegistroOncoEvolutivo;
import com.jnieto.entity.RegistroOncoValoracion;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Zona;
import com.jnieto.ui.CajaEnfCtes;
import com.jnieto.ui.CajaEpisodio;
import com.jnieto.ui.PanelPaciente;
import com.jnieto.ui.PantallaHCE;
import com.jnieto.ui.master.CajaMasterRegistro;
import com.jnieto.ui.master.ConsolaMaster;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class ConsolaHdiaOnco extends ConsolaMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4438773938016517429L;

	private PanelPaciente cajaPaciente;
	private CajaEnfCtes cajaEnfCtes;
	private CajaEpisodio cajaEpisodio;
	private CajaMasterRegistro cajaValoracion;
	private CajaMasterRegistro cajaEvolutivo;
	private CajaMasterRegistro cajaCuidados;

	public static String NOMBREPACKAGE = "com.jnieto.ui.hdiaonco.";

	public ConsolaHdiaOnco(Paciente paciente, EpisodioClase clase) {
		super(paciente, clase);
		zona = Zona.ZONA_HDIA_ONCO;
	}

	public ConsolaHdiaOnco(Paciente paciente, Long subambito) {
		super(paciente, subambito);
		zona = Zona.ZONA_HDIA_ONCO;
	}

	public ConsolaHdiaOnco(Paciente paciente, Proceso proceso) {
		super(paciente, proceso);
		zona = Zona.ZONA_HDIA_ONCO;
	}

	public ConsolaHdiaOnco(Paciente paciente, Episodio episodio) {
		super(paciente, episodio);
		zona = Zona.ZONA_HDIA_ONCO;
		cajaPaciente = new PanelPaciente(paciente);
		paciente.setEpisodioActual(episodio);
		cajaEnfCtes = new CajaEnfCtes(paciente, episodio);
		cajaEpisodio = new CajaEpisodio(paciente, episodio.getClase(), zona);

		RegistroOncoValoracion registroOncoValoracion = (RegistroOncoValoracion) new RegistroDAO()
				.getRegistroPacienteEpisodio(episodio.getPaciente(), episodio,
						RegistroOncoValoracion.PLANTILLLA_EDITOR_ONCO_VALORACION);
		if (registroOncoValoracion == null) {
			registroOncoValoracion = new RegistroOncoValoracion();
			registroOncoValoracion.setServicio(Servicio.SERVICIO_ONCOLOGIA);
			registroOncoValoracion.setPaciente(paciente);
			registroOncoValoracion.setEpisodio(episodio);
		}

		cajaValoracion = new CajaMasterRegistro(registroOncoValoracion, "FrmRegistroOncoValoracion",
				"RegistroOncoValoracion", NOMBREPACKAGE + "FrmRegistroOncoValoracion");

		RegistroOncoEvolutivo registroOncoEvolutivo = (RegistroOncoEvolutivo) new RegistroDAO()
				.getRegistroPacienteEpisodio(episodio.getPaciente(), episodio,
						RegistroOncoEvolutivo.PLANTILLLA_EDITOR_ONCO_EVOLUTI);
		if (registroOncoEvolutivo == null) {
			registroOncoEvolutivo = new RegistroOncoEvolutivo();
			registroOncoEvolutivo.setServicio(Servicio.SERVICIO_ONCOLOGIA);
			registroOncoEvolutivo.setPaciente(paciente);
			registroOncoEvolutivo.setEpisodio(episodio);
		}

		cajaEvolutivo = new CajaMasterRegistro(registroOncoEvolutivo, "FrmRegistroOncoEvolutivo",
				"RegistroOncoEvolutivo", NOMBREPACKAGE + "FrmRegistroOncoEvolutivo");

		RegistroOncoCuidados registroOncoCuidados = new RegistroOncoCuidados();
		registroOncoCuidados.setServicio(Servicio.SERVICIO_ONCOLOGIA);
		registroOncoCuidados.setPaciente(paciente);
		registroOncoCuidados.setEpisodio(episodio);
		cajaCuidados = new CajaMasterRegistro(registroOncoCuidados, "FrmRegistroOncoCuidados", "RegistroOncoCuidados",
				NOMBREPACKAGE + "FrmRegistroOncoCuidados");
		// Button bbButton = new ObjetosComunes().getBotonNuevo("+");
		// bbButton.addClickListener(e -> cc());
		cajaPaciente.addClickListener(e -> clickHce());
		fila1.addComponents(cajaPaciente, cajaEnfCtes);
		fila2.addComponents(cajaEpisodio, cajaValoracion);
		fila3.addComponents(cajaEvolutivo, cajaCuidados);
	}

	public void clickHce() {
		TabSheet tabSheet = ((PantallaHadiOnco) this.getParent().getParent().getParent()).getTabsheet();
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(new PantallaHCE(paciente, episodio.getCentro(), episodio.getServicio()));
		tabSheet.addTab(contenedorTabLayout, paciente.getApellidosNombre()).setClosable(true);
		tabSheet.setSelectedTab(tabSheet.getTab(contenedorTabLayout));
	}

	public ConsolaHdiaOnco(Paciente paciente, Episodio episodio, Proceso proceso) {
		super(paciente, episodio, proceso);
	}

	public ConsolaHdiaOnco(Component... children) {
		super(children);
	}

	@Override
	public void refrescar() {

	}

}
