package com.jnieto.ui.hdiaonco;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.dao.AgendaDAO;
import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Variable;
import com.jnieto.entity.Zona;
import com.jnieto.reports.HdiaOncoPdf;
import com.jnieto.ui.FrmEpisodio;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.PantallaCalendario;
import com.jnieto.ui.PantallaMapaCamas;
import com.jnieto.ui.master.VentanaFrm;
import com.jnieto.ui.master.VentanaVerPdf;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

public class PantallaHadiOnco extends HorizontalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8153927333391357414L;

	private TabSheet tabsheet = new TabSheet();
	private VerticalLayout contenedorTab = new VerticalLayout();
	private HorizontalLayout contenedorFiltroI1 = new HorizontalLayout();
	private HorizontalLayout contenedorFiltroI2 = new HorizontalLayout();
	private HorizontalLayout contenedorFiltroD = new HorizontalLayout();
	private HorizontalSplitPanel split = new HorizontalSplitPanel();

	private VerticalLayout columnaCitados = new VerticalLayout();
	private VerticalLayout columnaProgramados = new VerticalLayout();

	private DateField fechaCitas = new DateField();
	private DateField fechaTrata = new DateField();
	private CheckBox nuevos = new CheckBox("Nuev");
	private CheckBox revisiones = new CheckBox("Rev");
	private CheckBox tratamientos = new CheckBox("Tra");
	private CheckBox ttl = new CheckBox("Ttl");
	private CheckBox ttm = new CheckBox("Ttm");
	private CheckBox ttc = new CheckBox("Ttc");
	private CheckBox tto = new CheckBox("Tto");

	private CheckBox oncoServicio = new CheckBox("ONC");
	private CheckBox hemaServicio = new CheckBox("HEM");

	private CheckBox pendientes = new CheckBox("Pendientes");
	private CheckBox realizados = new CheckBox("Realizados");
	private CheckBox enproceso = new CheckBox("En proceso");
	private Button refrescarIzquierda, refrescarDerecha;
	private ComboBox<Agenda> comboAgendas;

	private Grid<Episodio> gridCitados = new Grid<Episodio>();
	private Grid<Episodio> gridProgramados = new Grid<Episodio>();

	private ArrayList<Episodio> listaCitados = new ArrayList<Episodio>();
	private ArrayList<Episodio> listaProgramados = new ArrayList<Episodio>();

	private final String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	private Button pdfBoton = new Button("");
	private Button pdfCalendario;
	private Button mapa;

	public PantallaHadiOnco() {
		this.setMargin(false);
		this.setSpacing(false);
		this.addComponent(tabsheet);
		this.setSizeFull();
		this.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

		FileResource resourcePdf = new FileResource(new File(basepath + "/WEB-INF/images/pdf.png"));
		pdfBoton.setIcon(resourcePdf);
		pdfBoton.addClickListener(e -> clickPdfCitados());

		pdfCalendario = new ObjetosComunes().getBotonCalendarioVerCitas();
		pdfCalendario.addClickListener(e -> clickPdfCalendario());

		mapa = new ObjetosComunes().getBotonMapaCamas();
		mapa.addClickListener(e -> clickMapa());

		tabsheet.setStyleName(MaterialTheme.TABSHEET_FRAMED);
		tabsheet.setResponsive(true);
		tabsheet.setSizeFull();
		tabsheet.addStyleName(MaterialTheme.PANEL_WELL);

		contenedorTab.setMargin(false);
		contenedorTab.setSpacing(false);
		contenedorTab.setSizeFull();
		contenedorTab.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

		contenedorFiltroI1.setMargin(false);
		contenedorFiltroI2.setMargin(false);
		contenedorFiltroD.setMargin(false);

		columnaCitados.setMargin(false);
		columnaCitados.setSpacing(false);
		columnaCitados.setHeight("100%");
		columnaCitados.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);

		columnaProgramados.setMargin(false);
		columnaProgramados.setSpacing(false);
		// .setHeight("1000px");
		// columnaProgramados.setSizeFull();

		split.addComponent(columnaCitados);
		// split.setSizeFull();
		// split.setHeight("800px");
		split.addStyleNames(MaterialTheme.LAYOUT_COMPONENT_GROUP_FLAT);
		// splitIzquierdoPanel.setHeightUndefined();
		split.setFirstComponent(columnaCitados);
		split.setSecondComponent(columnaProgramados);

		contenedorTab.addComponents(split);
		// contenedorTab.addComponents(columnaCitados, columnaProgramados);

		fechaCitas.setValue(LocalDate.now());
		fechaCitas.setDateFormat("dd/MM/yyyy");
		fechaCitas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		fechaCitas.addValueChangeListener(e -> cargaListaPacientes());
		fechaCitas.setWidth("140px");

		nuevos.addValueChangeListener(e -> cargaListaPacientes());
		revisiones.addValueChangeListener(e -> cargaListaPacientes());
		tratamientos.addValueChangeListener(e -> cargaListaPacientes());
		ttl.addValueChangeListener(e -> cargaListaPacientes());
		ttm.addValueChangeListener(e -> cargaListaPacientes());
		ttc.addValueChangeListener(e -> cargaListaPacientes());
		tto.addValueChangeListener(e -> cargaListaPacientes());

		oncoServicio.setValue(true);
		oncoServicio.addValueChangeListener(e -> cargaListaAgendas());
		hemaServicio.addValueChangeListener(e -> cargaListaAgendas());

		comboAgendas = new ObjetosComunes().getComboAgenda(Centro.CENTRO_DEFECTO, Servicio.SERVICIO_ONCOLOGIA, null,
				null);
		comboAgendas.addSelectionListener(event -> cargaListaPacientes());
		comboAgendas.setWidth("300px");

		fechaTrata.setValue(LocalDate.now());
		fechaTrata.setDateFormat("dd/MM/yyyy");
		fechaTrata.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		fechaTrata.addValueChangeListener(e -> cargaListaPacientes());
		fechaTrata.setWidth("140px");

		refrescarIzquierda = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "~",
				VaadinIcons.REFRESH, "Actualiza tabla de datos.");
		refrescarIzquierda.addClickListener(e -> cargaListaPacientes());

		refrescarDerecha = new ObjetosComunes().getBoton(Constantes.BOTON_TIPO_NORMAL, "40px", "~", VaadinIcons.REFRESH,
				"Actualiza tabla de datos.");
		refrescarDerecha.addClickListener(e -> cargaListaPacientes());

		contenedorFiltroI1.addComponents(fechaCitas, nuevos, revisiones, tratamientos, ttl, ttm, ttc, tto, comboAgendas,
				pdfBoton, refrescarIzquierda);
		contenedorFiltroI2.addComponents(oncoServicio, hemaServicio, comboAgendas, pdfBoton, pdfCalendario, mapa,
				refrescarIzquierda);

		contenedorFiltroD.addComponents(fechaTrata, pendientes, realizados, refrescarDerecha);

		gridCitados.setCaption("<b>Pacientes citados en las agendas de oncología no programados</b>");
		gridCitados.setCaptionAsHtml(true);

		gridCitados.addColumn(Episodio::getAgendaCodigoHora).setCaption("Age.Hora").setWidth(150);
		gridCitados.addColumn(Episodio::getPacienteNhc).setCaption("Nhc").setWidth(120);
		gridCitados.addColumn(Episodio::getPacienteApellidos).setCaption("Nombre");
		gridCitados.addColumn(Episodio::getPrestacionDescripcion).setCaption("Prestación");
		gridCitados.addSelectionListener(e -> seleccionaCitado());
		gridCitados.setHeight("800px");
		gridCitados.setWidth("100%");
		gridCitados.setStyleGenerator(e -> getEstilo(e));

		columnaCitados.addComponents(contenedorFiltroI1, contenedorFiltroI2, gridCitados);

		gridProgramados.setCaption("Tratamientos Planificados");
		gridProgramados.addColumn(Episodio::getCamaCodigoHora).setCaption("Cama.Hora").setWidth(150);
		gridProgramados.addColumn(Episodio::getPacienteNhc).setCaption("Nhc").setWidth(120);
		gridProgramados.addColumn(Episodio::getPacienteApellidos).setCaption("Nombre");
		gridProgramados.addColumn(Episodio::getPrestacionDescripcion).setCaption("Prestación");
		gridProgramados.setHeight("800px");
		gridProgramados.setWidth("100%");
		gridProgramados.setStyleGenerator(e -> getEstiloProgramado(e));
		gridProgramados.addSelectionListener(e -> seleccionaProgramado());

		columnaProgramados.addComponents(contenedorFiltroD, gridProgramados);

		tabsheet.addTab(contenedorTab, "Pacientes");
		cargaListaPacientes();
	}

	/*
	 * public void clickHce() { if (gridCitados.getSelectedItems().size() == 1) {
	 * Episodio episodio = gridCitados.getSelectedItems().iterator().next();
	 * VerticalLayout contenedorTabLayout = new VerticalLayout();
	 * contenedorTabLayout.addComponent( new PantallaHCE(episodio.getPaciente(),
	 * episodio.getCentro(), episodio.getServicio()));
	 * tabsheet.addTab(contenedorTabLayout,
	 * episodio.getPaciente().getApellidosNombre()).setClosable(true);
	 * tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout)); } }
	 */

	public void clickPdfCitados() {
		Agenda agenda = null;
		ArrayList<Variable> listaItemsPrestacion = getValoreCheck();

		if (comboAgendas.getSelectedItem().isPresent())
			agenda = comboAgendas.getSelectedItem().get();

		ArrayList<Servicio> servicios = new ArrayList<Servicio>();

		if (oncoServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_ONCOLOGIA);
		}
		if (hemaServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_HEMATOLGIA);
		}
		if (servicios.size() == 0) {
			servicios.add(Servicio.SERVICIO_ONCOLOGIA);
			servicios.add(Servicio.SERVICIO_HEMATOLGIA);
		}
		File fpdf = new HdiaOncoPdf(Centro.HNSS, servicios, agenda, fechaCitas.getValue(), listaItemsPrestacion)
				.getFile();
		if (fpdf != null) {
			new VentanaVerPdf(fpdf, this.getUI());
		}

	}

	public void clickPdfCalendario() {
		/*
		 * File fpdf = new CalendarioCitasPDF(Centro.HNSS, Servicio.SERVICIO_ONCOLOGIA,
		 * fechaCitas.getValue()).getFile(); if (fpdf != null) { Embedded pdf = new
		 * Embedded("", new FileResource(fpdf)); pdf.setMimeType("application/pdf");
		 * pdf.setType(Embedded.TYPE_BROWSER); pdf.setWidth("750px");
		 * pdf.setHeight("610px"); VerticalLayout contenedorTabLayout = new
		 * VerticalLayout(); contenedorTabLayout.addComponent(pdf);
		 * tabsheet.addTab(contenedorTabLayout, "Calendario").setClosable(true);
		 * tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout)); }
		 */
		PantallaCalendario pt = new PantallaCalendario(fechaCitas.getValue(), Centro.CENTRO_DEFECTO,
				Servicio.SERVICIO_ONCOLOGIA, comboAgendas.getValue());
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(pt);
		tabsheet.addTab(contenedorTabLayout, "Calendario").setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
	}

	public void clickMapa() {

		PantallaMapaCamas pt = new PantallaMapaCamas(this.getUI(), Centro.CENTRO_DEFECTO, Zona.ZONA_HDIA_ONCO,
				Episodio.CLASE_HDIA);
		VerticalLayout contenedorTabLayout = new VerticalLayout();
		contenedorTabLayout.addComponent(pt.getMapa());
		tabsheet.addTab(contenedorTabLayout, "Mapa").setClosable(true);
		tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
	}

	public void seleccionaCitado() {
		if (gridCitados.getSelectedItems().size() > 0) {
			// gridCitados.setEnabled(false);
			Episodio epicitado = gridCitados.getSelectedItems().iterator().next();
			if (new EpisodioDAO().getTieneEpisodioFechaClase(epicitado.getPaciente(),
					Utilidades.getFechaNumeroyyymmddDefecha(epicitado.getFinicio()),
					Episodio.CLASE_HDIA.getId()) == true) {
				new NotificacionInfo("Cita ya planificada en HDIA");
			} else {
				Episodio epiNew = new Episodio(epicitado);
				epiNew.setId(new Long(0));
				epiNew.setClase(Episodio.CLASE_HDIA);
				epiNew.setFfinal(null);
				VentanaFrm ventanaFrm = new VentanaFrm(getUI(), new FrmEpisodio(epiNew, Zona.ZONA_HDIA_ONCO),
						"Programa tratamiento oncología" + "");
				ventanaFrm.addCloseListener(e -> cargaListaPacientes());
			}
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	public void seleccionaProgramado() {
		if (gridProgramados.getSelectedItems().size() > 0) {
			Episodio episodioProgramado = gridProgramados.getSelectedItems().iterator().next();
			VerticalLayout contenedorTabLayout = new VerticalLayout();
			contenedorTabLayout.addComponent(new ConsolaHdiaOnco(episodioProgramado.getPaciente(), episodioProgramado));
			tabsheet.addTab(contenedorTabLayout, episodioProgramado.getPaciente().getApellidosNombre())
					.setClosable(true);
			tabsheet.setSelectedTab(tabsheet.getTab(contenedorTabLayout));
		} else {
			new NotificacionInfo(NotificacionInfo.GRID_SIN_ELEGIR_DATO);
		}
	}

	public ArrayList<Variable> getValoreCheck() {
		ArrayList<Variable> listaItemPrestaciones = new ArrayList<Variable>();
		if (nuevos.getValue())
			listaItemPrestaciones.add(Variable.CITA_PRESTACION_NUEVO);
		if (revisiones.getValue())
			listaItemPrestaciones.add(Variable.CITA_PRESTACION_REVISION);
		if (tratamientos.getValue()) {
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTM);
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTL);
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTC);
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTO);
		}
		if (ttl.getValue())
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTL);
		if (ttm.getValue())
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTM);
		if (ttc.getValue())
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTC);
		if (tto.getValue())
			listaItemPrestaciones.add(Variable.ONCO_PRESTACION_TTO);
		return listaItemPrestaciones;
	}

	public void cargaListaAgendas() {
		ArrayList<Servicio> servicios = new ArrayList<Servicio>();
		if (oncoServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_ONCOLOGIA);
		}
		if (hemaServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_HEMATOLGIA);

		}
		if (oncoServicio.getValue() == true) {
			comboAgendas.setItems(new AgendaDAO().getListaAgendas(Centro.CENTRO_DEFECTO, servicios));
		}
		cargaListaPacientes();
	}

	public void cargaListaPacientes() {
		Agenda agenda = null;
		if (comboAgendas.getSelectedItem().isPresent())
			agenda = comboAgendas.getSelectedItem().get();

		ArrayList<Variable> listaItemPrestaciones = getValoreCheck();
		ArrayList<Servicio> servicios = new ArrayList<Servicio>();
		Servicio servicio = new Servicio();
		if (oncoServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_ONCOLOGIA);
			servicio = Servicio.SERVICIO_ONCOLOGIA;
		} else if (hemaServicio.getValue() == true) {
			servicios.add(Servicio.SERVICIO_HEMATOLGIA);
			servicio = servicio.SERVICIO_HEMATOLGIA;
		} else {
			servicios.add(Servicio.SERVICIO_ONCOLOGIA);
			servicios.add(Servicio.SERVICIO_HEMATOLGIA);
		}

		if (listaItemPrestaciones.size() == 0) {
			listaCitados = new EpisodioDAO().getListaEpisodios(Episodio.CLASE_CONSULTAS.getId(), Centro.HNSS, servicio,
					agenda, null, fechaCitas.getValue(), "FECHAHORA");
		} else {
			listaCitados = new EpisodioDAO().getListaEpisodios(Episodio.CLASE_CONSULTAS.getId(), Centro.HNSS, servicios,
					agenda, null, fechaCitas.getValue(), "FECHAHORA", listaItemPrestaciones);
		}
		gridCitados.setItems(listaCitados);

		listaProgramados = new EpisodioDAO().getListaEpisodios(Episodio.CLASE_HDIA.getId(), Centro.HNSS,
				Servicio.SERVICIO_ONCOLOGIA, null, null, fechaTrata.getValue(), "FECHAHORA");
		gridProgramados.setItems(listaProgramados);
	}

	private String getEstilo(Episodio e) {
		if (new EpisodioDAO().getTieneEpisodioFechaClase(e.getPaciente(),
				Utilidades.getFechaNumeroyyymmddDefecha(e.getFinicio()), Episodio.CLASE_HDIA.getId()) == true) {
			return "RED";
		} else if (e.getFfinal() != null) {
			// return "RED";
		}
		return null;
	}

	private String getEstiloProgramado(Episodio e) {
		if (e.getFfinal() != null) {
			return "RED";
		}
		return null;
	}

	public TabSheet getTabsheet() {
		return tabsheet;
	}

	public void setTabsheet(TabSheet tabsheet) {
		this.tabsheet = tabsheet;
	}

}
