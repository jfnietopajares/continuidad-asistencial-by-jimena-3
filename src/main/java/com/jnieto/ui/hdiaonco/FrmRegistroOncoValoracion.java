package com.jnieto.ui.hdiaonco;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroOncoValoracion;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.validators.ValidatorEntero;
import com.jnieto.validators.ValidatorMovil;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class FrmRegistroOncoValoracion extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4425055667760544266L;

	private TextField diagnostico = new TextField("Diagnóstico");
	private TextField alergias = new TextField("Alergias");
	private TextArea antecedentes = new TextArea("Antecedentes");
	private TextArea tratamientoqt = new TextArea("Tratamiento QT");
	private TextField sitFamiliarSocial = new TextField("Situación familiar y social");
	private TextField telefono;
	private TextField ciclos = new TextField("Nº Ciclos");
	private DateField fechaInicio;
	private DateField fechaFin;
//respiracion
	private CheckBox respiNoAlteracion = new CheckBox("Sin alteración");
	private CheckBox respidisnea = new CheckBox("Disnea");
	private CheckBox respiOxigeno = new CheckBox("Oxigeno");
	private TextField respiOtros = new TextField("Otros:");
//alimentacion
	private ComboBox<String> aliDietaHabitual;
	private ComboBox<String> aliIngestaLiquidos;
	private TextField aliSuplemento = new TextField("Suplementos");
	private CheckBox aliDificultadDeglu = new CheckBox("Dif.Deglu.");
	private CheckBox aliNauseas = new CheckBox("Nauseas");
	private CheckBox aliVomitos = new CheckBox("Vómitos");
	private CheckBox aliAteraGusto = new CheckBox("Alteración gustos");
	private TextField aliOtros = new TextField("Otra valoración alimentación");
	// eliminación urinaira
	private TextField eliuriCantidad = new TextField("Cantidad");
	private TextField eliuriColor = new TextField("Color");
	private CheckBox eliuriEscozor = new CheckBox("Escozor");
	private CheckBox eliuriPortadorSV = new CheckBox("SV");
	private CheckBox eliuriUrostomia = new CheckBox("Urostomía");
	private CheckBox eliuriNefrostomia = new CheckBox("Nefrostomia");
	private TextField eliuriOtros = new TextField("Otros");
// eliminación intestinal
	private TextField elifecalDiarrea = new TextField("Diarrea");
	private TextField elifecalEstrenimiento = new TextField("Esteñimiento");
	private CheckBox elifecalOstomia = new CheckBox("Ostomía");
	private TextField elifecalOtro = new TextField("Otros");
//Movilizacion
	private ComboBox<String> movilizacion;
	private TextField movilizacionOtros = new TextField("Otros");
	private ComboBox<String> karnofsky;
// reposo sueño
	private ComboBox<String> repoSueno;
	private CheckBox repoSueInfusiones = new CheckBox("Precisa infusiones");
	private TextField repoSueMedicacion = new TextField("Precisa medicación");
	private TextField repoSueOtros = new TextField("Otros ");
// vestido y aseo
	private ComboBox<String> vestidoAseo;
	private TextField vestidoAseOtros = new TextField("Otros");
// higiene piel	
	private CheckBox pielNoAlteracion = new CheckBox("Sin alteración");
	private CheckBox pielSeca = new CheckBox("Seca");
	private CheckBox pielPalidez = new CheckBox("Palidez");
	private CheckBox pielIctericia = new CheckBox("Ictericia");
	private CheckBox pielPrurito = new CheckBox("Prurito");
	private CheckBox pielEritema = new CheckBox("Eritema");
	private CheckBox pielEdemas = new CheckBox("Edemas");
	private CheckBox pielHipersensibilidad = new CheckBox("Hipersensibilidad");
	private CheckBox pielHematomas = new CheckBox("Hematomas");
	private CheckBox pielFlebitis = new CheckBox("Flebitis");
	private ComboBox<String> pielHeridas;
	private TextField pielOtros = new TextField("Otros");
//seguridad
	private ComboBox<String> segAccesoTipo;
	private TextField segAccesoLoca = new TextField("Localización");
	private ComboBox<String> segAccesoPermeable;
//dolor
	private TextField dolorEva = new TextField("EVA");
	private TextField dolorLocalizacion = new TextField("Localización");
// situación emocional
	private ComboBox<String> emocioAnsiedad;
	private ComboBox<String> emocioDepresion;
	private TextField emocioPAD_T = new TextField("Malestar emocional PAD-T");
// comunicacion
	private ComboBox<String> comuAcompanado;
	private TextField comuOtraCompa = new TextField("Otro acompañante");
	private TextField comuRelacionFam = new TextField("Relación familiar");
	private TextField comuRelacionPer = new TextField("Rel. sanitarios");
	private CheckBox comuColabora = new CheckBox("Colabora ");
	private CheckBox comuConspiraSilen = new CheckBox("Silencio ");
	private CheckBox comuCambioSexual = new CheckBox("Cambio sexuali. ");
	private TextField comuOtro = new TextField("Otros aspectos");
// percpeción personal
	private TextField percepImagen = new TextField("Imagen personal");
	private TextField percepAutoestima = new TextField("Autoestima ");
	private CheckBox percepAlopecia = new CheckBox("Alopecia");
	private CheckBox percepMastectomia = new CheckBox("Mastectomía ");
	private CheckBox percepColostomia = new CheckBox("Colostomía ");
	private TextField percepOtros = new TextField("Otras valorciones ");
	// aprende
	private TextField aprenComprende = new TextField("Comprende la información");
	private TextField aprenDemanda = new TextField("Demanda información");

	private RegistroOncoValoracion registroValoracion;

	private Binder<RegistroOncoValoracion> binder = new Binder<RegistroOncoValoracion>();

	public FrmRegistroOncoValoracion(Registro registroParama) {
		super();
		registroValoracion = (RegistroOncoValoracion) registroParama;
		// campo del formulario
		diagnostico.setWidth("400px");
		diagnostico.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(diagnostico).bind(RegistroOncoValoracion::getDiagnosticoString,
				RegistroOncoValoracion::setDiagnostico);
		alergias.setWidth("400px");
		alergias.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(alergias).bind(RegistroOncoValoracion::getAlergiasString, RegistroOncoValoracion::setAlergias);
		fila1.addComponents(diagnostico, alergias);

		antecedentes.setWidth("400px");
		antecedentes.setHeight("50px");
		antecedentes.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(antecedentes).bind(RegistroOncoValoracion::getAntecedentesString,
				RegistroOncoValoracion::setAntecedentes);
		tratamientoqt.setWidth("400px");
		tratamientoqt.setHeight("50px");
		tratamientoqt.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tratamientoqt).bind(RegistroOncoValoracion::getTratamientoqtString,
				RegistroOncoValoracion::setTratamientoqt);
		fila2.addComponents(antecedentes, tratamientoqt);

		sitFamiliarSocial.setWidth("400px");
		sitFamiliarSocial.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(sitFamiliarSocial).bind(RegistroOncoValoracion::getSitFamiliarSocialString,
				RegistroOncoValoracion::setSitFamiliarSocial);
		telefono = new ObjetosComunes().getTelefono("Teléfono", "");
		binder.forField(telefono).withValidator(new ValidatorMovil(telefono.getValue()))
				.bind(RegistroOncoValoracion::getTelefonoString, RegistroOncoValoracion::setTelefono);
		ciclos.setWidth("50px");
		ciclos.getMaxLength();
		ciclos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(ciclos).withValidator(new ValidatorEntero(ciclos.getValue(), 1, 30))
				.bind(RegistroOncoValoracion::getCliclosString, RegistroOncoValoracion::setCliclos);
		fechaInicio = new ObjetosComunes().getFecha("F.Inicio", "inicio");
		binder.forField(fechaInicio).bind(RegistroOncoValoracion::getFechaInicioLocalDate,
				RegistroOncoValoracion::setFechaInicio);
		fechaFin = new ObjetosComunes().getFecha("F.Fin", "fin");
		binder.forField(fechaFin).bind(RegistroOncoValoracion::getFechaFinDate, RegistroOncoValoracion::setFechaFin);
		fila3.addComponents(sitFamiliarSocial, telefono, ciclos, fechaInicio, fechaFin);

		respiNoAlteracion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(respiNoAlteracion).bind(RegistroOncoValoracion::getRespiNoAlteracionBoolean,
				RegistroOncoValoracion::setRespiNoAlteracion);
		respidisnea.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(respidisnea).bind(RegistroOncoValoracion::getRespidisneaBoolean,
				RegistroOncoValoracion::setRespidisnea);
		respiOxigeno.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(respiOxigeno).bind(RegistroOncoValoracion::getRespiOxigenoBoolean,
				RegistroOncoValoracion::setRespiOxigeno);
		respiOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		respiOtros.setWidth("300px");
		binder.forField(respiOtros).bind(RegistroOncoValoracion::getRespiOtrosString,
				RegistroOncoValoracion::setRespiOtros);
		fila4.addComponents(respiNoAlteracion, respidisnea, respiOxigeno, respiOtros);
		fila4.setCaption("Respiración");
		fila4.addStyleNames(MaterialTheme.LAYOUT_WELL);

		aliDietaHabitual = new ObjetosClinicos().getComboString("Dieta", "", ObjetosClinicos.HDIA_ONCO_DIETAHABITUAL,
				"100px");
		binder.forField(aliDietaHabitual).bind(RegistroOncoValoracion::getAliDietaHabitualString,
				RegistroOncoValoracion::setAliDietaHabitual);
		aliIngestaLiquidos = new ObjetosClinicos().getComboString("Líquidos", "",
				ObjetosClinicos.HDIA_ONCO_INGESTALIQUIDOS, "100px");
		binder.forField(aliIngestaLiquidos).bind(RegistroOncoValoracion::getAliDietaHabitualString,
				RegistroOncoValoracion::setAliDietaHabitual);
		aliSuplemento.setWidth("200px");
		aliSuplemento.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliSuplemento).bind(RegistroOncoValoracion::getAliSuplementoString,
				RegistroOncoValoracion::setAliSuplemento);
		// fila5.addComponents();
		aliDificultadDeglu.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliDificultadDeglu).bind(RegistroOncoValoracion::getAliDificultadDegluBoolean,
				RegistroOncoValoracion::setAliDificultadDeglu);
		aliNauseas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliNauseas).bind(RegistroOncoValoracion::getAliNauseasBoolean,
				RegistroOncoValoracion::setAliNauseas);
		aliVomitos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliVomitos).bind(RegistroOncoValoracion::getAliVomitosBoolean,
				RegistroOncoValoracion::setAliVomitos);
		aliAteraGusto.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliAteraGusto).bind(RegistroOncoValoracion::getAliAteraGustoBoolean,
				RegistroOncoValoracion::setAliAteraGusto);
		aliOtros.setWidth("200px");
		aliOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(aliOtros).bind(RegistroOncoValoracion::getAliOtrosString, RegistroOncoValoracion::setAliOtros);
		fila5.addComponents(aliDietaHabitual, aliIngestaLiquidos, aliSuplemento, aliDificultadDeglu, aliNauseas,
				aliVomitos, aliOtros);
		fila5.setCaption("Alimentación");
		fila5.addStyleNames(MaterialTheme.LAYOUT_WELL);

		eliuriCantidad.setWidth("125px");
		eliuriCantidad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriCantidad).bind(RegistroOncoValoracion::getEliuriCantidadString,
				RegistroOncoValoracion::setEliuriCantidad);
		eliuriColor.setWidth("125px");
		eliuriColor.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriColor).bind(RegistroOncoValoracion::getEliuriColorString,
				RegistroOncoValoracion::setEliuriColor);
		eliuriEscozor.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriEscozor).bind(RegistroOncoValoracion::getEliuriEscozorBoolean,
				RegistroOncoValoracion::setEliuriEscozor);
		eliuriPortadorSV.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriPortadorSV).bind(RegistroOncoValoracion::getEliuriPortadorSVBoolean,
				RegistroOncoValoracion::setEliuriPortadorSV);
		eliuriNefrostomia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriNefrostomia).bind(RegistroOncoValoracion::geteliuriNefrostomiaboolean,
				RegistroOncoValoracion::seteliuriNefrostomia);
		eliuriUrostomia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriUrostomia).bind(RegistroOncoValoracion::getEliuriUrostomiaBoolean,
				RegistroOncoValoracion::setEliuriUrostomia);

		eliuriOtros.setWidth("250px");
		eliuriOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(eliuriOtros).bind(RegistroOncoValoracion::getEliuriOtrosString,
				RegistroOncoValoracion::setEliuriOtros);

		fila6.addComponents(eliuriCantidad, eliuriColor, eliuriEscozor, eliuriPortadorSV, eliuriNefrostomia,
				eliuriUrostomia, eliuriOtros);
		fila6.setCaption("Eliminación urinaria");
		fila6.addStyleNames(MaterialTheme.LAYOUT_WELL);

		elifecalDiarrea.setWidth("200px");
		elifecalDiarrea.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(elifecalDiarrea).bind(RegistroOncoValoracion::getElifecalDiarreaString,
				RegistroOncoValoracion::setElifecalDiarrea);

		elifecalEstrenimiento.setWidth("200px");
		elifecalEstrenimiento.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(elifecalEstrenimiento).bind(RegistroOncoValoracion::getElifecalEstrenimientoString,
				RegistroOncoValoracion::setElifecalEstrenimiento);

		elifecalOstomia.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(elifecalOstomia).bind(RegistroOncoValoracion::getElifecalOstomiaBoolean,
				RegistroOncoValoracion::setElifecalOstomia);

		elifecalOtro.setWidth("400px");
		elifecalOtro.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(elifecalOtro).bind(RegistroOncoValoracion::getElifecalOtroString,
				RegistroOncoValoracion::setElifecalOtro);

		fila7.addComponents(elifecalDiarrea, elifecalEstrenimiento, elifecalOstomia, elifecalOtro);
		fila7.setCaption("Eliminación Intestinal");
		fila7.addStyleNames(MaterialTheme.LAYOUT_WELL);

		movilizacion = new ObjetosClinicos().getComboString("Movilizacion", "", ObjetosClinicos.HDIA_ONCO_MOVILIZACION,
				"200px");
		binder.forField(movilizacion).bind(RegistroOncoValoracion::getMovilizacionString,
				RegistroOncoValoracion::setMovilizacion);
		movilizacionOtros.setWidth("200px");
		movilizacionOtros.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(movilizacionOtros).bind(RegistroOncoValoracion::getMovilizacionOtrosString,
				RegistroOncoValoracion::setMovilizacionOtros);

		karnofsky = new ObjetosClinicos().getComboString("Karnofsky", "", ObjetosClinicos.HDIA_ONCO_KARNOFSKY, "450px");
		binder.forField(karnofsky).bind(RegistroOncoValoracion::getKarnofskyString,
				RegistroOncoValoracion::setKarnofsky);

		fila8.addComponents(movilizacion, movilizacionOtros, karnofsky);
		fila8.setCaption("Movilizacion");
		fila8.addStyleNames(MaterialTheme.LAYOUT_WELL);

		repoSueno = new ObjetosClinicos().getComboString("Reposo sueño", "", ObjetosClinicos.HDIA_ONCO_TRANSTORNOSUEÑO,
				"200px");
		binder.forField(repoSueno).bind(RegistroOncoValoracion::getRepoSuenoString,
				RegistroOncoValoracion::setRepoSueno);
		binder.forField(repoSueInfusiones).bind(RegistroOncoValoracion::getRepoSueInfusionesBoolean,
				RegistroOncoValoracion::setRepoSueInfusiones);
		repoSueMedicacion.setWidth("250px");
		repoSueMedicacion.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(repoSueMedicacion).bind(RegistroOncoValoracion::getRepoSueMedicacionString,
				RegistroOncoValoracion::setRepoSueMedicacion);
		repoSueOtros.setWidth("250px");
		repoSueOtros.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(repoSueOtros).bind(RegistroOncoValoracion::getRepoSueOtrosString,
				RegistroOncoValoracion::setRepoSueOtros);

		fila9.addComponents(repoSueno, repoSueInfusiones, repoSueMedicacion, repoSueOtros);
		fila9.setCaption("Reposo sueño");
		fila9.addStyleNames(MaterialTheme.LAYOUT_WELL);

		vestidoAseo = new ObjetosClinicos().getComboString("Vestido aseo", "", ObjetosClinicos.HDIA_ONCO_VESTIDOASEO,
				"200px");
		binder.forField(vestidoAseo).bind(RegistroOncoValoracion::getVestidoAseoString,
				RegistroOncoValoracion::setVestidoAseo);
		vestidoAseOtros.setWidth("200px");
		vestidoAseOtros.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(vestidoAseOtros).bind(RegistroOncoValoracion::getVestidoAseoOtrosString,
				RegistroOncoValoracion::setVestidoAseoOtros);

		fila10.addComponents(vestidoAseo, vestidoAseOtros);
		fila10.setCaption("Vestido aseo ");
		fila10.addStyleNames(MaterialTheme.LAYOUT_WELL);

		binder.forField(pielNoAlteracion).bind(RegistroOncoValoracion::getPielNoAlteracionBoolean,
				RegistroOncoValoracion::setPielNoAlteracion);
		binder.forField(pielSeca).bind(RegistroOncoValoracion::getPielSecaBoolean, RegistroOncoValoracion::setPielSeca);
		binder.forField(pielPalidez).bind(RegistroOncoValoracion::getPielPalidezBoolean,
				RegistroOncoValoracion::setPielPalidez);
		binder.forField(pielIctericia).bind(RegistroOncoValoracion::getPielIctericiaBoolean,
				RegistroOncoValoracion::setPielIctericia);
		binder.forField(pielPrurito).bind(RegistroOncoValoracion::getPielPruritoBoolean,
				RegistroOncoValoracion::setPielPrurito);
		binder.forField(pielEritema).bind(RegistroOncoValoracion::getPielEritemaBoolean,
				RegistroOncoValoracion::setPielEritema);
		binder.forField(pielEdemas).bind(RegistroOncoValoracion::getPielEdemasBoolean,
				RegistroOncoValoracion::setPielEdemas);
		binder.forField(pielHipersensibilidad).bind(RegistroOncoValoracion::getPielHipersensibilidadBoolean,
				RegistroOncoValoracion::setPielHipersensibilidad);
		binder.forField(pielHematomas).bind(RegistroOncoValoracion::getPielHematomasBoolean,
				RegistroOncoValoracion::setPielHematomas);
		binder.forField(pielFlebitis).bind(RegistroOncoValoracion::getPielFlebitisBoolean,
				RegistroOncoValoracion::setPielFlebitis);
		fila11.addComponents(pielNoAlteracion, pielSeca, pielPalidez, pielIctericia, pielPrurito, pielEritema,
				pielEdemas, pielHipersensibilidad);

		pielHeridas = new ObjetosClinicos().getComboString("Heridas", "", ObjetosClinicos.HDIA_ONCO_PIELHERIDAS,
				"300px");
		binder.forField(pielHeridas).bind(RegistroOncoValoracion::getPielHeridasString,
				RegistroOncoValoracion::setPielHeridas);

		pielOtros.setWidth("400px");
		pielOtros.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(pielOtros).bind(RegistroOncoValoracion::getPielOtrosString,
				RegistroOncoValoracion::setPielOtros);

		fila12.addComponents(pielHematomas, pielFlebitis, pielHeridas, pielOtros);

		segAccesoTipo = new ObjetosClinicos().getComboString("Acceso venoso", "", ObjetosClinicos.HDIA_ONCO_ACCESOS,
				"200px");
		binder.forField(segAccesoTipo).bind(RegistroOncoValoracion::getSegAccesoTipoString,
				RegistroOncoValoracion::setSegAccesoTipo);

		segAccesoLoca.setWidth("400px");
		segAccesoLoca.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(segAccesoLoca).bind(RegistroOncoValoracion::getSegAccesoLocaString,
				RegistroOncoValoracion::setSegAccesoLoca);

		segAccesoPermeable = new ObjetosClinicos().getComoSiNo("Permeable", "");
		binder.forField(segAccesoPermeable).bind(RegistroOncoValoracion::getSegAccesoPermeableString,
				RegistroOncoValoracion::setSegAccesoPermeable);
		fila13.addComponents(segAccesoTipo, segAccesoLoca, segAccesoPermeable);

		dolorEva.setWidth("40px");
		dolorEva.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(dolorEva).withValidator(new ValidatorEntero(dolorEva.getValue(), 0, 10))
				.bind(RegistroOncoValoracion::getDolorEvaString, RegistroOncoValoracion::setDolorEva);

		dolorLocalizacion.setWidth("400px");
		dolorLocalizacion.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(dolorLocalizacion).bind(RegistroOncoValoracion::getDolorLocalizacionString,
				RegistroOncoValoracion::setDolorLocalizacion);
		fila14.addComponents(dolorEva, dolorLocalizacion);

		emocioAnsiedad = new ObjetosClinicos().getComboString("Ansiedad", "", ObjetosClinicos.VALORES_0_5, "80px");
		binder.forField(emocioAnsiedad).bind(RegistroOncoValoracion::getEmocioAnsiedadString,
				RegistroOncoValoracion::setEmocioAnsiedad);
		emocioDepresion = new ObjetosClinicos().getComboString("Depresión", "", ObjetosClinicos.VALORES_0_5, "80px");
		binder.forField(emocioDepresion).bind(RegistroOncoValoracion::getEmocioDepresionString,
				RegistroOncoValoracion::setEmocioDepresion);
		emocioPAD_T.setWidth("300px");
		emocioPAD_T.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(emocioPAD_T).bind(RegistroOncoValoracion::getEmocioPAD_TString,
				RegistroOncoValoracion::setEmocioPAD_T);

		fila14.addComponents(dolorEva, dolorLocalizacion, emocioAnsiedad, emocioDepresion, emocioPAD_T);

		comuAcompanado = new ObjetosClinicos().getComboString("Acompañante", "", ObjetosClinicos.ACOMPANANTES, "150px");
		binder.forField(comuAcompanado).bind(RegistroOncoValoracion::getComuAcompanadoString,
				RegistroOncoValoracion::setComuAcompanado);
		comuOtraCompa.setWidth("60px");
		comuOtraCompa.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(comuOtraCompa).bind(RegistroOncoValoracion::getComuOtraCompaString,
				RegistroOncoValoracion::setComuOtraCompa);
		comuRelacionFam.setWidth("60px");
		comuRelacionFam.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(comuRelacionFam).bind(RegistroOncoValoracion::getComuRelacionFamString,
				RegistroOncoValoracion::setComuRelacionFam);
		comuRelacionPer.setWidth("60px");
		comuRelacionPer.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(comuRelacionPer).bind(RegistroOncoValoracion::getComuRelacionPerString,
				RegistroOncoValoracion::setComuRelacionPer);

		binder.forField(comuColabora).bind(RegistroOncoValoracion::getComuColaboraBoolean,
				RegistroOncoValoracion::setComuColabora);

		binder.forField(comuConspiraSilen).bind(RegistroOncoValoracion::getComuConspiraSilenBoolean,
				RegistroOncoValoracion::setComuConspiraSilen);
		binder.forField(comuCambioSexual).bind(RegistroOncoValoracion::getComuCambioSexualBoolean,
				RegistroOncoValoracion::setComuCambioSexual);

		comuOtro.setWidth("60px");
		comuOtro.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(comuOtro).bind(RegistroOncoValoracion::getComuOtroStrig, RegistroOncoValoracion::setComuOtro);

		fila15.addComponents(comuAcompanado, comuOtraCompa, comuRelacionFam, comuRelacionPer, comuColabora,
				comuConspiraSilen, comuCambioSexual, comuOtro);

		percepImagen.setWidth("200px");
		percepImagen.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(percepImagen).bind(RegistroOncoValoracion::getPercepImagenString,
				RegistroOncoValoracion::setPercepImagen);

		percepAutoestima.setWidth("200px");
		percepAutoestima.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(percepAutoestima).bind(RegistroOncoValoracion::getPercepAutoestimaString,
				RegistroOncoValoracion::setPercepAutoestima);

		binder.forField(percepAlopecia).bind(RegistroOncoValoracion::getPercepAlopeciaBoolean,
				RegistroOncoValoracion::setPercepAlopecia);
		binder.forField(percepMastectomia).bind(RegistroOncoValoracion::getPercepMastectomiaBoolean,
				RegistroOncoValoracion::setPercepMastectomia);
		binder.forField(percepColostomia).bind(RegistroOncoValoracion::getPercepColostomiaBoolean,
				RegistroOncoValoracion::setPercepColostomia);

		percepOtros.setWidth("200px");
		percepOtros.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(percepOtros).bind(RegistroOncoValoracion::getPercepOtrosString,
				RegistroOncoValoracion::setPercepOtros);
		fila16.addComponents(percepImagen, percepAutoestima, percepAlopecia, percepMastectomia, percepColostomia,
				percepOtros);
		fila16.setCaption("Percepción de si mismo");
		fila16.addStyleName(MaterialTheme.PANEL_WELL);

		aprenComprende.setWidth("200px");
		aprenComprende.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(aprenComprende).bind(RegistroOncoValoracion::getAprenComprendeString,
				RegistroOncoValoracion::setAprenComprende);

		aprenDemanda.setWidth("200px");
		aprenDemanda.addStyleName(MaterialTheme.BUTTON_CUSTOM);
		binder.forField(aprenDemanda).bind(RegistroOncoValoracion::getAprenDemandaString,
				RegistroOncoValoracion::setAprenDemanda);
		fila17.addComponents(aprenDemanda, aprenComprende);
		fila17.setCaption("Comprensión de la enfermedad");
		fila17.addStyleName(MaterialTheme.PANEL_WELL);

		binder.readBean(registroValoracion);
		doActivaBotones(registroValoracion);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroValoracion);
			if (!new RegistroDAO().grabaDatos(registroValoracion)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}
	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registroValoracion != null) {
			super.doActivaBotones(registroValoracion);
		}

	}

}
