package com.jnieto.ui.hdiaonco;

import com.github.appreciated.material.MaterialTheme;
import com.jnieto.continuidad.MyUI;
import com.jnieto.dao.RegistroDAO;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroOncoCuidados;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.ObjetosClinicos;
import com.jnieto.ui.ObjetosComunes;
import com.jnieto.ui.master.FrmMaster;
import com.jnieto.validators.ValidatorDecimal;
import com.jnieto.validators.ValidatorEntero;
import com.vaadin.data.Binder;
import com.vaadin.data.ValidationException;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;

public class FrmRegistroOncoCuidados extends FrmMaster {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5455014338384029375L;

	private ComboBox<String> accesoVenoso;
	private TextField accesoLocalizacion = new TextField("Localización acceso");
	private CheckBox accesoExtraccion = new CheckBox("Extracción");
	private TextField accesoExtraDescri = new TextField("Descripción extracción");

	private TextField ttoDosis = new TextField("Dosis de tratamiento  ");
	private TextField ttoVia = new TextField("Vía de administración ");
	private TextField ttoHoraini = new TextField("Inicio");

	private TextField tas = new TextField("Tas");
	private TextField tad = new TextField("Tad");
	private TextField temperatura = new TextField("Tª");
	private TextField freCardiaca = new TextField("FC");
	private TextField freRespiratorio = new TextField("Fr");
	private TextField satOxigeno = new TextField("SaTO2");
	private TextField glucosa = new TextField("Glucosa");

	private CheckBox reaccAlergica = new CheckBox("R.Alérgica");
	private CheckBox flebitis = new CheckBox("Flebitis");
	private CheckBox extravasacion = new CheckBox("Extrasvasación");
	private CheckBox obstruccionCate = new CheckBox("Obstru.Catet");
	private CheckBox nauseas = new CheckBox("Naúseas");
	private CheckBox vomitos = new CheckBox("Vómitos");
	private TextField otrasComplica = new TextField("Otras complicaciones");

	private CheckBox infoPaProcedimiento = new CheckBox("Información procedimiento");
	private TextArea infoPaEfectos = new TextArea("Efectos secundarios");
	private CheckBox infoPaSigAlarma = new CheckBox("Signos y síntomas de alarma");
	private TextArea observaciones = new TextArea("Observaciones");

	private RegistroOncoCuidados registroCuidados;

	private Binder<RegistroOncoCuidados> binder = new Binder<RegistroOncoCuidados>();

	public FrmRegistroOncoCuidados(Registro registro) {
		super();
		registroCuidados = (RegistroOncoCuidados) registro;
		contenedorCampos.removeAllComponents();
		contenedorCampos.addComponents(fila1, fila2, fila3, fila4, fila5, fila6, fila7);
		registroCuidados = (RegistroOncoCuidados) registro;

		accesoVenoso = new ObjetosClinicos().getComboString("Acceso", "", ObjetosClinicos.HDIA_ONCO_ACCESOS, "200px");
		binder.forField(accesoVenoso).bind(RegistroOncoCuidados::getAccesoVenosoString,
				RegistroOncoCuidados::setAccesoVenoso);

		accesoLocalizacion.setWidth("300px");
		accesoLocalizacion.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(accesoLocalizacion).bind(RegistroOncoCuidados::getAccesoLocalizacionString,
				RegistroOncoCuidados::setAccesoLocalizacion);

		accesoExtraccion.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(accesoExtraccion).bind(RegistroOncoCuidados::getAccesoExtraccionBoolean,
				RegistroOncoCuidados::setAccesoExtraccion);

		accesoExtraDescri.setWidth("300px");
		accesoExtraDescri.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(accesoExtraDescri).bind(RegistroOncoCuidados::getAccesoExtraDescriString,
				RegistroOncoCuidados::setAccesoExtraDescri);
		fila1.addComponents(accesoVenoso, accesoLocalizacion, accesoExtraccion, accesoExtraDescri);

		ttoDosis.setWidth("300px");
		ttoDosis.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(ttoDosis).bind(RegistroOncoCuidados::getTtoDosisString, RegistroOncoCuidados::setTtoDosis);

		ttoVia.setWidth("300px");
		ttoVia.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(ttoVia).bind(RegistroOncoCuidados::getTtoViaString, RegistroOncoCuidados::setTtoVia);

		ttoHoraini = new ObjetosComunes().getHora("H.Ini.", "");
		binder.forField(ttoHoraini).bind(RegistroOncoCuidados::getTtoHorainiString,
				RegistroOncoCuidados::setTtoHoraini);
		fila2.addComponents(ttoDosis, ttoVia, ttoHoraini);

		temperatura.setWidth("60px");
		temperatura.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(temperatura)
				.withValidator(new ValidatorDecimal(temperatura.getValue(),
						registroCuidados.VAR_ONCO_CUIDADOS_T.getValorInferior(),
						registroCuidados.VAR_ONCO_CUIDADOS_T.getValorSuperior()))
				.bind(RegistroOncoCuidados::getTemperaturaString, RegistroOncoCuidados::setTemperatura);

		tas.setWidth("60px");
		tas.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tas).withValidator(new ValidatorEntero(tas.getValue(), 5, 300))
				.bind(RegistroOncoCuidados::getTadString, RegistroOncoCuidados::setTas);

		tad.setWidth("60px");
		tad.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(tad).withValidator(new ValidatorEntero(tad.getValue(), 10, 300))
				.bind(RegistroOncoCuidados::getTadString, RegistroOncoCuidados::setTad);

		freCardiaca.setWidth("60px");
		freCardiaca.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(freCardiaca).withValidator(new ValidatorEntero(freCardiaca.getValue(), 20, 300))
				.bind(RegistroOncoCuidados::getFreCardiacaString, RegistroOncoCuidados::setFreCardiaca);

		satOxigeno.setWidth("60px");
		satOxigeno.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(satOxigeno).withValidator(new ValidatorEntero(satOxigeno.getValue(), 50, 100))
				.bind(RegistroOncoCuidados::getSatOxigenoString, RegistroOncoCuidados::setSatOxigeno);

		glucosa.setWidth("60px");
		glucosa.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(glucosa).withValidator(new ValidatorEntero(glucosa.getValue(), 20, 900))
				.bind(RegistroOncoCuidados::getGlucosaString, RegistroOncoCuidados::setGlucosa);

		fila3.addComponents(temperatura, tas, tad, freCardiaca, satOxigeno, glucosa);

		reaccAlergica.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(reaccAlergica).bind(RegistroOncoCuidados::getReaccAlergicaBolean,
				RegistroOncoCuidados::setReaccAlergica);

		flebitis.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(flebitis).bind(RegistroOncoCuidados::getFlebitisBoolean, RegistroOncoCuidados::setFlebitis);

		extravasacion.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(extravasacion).bind(RegistroOncoCuidados::getExtravasacionBoolean,
				RegistroOncoCuidados::setExtravasacion);

		obstruccionCate.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(obstruccionCate).bind(RegistroOncoCuidados::getObstruccionCateBoolean,
				RegistroOncoCuidados::setObstruccionCate);

		nauseas.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(nauseas).bind(RegistroOncoCuidados::getNauseasBoolean, RegistroOncoCuidados::setNauseas);

		vomitos.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(vomitos).bind(RegistroOncoCuidados::getVomitosBoolean, RegistroOncoCuidados::setVomitos);

		fila4.addComponents(reaccAlergica, flebitis, extravasacion, obstruccionCate, nauseas, vomitos);

		otrasComplica.setWidth("400px");
		otrasComplica.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(otrasComplica).bind(RegistroOncoCuidados::getOtrasComplicaString,
				RegistroOncoCuidados::setOtrasComplica);
		fila5.addComponent(otrasComplica);

		infoPaProcedimiento.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(infoPaProcedimiento).bind(RegistroOncoCuidados::getInfoPaProcedimientoBoolean,
				RegistroOncoCuidados::setInfoPaProcedimiento);

		infoPaSigAlarma.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(infoPaSigAlarma).bind(RegistroOncoCuidados::getInfoPaSigAlarmaBoolean,
				RegistroOncoCuidados::setInfoPaSigAlarma);

		infoPaEfectos.setWidth("400px");
		infoPaEfectos.setHeight("60px");
		infoPaEfectos.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM + MaterialTheme.TEXTAREA_SMALL);
		infoPaEfectos.addStyleName(MaterialTheme.CHECKBOX_SMALL);
		binder.forField(infoPaEfectos).bind(RegistroOncoCuidados::getInfoPaEfectosString,
				RegistroOncoCuidados::setInfoPaEfectos);
		fila6.addComponents(infoPaProcedimiento, infoPaSigAlarma, infoPaEfectos);

		observaciones.setWidth("500px");
		observaciones.setHeight("80px");
		observaciones.addStyleName(MaterialTheme.TEXTFIELD_CUSTOM);
		binder.forField(observaciones).bind(RegistroOncoCuidados::getObservacionesString,
				RegistroOncoCuidados::setObservaciones);

		fila7.addComponent(observaciones);
		binder.readBean(registroCuidados);
		doActivaBotones(registroCuidados);
	}

	@Override
	public void cerrarClick() {
		MyUI.getCurrent().getWindows().iterator().next().close();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void grabarClick() {
		try {
			binder.writeBean(registroCuidados);
			if (!new RegistroDAO().grabaDatos(registroCuidados)) {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_ERROR_GUARDADOS);
			} else {
				new NotificacionInfo(NotificacionInfo.FORMULARIO_DATOS_GUARDADOS);
			}
			this.cerrarClick();
		} catch (ValidationException e) {
			new NotificacionInfo(NotificacionInfo.BINDER_DATOS_ERRORVALIDACION);
		} finally {

		}

	}

	@Override
	public void borrarClick() {

	}

	@Override
	public void borraElRegistro() {

	}

	@Override
	public boolean doValidaFormulario() {
		return false;
	}

	public void doActivaBotones(Registro registro) {
		if (registroCuidados != null) {
			super.doActivaBotones(registroCuidados);
		}

	}
}
