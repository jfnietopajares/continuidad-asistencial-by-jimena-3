package com.jnieto.ui;

import java.util.ArrayList;

import com.jnieto.dao.EpisodioDAO;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Zona;
import com.jnieto.ui.master.CajaMasterGrid;
import com.jnieto.ui.master.VentanaFrm;
import com.vaadin.ui.Grid;

public class CajaEpisodio extends CajaMasterGrid {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4897625259190290801L;

	private Grid<Episodio> grid = new Grid<Episodio>();
	private ArrayList<Episodio> listaEpisodios = new ArrayList<Episodio>();
	private Centro centro = Centro.CENTRO_DEFECTO;
	private Servicio servicio;

	public CajaEpisodio(Paciente paciente, EpisodioClase clase, Zona zonaParam) {
		super(paciente, clase, zonaParam);
		lbltitulo.setCaption("Episodios");
		if (!clase.isAndirActivo()) {
			this.nuevo.setEnabled(false);
		}
		grid.setSizeUndefined();
		grid.setHeightByRows(5);
		grid.addColumn(Episodio::getFechaHora).setCaption("Fecha");
		grid.addColumn(Episodio::getClaseLong).setCaption("Tip");
		grid.addColumn(Episodio::getServicioCodigo).setCaption("Serv");
		if (clase != null) {
			if (clase.getId().equals(Episodio.CLASE_HOSPITALIZACION.getId())) {
				grid.addColumn(Episodio::getCamaCodigo).setCaption("Cama");
			} else if (clase.getId().equals(Episodio.CLASE_CONSULTAS.getId())) {
				grid.addColumn(Episodio::getAgendaCodigo).setCaption("Agenda");
			} else if (clase.getId().equals(Episodio.CLASE_URGENCIAS.getId())) {
				grid.addColumn(Episodio::getAgendaCodigo).setCaption("Agenda");
			} else if (clase.getId().equals(Episodio.CLASE_HDIA.getId())) {
				grid.addColumn(Episodio::getCamaCodigo).setCaption("Zona");
			}
		}
		refrescarClick();
		grid.addSelectionListener(e -> seleccionaClick());
		contenedorGrid.addComponent(grid);
	}

	@Override
	public void nuevoClick() {
		Episodio episodio = new Episodio();
		episodio.setPaciente(paciente);
		episodio.setClase(clase);
		FrmEpisodio fre = new FrmEpisodio(episodio, zona);
		VentanaFrm vFrm = new VentanaFrm(this.getUI(), fre, episodio.getDescripcionClase(episodio.getClaseLong()));
		vFrm.addCloseListener(e -> cerrarVentana());
	}

	public void cerrarVentana() {
		refrescarClick();
	}

	@Override
	public void refrescarClick() {
		listaEpisodios = new EpisodioDAO().getListaEpisodios(paciente, clase);
		grid.setItems(listaEpisodios);
		if (listaEpisodios.size() > 0) {
			grid.select(listaEpisodios.get(0));
			setEpisodio(grid.getSelectedItems().iterator().next());
		}
		doActivaBotones();
	}

	@Override
	public void ayudaClick() {

	}

	@Override
	public void seleccionaClick() {
		doActivaBotones();
		if (grid.getSelectedItems().size() > 0) {
			// Episodio episodio = new Episodio();
			// episodio = grid.getSelectedItems().iterator().next();
			// FrmEpisodio fre = new FrmEpisodio(episodio);
			// VentanaFrm vFrm = new VentanaFrm(this.getUI(), fre);
			// vFrm.addClickListener(e -> cerrarVentana());
		}
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public Zona getZona() {
		return zona;
	}

	public void setZona(Zona zona) {
		this.zona = zona;
	}

	@Override
	public void editarClick() {
		if (grid.getSelectedItems().size() > 0) {
			Episodio episodio = new Episodio();
			episodio = grid.getSelectedItems().iterator().next();
			FrmEpisodio fre = new FrmEpisodio(episodio, zona);
			VentanaFrm vFrm = new VentanaFrm(this.getUI(), fre, episodio.getDescripcionClase(episodio.getClaseLong()));
			vFrm.addClickListener(e -> cerrarVentana());
		}
	}

	public void doActivaBotones() {
		if (grid.getSelectedItems().size() > 0)
			editar.setEnabled(true);
		else
			editar.setEnabled(false);

		nuevo.setEnabled(new EpisodioDAO().getTodosEpisodiosCerrados(paciente.getId(), clase));
	}

}
