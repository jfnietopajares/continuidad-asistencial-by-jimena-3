package com.jnieto.ui;

import java.io.File;
import java.time.LocalDate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.vaadin.dialogs.ConfirmDialog;

import com.jnieto.continuidad.MyUI;
import com.jnieto.controlador.AccesoControlador;
import com.jnieto.controlador.AuthService;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Funcionalidad;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.admin.PantallaCentros;
import com.jnieto.ui.admin.PantallaFuncionalidades;
import com.jnieto.ui.admin.PantallaPatrametros;
import com.jnieto.ui.admin.PantallaServicios;
import com.jnieto.ui.admin.PantallaUsuarios;
import com.jnieto.ui.admin.PantallaVerAccesos;
import com.jnieto.ui.admin.PantallaVerMensajes;
import com.jnieto.ui.colon.PantallaTSOH;
import com.jnieto.ui.hdiaonco.PantallaHadiOnco;
import com.jnieto.ui.master.VentanaCambioClave;
import com.jnieto.ui.master.VentanaHtml;
import com.jnieto.ui.oxigeno.PantallaPeticiones;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Parametros;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;

/**
 * The Class MenuPrincipal. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class MenuPrincipal extends MenuBar {

	private static final long serialVersionUID = -7340685914224566428L;

	private static final Logger logger = LogManager.getLogger(MenuPrincipal.class);

	public final static String MENU_USUARIO_VERDATOS = "Ver usuario";
	public final static String MENU_USUARIO_CAMBIARCLAVE = "Cambiar clave";
	public final static String MENU_ACTUALIZA_ENTORNO = "Actualiza entorno";
	public final static String MENU_MISC_VERSION = "A cerca de ";

	public final static String MENU_MAMA = "Mama";
	public final static String MENU_MAMA_ACTIVOS = "Pacientes mama";
	public final static String MENU_MAMA_EXCEL = "Listados mama";

	public final static String MENU_COLON = "Colon";
	public final static String MENU_COLON_ACTIVOS = "Pacientes colon";
	public final static String MENU_COLON_EXCEL = "Listados colon";
	public final static String MENU_COLON_IMPORTAR = "Actualizar datos";

	public final static String MENU_PALIATIVOS = "Paliativos";
	public final static String MENU_PALIATIVOS_ACTIVOS = "Pacientes paliativos";
	public final static String MENU_PALIATIVOS_EXCEL = "Listados  paliativos";

	public final static String MENU_OXIGENO = "Oxigenoterapia";
	public final static String MENU_OXIGENO_PETICIONES = "Peticiones realizadas";
	public final static String MENU_OXIGENO_PACIENTE_LISTA = "Pacientes oxigenoterapia";
	public final static String MENU_OXIGENO_LISTADO = "Listados oxigeno";

	public final static String MENU_REHABILITACION = "Rehabilitación";
	public final static String MENU_REHABILITACION_PETICIONES = "Peticiones Rehabilitación";
	public final static String MENU_REHABILITACION_PACIENTES = "Pacientes por téncias";
	public final static String MENU_REHABILITACION_LISTADO = "Listados Rehabilitación";

	public final static String MENU_ADMIN = "Administrar";
	public final static String MENU_ADMIN_CENTROS = "Centros";
	public final static String MENU_ADMIN_FUNCIONALIDADES = "Funcionalidades";
	public final static String MENU_ADMIN_PARAMETROS = "Parámetros";
	public final static String MENU_ADMIN_SERVICIOS = "Servicios";
	// public final static String MENU_ADMIN_TECNICAS = "Técnicas";
	public final static String MENU_ADMIN_USUARIOS = "Usuarios";
	public final static String MENU_ADMIN_VER_CONFIGURACION = "Configuración";
	public final static String MENU_ADMIN_VER_MENSAJES = "Mensajes";
	public final static String MENU_ADMIN_VER_ACCESOS = "Accesos";

	public final static String MENU_PACI = "Pacientes";
	public final static String MENU_PACI_LISTA = "Lista";
	public final static String MENU_PACI_BUSCAR = "Buscar";
	public final static String MENU_PACI_FICHA = "Ficha";

	public final static String MENU_HDIA = "H.Día";
	public final static String MENU_HDIA_MEDICO = "H.D.Médico";
	public final static String MENU_HDIA_ONCO = "H.D.Oncológico";
	public final static String MENU_HDIA_PSQ = "H.D.Psiquiatrico";

	public final static String MENU_INGRESADOS = "Ingresados";
	public final static String MENU_CONSULTAS = "Consultas";
	public final static String MENU_URGENCIAS = "Urgencias";
	public final static String MENU_QUIROFANO = "Quirofano";

	public final static String MENU_PARTOS = "Partos";

	public final static String MENU_AYUDA = "Ayuda";
	public final static String MENU_SALIR = "Salir";
	public final static String MENU_MISC_AYUDA = "Ayuda";

	private final String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();

	/**
	 * Instantiates a new menu principal.
	 *
	 * @param usuario the usuario
	 */
	public MenuPrincipal(Usuario usuario) {
		// this.setWidth(100.0f, Unit.PERCENTAGE);
		this.setSizeFull();
		// this.setResponsive(false);
		// https://app.streamlineicons.com/streamline-bold/health-beauty
		FileResource resourceMama = new FileResource(new File(basepath + "/WEB-INF/images/mujer.png"));
		FileResource resourceColon = new FileResource(new File(basepath + "/WEB-INF/images/heces1.png"));
		FileResource resourceOxigeno = new FileResource(new File(basepath + "/WEB-INF/images/pulmon.png"));
		FileResource resourceRehabilitacion = new FileResource(
				new File(basepath + "/WEB-INF/images/rehabilitacion.png"));
		FileResource resourcePaliativo = new FileResource(new File(basepath + "/WEB-INF/images/paliativos.png"));
		FileResource resourceAdmin = new FileResource(new File(basepath + "/WEB-INF/images/admin.png"));
		FileResource resourcePaciente = new FileResource(new File(basepath + "/WEB-INF/images/pacientes.png"));
		FileResource resourceUsuario = new FileResource(new File(basepath + "/WEB-INF/images/usuario.png"));
		FileResource resourceHdia = new FileResource(new File(basepath + "/WEB-INF/images/hdia1.png"));
		FileResource resourceConsultas = new FileResource(new File(basepath + "/WEB-INF/images/consultas.png"));

		FileResource resourceHospitalizacion = new FileResource(
				new File(basepath + "/WEB-INF/images/hospitalizacion.png"));
		FileResource resourceUrgencias = new FileResource(new File(basepath + "/WEB-INF/images/urgencias.png"));
		FileResource resourceQuirofano = new FileResource(new File(basepath + "/WEB-INF/images/quirofano.png"));

		FileResource resourcePartos = new FileResource(new File(basepath + "/WEB-INF/images/partos.png"));

		MenuBar.Command unComando = new MenuBar.Command() {

			private static final long serialVersionUID = -4816907840054220837L;

			public void menuSelected(MenuItem selectedItem) {

				switch (selectedItem.getText()) {
				case MenuPrincipal.MENU_INGRESADOS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_HOSPITALIZACION));
					break;
				case MenuPrincipal.MENU_CONSULTAS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_CONSULTAS));
					break;
				case MenuPrincipal.MENU_URGENCIAS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_URGENCIAS));
					break;
				case MenuPrincipal.MENU_QUIROFANO:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_QUI_PROGRMACION));
					break;
				case MenuPrincipal.MENU_USUARIO_VERDATOS:
					Label lbl = new Label(usuario.getHtmlDatos() + "<br>" + Utilidades.getInformacionCliente());
					new VentanaHtml(getUI(), lbl, VentanaHtml.VENTANAHTMLDATOSUSUARIO);
					break;
				case MenuPrincipal.MENU_USUARIO_CAMBIARCLAVE:
					new VentanaCambioClave(getUI());
					break;
				case MenuPrincipal.MENU_ACTUALIZA_ENTORNO:
					try {
						MyUI.objParametros = new Parametros().getParametros();
					} catch (Exception e) {
						logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
					}
					break;
				case MenuPrincipal.MENU_MISC_VERSION:
					new VentanaHtml(getParent().getUI(), new Label(
							Constantes.APLICACION_NOMBRE_VENTANA.concat("<br>").concat(Constantes.APLICACION_VERSION)),
							"<br> Juan Nieto");
					break;
				case "calendario":
					((MyUI) getUI()).actualiza(new PantallaCalendario(LocalDate.now(), null, null, null));
					break;
				case MenuPrincipal.MENU_PACI_LISTA:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(null));
					break;
				case MenuPrincipal.MENU_PACI_BUSCAR:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(new Long(0)));
					break;
				case MenuPrincipal.MENU_PACI_FICHA:
					((MyUI) getUI()).actualiza(new FrmPaciente(new Paciente()));
					break;

				case MenuPrincipal.MENU_MAMA_ACTIVOS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(Proceso.SUBAMBITO_MAMA));
					break;
				case MenuPrincipal.MENU_MAMA_EXCEL:
					// new VentanaVerPdf(getUI(), new PdfMama().getDest());
					((MyUI) getUI()).actualiza(new PantallaReports());
					break;
				case MenuPrincipal.MENU_COLON_IMPORTAR:
					((MyUI) getUI()).actualiza(new PantallaTSOH());
					break;
				case MenuPrincipal.MENU_COLON_ACTIVOS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(Proceso.SUBAMBITO_COLON));
					break;
				case MenuPrincipal.MENU_COLON_EXCEL:
					((MyUI) getUI()).actualiza(new PantallaReports());
					break;

				case MenuPrincipal.MENU_PALIATIVOS_ACTIVOS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(Proceso.SUBAMBITO_PALIATIVOS));
					break;
				case MenuPrincipal.MENU_PALIATIVOS_EXCEL:
					((MyUI) getUI()).actualiza(new PantallaReports());
					break;

				case MenuPrincipal.MENU_OXIGENO_PETICIONES:
					((MyUI) getUI()).actualiza(new PantallaPeticiones());
					break;
				case MenuPrincipal.MENU_OXIGENO_PACIENTE_LISTA:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(Proceso.SUBAMBITO_OXIGENO));
					break;
				case MenuPrincipal.MENU_OXIGENO_LISTADO:
					((MyUI) getUI()).actualiza(new PantallaReports());
					break;

				case MenuPrincipal.MENU_HDIA_MEDICO:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_HDIA));
					break;
				case MenuPrincipal.MENU_HDIA_ONCO:
					((MyUI) getUI()).actualiza(new PantallaHadiOnco());
					break;
				case MenuPrincipal.MENU_HDIA_PSQ:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacienteEpisodios(Episodio.CLASE_HDIA));
					break;
				case MenuPrincipal.MENU_PARTOS:
					((MyUI) getUI()).actualiza(new PantallaBuscarPacientesProcesos(Proceso.SUBAMBITO_PARTOS));
					break;

				case MenuPrincipal.MENU_ADMIN_CENTROS:
					((MyUI) getUI()).actualiza(new PantallaCentros());
					break;
				case MenuPrincipal.MENU_ADMIN_FUNCIONALIDADES:
					((MyUI) getUI()).actualiza(new PantallaFuncionalidades());
					break;
				case MenuPrincipal.MENU_ADMIN_PARAMETROS:
					((MyUI) getUI()).actualiza(new PantallaPatrametros());
					break;
				case MenuPrincipal.MENU_ADMIN_SERVICIOS:
					((MyUI) getUI()).actualiza(new PantallaServicios());
					break;
				case MenuPrincipal.MENU_ADMIN_USUARIOS:
					((MyUI) getUI()).actualiza(new PantallaUsuarios());
					break;
				case MenuPrincipal.MENU_ADMIN_VER_CONFIGURACION:
					new VentanaHtml(getUI(), new Label(
							Parametros.verParametros(MyUI.objParametros) + "<hr>" + Utilidades.getInformacionCliente()),
							VentanaHtml.VENTANAHTMLDATOSUSUARIO);
					break;
				case MenuPrincipal.MENU_ADMIN_VER_MENSAJES:
					((MyUI) getUI()).actualiza(new PantallaVerMensajes());
					break;
				case MenuPrincipal.MENU_ADMIN_VER_ACCESOS:
					((MyUI) getUI()).actualiza(new PantallaVerAccesos());
					break;

				case MenuPrincipal.MENU_SALIR:
					ConfirmDialog.show(((MyUI) getUI()), Constantes.CONFIRMACION_TITULO,
							Constantes.CONFIRMACION_SALIR_MENSAJE, Constantes.CONFIRMACION_BOTONSI,
							Constantes.CONFIRMACION_BOTONNO, new ConfirmDialog.Listener() {
								private static final long serialVersionUID = 6169352858399108337L;

								public void onClose(ConfirmDialog dialog) {
									if (dialog.isConfirmed()) {
										new AccesoControlador().doAccesoUsuarioLogout((Usuario) VaadinSession
												.getCurrent().getAttribute(Constantes.SESSION_USERNAME));
										AuthService.logOut();
										((MyUI) getUI()).showPublicComponent();
									}
								}
							});

					break;
				}
			}
		};

//		MenuItem continuidad = this.addItem("", VaadinIcons.DOCTOR, null);
		MenuItem continuidad = this.addItem("", resourceUsuario, null);

		continuidad.addItem(MenuPrincipal.MENU_USUARIO_VERDATOS, null, unComando);
		continuidad.addItem(MenuPrincipal.MENU_USUARIO_CAMBIARCLAVE, null, unComando);
		continuidad.addItem(MenuPrincipal.MENU_ACTUALIZA_ENTORNO, null, unComando);
		continuidad.addItem(MenuPrincipal.MENU_MISC_VERSION, null, unComando);
		continuidad.addItem("calendario", null, unComando);

		for (Funcionalidad funcionalidad : usuario.getFuncionaliades()) {
			if (funcionalidad.getId().equals(Funcionalidad.MENU_INGRESADOS_FUNCIONALIDAD)) {
				MenuItem menuIngresados = this.addItem(MenuPrincipal.MENU_INGRESADOS, null, unComando);
				menuIngresados.setIcon(resourceHospitalizacion);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_CONSULTA_FUNCIONALIDAD)) {
				MenuItem menuConsultas = this.addItem(MenuPrincipal.MENU_CONSULTAS, null, unComando);
				menuConsultas.setIcon(resourceConsultas);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_URGENCIAS_FUNCIONALIDAD)) {
				MenuItem menuUrgencias = this.addItem(MenuPrincipal.MENU_URGENCIAS, null, unComando);
				menuUrgencias.setIcon(resourceUrgencias);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_QUIROFANO_FUNCIONALIDAD)) {
				MenuItem menuQuirofano = this.addItem(MenuPrincipal.MENU_QUIROFANO, null, unComando);
				menuQuirofano.setIcon(resourceQuirofano);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_PACI_FUNCIONALIDAD)) {
				MenuItem menuPacientes = this.addItem(MenuPrincipal.MENU_PACI);
				menuPacientes.setIcon(resourcePaciente);
				menuPacientes.addItem(MenuPrincipal.MENU_PACI_LISTA, null, unComando);
				menuPacientes.addItem(MenuPrincipal.MENU_PACI_BUSCAR, null, unComando);
				menuPacientes.addItem(MenuPrincipal.MENU_PACI_FICHA, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_MAMA_FUNCIONALIDAD)) {
				// Menú mama
				MenuItem menuMama = this.addItem(MenuPrincipal.MENU_MAMA, null, null);
				menuMama.setIcon(resourceMama);
				menuMama.addItem(MenuPrincipal.MENU_MAMA_ACTIVOS, null, unComando);
				menuMama.addItem(MenuPrincipal.MENU_MAMA_EXCEL, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_COLON_FUNCIONALIDAD)) {
				// Menú colon
				MenuItem menuColon = this.addItem(MenuPrincipal.MENU_COLON, null, null);
				menuColon.setIcon(resourceColon);
				menuColon.addItem(MenuPrincipal.MENU_COLON_IMPORTAR, null, unComando);
				menuColon.addItem(MenuPrincipal.MENU_COLON_ACTIVOS, null, unComando);
				menuColon.addItem(MenuPrincipal.MENU_COLON_EXCEL, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_PALIATIVOS_FUNCIONALIDAD)) {
				// Menú paliativos
				MenuItem menuPaliativos = this.addItem(MenuPrincipal.MENU_PALIATIVOS, null, null);
				menuPaliativos.setIcon(resourcePaliativo);
				menuPaliativos.addItem(MenuPrincipal.MENU_PALIATIVOS_ACTIVOS, null, unComando);
				menuPaliativos.addItem(MenuPrincipal.MENU_PALIATIVOS_EXCEL, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_OXIGENO_FUNCIONALIDAD)) {
				MenuItem oxigeno = this.addItem(MenuPrincipal.MENU_OXIGENO, null, null);
				oxigeno.setIcon(resourceOxigeno);
				oxigeno.addItem(MenuPrincipal.MENU_OXIGENO_PETICIONES, null, unComando);
				oxigeno.addItem(MenuPrincipal.MENU_OXIGENO_PACIENTE_LISTA, null, unComando);
				oxigeno.addItem(MenuPrincipal.MENU_OXIGENO_LISTADO, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_REHABILITACION_FUNCIONALIDAD)) {
				MenuItem rehabilitacion = this.addItem(MenuPrincipal.MENU_REHABILITACION, null, null);
				rehabilitacion.setIcon(resourceRehabilitacion);
				rehabilitacion.addItem(MenuPrincipal.MENU_REHABILITACION_PETICIONES, null, unComando);
				rehabilitacion.addItem(MenuPrincipal.MENU_REHABILITACION_PACIENTES, null, unComando);
				rehabilitacion.addItem(MenuPrincipal.MENU_REHABILITACION_LISTADO, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_ADMIN_FUNCIONALIDAD)) {
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_HDIA_FUNCIONALIDAD)) {
				MenuItem hdia = this.addItem(MenuPrincipal.MENU_HDIA, null, null);
				hdia.setIcon(resourceHdia);
				hdia.addItem(MenuPrincipal.MENU_HDIA_MEDICO, null, unComando);
				hdia.addItem(MenuPrincipal.MENU_HDIA_ONCO, null, unComando);
				hdia.addItem(MenuPrincipal.MENU_HDIA_PSQ, null, unComando);
			} else if (funcionalidad.getId().equals(Funcionalidad.MENU_PARTOS_FUNCIONALIDAD)) {
				MenuItem menuPartos = this.addItem(MenuPrincipal.MENU_PARTOS, null, unComando);
				menuPartos.setIcon(resourcePartos);
			}
		}
		Usuario objUsuario = (Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME);
		// es administrador
		if (objUsuario.getEstado() == 2) {
			MenuItem tablas = this.addItem(MenuPrincipal.MENU_ADMIN, null, null);
			tablas.setIcon(resourceAdmin);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_CENTROS, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_FUNCIONALIDADES, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_PARAMETROS, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_SERVICIOS, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_USUARIOS, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_VER_MENSAJES, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_VER_ACCESOS, null, unComando);
			tablas.addItem(MenuPrincipal.MENU_ADMIN_VER_CONFIGURACION, null, unComando);
		}
		// MenuItem relleno = this.addItem(" ", null, null);

		String username;

		if (objUsuario != null)
			username = objUsuario.getApellidosNombre();
		else
			username = "?";

		MenuItem usuarioActivo = this.addItem(username, null, unComando);
		usuarioActivo.setIcon(VaadinIcons.POWER_OFF);
		usuarioActivo.setText(MenuPrincipal.MENU_SALIR);
		usuarioActivo.setDescription(username);

	}

}
