package com.jnieto.ui;

import java.io.File;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.jnieto.entity.Cama;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Paciente;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.FileResource;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class CamaSillon extends Panel {
	/**
	* 
	*/
	private static final long serialVersionUID = 3712534373681440342L;
	private VerticalLayout contenedor = new VerticalLayout();
	private HorizontalLayout fila1 = new HorizontalLayout();
	private HorizontalLayout fila2 = new HorizontalLayout();
	private Episodio episodio;
	private String basepath;// = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();
	private FileResource iconoPaciente;
	private Label nombreLabel = new Label();
	private Label fechaLabel = new Label();
	private Label tiempo = new Label();
	private Image iconoCama = new Image("", new FileResource(new File(basepath + "/WEB-INF/images/1.png")));
	private Image puntoVerde = new Image("", new FileResource(new File(basepath + "/WEB-INF/images/punto_verde.png")));

	public CamaSillon(Episodio epi, String basepath) {
		episodio = epi;
		this.basepath = basepath;
		this.setWidth("300px");
// this.setHeight("100px");
		contenedor.setMargin(false);
		contenedor.setSpacing(false);
		fila1.setMargin(false);
		fila2.setMargin(true);
		contenedor.addComponents(fila1, fila2);
		this.setCaptionAsHtml(true);
		this.setContent(contenedor);
		fechaLabel.setCaptionAsHtml(true);
		fechaLabel.setContentMode(ContentMode.HTML);
		nombreLabel.setCaptionAsHtml(true);
		nombreLabel.setContentMode(ContentMode.HTML);
		if (episodio != null) {
// fila 1 icono y nombre de cama/sillo
			if (episodio.getCama() != null) {
				iconoCama = new Image("[" + episodio.getCama().getCamaNumero() + "]", getIcono(episodio));
			}
// fila2 nombre del paciente
			if (episodio.getPaciente() != null) {
				fechaLabel.setCaption("<b>" + episodio.getFechaHora() + "</b>");
				fechaLabel.setValue(this.getNombrePacienteHtml(episodio));
				nombreLabel.setValue(this.getNombrePacienteHtml(episodio));
				long hh = (Utilidades.tiempoTrascurridoHoras(episodio.getFinicio(), episodio.getHinicio()))[0];
				long mm = (Utilidades.tiempoTrascurridoHoras(episodio.getFinicio(), episodio.getHinicio()))[1];
				tiempo.setValue(Long.toString(hh) + "h " + Long.toString(mm) + "m");
				tiempo.setIcon(VaadinIcons.CLOCK);
// fila2.addComponent(nombreLabel);
			}
			fila1.addComponents(iconoCama, fechaLabel, tiempo);
			fila2.addComponent(new Label("..."));
		}
	}

	public FileResource getIcono(Episodio episodio) {
		FileResource iconoPaciente = new FileResource(new File(basepath + "/WEB-INF/images/1.png"));
		if (episodio == null || episodio.getCama() == null) {
			return iconoPaciente;
		}

		if (episodio.getCama().getCama().length() >= 6
				&& episodio.getCamaCodigo().substring(0, 6).equals(Cama.PREFIJOSILLON)) {
			iconoPaciente = new FileResource(new File(basepath + "/WEB-INF/images/sillon.png"));
			if (episodio.getPaciente() != null) {
				if (episodio.getPaciente().getSexo().equals(Constantes.SEXOHOMBRE))
					iconoPaciente = new FileResource(new File(basepath + "/WEB-INF/images/sillonhombre.png"));

				if (episodio.getPaciente().getSexo().equals(Constantes.SEXOMUJER))
					iconoPaciente = new FileResource(new File(basepath + "/WEB-INF/images/sillonmujer.png"));
			}
		} else {
			iconoPaciente = new FileResource(new File(basepath + "/WEB-INF/images/cama.png"));
		}

		return iconoPaciente;

	}

	public String getNombrePacienteHtml(Episodio episodio) {
		String htmlString = "";

		if (episodio != null && episodio.getPaciente() != null) {
			Paciente paciente = episodio.getPaciente();
			htmlString = htmlString.concat("<b>" + paciente.getApellidosNombre() + "</b>");
			if (paciente.getFnac() != null) {
				Long edad = ChronoUnit.YEARS.between(paciente.getFnac(), LocalDate.now());
				htmlString = htmlString.concat("(" + Long.toString(edad) + ")");
			}
		}
		return htmlString;
	}
}
