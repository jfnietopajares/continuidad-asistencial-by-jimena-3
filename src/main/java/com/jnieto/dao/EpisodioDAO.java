package com.jnieto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Agenda;
import com.jnieto.entity.Cama;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.EpisodioClase;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.PagiLisReg;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Variable;
import com.jnieto.entity.Zona;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

public class EpisodioDAO extends ConexionDAO {

	private Episodio episodio = null;

	private static final Logger logger = LogManager.getLogger(EpisodioDAO.class);

	public EpisodioDAO() {
		super();
	}

	public boolean grabaDatos(Episodio episodio) {
		this.episodio = episodio;
		boolean actualizado = false;
		if (episodio.getId().equals(new Long(0))) {
			actualizado = this.insertaDatos(episodio);
		} else {
			actualizado = this.actualizaDatos(episodio);
		}
		return actualizado;
	}

	public boolean insertaDatos(Episodio episodio) {
		Connection connection = null;
		this.episodio = episodio;
		Long id = null;
		boolean insertado = false;
		try {
			connection = super.getConexionBBDD();
			id = new UtilidadesDAO().getSiguienteId("episodios");
			episodio.setId(id);
			sql = " INSERT INTO episodios (id,paciente,clase,finicio,hinicio,ffinal,hfinal, centro,servicio,userid, observacion, canal, icu ,idcama) "
					+ "  VALUES (?,?,?,?,?,?,?,?,?,?,?,?, ?,?)  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, episodio.getId());
			statement.setLong(2, episodio.getPaciente().getId());
			statement.setLong(3, episodio.getClase().getId());
			statement.setLong(4, Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFinicio()));
			statement.setLong(5, episodio.getHinicio());
			if (episodio.getFfinal() == null)
				statement.setLong(6, Constantes.FEHAFIN_DEFECTO);
			else
				statement.setLong(6, Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFfinal()));

			if (episodio.getHfinal() == null) {
				statement.setLong(7, new Long(0));
			} else {
				statement.setLong(7, episodio.getHfinal());
			}
			statement.setLong(8, episodio.getCentro().getId());
			statement.setLong(9, episodio.getServicio().getId());
			statement.setString(10, episodio.getUserid().getUserid());
			if (episodio.getObservacion() != null) {
				statement.setString(11, episodio.getObservacion());
			} else {
				statement.setString(11, null);
			}
			statement.setLong(12, episodio.getCanal());
			// icu se inventa un ico y pone el mismo numero que el id
			statement.setString(13, episodio.getId().toString());

			statement.setLong(14, episodio.getCama().getId());
			logger.debug(
					" INSERT INTO episodios (id,paciente,clase,finicio,hinicio,ffinal,hfinal, centro,servicio,userid, observacion, canal, icu ) VALUES ( "
							+ episodio.getId() + "," + episodio.getPaciente().getId() + "," + episodio.getClase() + ","
							+ Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFinicio()) + ","
							+ episodio.getHinicio() + ","
							+ Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFfinal()) + "," + episodio.getHfinal()
							+ "," + episodio.getId() + "','" + episodio.getServicio().getId() + "','"
							+ episodio.getUserid().getUserid() + "," + episodio.getObservacion() + "')  ");

			insertado = statement.executeUpdate() > 0;

			statement.close();

		} catch (SQLException e) {
			logger.error(
					" INSERT INTO episodios (id,paciente,clase,finicio,hinicio,ffinal,hfinal, centro,servicio,userid, observacion, canal, icu ) VALUES ( "
							+ episodio.getId() + "," + episodio.getPaciente().getId() + "," + episodio.getClase() + ","
							+ Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFinicio()) + ","
							+ episodio.getHinicio() + ","
							+ Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFfinal()) + "," + episodio.getHfinal()
							+ "," + episodio.getId() + "','" + episodio.getServicio().getId() + "','"
							+ episodio.getUserid().getUserid() + "," + episodio.getObservacion() + "')  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	public boolean actualizaDatos(Episodio episodio) {
		Connection connection = null;
		this.episodio = episodio;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   episodios SET paciente=?, clase=?, finicio=?, hinicio=?"
					+ " , ffinal=?, hfinal=?,centro=?,servicio=?,userid=?, observacion=? , canal=?, idcama=? WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);

			statement.setLong(1, episodio.getPaciente().getId());
			statement.setLong(2, episodio.getClase().getId());
			statement.setLong(3, Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFinicio()));
			statement.setLong(4, episodio.getHinicio());
			statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(episodio.getFfinal()));
			if (episodio.getHfinal() != null) {
				statement.setLong(6, episodio.getHfinal());
			} else {
				statement.setLong(6, new Long(0));
			}
			statement.setLong(7, episodio.getCentro().getId());
			statement.setLong(8, episodio.getServicio().getId());

			statement.setString(9, episodio.getUserid().getUserid());
			statement.setString(10, episodio.getObservacion());
			statement.setLong(11, episodio.getCanal());
			if (episodio.getCama() != null)
				statement.setLong(12, episodio.getCama().getId());
			else
				statement.setNull(12, Types.INTEGER);

			statement.setLong(13, episodio.getId());
			logger.debug(" UPDATE   episodios SET paciente=?, clase=?, finicio=?, hinicio=?"
					+ " , ffinal=?, hfinal=?,centro=?,servicio=?,usuario=? WHERE id=? ");

			actualizado = statement.executeUpdate() > 0;
			statement.close();

		} catch (SQLException e) {
			logger.error(" ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	public Episodio getRegistroResulset(ResultSet res, Paciente paciente, Centro centro, Servicio servicio,
			Agenda agenda, Zona zona, Cama cama) {
		Episodio epi = new Episodio();
		try {
			epi.setId(res.getLong("id"));
			if (paciente == null)
				epi.setPaciente(new PacienteDAO().getPacientePorId(res.getLong("paciente")));
			else
				epi.setPaciente(paciente);

			epi.setClase(new EpisodioClaseDAO().getRegistroPorid(res.getLong("clase")));

			if (res.getLong("finicio") != Constantes.FEHAFIN_CERO) {
				epi.setFinicio(Utilidades.getFechaLocalDate(res.getLong("finicio")));
			} else {
				epi.setFinicio(null);
			}

			if (res.getLong("ffinal") != Constantes.FEHAFIN_CERO
					&& res.getLong("ffinal") != Constantes.FEHAFIN_DEFECTO) {
				epi.setFfinal(Utilidades.getFechaLocalDate(res.getLong("ffinal")));
			} else {
				epi.setFfinal(null);
			}
			if (res.getLong("hinicio") > 0)
				epi.setHinicio(Utilidades.getHoraHH_MM(res.getLong("hinicio")));
			else
				epi.setHinicio(res.getLong("hinicio"));

			if (res.getLong("hfinal") > 0)
				epi.setHfinal(Utilidades.getHoraHH_MM(res.getLong("hfinal")));
			else
				epi.setHfinal(res.getLong("hfinal"));

			if (centro == null) {
				epi.setCentro(new CentroDAO().getRegistroId(res.getLong("centro")));
			} else {
				epi.setCentro(centro);
			}
			if (servicio == null) {
				epi.setServicio(new ServiciosDAO().getRegistroId(res.getLong("servicio")));
			} else {
				epi.setServicio(servicio);
			}
			epi.setUserid(new UsuarioDAO().getUsuarioUserid(res.getString("userid"), false));
			epi.setCie9(res.getString("cie9"));
			epi.setObservacion(res.getString("observacion"));

			/*
			 * private Integer prioridad; private Long canal; private Long origen; private
			 * Long motivo_llegada; private Long motivo_alta; private Long tipo_alta;
			 * private Long demora; private Long tipo_financiacion; private Long
			 * alta_resulucion; private Long procede; private Long triaje;
			 */
			if (cama != null) {
				epi.setCama(cama);
			} else {

				Cama caman = new Cama();
				caman = new CamaDAO().getRegistrPorId(res.getLong("idcama"), zona);
				epi.setCama(caman);

			}

			if (agenda != null) {
				epi.setAgenda(agenda);
			} else if (res.getLong("agenda") > 0) {
				epi.setAgenda(new AgendaDAO().getRegistrPorId(res.getLong("agenda")));
			}
			if (res.getLong("prestacion") > 0) {
				Variable prestacion = new Variable();
				prestacion = new CatalogoDAO().getRegistroPorId(res.getLong("prestacion"));
				epi.setPrestacion(prestacion);
			}

			// private Usuario residente;
			// private Long subservicio;
			// private String turno;
			// private String dias;
			// private String garante;
			epi.setIcu(res.getString("icu"));
//			private Long problema;
			// private Long estado_presencia;

		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return epi;
	}

	public String getSqlWhere(Long clase, Centro centro, Servicio servicio, Agenda agenda, LocalDate desde,
			LocalDate hasta) {
		String sql = "";
		if (!clase.equals(new Long(0)))
			sql = " AND e.clase=" + clase;
		if (centro != null)
			sql = sql.concat(" AND e.centro=" + centro.getId());
		if (servicio != null)
			sql = sql.concat(" AND e.servicio=" + servicio.getId());
		if (agenda != null)
			sql = sql.concat(" AND e.agenda=" + agenda.getId());
		if (desde != null) {
			sql = sql.concat(" AND e.finicio>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
		}
		if (hasta != null) {
			sql = sql.concat(" AND e.ffinal<=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
		}
		return sql;
	}

	public String getSqlWhere(Long clase, Centro centro, ArrayList<Servicio> servicios, Agenda agenda, LocalDate desde,
			LocalDate hasta) {
		String sql = "";
		if (!clase.equals(new Long(0)))
			sql = " AND e.clase=" + clase;
		if (centro != null)
			sql = sql.concat(" AND e.centro=" + centro.getId());
		if (servicios != null) {
			sql = sql.concat(" AND e.servicio in (");
			int j = 0;
			for (Servicio servicio : servicios) {
				if (j > 0)
					sql = sql.concat(",");
				j++;
				sql = sql.concat(Long.toString(servicio.getId()));
			}
			sql = sql.concat(" ) ");
		}
		if (agenda != null)
			sql = sql.concat(" AND e.agenda=" + agenda.getId());
		if (desde != null) {
			sql = sql.concat(" AND e.finicio>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
		}
		if (hasta != null) {
			sql = sql.concat(" AND e.ffinal<=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
		}
		return sql;
	}

	public String getSqlWhere(ArrayList<Variable> listaItemPrestaciones) {
		String sql = "";
		String cadena = "";
		if (listaItemPrestaciones != null) {
			for (Variable prestacion : listaItemPrestaciones) {
				if (cadena.length() > 1)
					cadena = cadena.concat(",");

				cadena = cadena.concat(prestacion.getItem().toString());
			}
			if (cadena.length() > 1)
				sql = " AND prestacion in (" + cadena + ")";
		}
		return sql;
	}

	public ArrayList<Episodio> getListaEpisodiossPaginados(Long clase, Centro centro, Servicio servicio, Agenda agenda,
			Zona zona, LocalDate desde, LocalDate hasta, PagiLisReg paginacion, String orden) {
		Connection connection = null;
		ArrayList<Episodio> listaEpisodios = new ArrayList<>();
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			if (persistencia.equals(Constantes.MYSQL_STRING)) {
				sql = "SELECT  @rownum:=@rownum+1  as numeroorden ,e.*,p.ape1,p.ape2,p.nombre"
						+ "	FROM episodios e ,  (SELECT @rownum:=0) r"
						+ "	  JOIN pacientes p ON p.id=e.paciente  WHERE  1=1 ";
			} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
				sql = "SELECT  row_number() over (ORDER BY observacion) as numeroorden ,e.*,p.ape1,p.ape2,p.nombre	"
						+ " FROM episodios e	" + " JOIN pacientes p ON p.id=e.paciente WHERE  1=1 ";
			}
			sql = sql.concat(getSqlWhere(clase, centro, servicio, agenda, desde, hasta));
			if (orden.equals("PACIENTE")) {
				sql = sql.concat(" ORDER BY ape1,ape2,nombre ");
			} else if (orden.equals("FECHAHORA")) {
				sql = sql.concat(" ORDER BY finicio,hinicio ");
			}

			logger.debug(sql);

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();
			while (resulSet.next()) {
				if (paginacion.getDireccion() == 1) {
					if (resulSet.getInt("numeroorden") > paginacion.getAnterior()) {
						episodio = getRegistroResulset(resulSet, null, centro, servicio, agenda, zona, null);
						episodio.setNumeroOrden(resulSet.getInt("numeroorden"));
						listaEpisodios.add(episodio);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				} else {
					if (resulSet.getInt("numeroorden") >= paginacion.getAnterior()) {
						episodio = getRegistroResulset(resulSet, null, centro, servicio, agenda, zona, null);
						episodio.setNumeroOrden(resulSet.getInt("numeroorden"));
						listaEpisodios.add(episodio);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				}
			}
			statement.close();

		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaEpisodios;
	}

	public ArrayList<Episodio> getListaEpisodios(Long clase, Centro centro, Servicio servicio, Agenda agenda, Zona zona,
			LocalDate desde, String orden) {
		Connection connection = null;
		ArrayList<Episodio> listaEpisodios = new ArrayList<>();
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			if (persistencia.equals(Constantes.MYSQL_STRING)) {
				sql = " SELECT   @rownum:=@rownum+1  as numeroorden ,  e.*,e.centro,p.ape1,p.ape2,p.nombre "
						+ ",p.id as idpaciente,p.fnac,p.sexo,p.tarjeta,p.nss,p.dni,p.telefono,p.movil,p.nbdp "
						+ " ,h.nhc "
						+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico "
						+ " , s.id as idservicio,s.codigo as codigoservicio " + " , s.descripcion as descservicio "
						+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado as camaestado "
						+ " FROM (SELECT @rownum:=0) r, episodios e " + " JOIN pacientes p ON p.id=e.paciente "
						+ " JOIN historias h ON h.paciente=p.id " + " JOIN centros t ON t.id=e.CENTRO "
						+ " JOIN servicios s ON s.id=e.servicio " + " LEFT JOIN camas c  ON c.id=e.idcama   "
						+ " WHERE  1=1  ";
			} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
				sql = "SELECT  row_number() over (ORDER BY hinicio) as numeroorden ,e.*,p.ape1,p.ape2,p.nombre,p.id as idpaciente,p.fnac,p.sexo,p.tarjeta,p.nss,p.dni,p.telefono,p.movil,p.nbdp"
						+ ",h.nhc"
						+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico, s.id as idservicio,s.codigo as codigoservicio, s.descripcion as descservicio"
						+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado as camaestado "
						+ " FROM episodios e	" + " JOIN pacientes p ON p.id=e.paciente  "
						+ " JOIN historias h ON h.paciente=p.id " + " JOIN centros t ON t.id=e.centro  "
						+ " JOIN servicios s ON s.id=e.servicio" + " LEFT JOIN camas c  ON c.id=e.idcama  "
						+ " WHERE  1=1 ";
			}
			sql = sql.concat(getSqlWhere(clase, centro, servicio, agenda, null, null));
			sql = sql.concat(" AND finicio= " + Utilidades.getFechaNumeroyyymmddDefecha(desde));
			if (orden.equals("PACIENTE")) {
				sql = sql.concat(" ORDER BY ape1,ape2,nombre ");
			} else if (orden.equals("FECHAHORA")) {
				sql = sql.concat(" ORDER BY finicio,hinicio ");
			}
			logger.debug(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();
			while (resulSet.next()) {
				Paciente paciente = new Paciente(resulSet.getLong("idpaciente"), resulSet.getString("nombre"),
						resulSet.getString("ape1"), resulSet.getString("ape2"),
						Utilidades.getFechaLocalDate(resulSet.getLong("fnac")), resulSet.getInt("sexo"),
						resulSet.getString("nhc"), resulSet.getString("tarjeta"), resulSet.getString("nss"),
						resulSet.getString("dni"), resulSet.getString("telefono"), resulSet.getString("movil"),
						resulSet.getString("nbdp"));
				Cama cama;
				if (zona == null)
					cama = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("camaestado"), new Zona(resulSet.getLong("zona")));
				else
					cama = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("estado"), zona);

				episodio = getRegistroResulset(resulSet, paciente, centro, servicio, agenda, zona, cama);
				episodio.setNumeroOrden(resulSet.getInt("numeroorden"));
				listaEpisodios.add(episodio);
			}
			statement.close();
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaEpisodios;
	}

	public ArrayList<Episodio> getListaEpisodios(Long clase, Centro centro, ArrayList<Servicio> servicios,
			Agenda agenda, Zona zona, LocalDate desde, String orden, ArrayList<Variable> listaItemPrestaciones) {
		Connection connection = null;
		ArrayList<Episodio> listaEpisodios = new ArrayList<>();
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			if (persistencia.equals(Constantes.MYSQL_STRING)) {
				sql = " SELECT   @rownum:=@rownum+1  as numeroorden ,  e.*,e.centro,p.ape1,p.ape2,p.nombre "
						+ ",p.id as idpaciente,p.fnac,p.sexo,p.tarjeta,p.nss,p.dni,p.telefono,p.movil,p.nbdp "
						+ " ,h.nhc "
						+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico "
						+ " , s.id as idservicio,s.codigo as codigoservicio " + " , s.descripcion as descservicio "
						+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado as camaestado "
						+ " FROM (SELECT @rownum:=0) r, episodios e " + " JOIN pacientes p ON p.id=e.paciente "
						+ " JOIN historias h ON h.paciente=p.id " + " JOIN centros t ON t.id=e.CENTRO "
						+ " JOIN servicios s ON s.id=e.servicio " + " LEFT JOIN camas c  ON c.id=e.idcama   "
						+ " WHERE  1=1  ";
			} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
				sql = "SELECT  row_number() over (ORDER BY hinicio) as numeroorden ,e.*,p.ape1,p.ape2,p.nombre,p.id as idpaciente,p.fnac,p.sexo,p.tarjeta,p.nss,p.dni,p.telefono,p.movil,p.nbdp"
						+ ",h.nhc"
						+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico, s.id as idservicio,s.codigo as codigoservicio, s.descripcion as descservicio"
						+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado as camaestado "
						+ " FROM episodios e	" + " JOIN pacientes p ON p.id=e.paciente  "
						+ " JOIN historias h ON h.paciente=p.id " + " JOIN centros t ON t.id=e.centro  "
						+ " JOIN servicios s ON s.id=e.servicio" + " LEFT JOIN camas c  ON c.id=e.idcama  "
						+ " WHERE  1=1 ";
			}
			sql = sql.concat(getSqlWhere(clase, centro, servicios, agenda, null, null));
			sql = sql.concat(" AND finicio= " + Utilidades.getFechaNumeroyyymmddDefecha(desde));
			sql = sql.concat(getSqlWhere(listaItemPrestaciones));
			if (orden.equals("PACIENTE")) {
				sql = sql.concat(" ORDER BY ape1,ape2,nombre ");
			} else if (orden.equals("FECHAHORA")) {
				sql = sql.concat(" ORDER BY finicio,hinicio ");
			}
			logger.debug(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();
			while (resulSet.next()) {
				Paciente paciente = new Paciente(resulSet.getLong("idpaciente"), resulSet.getString("nombre"),
						resulSet.getString("ape1"), resulSet.getString("ape2"),
						Utilidades.getFechaLocalDate(resulSet.getLong("fnac")), resulSet.getInt("sexo"),
						resulSet.getString("nhc"), resulSet.getString("tarjeta"), resulSet.getString("nss"),
						resulSet.getString("dni"), resulSet.getString("telefono"), resulSet.getString("movil"),
						resulSet.getString("nbdp"));
				Cama cama;
				if (zona == null)
					cama = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("camaestado"), new Zona(resulSet.getLong("zona")));
				else
					cama = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("estado"), zona);

				episodio = getRegistroResulset(resulSet, paciente, centro, null, agenda, zona, cama);
				episodio.setNumeroOrden(resulSet.getInt("numeroorden"));
				listaEpisodios.add(episodio);
			}
			statement.close();
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaEpisodios;
	}

	public ArrayList<Episodio> getListaEpisodios(Integer clase, Centro centro) {
		ArrayList<Episodio> listaEpisodios = new ArrayList<>();

		return listaEpisodios;
	}

	public ArrayList<Episodio> getListaEpisodios(Paciente paciente, EpisodioClase clase) {
		Connection connection = null;
		Episodio episodio = null;
		ArrayList<Episodio> listaEpisodios = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM episodios WHERE paciente=" + paciente.getId() + " AND clase=" + clase.getId()
					+ " ORDER BY finicio desc, hinicio desc ";
			logger.debug(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();
			while (resulSet.next()) {
				episodio = getRegistroResulset(resulSet, paciente, null, null, null, null, null);
				listaEpisodios.add(episodio);
			}
			statement.close();

		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaEpisodios;
	}

	public ArrayList<Episodio> getListaEpisodiosCitas(Long clase, Centro centro, Servicio servicio, Agenda agenda,
			LocalDate desde, ArrayList<Variable> listaItemPrestaciones, LocalDate unaFecha) {

		ArrayList<Episodio> calendarioCitas = new ArrayList<Episodio>();
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			String sqlCondicion = getSqlWhere(clase, centro, servicio, agenda, null, null);

			if (desde != null)
				sqlCondicion = sqlCondicion.concat(" AND finicio> " + Utilidades.getFechaNumeroyyymmddDefecha(desde));
			if (unaFecha != null)
				sqlCondicion = sqlCondicion
						.concat(" AND finicio= " + Utilidades.getFechaNumeroyyymmddDefecha(unaFecha));

			sqlCondicion = sqlCondicion.concat(getSqlWhere(listaItemPrestaciones));
			if (agenda == null) {
				sql = " Select e.prestacion,e.finicio,c.descripcion,count(*) as citados " + " FROM  episodios e "
						+ " JOIN catalogo c ON e.prestacion=c.id " + " WHERE ffinal=99999999  ";
				sql = sql.concat(sqlCondicion);
				sql = sql.concat(" GROUP BY finicio,prestacion,descripcion ORDER by finicio,prestacion,descripcion");
			}

			logger.debug(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();

			while (resulSet.next()) {
				Episodio calendario = new Episodio();
				calendario.setCentro(centro);
				calendario.setFinicio(Utilidades.getFechaLocalDate(resulSet.getLong("finicio")));

				calendario
						.setPrestacion(new Variable(resulSet.getLong("prestacion"), resulSet.getString("descripcion")));
				calendario.setCitasDadas(resulSet.getInt("citados"));
				calendarioCitas.add(calendario);
			}
			statement.close();
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}

		return calendarioCitas;
	}

	public ArrayList<Episodio> getCamasEpisodios(Centro centro, Zona zona, Usuario usuario) {
		Connection connection = null;
		ArrayList<Episodio> lista = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			if (connection != null) {
				sql = " SELECT  e.*,p.ape1,p.ape2,p.nombre,p.id as idpaciente,p.fnac,p.sexo,h.nhc  "
						+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico "
						+ " , s.id as idservicio,s.codigo as codigoservicio, s.descripcion as descservicio "
						+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado "
						+ " ,u.nombre as usunombre,u.userid as usuuserid,u.apellido1 as usuapellido1,u.apellido2 as usuapellido2,u.email as usuemail "
						+ " FROM camas c "
						+ "   LEFT JOIN episodios e  ON c.id=e.idcama and (ffinal=99999999 OR ffinal=0 )"
						+ " LEFT JOIN pacientes p ON p.id=e.paciente " + "   LEFT JOIN historias h ON h.paciente=p.id "
						+ " LEFT JOIN centros t ON t.id=e.centro  " + "   LEFT JOIN servicios s ON s.id=e.servicio "
						+ " LEFT JOIN usuarios u ON u.userid=e.userid " + " WHERE c.ZONA=" + zona.getId()
						+ " ORDER BY cama";
				logger.debug(sql);
				PreparedStatement statement = connection.prepareStatement(sql);
				ResultSet resulSet = statement.executeQuery();
				Episodio episodio = null;
				while (resulSet.next()) {
					episodio = new Episodio();
					Cama camaVar;
					if (zona == null)
						camaVar = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
								resulSet.getString("estado"), new Zona(resulSet.getLong("zona")));
					else
						camaVar = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
								resulSet.getString("estado"), zona);

					episodio.setId(resulSet.getLong("id"));
					episodio.setZona(zona);
					episodio.setCama(camaVar);
					EpisodioClase epiclase = Episodio.getClaseFromIdlcase(resulSet.getLong("clase"));
					episodio.setClase(epiclase);
					if (resulSet.getLong("paciente") != 0) {
						Paciente paciente = new Paciente();
						// p.ape1,p.ape2,p.nombre,p.id as idpaciente,p.fnac,p.sexo,h.nhc
						paciente.setApellido1(resulSet.getString("ape1"));
						paciente.setApellido2(resulSet.getString("ape2"));
						paciente.setNombre(resulSet.getString("nombre"));
						paciente.setId(resulSet.getLong("idpaciente"));
						paciente.setFnac(Utilidades.getFechaLocalDate(resulSet.getLong("fnac")));
						paciente.setSexo(resulSet.getInt("sexo"));
						paciente.setNumerohc(resulSet.getString("nhc"));
						episodio.setPaciente(paciente);
					}
					Usuario usu;
					if (resulSet.getString("userid") == null || resulSet.getString("userid").isEmpty()) {
						usu = usuario;
					} else {
						usu = new Usuario();
						usu.setUserid(resulSet.getString("usuuserid"));
						usu.setNombre(resulSet.getString("usunombre"));
						usu.setApellido1(resulSet.getString("usuapellido1"));
						usu.setApellido2(resulSet.getString("usuapellido2"));
						usu.setMail(resulSet.getString("usuemail"));
					}
					episodio.setUserid(usu);
					if (resulSet.getLong("finicio") != 0)
						episodio.setFinicio(Utilidades.getFechaLocalDate(resulSet.getLong("finicio")));

					if (resulSet.getLong("ffinal") != 0 && resulSet.getLong("ffinal") != Constantes.FEHAFIN_DEFECTO) {
						episodio.setFfinal(Utilidades.getFechaLocalDate(resulSet.getLong("ffinal")));
					}

					if (resulSet.getLong("hinicio") != 0)
						episodio.setHinicio(resulSet.getLong("hinicio"));

					if (resulSet.getLong("hfinal") != 0)
						episodio.setHfinal(resulSet.getLong("hfinal"));

					lista.add(episodio);
				}
				statement.close();
			} else {
				logger.error(ConexionDAO.ERROR_BBDD_SIN_CONEXION);
			}
		} catch (

		SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return lista;
	}

	public Episodio getRegistroPorId(Long id, Paciente paciente, Centro centro, Servicio servicio, Agenda agenda,
			Zona zona, Cama cama) {
		Connection connection = null;
		Episodio episodio = null;
		try {
			connection = super.getConexionBBDD();
			sql = " SELECT  e.*,p.ape1,p.ape2,p.nombre,p.id as idpaciente,p.fnac,p.sexo,h.nhc"
					+ " ,t.id as idcentro, t.codigo as codigocentro,t.descripcion as descentro, t.nemonico, s.id as idservicio,s.codigo as codigoservicio, s.descripcion as descservicio"
					+ " , c.cama,c.id as idcama, zona, c.id as idcama,c.estado " + " FROM episodios e	"
					+ " JOIN pacientes p ON p.id=e.paciente  " + " JOIN historias h ON h.paciente=p.id "
					+ " JOIN centros t ON t.id=e.centro  " + " JOIN servicios s ON s.id=e.servicio"
					+ " LEFT JOIN camas c  ON c.id=e.idcama WHERE e.id=" + id;
			logger.debug(sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resulSet = statement.executeQuery();
			episodio = new Episodio();
			if (resulSet.next()) {
				Cama camaVar;
				if (zona == null)
					camaVar = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("estado"), new Zona(resulSet.getLong("zona")));
				else
					camaVar = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("estado"), zona);

				episodio = getRegistroResulset(resulSet, paciente, centro, servicio, agenda, zona, camaVar);
			}
			statement.close();

		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return episodio;
	}

	public boolean getReferenciasExternas(Long idEpisodio) {
		Connection connection = null;
		boolean referencias = false;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id from registros where episodio=?" + " UNION " + "SELECT id FROM informes WHERE episodio=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idEpisodio);
			statement.setLong(2, idEpisodio);
			logger.debug("SELECT id from registros where episodio=" + idEpisodio + " UNION "
					+ "SELECT id FROM informes WHERE episodio= " + idEpisodio);

			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				referencias = true;
			}
			statement.close();
		} catch (SQLException e) {
			logger.error("SELECT id from registros where episodio=" + idEpisodio + " UNION "
					+ "SELECT id FROM informes WHERE episodio= " + idEpisodio);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return referencias;
	}

	public boolean getTodosEpisodiosCerrados(Long idpaciente, EpisodioClase clase) {
		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT *  FROM  episodios  WHERE paciente=?  AND ffinal=? AND clase=? ";
			statement = connection.prepareStatement(sql);
			statement.setLong(1, idpaciente);
			statement.setLong(2, Constantes.FEHAFIN_CERO);
			statement.setLong(3, clase.getId());
			logger.debug("SELECT count(*) as casos  FROM  episodios  WHERE paciente=" + idpaciente + "  AND ffinal="
					+ Constantes.FEHAFIN_CERO + " AND clase= " + clase.getId());

			ResultSet resulSet = statement.executeQuery();

			if (resulSet.next()) {
				return false;
			} else {
				return true;
			}

		} catch (SQLException e) {
			logger.error("SELECT count(*) as casos  FROM  episodios  WHERE paciente=" + idpaciente + "  AND ffinal="
					+ Constantes.FEHAFIN_CERO + " AND clase= " + clase.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				statement.close();
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return true;
	}

	/**
	 * 
	 * @param episodio
	 * @param clase
	 * @param zona
	 * @return true si el paciente tiene para esa fecha, la clase pasada, la zona un
	 *         fila ya añadida en episodio
	 */
	public boolean getTieneEpisodioFechaClase(Paciente paciente, Long finicio, Long clase) {
		boolean esta = false;
		Connection connection = null;
		String sql = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id  FROM  episodios   WHERE paciente=" + paciente.getId() + " AND clase=" + clase
					+ " AND finicio= " + finicio;
			logger.debug(sql);
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);

			if (resulSet.next()) {
				esta = true;
			} else {
				esta = false;
			}
		} catch (SQLException e) {
			logger.error(sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {

				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return esta;
	}

}
