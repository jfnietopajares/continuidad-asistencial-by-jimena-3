package com.jnieto.dao;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Campos_i;
import com.jnieto.entity.Canal;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Informe;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class InformesDAO.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class InformesDAO extends ConexionDAO implements InterfaceDAO {

	private static final Logger logger = LogManager.getLogger(InformesDAO.class);

	private Informe informe = null;

	private String sql;

	public InformesDAO() {
	}

	@Override
	public boolean getReferenciasExternas(Long id) {
		return false;
	}

	@Override
	public boolean grabaDatos(Object object) {
		this.informe = (Informe) object;
		boolean actualizado = false;
		if (informe.getId() == 0) {
			actualizado = this.insertaDatos(informe);
		} else {
			actualizado = this.actualizaDatos(informe);
		}
		return actualizado;
	}

	@Override
	public boolean actualizaDatos(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE informes SET  descripcion=?,  fecha=? , hora =? WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, informe.getDescripcion());
			statement.setLong(2, Utilidades.getFechaNumeroyyymmddDefecha(informe.getFecha()));
			statement.setLong(3, informe.getHora());
			statement.setLong(4, informe.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE informes SET  descripcion= '" + informe.getDescripcion() + "',  fecha="
					+ informe.getFecha() + " , hora =" + informe.getHora() + " WHERE id= " + informe.getId());
		} catch (SQLException e) {
			logger.error(" UPDATE informes SET  descripcion= '" + informe.getDescripcion() + "',  fecha="
					+ informe.getFecha() + " , hora =" + informe.getHora() + " WHERE id= " + informe.getId());
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	@Override
	public boolean insertaDatos(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		Long id = null;
		boolean insertado = false;
		FileInputStream is = null;

		try {
			connection = super.getConexionBBDD();
			id = new UtilidadesDAO().getSiguienteId("informes");
			informe.setId(id);
			is = new FileInputStream(informe.getFicheroInformeFile());
			sql = " INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, informe.getId());
			statement.setLong(2, informe.getCentro().getId());
			statement.setInt(3, informe.getEstado());
			statement.setInt(4, informe.getTipoxml());
			statement.setInt(5, informe.getTipobin());
			statement.setLong(6, informe.getCanal());
			statement.setInt(7, informe.getFlag());
			statement.setInt(8, informe.getVersion());
			statement.setLong(9, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setInt(10, informe.getBloqueado());
			statement.setLong(11, informe.getAmbito());
			statement.setString(12, informe.getDescripcion());
			statement.setLong(13, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(14, Utilidades.getHoraNumeroAcual());
			statement.setLong(15, informe.getServicio().getId());
			statement.setBlob(16, is);
			statement.setLong(17, informe.getPaciente().getId());
			statement.setLong(18, informe.getTipo_documento());
			statement.setLong(19, informe.getProblema().getId());
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (" + informe.getId() + "," + informe.getEstado() + "," + informe.getTipoxml() + ","
					+ informe.getTipobin() + "," + informe.getCanal() + "," + informe.getFlag() + ","
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + informe.getBloqueado() + ","
					+ informe.getAmbito() + ",'" + informe.getDescripcion() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + Utilidades.getHoraNumeroAcual()
					+ "," + is + "," + informe.getPaciente().getId() + "," + informe.getTipo_documento() + ","
					+ informe.getProblema().getId() + ")  ");
		} catch (SQLException e) {
			logger.error(" INSERT INTO informes (id,centro, estado ,tipoxml ,tipobin,canal,flag,version,ultimoguardado"
					+ ",bloqueado,ambito ,descripcion,fecha,hora,servicio,docubin,paciente,tipo_documento,problema)"
					+ " VALUES (" + informe.getId() + "," + informe.getEstado() + "," + informe.getTipoxml() + ","
					+ informe.getTipobin() + "," + informe.getCanal() + "," + informe.getFlag() + ","
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + informe.getBloqueado() + ","
					+ informe.getAmbito() + ",'" + informe.getDescripcion() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + "," + Utilidades.getHoraNumeroAcual()
					+ "," + is + "," + informe.getPaciente().getId() + "," + informe.getTipo_documento() + ","
					+ informe.getProblema().getId() + ")  ");
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	public Informe getRegistroResulset(ResultSet rs, boolean conCampos_I, Paciente paciente, Centro centro,
			Servicio servicio) {
		Informe informe = new Informe();

		try {
			informe.setNumeroOrden(rs.getInt("numeroorden"));
			informe.setId(rs.getLong("id"));
			informe.setDescripcion(rs.getString("descripcion"));
			if (paciente == null) {
				informe.setPaciente(new PacienteDAO().getPacientePorId(rs.getLong("paciente")));
			} else {
				informe.setPaciente(paciente);
			}
			informe.setProblema(new Proceso(rs.getLong("problema")));
			informe.setEpisodio(rs.getLong("episodio"));
			if (centro == null) {
				informe.setCentro(new CentroDAO().getRegistroId(rs.getLong("centro")));
			} else {
				informe.setCentro(centro);
			}
			if (servicio == null) {
				informe.setServicio(new ServiciosDAO().getRegistroId(rs.getLong("servicio")));
			} else {
				informe.setServicio(servicio);
			}
			informe.setReferecia(rs.getString("referencia"));
			informe.setFecha(Utilidades.getFechaLocalDate(rs.getLong("fecha")));
			informe.setHora(rs.getLong("hora"));
			informe.setEstado(rs.getInt("estado"));
			// informe.setDocuxml(docuxml);
			informe.setTipoxml(rs.getInt("tipoxml"));
			// informe.setDocubin(docubin);
			informe.setTipobin(rs.getInt("tipobin"));
			informe.setPeticion(rs.getLong("peticion"));
			informe.setUserid(new UsuarioDAO().getUsuarioUserid(rs.getString("userid"), false));
			informe.setCanal(rs.getLong("canal"));
			informe.setTipoinforme(rs.getInt("tipoinforme"));
			informe.setUseridauth(new Usuario(rs.getString("useridauth")));
			informe.setSrvauth(new Servicio(rs.getLong("srvauth")));
			informe.setUseridredactor(new Usuario(rs.getString("useridredactor")));

			informe.setPlantalla_editor(rs.getLong("plantilla_editor"));

			informe.setFlag(rs.getInt("flag"));
			informe.setPertenece(rs.getLong("pertenece"));
			informe.setVersion(rs.getInt("version"));
			informe.setNive_visibilidad(rs.getInt("nivel_visibilidad"));
			informe.setSubservicio(rs.getLong("subservicio"));
			informe.setUseridpeticionario(new Usuario(rs.getString("useridpeticionario")));
			informe.setVisto(rs.getInt("visto"));
			informe.setUltimoguardado(rs.getLong("ultimoguardado"));
			informe.setBloqueado(rs.getInt("bloqueado"));
			informe.setAlmacenamiento(rs.getLong("almacenamiento"));
			informe.setTipo_documento(rs.getLong("tipo_documento"));
			informe.setAmbito(rs.getLong("ambito"));
			informe.setServicio_realizador(new Servicio(rs.getLong("servicio_realizador")));
			informe.setFecha_proceso(rs.getLong("fecha_proceso"));
			informe.setReferencia_almacenamiento(rs.getString("referencia_almacenamiento"));
			informe.setNum_accesos(rs.getInt("num_accesos"));
			informe.setProblema(new Proceso(rs.getLong("problema")));
			informe.setUser_visto(new Usuario(rs.getString("user_visto")));
			informe.setFecha_visto(rs.getLong("fecha_visto"));
			informe.setComentario_Visto(rs.getString("comentario_visto"));
			if (conCampos_I == true) {
				informe.setListaCampos(getListaCamposInformeso(informe.getId()));
			}
		} catch (SQLException e) {
			logger.error(NotificacionInfo.SQLERRORRESULSET, e);
		}
		return informe;
	}

	public Campos_i getCamposResulSet(ResultSet rs) {
		Campos_i campo_i = new Campos_i();

		try {
			campo_i.setId(rs.getLong("id"));
			campo_i.setInforme(rs.getLong("informe"));
			campo_i.setItem(rs.getLong("item"));
			campo_i.setUserid(rs.getString("userid"));
			campo_i.setFecha(rs.getLong("fecha"));
			campo_i.setHora(rs.getLong("hora"));
			campo_i.setDescripcion(rs.getString("descripcion"));
			campo_i.setUnidades(rs.getString("unidades"));
			campo_i.setReferencias(rs.getString("referencias"));
			campo_i.setFlags(rs.getString("flags"));
			campo_i.setDato(rs.getClob("dato"));
			campo_i.setCodigo(rs.getString("codigo"));
			campo_i.setTipo_codigo(rs.getString("tipo_codigo"));
			campo_i.setEstado(rs.getInt("estado"));
			campo_i.setPegado_en_informe(rs.getInt("pegado_en_informe"));
			campo_i.setDatobin(rs.getBlob("datobin"));
			campo_i.setNivel_visibilida(rs.getInt("nivel_visibilidad"));
			campo_i.setLateralidad(rs.getLong("lateralidad"));
			campo_i.setItemvalor(rs.getLong("itemvalor"));
			campo_i.setCanal(rs.getLong("canal"));
			campo_i.setPrueba(rs.getLong("prueba"));
		} catch (SQLException e) {
			logger.error(NotificacionInfo.SQLERRORRESULSET, e);
		}

		return campo_i;
	}

	public ArrayList<Campos_i> getListaCamposInformeso(Long id) {
		Connection connection = null;
		ArrayList<Campos_i> listaCampos = new ArrayList<Campos_i>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT *  FROM campos_i  WHERE informe = ?  AND estado=? ORDER BY  id";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resuSulset = statement.executeQuery();
			while (resuSulset.next()) {
				Campos_i campo_i = getCamposResulSet(resuSulset);
				listaCampos.add(campo_i);
			}
			statement.close();
			logger.debug("SELECT *  FROM campos_i  WHERE informe = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error("SELECT *  FROM campos_i  WHERE informe = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaCampos;
	}

	@Override
	public Informe getRegistroId(Long id) {
		Connection connection = null;
		Informe informe = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT  row_number()  as numeroorden,i.*   FROM informes i  WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				informe = getRegistroResulset(resulSet, true, null, null, null);
			}
			statement.close();
			logger.debug("SELECT *   FROM centros  WHERE id=" + id);
		} catch (SQLException e) {
			logger.debug("SELECT *   FROM centros  WHERE id=" + id);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return informe;
	}

	@Override
	public String getSqlWhere(String cadena) {
		return null;
	}

	@Override
	public boolean borraDatos(Object objeto) {
		return false;
	}

	/**
	 * Gets the id informe des tipo. Recupera el id del informe para ese paciente,
	 * descripcion y tipo de documento. Este método se usa para no repetir informes
	 * en los procesos que tienen que ser únicos; por ejemplo en el de screening de
	 * mama donde el informe que hace la empresa es único
	 *
	 * @param paciente    un paciente
	 * @param descrString la descripcion del informe
	 * @param tipo        el tipo de documento
	 * @return el id del infome si existe , 0 si no existe.
	 */
	public Long getIdInformeDesTipo(Paciente paciente, String descrString, Long tipo, Long proceso) {
		Connection connection = null;
		Long id = new Long(0);
		informe = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM informes  WHERE paciente=?  AND trim(descripcion)= '" + descrString.trim()
					+ "' AND tipo_documento=?  AND problema=?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setLong(2, tipo);
			statement.setLong(3, proceso);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				id = resulSet.getLong("id");
			}
			statement.close();
			logger.debug(
					sql = "SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND trim(descripcion)= '"
							+ descrString.trim() + "' AND tipo_documento=" + tipo + "  AND problema=" + proceso + "");
		} catch (SQLException e) {
			logger.error(
					sql = "SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND trim(descripcion)= '"
							+ descrString.trim() + "' AND tipo_documento=" + tipo + "  AND problema=" + proceso + "");
			logger.error(CentroDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return id;
	}

	public String geTtextoBotonInfCanal(Paciente paciente, Long canal) {
		Connection connection = null;
		String texto = "";
		String texto1 = "";
		if (paciente != null)
			try {
				connection = super.getConexionBBDD();
				sql = "SELECT  i.fecha,i.hora,SUBSTR( c.nombre,6,100) as nombre FROM informes i JOIN canales c ON c.id=i.canal "
						+ "WHERE paciente=?  AND canal=? AND estado=? ORDER BY fecha desc";
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setLong(1, paciente.getId());
				statement.setLong(2, canal);
				statement.setLong(3, Informe.VAR_RESGISTRO_ESTADO_NORMAL);
				ResultSet resulSet = statement.executeQuery();
				if (resulSet.next()) {
					texto1 = resulSet.getString("nombre");
					texto = Utilidades.getFechFormatoayyyymmdd(resulSet.getLong("fecha"), Constantes.SEPARADOR_FECHA)
							+ " " + Utilidades.getHoraHH_MM(resulSet.getLong("hora"));
					int total = 0;
					while (resulSet.next()) {
						total++;
					}
					texto = texto1 + " " + total + " " + texto;
				} else {
					if (canal == Canal.CANAL_ANATOMIA)
						texto = " Sin informes de anatomia";
					else if (canal == Canal.CANAL_JIMENA)
						texto = " Sin informes de jimena";
					else if (canal == Canal.CANAL_LABORATORIO)
						texto = " Sin informes de laboratorio";
					else if (canal == Canal.CANAL_RADIOLOGIA)
						texto = " Sin informes de radiologia";
					else
						texto = " tipo ? ";
				}
				statement.close();
				logger.debug(
						"SELECT  i.fecha,i.hora,SUBSTR( c.nombre,6,100) as nombre FROM informes i JOIN canales c ON c.id=i.canal WHERE paciente="
								+ paciente.getId() + "  AND canal =" + canal + " and estado="
								+ Informe.VAR_RESGISTRO_ESTADO_NORMAL);
			} catch (SQLException e) {
				logger.error(
						"SELECT  i.fecha,i.hora,SUBSTR( c.nombre,6,100) as nombre FROM informes i JOIN canales c ON c.id=i.canal WHERE paciente="
								+ paciente.getId() + "  AND canal =" + canal + " and estado="
								+ Informe.VAR_RESGISTRO_ESTADO_NORMAL);
				logger.error(CentroDAO.ERROR_BBDD_SQL, e);
			} catch (Exception e) {
				logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
			}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return texto;
	}

	public ArrayList<Informe> getListaInformesPaciCanalNumero(Paciente paciente, Long canal, int cuantos) {
		Connection connection = null;
		ArrayList<Informe> listaInformes = new ArrayList<Informe>();
		if (paciente != null)
			try {
				connection = super.getConexionBBDD();
				if (persistencia.equals(Constantes.MYSQL_STRING)) {
					sql = "SELECT  @rownum:=@rownum+1  as numeroorden ,i.* "
							+ ", c.id as idcentro,c.codigo as codigocentro, c.descripcion as descentro,c.nemonico "
							+ " , s.id as idservicio, s.codigo as codigoservicio, s.descripcion as descservcicio  "
							+ "	FROM informes i,  (SELECT @rownum:=0) r" + " JOIN centros c ON c.id=i.centro "
							+ " JOIN servicios s ON s.id=i.servicio " + " WHERE estado = "
							+ Informe.VAR_RESGISTRO_ESTADO_NORMAL + " paciente=" + paciente.getId() + "   AND canal="
							+ canal + "  ORDER BY fecha desc";

				} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
					sql = "SELECT   row_number() over (ORDER BY fecha,hora desc ) as numeroorden ,i.*"
							+ ", c.id as idcentro,c.codigo as codigocentro, c.descripcion as descentro,c.nemonico "
							+ " , s.id as idservicio, s.codigo as codigoservicio, s.descripcion as descservcicio  "
							+ " FROM informes i " + " JOIN centros c ON c.id=i.centro "
							+ " JOIN servicios s ON s.id=i.servicio " + " WHERE estado = "
							+ Informe.VAR_RESGISTRO_ESTADO_NORMAL + " AND i.paciente=" + paciente.getId()
							+ "   AND i.canal=" + canal + "  ORDER BY fecha desc";
				}
				logger.debug(sql);
				Statement statement = connection.createStatement();
				ResultSet resulSet = statement.executeQuery(sql);

				int contador = 0;
				while (resulSet.next() && (contador <= cuantos) || contador == 0) {
					Centro centroBd = new Centro(resulSet.getLong("idcentro"), resulSet.getString("codigocentro"),
							resulSet.getString("descentro"), resulSet.getString("nemonico"));
					Servicio servicioBd = new Servicio(resulSet.getLong("idservicio"),
							resulSet.getString("codigoservicio"), resulSet.getString("descservcicio"));
					Informe informe = getRegistroResulset(resulSet, false, paciente, centroBd, servicioBd);
					listaInformes.add(informe);
					contador++;
				}
				statement.close();
			} catch (SQLException e) {
				logger.error(sql);
				logger.error(NotificacionInfo.SQLERRORRESULSET, e);
			} catch (Exception e) {
				logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
			}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaInformes;
	}

	/**
	 * 
	 * @param paciente
	 * @param cuantos
	 * @return Array list con los 'cuantos' informes del paciente que no son de los
	 *         canales. Canal.CANAL_ANATOMIA Canal.CANAL_JIMENA
	 *         Canal.CANAL_LABORATORIO Canal.CANAL_RADIOLOGIA
	 */
	public ArrayList<Informe> getListaInformesPaciOtrosCanalesNumero(Paciente paciente, int cuantos) {
		Connection connection = null;
		ArrayList<Informe> listaInformes = new ArrayList<Informe>();
		if (paciente != null)
			try {
				connection = super.getConexionBBDD();
				if (persistencia.equals(Constantes.MYSQL_STRING)) {
					sql = "SELECT  @rownum:=@rownum+1  as numeroorden ,i.*" + "	FROM informes i,  (SELECT @rownum:=0) r"
							+ " WHERE estado = " + Informe.VAR_RESGISTRO_ESTADO_NORMAL + " AND paciente="
							+ paciente.getId() + "   AND canal NOT IN (" + Canal.CANAL_ANATOMIA + ","
							+ Canal.CANAL_JIMENA + "," + Canal.CANAL_LABORATORIO + "," + Canal.CANAL_RADIOLOGIA
							+ ")  ORDER BY fecha desc ";
				} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
					sql = "SELECT   row_number() over (ORDER BY fecha,hora desc ) as numeroorden ,i.* FROM informes i "
							+ " WHERE estado = " + Informe.VAR_RESGISTRO_ESTADO_NORMAL + " AND paciente="
							+ paciente.getId() + "   AND canal NOT IN (" + Canal.CANAL_ANATOMIA + ","
							+ Canal.CANAL_JIMENA + "," + Canal.CANAL_LABORATORIO + "," + Canal.CANAL_RADIOLOGIA
							+ ")  ORDER BY fecha desc ";
				}

				Statement statement = connection.createStatement();
				ResultSet resulSet = statement.executeQuery(sql);

				logger.debug(sql);

				int contador = 0;
				while (resulSet.next() && (contador <= cuantos) || contador == 0) {
					Informe informe = getRegistroResulset(resulSet, false, paciente, null, null);
					listaInformes.add(informe);
					contador++;
				}
				statement.close();
			} catch (SQLException e) {
				logger.error(sql);
				logger.error(NotificacionInfo.SQLERRORRESULSET, e);
			} catch (Exception e) {
				logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
			}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return listaInformes;
	}

	public Long getIdInformeProceso(Paciente paciente, Long proceso) {

		Connection connection = null;
		Long id = new Long(0);
		if (paciente == null || proceso.equals(new Long(0)))
			return id;
		// informe = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM informes  WHERE paciente=?  AND problema=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setLong(2, proceso);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				id = resulSet.getLong("id");
			}
			statement.close();
			logger.debug(
					"SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND problema=" + proceso + "");
		} catch (SQLException e) {
			logger.error(
					"SELECT id FROM informes  WHERE paciente=" + paciente.getId() + "  AND problema=" + proceso + "");
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return id;
	}

	public boolean actualizaFichero(Object mensajeparam) {
		Connection connection = null;
		informe = (Informe) mensajeparam;
		boolean actualizado = false;
		FileInputStream is = null;
		try {
			connection = super.getConexionBBDD();
			is = new FileInputStream(informe.getFicheroInformeFile());
			sql = " UPDATE informes SET  docubin=?,  fecha=? , hora =? WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setBlob(1, is);
			statement.setLong(2, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(3, Utilidades.getHoraNumeroAcual());
			statement.setLong(4, informe.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(sql = " UPDATE informes SET  docubin=" + is + ",  fecha="
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + " , hora ="
					+ Utilidades.getHoraNumeroAcual() + " WHERE id=" + informe.getId());
			actualizado = true;
		} catch (SQLException e) {
			logger.error(sql = " UPDATE informes SET  docubin=" + is + ",  fecha="
					+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + " , hora ="
					+ Utilidades.getHoraNumeroAcual() + " WHERE id=" + informe.getId());
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	public java.sql.Blob getBlobPdfId(Long id) {
		Connection connection = null;
		java.sql.Blob pdfBlob = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT docubin  FROM informes  WHERE id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				pdfBlob = resulSet.getBlob("docubin");
			}
			statement.close();
			logger.debug("SELECT docubin  FROM informes  WHERE id= " + id);
		} catch (SQLException e) {
			logger.error("SELECT docubin  FROM informes  WHERE id= " + id);
			logger.error(CentroDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		try {
			connection.close();
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
		}
		return pdfBlob;
	}

	@Override
	public Object getRegistroResulset(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}
}
