
package com.jnieto.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Campos_r;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Registro;
import com.jnieto.entity.RegistroColon;
import com.jnieto.entity.RegistroEnfConstantes;
import com.jnieto.entity.RegistroEvolutivoMedico;
import com.jnieto.entity.RegistroMama;
import com.jnieto.entity.RegistroO2Incidencia;
import com.jnieto.entity.RegistroOncoCuidados;
import com.jnieto.entity.RegistroOncoEvolutivo;
import com.jnieto.entity.RegistroOncoValoracion;
import com.jnieto.entity.RegistroOxi;
import com.jnieto.entity.RegistroOxiAerosol;
import com.jnieto.entity.RegistroOxiDomi;
import com.jnieto.entity.RegistroOxiHipercapnia;
import com.jnieto.entity.RegistroOxiMapnea;
import com.jnieto.entity.RegistroPartoAlivioDolor;
import com.jnieto.entity.RegistroPartoAlumbramiento;
import com.jnieto.entity.RegistroPartoBuenasPracticas;
import com.jnieto.entity.RegistroPartoConstantes;
import com.jnieto.entity.RegistroPartoExpulsivo;
import com.jnieto.entity.RegistroPartoGestacion;
import com.jnieto.entity.RegistroPartoInduccion;
import com.jnieto.entity.RegistroPartoIngreso;
import com.jnieto.entity.RegistroPartoMedicacion;
import com.jnieto.entity.RegistroPartoPuerperio;
import com.jnieto.entity.RegistroPartoRecienNacido;
import com.jnieto.entity.RegistroQuiSegEntrada;
import com.jnieto.entity.RegistroQuiSegPausa;
import com.jnieto.entity.RegistroQuiSegPre;
import com.jnieto.entity.RegistroQuiSegSalida;
import com.jnieto.entity.Usuario;
import com.jnieto.entity.Variable;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;
import com.vaadin.server.VaadinSession;

/**
 * The Class RegistroDAO.
 * 
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class RegistroDAO extends ConexionDAO implements InterfaceDAO {

	protected String sql;

	private static final Logger logger = LogManager.getLogger(RegistroDAO.class);

	/**
	 * Instantiates a new registro DAO.
	 */
	public RegistroDAO() {
		super();
	}

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public Connection getConnection() {
		return super.getConexionBBDD();
	}

	/**
	 * Graba datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean grabaDatos(Registro registro) {
		boolean actualizado = false;
		if (registro.getId() == 0) {
			actualizado = this.insertaDatos(registro);
		} else {
			actualizado = this.actualizaDatos(registro);
		}
		return actualizado;
	}

	/**
	 * Actualiza datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean actualizaDatos(Registro registro) {
		boolean actualizado = false;
		try {
			// actualizado = doActualizaRegistro(registro);
			doActualizaEstadoRegistro(registro);
			actualizado = this.insertaDatos(registro);
			if (actualizado == true) {
				// poner en campos_r actualizado=5
				if (this.doCambiaEstadoCamposR(registro) == true) {
					doInsertaCamposR(registro);
				} // campos_R estado=5
			} // ok de insercion de registro
		} catch (Exception e) {
			logger.error(NotificacionInfo.MSG_EXCEPTION, e);
		}
		return actualizado;
	}

	/**
	 * Inserta datos.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean insertaDatos(Registro registro) {
		boolean insertado = false;
		Long idRegistro;
		try {
			idRegistro = new UtilidadesDAO().getSiguienteId("registros");
			registro.setId(idRegistro);
			insertado = doInsertaRegistro(registro);
			if (insertado == true) {
				// campos comunes a los registros
				if (!doInsertaCamposR(registro))
					return false;
			} // ok de insercion de registro
		} catch (Exception e) {
			logger.error(NotificacionInfo.MSG_EXCEPTION, e);
		}
		return insertado;
	}

	/**
	 * Haz inserta registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doInsertaRegistro(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		PreparedStatement statement;
		Long idproblemaLong = null;
		try {
			connection = super.getConexionBBDD();
			if (registro.getEpisodio() == null) {
				sql = " INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
						+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  "
						+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
				statement = connection.prepareStatement(sql);
				statement.setLong(1, registro.getId());
				statement.setString(2, registro.getDescripcion());
				statement.setLong(3, registro.getPaciente().getId());
				statement.setLong(4, registro.getCentro().getId());
				statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
				statement.setLong(6, Utilidades.getHoraNumeroAcual());
				statement.setInt(7, registro.getEstado());
				statement.setLong(8, registro.getServicio().getId());
				statement.setString(9, registro.getUserid().getUserid());
				statement.setLong(10, registro.getPlantilla_editor());
				statement.setLong(11, registro.getCanal());
				statement.setLong(12, registro.getTiporegistro());
				statement.setLong(13, registro.getProblema().getId());
				statement.setString(14, registro.getUseridredactor().getUserid());
			} else {

				if (registro.getProblema() != null) {
					idproblemaLong = registro.getProblema().getId();
				} else {
					idproblemaLong = null;
				}
				sql = " INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,episodio,servicio,userid"
						+ ",plantilla_editor,canal,tiporegistro,problema,useridredactor )  "
						+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
				statement = connection.prepareStatement(sql);
				statement.setLong(1, registro.getId());
				statement.setString(2, registro.getDescripcion());
				statement.setLong(3, registro.getPaciente().getId());
				statement.setLong(4, registro.getCentro().getId());
				statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
				statement.setLong(6, Utilidades.getHoraNumeroAcual());
				statement.setInt(7, registro.getEstado());
				statement.setLong(8, registro.getEpisodio().getId()); //
				statement.setLong(9, registro.getServicio().getId());
				statement.setString(10, registro.getUserid().getUserid());
				statement.setLong(11, registro.getPlantilla_editor());
				statement.setLong(12, registro.getCanal());
				statement.setLong(13, registro.getTiporegistro());
				if (idproblemaLong != null)
					statement.setLong(14, idproblemaLong);
				else
					statement.setNull(14, 0);
				statement.setString(15, registro.getUseridredactor().getUserid());
			}
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug("INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
					+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  " + "            VALUES ("
					+ registro.getId() + ",'" + registro.getDescripcion() + "'," + registro.getPaciente().getId() + ","
					+ registro.getCentro().getId() + "," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now())
					+ "," + Utilidades.getHoraNumeroAcual() + "," + registro.getEstado() + "," + registro.getEpisodio()
					+ "," + registro.getServicio().getId() + ",'" + registro.getUserid().getUserid() + "',"
					+ registro.getPlantilla_editor() + "," + registro.getCanal() + "," + registro.getTiporegistro()
					+ "," + idproblemaLong + ")  ");
		} catch (SQLException e) {
			logger.error("INSERT INTO registros  (id, descripcion, paciente ,  centro,fecha,hora,estado,servicio,userid"
					+ ",plantilla_editor,canal,tiporegistro,problema, useridredactor)  " + "            VALUES ("
					+ registro.getId() + ",'" + registro.getDescripcion() + "'," + registro.getPaciente().getId() + ","
					+ registro.getCentro().getId() + "," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now())
					+ "," + Utilidades.getHoraNumeroAcual() + "," + registro.getEstado() + "," + registro.getEpisodio()
					+ "," + registro.getServicio().getId() + ",'" + registro.getUserid().getUserid() + "',"
					+ registro.getPlantilla_editor() + "," + registro.getCanal() + "," + registro.getTiporegistro()
					+ "," + idproblemaLong + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz borrar registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doBorrarRegistro(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		try {
			connection = super.getConexionBBDD();
			sql = " DELETE FROM campos_r WHERE registro=?   ";
			logger.info(" Borra campos_r  " + sql);
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, registro.getId());
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" DELETE FROM campos_r WHERE registro= " + registro.getId());
			sql = " DELETE FROM registros WHERE id =?   ";
			PreparedStatement statement1 = connection.prepareStatement(sql);
			statement1.setLong(1, registro.getId());
			rowInserted = statement1.executeUpdate() > 0;
			statement.close();
			logger.debug(" DELETE FROM registros WHERE id =?   " + registro.getId());
		} catch (SQLException e) {
			logger.error(" DELETE FROM campos_r WHERE registro= " + registro.getId());
			logger.error(" DELETE FROM registros WHERE id =?   " + registro.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz inserta campos R.
	 *
	 * @param registro the registro
	 * @param variable the variable
	 * @param valor    the valor
	 * @return true, if successful
	 */
	public boolean doInsertaCamposR(Registro registro, Variable variable, String valor) {
		Connection connection = null;
		boolean insertado = false;
		Long idCamposr = null;
		try {

			idCamposr = new UtilidadesDAO().getSiguienteId("campos_r");
			connection = super.getConexionBBDD();
			sql = " INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
					+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?)  ";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idCamposr);
			statement.setLong(2, registro.getId());
			statement.setLong(3, variable.getItem());
			statement.setString(4, registro.getUserid().getUserid());
			statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(6, Utilidades.getHoraNumeroAcual());
			statement.setString(7, variable.getDescripcion());
			statement.setLong(8, Registro.ORDEN_DEFECTO); //
			statement.setLong(9, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setString(10, valor);
			statement.setLong(11, Registro.CANAL_DEFECTO);
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'" + registro.getUserid().getUserid() + "',"
							+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'" + valor + "',"
							+ Registro.CANAL_DEFECTO + ")  ");
		} catch (SQLException e) {
			logger.error(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'" + registro.getUserid().getUserid() + "',"
							+ Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'" + valor + "',"
							+ Registro.CANAL_DEFECTO + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	/**
	 * Haz actualiza registro.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doActualizaRegistro(Registro registro) {
		Connection connection = null;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   registros  SET  centro=?,servicio=?,userid=? , fecha=? , hora=?  WHERE id =?   ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, registro.getCentro().getId());
			statement.setLong(2, registro.getServicio().getId());
			statement.setString(3, registro.getUserid().getUserid());
			statement.setLong(4, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(5, Utilidades.getHoraNumeroAcual());
			statement.setLong(6, registro.getId());
			actualizado = statement.executeUpdate() > 0;
			statement.close();

		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	public boolean doActualizaEstadoRegistro(Registro registro) {
		Connection connection = null;
		boolean actualizado = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   registros  SET  estado=? WHERE id =?   ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(2, registro.getId());
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO);
			actualizado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE   registros  SET  estado=" + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO + " WHERE id = "
					+ registro.getId());
		} catch (SQLException e) {
			logger.error(" UPDATE   registros  SET  estado=" + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO + " WHERE id = "
					+ registro.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return actualizado;
	}

	/**
	 * Haz cambia estado campos R.
	 *
	 * @param registro the registro
	 * @return true, if successful
	 */
	public boolean doCambiaEstadoCamposR(Registro registro) {
		Connection connection = null;
		boolean rowInserted = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE   campos_r SET estado=? WHERE registro=? AND estado=?  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO);
			statement.setLong(2, registro.getId());
			statement.setInt(3, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" UPDATE   campos_r SET estado= " + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO
					+ " WHERE registro=" + registro.getId() + " AND estado=  " + Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error(" UPDATE   campos_r SET estado= " + Registro.VAR_RESGISTRO_ESTADO_SUSTITUIDO
					+ " WHERE registro=" + registro.getId() + " AND estado=  " + Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Haz inserta campos R.
	 *
	 * @param registro the registro. Recibe como parámetro un objeto tipo registro.
	 *                 Todos las clases tipo registro extienden de Registro y puede
	 *                 ser un subtipo de mama, colon, oxigenoterapia, incidencias.
	 * 
	 *                 Los objetos Bean de estos tipos tienen definido un método que
	 *                 comienza con el profijo getVariableNombreAtributo que
	 *                 devuelve un dato tipo Variable correspondiente a ese
	 *                 atributo.
	 * 
	 *                 Este método recorre los métodos del objeto y los que su
	 *                 prefijo coinciden con el patrón "getVariable", invoca al
	 *                 método y hace casting al tipo Variable del objeto devuelto.
	 * 
	 * @return true, if successful
	 */
	public boolean doInsertaCamposR(Object registro) {

		boolean insertado = true;
		Method[] metodos = registro.getClass().getMethods();
		for (Method m : metodos) {
			if (m.getName().length() > 10) {
				if (m.getName().substring(0, 11).equals("getVariable")) {
					try {
						if (m.invoke(registro, null) instanceof Variable) {
							Variable variable = (Variable) m.invoke(registro, null);
							if (variable != null) {
								if (variable.getValor() != null) {
									if (!variable.getValor().isEmpty()) {
										insertado = doInsertaCamposRBBDD((Registro) registro, variable);
									}
								}
							}
						}
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
					} catch (Exception e) {
						logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
					}
				}
			}
		}
		return insertado;
	}

	/**
	 * Haz inserta campos RBBDD.
	 *
	 * @param registro the registro
	 * @param variable the variable
	 * @return true, if successful
	 */
	public boolean doInsertaCamposRBBDD(Registro registro, Variable variable) {
		Connection connection = null;
		boolean insertado = false;
		Long idCamposr = null;
		try {

			idCamposr = new UtilidadesDAO().getSiguienteId("campos_r");
			connection = super.getConexionBBDD();
			sql = " INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
					+ "            VALUES (?,?,?,?,?,?,?,?,?,?,?)  ";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idCamposr);
			statement.setLong(2, registro.getId());
			statement.setLong(3, variable.getItem());
			statement.setString(4,
					((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME)).getUserid());
			statement.setLong(5, Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()));
			statement.setLong(6, Utilidades.getHoraNumeroAcual());
			statement.setString(7, variable.getDescripcion());
			statement.setLong(8, Registro.ORDEN_DEFECTO); //
			statement.setLong(9, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setString(10, variable.getValor());
			statement.setLong(11, Registro.CANAL_DEFECTO);
			insertado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'"
							+ ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
									.getUserid()
							+ "'," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'"
							+ variable.getValor() + "'," + Registro.CANAL_DEFECTO + ")  ");
		} catch (SQLException e) {
			logger.error(
					" INSERT INTO campos_r   (id, registro,item,userid,fecha,hora,descripcion,orden,estado,dato,canal )  "
							+ "            VALUES (" + idCamposr + "," + registro.getId() + "," + variable.getItem()
							+ ",'"
							+ ((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME))
									.getUserid()
							+ "'," + Utilidades.getFechaNumeroyyymmddDefecha(LocalDate.now()) + ","
							+ Utilidades.getHoraNumeroAcual() + ",'" + variable.getDescripcion() + "',"
							+ Registro.ORDEN_DEFECTO + "," + Registro.VAR_RESGISTRO_ESTADO_NORMAL + ",'"
							+ variable.getValor() + "'," + Registro.CANAL_DEFECTO + ")  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return insertado;
	}

	/**
	 * Gets the registro por id.
	 *
	 * @param id the id
	 * @return the registro por id
	 */
	public Registro getRegistroPorId(Long id) {
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.* FROM registros r  WHERE r.id = ?  AND r.estado=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				return getRegistroResulset(resuSulset);
			}
			statement.close();
			logger.debug("SELECT r.* FROM registros r  WHERE r.id = " + id + "  AND r.estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error("SELECT r.* FROM registros r  WHERE r.id = " + id + "  AND r.estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return null;
	}

	/**
	 * Gets the registro resulset.
	 *
	 * @param resuSulset the resu sulset
	 * @return the registro resulset
	 */

	public Registro getRegistroResulset(ResultSet resuSulset) {
		Long id;
		try {
			id = resuSulset.getLong("plantilla_editor");

			if (id.equals(RegistroMama.PLANTILLLA_EDITOR_REGISTROMAMA)) {
				RegistroMama registro = new RegistroMama();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroColon.PLANTILLLA_EDITOR_REGISTROCOLON)) {
				RegistroColon registro = new RegistroColon();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiAerosol.PLANTILLLA_EDITOR_REGISTROAEROSOL)) {
				RegistroOxiAerosol registro = new RegistroOxiAerosol();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiMapnea.PLANTILLLA_EDITOR_REGISTROMAPNEA)) {
				RegistroOxiMapnea registro = new RegistroOxiMapnea();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiDomi.PLANTILLLA_EDITOR_REGISTRODOMI)) {
				RegistroOxiDomi registro = new RegistroOxiDomi();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroOxiHipercapnia.PLANTILLLA_EDITOR_REGISTROEQPOYO)) {
				RegistroOxiHipercapnia registro = new RegistroOxiHipercapnia();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				RegistroOxiHipercapnia registroOxiEapoyo = new RegistroOxiHipercapmiaDAO().getValoresCamposR(registro);
				return registroOxiEapoyo;
			} else if (id.equals(RegistroO2Incidencia.PLANTILLLA_EDITOR_REGISTROINCIDENCIA)) {
				RegistroO2Incidencia registro = new RegistroO2Incidencia();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			} else if (id.equals(RegistroEnfConstantes.PLANTILLLA_EDITOR_ENF_CTES)) {
				RegistroEnfConstantes registro = new RegistroEnfConstantes();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				return registro;
			} else if (id.equals(RegistroPartoGestacion.PLANTILLLA_EDITOR_PAR_GESTACION)) {
				RegistroPartoGestacion registro = new RegistroPartoGestacion();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_GESTA_ALERGIA.getItem())) {
						v = registro.VAR_PARTO_GESTA_ALERGIA;
						v.setValor(campo.getDato());
						registro.setAlergia(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_ECO20.getItem())) {
						v = registro.VAR_PARTO_GESTA_ECO20;
						v.setValor(campo.getDato());
						registro.setEco20semanas(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_EDAD.getItem())) {
						v = registro.VAR_PARTO_GESTA_EDAD;
						v.setValor(campo.getDato());
						registro.setEdad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_EGESTAIONAL.getItem())) {
						v = registro.VAR_PARTO_GESTA_EGESTAIONAL;
						v.setValor(campo.getDato());
						registro.setEdadGestacional(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_FPP.getItem())) {
						v = registro.VAR_PARTO_GESTA_FPP;
						v.setValor(campo.getDato());
						registro.setFechaprobableParto(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_FUR.getItem())) {
						v = registro.VAR_PARTO_GESTA_FUR;
						v.setValor(campo.getDato());
						registro.setFechaUltimaRegla(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_GABO.getItem())) {
						v = registro.VAR_PARTO_GESTA_GABO;
						v.setValor(campo.getDato());
						registro.setGrupoABO(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_GEMELAR.getItem())) {
						v = registro.VAR_PARTO_GESTA_GEMELAR;
						v.setValor(campo.getDato());
						registro.setGemelar(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_NUMEROFETOS.getItem())) {
						v = registro.VAR_PARTO_GESTA_NUMEROFETOS;
						v.setValor(campo.getDato());
						registro.setNumeroFetos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PARIDAD.getItem())) {
						v = registro.VAR_PARTO_GESTA_PARIDAD;
						v.setValor(campo.getDato());
						registro.setParidad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_CRECIMI.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_CRECIMI;
						v.setValor(campo.getDato());
						registro.setPatoFetoDefCremiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_HIDRAMNIOS.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_HIDRAMNIOS;
						v.setValor(campo.getDato());
						registro.setPatoFetoMalformacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_LIQADMI.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_LIQADMI;
						v.setValor(campo.getDato());
						registro.setPatoFetoAlteracionLiq(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_MALFORMACIÓN.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_MALFORMACIÓN;
						v.setValor(campo.getDato());
						registro.setPatoFetoMalformacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_NO.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_NO;
						v.setValor(campo.getDato());
						registro.setPatoFetoNo(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_OLIGOAMNIOS.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_OLIGOAMNIOS;
						v.setValor(campo.getDato());
						registro.setPatoFetoOligoamnios(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_FET_OTROS.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_FET_OTROS;
						v.setValor(campo.getDato());
						registro.setPatoFetoOtros(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_APPE.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_APPE;
						v.setValor(campo.getDato());
						registro.setAPPE(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_AR.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_AR;
						v.setValor(campo.getDato());
						registro.setAR(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_CANCER.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_CANCER;
						v.setValor(campo.getDato());
						registro.setCancer(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_DIABE.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_DIABE;
						v.setValor(campo.getDato());
						registro.setPatoMatDiabetesGestional(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_DMP.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_DMP;
						v.setValor(campo.getDato());
						registro.setDMG(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_DMPG.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_DMPG;
						v.setValor(campo.getDato());
						registro.setDMPG(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_EII.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_EII;
						v.setValor(campo.getDato());
						registro.setEII(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_EPILEPSIA.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_EPILEPSIA;
						v.setValor(campo.getDato());
						registro.setEpilepsia(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_FETOMUERTO.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_FETOMUERTO;
						v.setValor(campo.getDato());
						registro.setFetomuerto(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_HTA.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_HTA;
						v.setValor(campo.getDato());
						registro.setHTA(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_IP_UTERINAS.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_IP_UTERINAS;
						v.setValor(campo.getDato());
						registro.setIPuterinas_95(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_NO.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_NO;
						v.setValor(campo.getDato());
						registro.setPatoMatNo(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_OTROS.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_OTROS;
						v.setValor(campo.getDato());
						registro.setPatoMatOtros(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_REUMATOL.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_REUMATOL;
						v.setValor(campo.getDato());
						registro.setReumatogicas(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_TROMBO.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_TROMBO;
						v.setValor(campo.getDato());
						registro.setPatoMatTrombofilia(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_PAT_MAT_TVP.getItem())) {
						v = registro.VAR_PARTO_GESTA_PAT_MAT_TVP;
						v.setValor(campo.getDato());
						registro.setTVP(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_RUBE.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_RUBE;
						v.setValor(campo.getDato());
						registro.setSero1TRubeola(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_SIFI.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_SIFI;
						v.setValor(campo.getDato());
						registro.setSero1Tsifilis(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_TOXO.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_TOXO;
						v.setValor(campo.getDato());
						registro.setSero1TToxo(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_VHB.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_VHB;
						v.setValor(campo.getDato());
						registro.setSero1TVHB(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_VHC.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_VHC;
						v.setValor(campo.getDato());
						registro.setSero1TVHC(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO1_VIH.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO1_VIH;
						v.setValor(campo.getDato());
						registro.setSero1TVIH(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_RUBE.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_RUBE;
						v.setValor(campo.getDato());
						registro.setSero3TRubeola(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_SIFI.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_SIFI;
						v.setValor(campo.getDato());
						registro.setSero3Tsifilis(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_TOXO.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_TOXO;
						v.setValor(campo.getDato());
						registro.setSero3TToxo(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_VHB.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_VHB;
						v.setValor(campo.getDato());
						registro.setSero3TVHB(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_VHC.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_VHC;
						v.setValor(campo.getDato());
						registro.setSero3TVHC(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SERO3_VIH.getItem())) {
						v = registro.VAR_PARTO_GESTA_SERO3_VIH;
						v.setValor(campo.getDato());
						registro.setSero3TVIH(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SGB_FECHA.getItem())) {
						v = registro.VAR_PARTO_GESTA_SGB_FECHA;
						v.setValor(campo.getDato());
						registro.setEsteGrupoBFecha(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_SGB_VALOR.getItem())) {
						v = registro.VAR_PARTO_GESTA_SGB_VALOR;
						v.setValor(campo.getDato());
						registro.setEsteGrupoBValor(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_GESTA_TECREPUASIST.getItem())) {
						v = registro.VAR_PARTO_GESTA_TECREPUASIST;
						v.setValor(campo.getDato());
						registro.setTecReproAsist(v);
					}

				}
				return registro;
			} else if (id.equals(RegistroPartoIngreso.PLANTILLLA_EDITOR_PAR_INGRESO)) {
				RegistroPartoIngreso registro = new RegistroPartoIngreso();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_FECHA.getItem())) {
						v = registro.VAR_PARTO_INGRESO_FECHA;
						v.setValor(campo.getDato());
						registro.setFechaIngreso(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_HORA.getItem())) {
						v = registro.VAR_PARTO_INGRESO_HORA;
						v.setValor(campo.getDato());
						registro.setHoraIngreso(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_OBSERVADOR.getItem())) {
						v = registro.VAR_PARTO_INGRESO_OBSERVADOR;
						v.setValor(campo.getDato());
						registro.setObservador(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_TIPO_INGRESO.getItem())) {
						v = registro.VAR_PARTO_INGRESO_TIPO_INGRESO;
						v.setValor(campo.getDato());
						registro.setTipoIngreso(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_SITUACION.getItem())) {
						v = registro.VAR_PARTO_INGRESO_SITUACION;
						v.setValor(campo.getDato());
						registro.setSituacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_PRESENTACION.getItem())) {
						v = registro.VAR_PARTO_INGRESO_PRESENTACION;
						v.setValor(campo.getDato());
						registro.setPresentacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INGRESO_BISHOP.getItem())) {
						v = registro.VAR_PARTO_INGRESO_BISHOP;
						v.setValor(campo.getDato());
						registro.setBishop(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoInduccion.PLANTILLLA_EDITOR_PAR_INDUCCION)) {
				RegistroPartoInduccion registro = new RegistroPartoInduccion();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_INDU_PATOMAT.getItem())) {
						v = registro.VAR_PARTO_INDU_PATOMAT;
						v.setValor(campo.getDato());
						registro.setPatologiaMaterna(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_PATOFETCIR.getItem())) {
						v = registro.VAR_PARTO_INDU_PATOMAT;
						v.setValor(campo.getDato());
						registro.setPatologiaFetalCir(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_PATOFETPEG.getItem())) {
						v = registro.VAR_PARTO_INDU_PATOFETPEG;
						v.setValor(campo.getDato());
						registro.setPatologiaFetalPeg(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_ROTURAMAS12.getItem())) {
						v = registro.VAR_PARTO_INDU_ROTURAMAS12;
						v.setValor(campo.getDato());
						registro.setRoturaBolasmas12(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_GESPROLONGADA.getItem())) {
						v = registro.VAR_PARTO_INDU_GESPROLONGADA;
						v.setValor(campo.getDato());
						registro.setGestacionFetalProlongada(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_ALTERACIONLA.getItem())) {
						v = registro.VAR_PARTO_INDU_ALTERACIONLA;
						v.setValor(campo.getDato());
						registro.setAlteracionLiqAmni(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_OTRASINDIC.getItem())) {
						v = registro.VAR_PARTO_INDU_OTRASINDIC;
						v.setValor(campo.getDato());
						registro.setOtrasIndicacionesInduccion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_MADURACIONCERVICAL.getItem())) {
						v = registro.VAR_PARTO_INDU_MADURACIONCERVICAL;
						v.setValor(campo.getDato());
						registro.setMaduracioncervical(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_MADURACIONHORAS.getItem())) {
						v = registro.VAR_PARTO_INDU_MADURACIONHORAS;
						v.setValor(campo.getDato());
						registro.setHoras(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_PREPARAOXI.getItem())) {
						v = registro.VAR_PARTO_INDU_PREPARAOXI;
						v.setValor(campo.getDato());
						registro.setPreparacionOxiticica(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_INDU_EVOLUCION.getItem())) {
						v = registro.VAR_PARTO_INDU_EVOLUCION;
						v.setValor(campo.getDato());
						registro.setEvolucion(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoAlivioDolor.PLANTILLLA_EDITOR_PAR_ALIVIODOLOR)) {
				RegistroPartoAlivioDolor registro = new RegistroPartoAlivioDolor();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_FARMA.getItem())) {
						v = registro.VAR_PARTO_DOLOR_FARMA;
						v.setValor(campo.getDato());
						registro.setFarmacologico(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_CALOR.getItem())) {
						v = registro.VAR_PARTO_DOLOR_CALOR;
						v.setValor(campo.getDato());
						registro.setCalorLocal(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_DUCHA.getItem())) {
						v = registro.VAR_PARTO_DOLOR_DUCHA;
						v.setValor(campo.getDato());
						registro.setDucha(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_MASAJE.getItem())) {
						v = registro.VAR_PARTO_DOLOR_MASAJE;
						v.setValor(campo.getDato());
						registro.setMasaje(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_MOVIMIENTO.getItem())) {
						v = registro.VAR_PARTO_DOLOR_MOVIMIENTO;
						v.setValor(campo.getDato());
						registro.setMovimiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_PELOTA.getItem())) {
						v = registro.VAR_PARTO_DOLOR_PELOTA;
						v.setValor(campo.getDato());
						registro.setPelota(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_DOLOR_OTROS.getItem())) {
						v = registro.VAR_PARTO_DOLOR_OTROS;
						v.setValor(campo.getDato());
						registro.setOtros(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoConstantes.PLANTILLLA_EDITOR_PAR_CONSTANTES)) {
				RegistroPartoConstantes registro = new RegistroPartoConstantes();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_CTE_FC.getItem())) {
						v = registro.VAR_PARTO_CTE_FC;
						v.setValor(campo.getDato());
						registro.setFc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_FIEBRE_MATERNA.getItem())) {
						v = registro.VAR_PARTO_CTE_FIEBRE_MATERNA;
						v.setValor(campo.getDato());
						registro.setFiebreMaterna(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_INTERPRETACIONRECO.getItem())) {
						v = registro.VAR_PARTO_CTE_INTERPRETACIONRECO;
						v.setValor(campo.getDato());
						registro.setInterpretacioRC(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_PHFETAL.getItem())) {
						v = registro.VAR_PARTO_CTE_PHFETAL;
						v.setValor(campo.getDato());
						registro.setPhFetal(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_PHFETAL_VALOR.getItem())) {
						v = registro.VAR_PARTO_CTE_PHFETAL_VALOR;
						v.setValor(campo.getDato());
						registro.setPhFetalValor(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_T.getItem())) {
						v = registro.VAR_PARTO_CTE_T;
						v.setValor(campo.getDato());
						registro.setTemperatura(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_TAD.getItem())) {
						v = registro.VAR_PARTO_CTE_TAD;
						v.setValor(campo.getDato());
						registro.setTad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_TAS.getItem())) {
						v = registro.VAR_PARTO_CTE_TAS;
						v.setValor(campo.getDato());
						registro.setTas(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_CTE_TIPO_REGECOCARDIO.getItem())) {
						v = registro.VAR_PARTO_CTE_TIPO_REGECOCARDIO;
						v.setValor(campo.getDato());
						registro.setRegistroCaridiotocografico(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoExpulsivo.PLANTILLLA_EDITOR_PAR_EXPULSIVO)) {
				RegistroPartoExpulsivo registro = new RegistroPartoExpulsivo();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_HORAS.getItem())) {
						v = registro.VAR_PARTO_EXPUL_HORAS;
						v.setValor(campo.getDato());
						registro.setHoras(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_INDICACIONCESAREA.getItem())) {
						v = registro.VAR_PARTO_EXPUL_INDICACIONCESAREA;
						v.setValor(campo.getDato());
						registro.setIndicacionCesarea(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_INTERPRETACIONRC.getItem())) {
						v = registro.VAR_PARTO_EXPUL_INTERPRETACIONRC;
						v.setValor(campo.getDato());
						registro.setInterpretacioRC(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_PHFETAL.getItem())) {
						v = registro.VAR_PARTO_EXPUL_PHFETAL;
						v.setValor(campo.getDato());
						registro.setPhFetal(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_T.getItem())) {
						v = registro.VAR_PARTO_EXPUL_T;
						v.setValor(campo.getDato());
						registro.setTemperatura(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_EXPUL_TIPOPARTO_.getItem())) {
						v = registro.VAR_PARTO_EXPUL_TIPOPARTO_;
						v.setValor(campo.getDato());
						registro.setPartoTipo(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoAlumbramiento.PLANTILLLA_EDITOR_PAR_ALUMBRAMIENTO)) {
				RegistroPartoAlumbramiento registro = new RegistroPartoAlumbramiento();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_ALUM_CORDONDONACION.getItem())) {
						v = registro.VAR_PARTO_ALUM_CORDONDONACION;
						v.setValor(campo.getDato());
						registro.setCordonDonacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_CORDONINSERCION.getItem())) {
						v = registro.VAR_PARTO_ALUM_CORDONINSERCION;
						v.setValor(campo.getDato());
						registro.setCordonInsercion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_CORDONVASOS.getItem())) {
						v = registro.VAR_PARTO_ALUM_CORDONVASOS;
						v.setValor(campo.getDato());
						registro.setCordonVasos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_HEMORRACANTIDAD.getItem())) {
						v = registro.VAR_PARTO_ALUM_HEMORRACANTIDAD;
						v.setValor(campo.getDato());
						registro.setHemorragiaCantidad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_HEMORRASINGOS.getItem())) {
						v = registro.VAR_PARTO_ALUM_HEMORRASINGOS;
						v.setValor(campo.getDato());
						registro.setHemorragiaSingos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_HEMORRATRATAMIENTO.getItem())) {
						v = registro.VAR_PARTO_ALUM_HEMORRATRATAMIENTO;
						v.setValor(campo.getDato());
						registro.setHemorragiaTratamiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_MEMBRANASFETALES.getItem())) {
						v = registro.VAR_PARTO_ALUM_MEMBRANASFETALES;
						v.setValor(campo.getDato());
						registro.setMembranasFetales(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_PLACENTA.getItem())) {
						v = registro.VAR_PARTO_ALUM_PLACENTA;
						v.setValor(campo.getDato());
						registro.setPlacenta(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_ALUM_TIPOPARTO.getItem())) {
						v = registro.VAR_PARTO_ALUM_TIPOPARTO;
						v.setValor(campo.getDato());
						registro.setTipoParto(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoMedicacion.PLANTILLLA_EDITOR_PAR_MEDICAMENTO)) {
				RegistroPartoMedicacion registro = new RegistroPartoMedicacion();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_MEDICA_ANALGESIA.getItem())) {
						v = registro.VAR_PARTO_MEDICA_ANALGESIA;
						v.setValor(campo.getDato());
						registro.setAnalgesia(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_MEDICA_ANALGESIAANTIHEMETIC.getItem())) {
						v = registro.VAR_PARTO_MEDICA_ANALGESIAANTIHEMETIC;
						v.setValor(campo.getDato());
						registro.setAnalgesiaAntihemetico(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_MEDICA_CESAREA.getItem())) {
						v = registro.VAR_PARTO_MEDICA_CESAREA;
						v.setValor(campo.getDato());
						registro.setCesarea(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_MEDICA_DIABETES.getItem())) {
						v = registro.VAR_PARTO_MEDICA_DIABETES;
						v.setValor(campo.getDato());
						registro.setDiabetes(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_MEDICA_OTRAS.getItem())) {
						v = registro.VAR_PARTO_MEDICA_OTRAS;
						v.setValor(campo.getDato());
						registro.setOtraMedicacion(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoPuerperio.PLANTILLLA_EDITOR_PAR_PUERPERIO)) {
				RegistroPartoPuerperio registro = new RegistroPartoPuerperio();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_ALTURAUTERO.getItem())) {
						v = registro.VAR_PARTO_PUERPE_ALTURAUTERO;
						v.setValor(campo.getDato());
						registro.setAlturaFondoUtero(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_ANALGESIA.getItem())) {
						v = registro.VAR_PARTO_PUERPE_ANALGESIA;
						v.setValor(campo.getDato());
						registro.setAnalgesia(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_DOLOR.getItem())) {
						v = registro.VAR_PARTO_PUERPE_DOLOR;
						v.setValor(campo.getDato());
						registro.setDolor(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_LACTANCIAM.getItem())) {
						v = registro.VAR_PARTO_PUERPE_LACTANCIAM;
						v.setValor(campo.getDato());
						registro.setLactanciaMaterna(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_MEDICACION.getItem())) {
						v = registro.VAR_PARTO_PUERPE_MEDICACION;
						v.setValor(campo.getDato());
						registro.setMedicacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_PIELPIEL.getItem())) {
						v = registro.VAR_PARTO_PUERPE_PIELPIEL;
						v.setValor(campo.getDato());
						registro.setPielConPiel(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_SANGRADO.getItem())) {
						v = registro.VAR_PARTO_PUERPE_SANGRADO;
						v.setValor(campo.getDato());
						registro.setSangrado(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_TAS.getItem())) {
						v = registro.VAR_PARTO_PUERPE_TAS;
						v.setValor(campo.getDato());
						registro.setTas(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_TAD.getItem())) {
						v = registro.VAR_PARTO_PUERPE_TAD;
						v.setValor(campo.getDato());
						registro.setTad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_FC.getItem())) {
						v = registro.VAR_PARTO_PUERPE_FC;
						v.setValor(campo.getDato());
						registro.setFc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_PUERPE_DIURESIS.getItem())) {
						v = registro.VAR_PARTO_PUERPE_DIURESIS;
						v.setValor(campo.getDato());
						registro.setDiuresis(v);
					}

				}
				return registro;
			} else if (id.equals(RegistroPartoBuenasPracticas.PLANTILLLA_EDITOR_PAR_BUENASPRACTICAS)) {
				RegistroPartoBuenasPracticas registro = new RegistroPartoBuenasPracticas();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_BP_BUENASPRACRN.getItem())) {
						v = registro.VAR_PARTO_BP_BUENASPRACRN;
						v.setValor(campo.getDato());
						registro.setBuenasRN(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_DONACIONC.getItem())) {
						v = registro.VAR_PARTO_BP_DONACIONC;
						v.setValor(campo.getDato());
						registro.setDonaCordon(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_ELECCIONA.getItem())) {
						v = registro.VAR_PARTO_BP_ELECCIONA;
						v.setValor(campo.getDato());
						registro.setAcompanamiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_ELECCIONPOST.getItem())) {
						v = registro.VAR_PARTO_BP_ELECCIONPOST;
						v.setValor(campo.getDato());
						registro.setEleccPostura(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_INGESTALIQ.getItem())) {
						v = registro.VAR_PARTO_BP_INGESTALIQ;
						v.setValor(campo.getDato());
						registro.setIngestaLiq(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_MOVIMIENTODILA.getItem())) {
						v = registro.VAR_PARTO_BP_MOVIMIENTODILA;
						v.setValor(campo.getDato());
						registro.setMoviDilatacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_PLANPLARTO.getItem())) {
						v = registro.VAR_PARTO_BP_PLANPLARTO;
						v.setValor(campo.getDato());
						registro.setPlanParto(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_BP_PROTOCOLOACOGIDA.getItem())) {
						v = registro.VAR_PARTO_BP_PROTOCOLOACOGIDA;
						v.setValor(campo.getDato());
						registro.setProtocoloAcogida(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroPartoRecienNacido.PLANTILLLA_EDITOR_PAR_RECIENNACIDO)) {
				RegistroPartoRecienNacido registro = new RegistroPartoRecienNacido();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_PARTO_RN_APGAR1.getItem())) {
						v = registro.VAR_PARTO_RN_APGAR1;
						v.setValor(campo.getDato());
						registro.setApgar1Puntuacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_APGAR10.getItem())) {
						v = registro.VAR_PARTO_RN_APGAR10;
						v.setValor(campo.getDato());
						registro.setApgar10Puntuacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_APGAR5.getItem())) {
						v = registro.VAR_PARTO_RN_APGAR5;
						v.setValor(campo.getDato());
						registro.setApgar10Puntuacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_COLOR1.getItem())) {
						v = registro.VAR_PARTO_RN_COLOR1;
						v.setValor(campo.getDato());
						registro.setApgar1Color(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_COLOR10.getItem())) {
						v = registro.VAR_PARTO_RN_COLOR10;
						v.setValor(campo.getDato());
						registro.setApgar10Color(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_COLOR5.getItem())) {
						v = registro.VAR_PARTO_RN_COLOR5;
						v.setValor(campo.getDato());
						registro.setApgar5Color(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_CORDONARTERIAL.getItem())) {
						v = registro.VAR_PARTO_RN_CORDONARTERIAL;
						v.setValor(campo.getDato());
						registro.setCordonArterial(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_CORDONVENOSO.getItem())) {
						v = registro.VAR_PARTO_RN_CORDONVENOSO;
						v.setValor(campo.getDato());
						registro.setCordonVenoso(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_FC1.getItem())) {
						v = registro.VAR_PARTO_RN_FC1;
						v.setValor(campo.getDato());
						registro.setApgar1Fc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_FC10.getItem())) {
						v = registro.VAR_PARTO_RN_FC10;
						v.setValor(campo.getDato());
						registro.setApgar10Fc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_FC5.getItem())) {
						v = registro.VAR_PARTO_RN_FC5;
						v.setValor(campo.getDato());
						registro.setApgar5Fc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_FECHANACI.getItem())) {
						v = registro.VAR_PARTO_RN_FECHANACI;
						v.setValor(campo.getDato());
						registro.setFechaNacimiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_HORANACI.getItem())) {
						v = registro.VAR_PARTO_RN_HORANACI;
						v.setValor(campo.getDato());
						registro.setHoraNacimiento(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_INGRESONENOTANOS.getItem())) {
						v = registro.VAR_PARTO_RN_INGRESONENOTANOS;
						v.setValor(campo.getDato());
						registro.setIngresNeonatos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_LACTANCIA.getItem())) {
						v = registro.VAR_PARTO_RN_LACTANCIA;
						v.setValor(campo.getDato());
						registro.setLactanciaParitorio(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_MORTALIDAD.getItem())) {
						v = registro.VAR_PARTO_RN_MORTALIDAD;
						v.setValor(campo.getDato());
						registro.setMortalidad(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_NUMEROHC.getItem())) {
						v = registro.VAR_PARTO_RN_NUMEROHC;
						v.setValor(campo.getDato());
						registro.setNumerohc(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_PIELCONPIL.getItem())) {
						v = registro.VAR_PARTO_RN_PIELCONPIL;
						v.setValor(campo.getDato());
						registro.setPielConPiel(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_PCEFALICO.getItem())) {
						v = registro.VAR_PARTO_RN_PCEFALICO;
						v.setValor(campo.getDato());
						registro.setpCefalico(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_PESO.getItem())) {
						v = registro.VAR_PARTO_RN_PESO;
						v.setValor(campo.getDato());
						registro.setPeso(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_PINZAMIENTO.getItem())) {
						v = registro.VAR_PARTO_RN_PINZAMIENTO;
						v.setValor(campo.getDato());
						registro.setPinzaCordon(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_REANIMACIONNONATAL.getItem())) {
						v = registro.VAR_PARTO_RN_REANIMACIONNONATAL;
						v.setValor(campo.getDato());
						registro.setReanimacion(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_REFLEJOS1.getItem())) {
						v = registro.VAR_PARTO_RN_REFLEJOS1;
						v.setValor(campo.getDato());
						registro.setApgar1Reflejos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_REFLEJOS10.getItem())) {
						v = registro.VAR_PARTO_RN_REFLEJOS10;
						v.setValor(campo.getDato());
						registro.setApgar10Reflejos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_REFLEJOS5.getItem())) {
						v = registro.VAR_PARTO_RN_REFLEJOS5;
						v.setValor(campo.getDato());
						registro.setApgar5Reflejos(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_RESPIRACION1.getItem())) {
						v = registro.VAR_PARTO_RN_RESPIRACION1;
						v.setValor(campo.getDato());
						registro.setApgar1Respira(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_RESPIRACION10.getItem())) {
						v = registro.VAR_PARTO_RN_RESPIRACION10;
						v.setValor(campo.getDato());
						registro.setApgar10Respira(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_RESPIRACION5.getItem())) {
						v = registro.VAR_PARTO_RN_RESPIRACION5;
						v.setValor(campo.getDato());
						registro.setApgar5Respira(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_SEXO.getItem())) {
						v = registro.VAR_PARTO_RN_SEXO;
						v.setValor(campo.getDato());
						registro.setSexo(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_TALLA.getItem())) {
						v = registro.VAR_PARTO_RN_TALLA;
						v.setValor(campo.getDato());
						registro.setTalla(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_TONO1.getItem())) {
						v = registro.VAR_PARTO_RN_TONO1;
						v.setValor(campo.getDato());
						registro.setApgar1Tono(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_TONO10.getItem())) {
						v = registro.VAR_PARTO_RN_TONO10;
						v.setValor(campo.getDato());
						registro.setApgar10Tono(v);
					} else if (campo.getItem().equals(registro.VAR_PARTO_RN_TONO5.getItem())) {
						v = registro.VAR_PARTO_RN_TONO5;
						v.setValor(campo.getDato());
						registro.setApgar5Tono(v);
					}
				}
				return registro;
			}

			else if (id.equals(RegistroOncoValoracion.PLANTILLLA_EDITOR_ONCO_VALORACION)) {
				RegistroOncoValoracion registro = new RegistroOncoValoracion();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALERGIA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALERGIA;
						v.setValor(campo.getDato());
						registro.setAlergias(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIALTERAGUSTO.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIALTERAGUSTO;
						v.setValor(campo.getDato());
						registro.setAliAteraGusto(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIDDIETAHA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIDDIETAHA;
						v.setValor(campo.getDato());
						registro.setAliDietaHabitual(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIDIFICULDEGLU.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIDIFICULDEGLU;
						v.setValor(campo.getDato());
						registro.setAliDificultadDeglu(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIINGESLIQUIDOS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIINGESLIQUIDOS;
						v.setValor(campo.getDato());
						registro.setAliIngestaLiquidos(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALINAUSEAS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALINAUSEAS;
						v.setValor(campo.getDato());
						registro.setAliNauseas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIOTROS;
						v.setValor(campo.getDato());
						registro.setAliOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALISUPLEMENTO.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALISUPLEMENTO;
						v.setValor(campo.getDato());
						registro.setAliSuplemento(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ALIVOMITOS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ALIVOMITOS;
						v.setValor(campo.getDato());
						registro.setAliVomitos(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ANTECEDENTES.getItem())) {
						v = registro.VAR_ONCO_VALORA_ANTECEDENTES;
						v.setValor(campo.getDato());
						registro.setAntecedentes(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_APRECOMPREDE.getItem())) {
						v = registro.VAR_ONCO_VALORA_APRECOMPREDE;
						v.setValor(campo.getDato());
						registro.setAprenComprende(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_APREDEMANDA.getItem())) {
						v = registro.VAR_ONCO_VALORA_APREDEMANDA;
						v.setValor(campo.getDato());
						registro.setAprenDemanda(v);
					}

					else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_CICLOS.getItem())) {
						v = registro.VAR_ONCO_VALORA_CICLOS;
						v.setValor(campo.getDato());
						registro.setCliclos(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUACOPA.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUACOPA;
						v.setValor(campo.getDato());
						registro.setComuAcompanado(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUCASEXU.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUCASEXU;
						v.setValor(campo.getDato());
						registro.setComuCambioSexual(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUCOLABORA.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUCOLABORA;
						v.setValor(campo.getDato());
						registro.setComuColabora(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUCONSPI.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUCONSPI;
						v.setValor(campo.getDato());
						registro.setComuConspiraSilen(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUOTRACOMPA.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUOTRACOMPA;
						v.setValor(campo.getDato());
						registro.setComuOtraCompa(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMUOTRO.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMUOTRO;
						v.setValor(campo.getDato());
						registro.setComuOtro(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMURELFAM.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMURELFAM;
						v.setValor(campo.getDato());
						registro.setComuRelacionFam(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_COMURELPER.getItem())) {
						v = registro.VAR_ONCO_VALORA_COMURELPER;
						v.setValor(campo.getDato());
						registro.setComuRelacionPer(v);
					}

					else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_DIAGNOSTICO.getItem())) {
						v = registro.VAR_ONCO_VALORA_DIAGNOSTICO;
						v.setValor(campo.getDato());
						registro.setDiagnostico(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_DOLOREVA.getItem())) {
						v = registro.VAR_ONCO_VALORA_DOLOREVA;
						v.setValor(campo.getDato());
						registro.setDolorEva(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_DOLORLOCA.getItem())) {
						v = registro.VAR_ONCO_VALORA_DOLORLOCA;
						v.setValor(campo.getDato());
						registro.setDolorLocalizacion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIFECALDIARREA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIFECALDIARREA;
						v.setValor(campo.getDato());
						registro.setElifecalDiarrea(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIFECALESTRENIMIENTO.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIFECALESTRENIMIENTO;
						v.setValor(campo.getDato());
						registro.setElifecalEstrenimiento(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIFECALOSTOMIA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIFECALOSTOMIA;
						v.setValor(campo.getDato());
						registro.setElifecalOstomia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIFECALOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIFECALOTROS;
						v.setValor(campo.getDato());
						registro.setElifecalOtro(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURICANTIDAD.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURICANTIDAD;
						v.setValor(campo.getDato());
						registro.setEliuriCantidad(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURICOLOR.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURICOLOR;
						v.setValor(campo.getDato());
						registro.setEliuriColor(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURIESCOZOR.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURIESCOZOR;
						v.setValor(campo.getDato());
						registro.setEliuriEscozor(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURINEFROSTOMIA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURINEFROSTOMIA;
						v.setValor(campo.getDato());
						registro.setEliuriNefrostomia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURIOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURIOTROS;
						v.setValor(campo.getDato());
						registro.setEliuriOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURISV.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURISV;
						v.setValor(campo.getDato());
						registro.setEliuriPortadorSV(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_ELIURIUROSTOMIA.getItem())) {
						v = registro.VAR_ONCO_VALORA_ELIURIUROSTOMIA;
						v.setValor(campo.getDato());
						registro.setEliuriUrostomia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_EMOCIANSIEDAD.getItem())) {
						v = registro.VAR_ONCO_VALORA_EMOCIANSIEDAD;
						v.setValor(campo.getDato());
						registro.setEmocioAnsiedad(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_EMOCIDEPRE.getItem())) {
						v = registro.VAR_ONCO_VALORA_EMOCIDEPRE;
						v.setValor(campo.getDato());
						registro.setEmocioDepresion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_EMOCIPAD_T.getItem())) {
						v = registro.VAR_ONCO_VALORA_EMOCIPAD_T;
						v.setValor(campo.getDato());
						registro.setEmocioPAD_T(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_FFIN.getItem())) {
						v = registro.VAR_ONCO_VALORA_FFIN;
						v.setValor(campo.getDato());
						registro.setFechaFin(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_FINICIO.getItem())) {
						v = registro.VAR_ONCO_VALORA_FINICIO;
						v.setValor(campo.getDato());
						registro.setFechaInicio(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_MOVILIZA.getItem())) {
						v = registro.VAR_ONCO_VALORA_MOVILIZA;
						v.setValor(campo.getDato());
						registro.setMovilizacion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_MOVILIZAKARNOFSKY.getItem())) {
						v = registro.VAR_ONCO_VALORA_MOVILIZAKARNOFSKY;
						v.setValor(campo.getDato());
						registro.setKarnofsky(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_MOVILIZAOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_MOVILIZAOTROS;
						v.setValor(campo.getDato());
						registro.setMovilizacionOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPALOPE.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPALOPE;
						v.setValor(campo.getDato());
						registro.setPercepAlopecia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPAUTOESTIMA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPAUTOESTIMA;
						v.setValor(campo.getDato());
						registro.setPercepAutoestima(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPCOLOST.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPCOLOST;
						v.setValor(campo.getDato());
						registro.setPercepColostomia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPIMAGEN.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPIMAGEN;
						v.setValor(campo.getDato());
						registro.setPercepImagen(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPMASTEC.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPMASTEC;
						v.setValor(campo.getDato());
						registro.setPercepMastectomia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PERCEPOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_PERCEPOTROS;
						v.setValor(campo.getDato());
						registro.setPercepOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELEDEMAS.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELEDEMAS;
						v.setValor(campo.getDato());
						registro.setPielEdemas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELERITEMA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELERITEMA;
						v.setValor(campo.getDato());
						registro.setPielEritema(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELFLEBITIS.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELFLEBITIS;
						v.setValor(campo.getDato());
						registro.setPielFlebitis(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELHEMATOMA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELHEMATOMA;
						v.setValor(campo.getDato());
						registro.setPielHematomas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELHERIDA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELHERIDA;
						v.setValor(campo.getDato());
						registro.setPielHeridas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELHIPERSENSI.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELHIPERSENSI;
						v.setValor(campo.getDato());
						registro.setPielHeridas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELICTERICIA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELICTERICIA;
						v.setValor(campo.getDato());
						registro.setPielHeridas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELNOALTERA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELNOALTERA;
						v.setValor(campo.getDato());
						registro.setPielNoAlteracion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELOTROS;
						v.setValor(campo.getDato());
						registro.setPielOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELPALIDEZ.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELPALIDEZ;
						v.setValor(campo.getDato());
						registro.setPielPalidez(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELPRURITO.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELPRURITO;
						v.setValor(campo.getDato());
						registro.setPielPrurito(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_PIELSECA.getItem())) {
						v = registro.VAR_ONCO_VALORA_PIELSECA;
						v.setValor(campo.getDato());
						registro.setPielSeca(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_RESPIDISNEA.getItem())) {
						v = registro.VAR_ONCO_VALORA_RESPIDISNEA;
						v.setValor(campo.getDato());
						registro.setRespiNoAlteracion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_RESPIDISNEA.getItem())) {
						v = registro.VAR_ONCO_VALORA_RESPIDISNEA;
						v.setValor(campo.getDato());
						registro.setRespidisnea(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_RESPIOXIGENO.getItem())) {
						v = registro.VAR_ONCO_VALORA_RESPIOXIGENO;
						v.setValor(campo.getDato());
						registro.setRespiOxigeno(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_RESPIOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_RESPIOTROS;
						v.setValor(campo.getDato());
						registro.setRespiOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_REPOSUEINFUSIONES.getItem())) {
						v = registro.VAR_ONCO_VALORA_REPOSUEINFUSIONES;
						v.setValor(campo.getDato());
						registro.setRepoSueInfusiones(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_REPOSUEMEDICACION.getItem())) {
						v = registro.VAR_ONCO_VALORA_REPOSUEMEDICACION;
						v.setValor(campo.getDato());
						registro.setRepoSueMedicacion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_REPOSUENO.getItem())) {
						v = registro.VAR_ONCO_VALORA_REPOSUENO;
						v.setValor(campo.getDato());
						registro.setRepoSueno(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_REPOSUEOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_REPOSUEOTROS;
						v.setValor(campo.getDato());
						registro.setRepoSueOtros(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_SEGACCESO.getItem())) {
						v = registro.VAR_ONCO_VALORA_SEGACCESO;
						v.setValor(campo.getDato());
						registro.setSegAccesoTipo(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_SEGACCESOLOCA.getItem())) {
						v = registro.VAR_ONCO_VALORA_SEGACCESOLOCA;
						v.setValor(campo.getDato());
						registro.setSegAccesoLoca(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_SEGACCESOPERME.getItem())) {
						v = registro.VAR_ONCO_VALORA_SEGACCESOPERME;
						v.setValor(campo.getDato());
						registro.setSegAccesoPermeable(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_SITSOCIALFAMILIAR.getItem())) {
						v = registro.VAR_ONCO_VALORA_SITSOCIALFAMILIAR;
						v.setValor(campo.getDato());
						registro.setSitFamiliarSocial(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_TELEFONO.getItem())) {
						v = registro.VAR_ONCO_VALORA_TELEFONO;
						v.setValor(campo.getDato());
						registro.setTelefono(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_TRATAMIENTOQT.getItem())) {
						v = registro.VAR_ONCO_VALORA_TRATAMIENTOQT;
						v.setValor(campo.getDato());
						registro.setTratamientoqt(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_VESTIDO.getItem())) {
						v = registro.VAR_ONCO_VALORA_VESTIDO;
						v.setValor(campo.getDato());
						registro.setVestidoAseo(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_VALORA_VESTIDOOTROS.getItem())) {
						v = registro.VAR_ONCO_VALORA_VESTIDOOTROS;
						v.setValor(campo.getDato());
						registro.setVestidoAseoOtros(v);
					}

				}
				return registro;
			} else if (id.equals(RegistroOncoEvolutivo.PLANTILLLA_EDITOR_ONCO_EVOLUTI)) {
				RegistroOncoEvolutivo registro = new RegistroOncoEvolutivo();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_ONCO_EVOL_OBSER.getItem())) {
						v = registro.VAR_ONCO_EVOL_OBSER;
						v.setValor(campo.getDato());
						registro.setObservaciones(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroOncoCuidados.PLANTILLLA_EDITOR_ONCO_CUIDA)) {
				RegistroOncoCuidados registro = new RegistroOncoCuidados();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_ACCESO.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_ACCESO;
						v.setValor(campo.getDato());
						registro.setAccesoVenoso(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_ACCESOLOCA.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_ACCESOLOCA;
						v.setValor(campo.getDato());
						registro.setAccesoLocalizacion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_ALERGIA.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_ALERGIA;
						v.setValor(campo.getDato());
						registro.setReaccAlergica(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_EFECTOSADVERSO.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_EFECTOSADVERSO;
						v.setValor(campo.getDato());
						registro.setInfoPaEfectos(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_EXTRACCION.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_EXTRACCION;
						v.setValor(campo.getDato());
						registro.setAccesoExtraccion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_EXTRADESCRI.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_EXTRADESCRI;
						v.setValor(campo.getDato());
						registro.setAccesoExtraDescri(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_EXTRASVA.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_EXTRASVA;
						v.setValor(campo.getDato());
						registro.setExtravasacion(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_FC.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_FC;
						v.setValor(campo.getDato());
						registro.setFreCardiaca(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_FLEBI.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_FLEBI;
						v.setValor(campo.getDato());
						registro.setFlebitis(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_GLU.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_GLU;
						v.setValor(campo.getDato());
						registro.setGlucosa(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_INFOPROCE.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_INFOPROCE;
						v.setValor(campo.getDato());
						registro.setInfoPaProcedimiento(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_NAUSEAS.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_NAUSEAS;
						v.setValor(campo.getDato());
						registro.setNauseas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_OBSERVACIONES.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_OBSERVACIONES;
						v.setValor(campo.getDato());
						registro.setObservaciones(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_OBSTRUC.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_OBSTRUC;
						v.setValor(campo.getDato());
						registro.setObstruccionCate(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_OTRASCOMPL.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_OTRASCOMPL;
						v.setValor(campo.getDato());
						registro.setOtrasComplica(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_SAT02.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_SAT02;
						v.setValor(campo.getDato());
						registro.setSatOxigeno(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_SIGNOSALARMA.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_SIGNOSALARMA;
						v.setValor(campo.getDato());
						registro.setInfoPaSigAlarma(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_T.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_T;
						v.setValor(campo.getDato());
						registro.setTemperatura(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_TAD.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_TAD;
						v.setValor(campo.getDato());
						registro.setTemperatura(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_TAS.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_TAS;
						v.setValor(campo.getDato());
						registro.setTas(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_TTODOSIS.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_TTODOSIS;
						v.setValor(campo.getDato());
						registro.setTtoDosis(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_TTOHORAINI.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_TTOHORAINI;
						v.setValor(campo.getDato());
						registro.setTtoHoraini(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_TTOVIA.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_TTOVIA;
						v.setValor(campo.getDato());
						registro.setTtoVia(v);
					} else if (campo.getItem().equals(registro.VAR_ONCO_CUIDADOS_VOMITOS.getItem())) {
						v = registro.VAR_ONCO_CUIDADOS_VOMITOS;
						v.setValor(campo.getDato());
						registro.setVomitos(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroEvolutivoMedico.PLANTILLLA_EDITOR_EVOLUTIVOMEDICO)) {
				RegistroEvolutivoMedico registro = new RegistroEvolutivoMedico();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_EVOLUTIVO_SITACTU.getItem())) {
						v = registro.VAR_EVOLUTIVO_SITACTU;
						v.setValor(campo.getDato());
						registro.setSitActual(v);
					} else if (campo.getItem().equals(registro.VAR_EVOLUTIVO_TRATAMIENTO.getItem())) {
						v = registro.VAR_EVOLUTIVO_TRATAMIENTO;
						v.setValor(campo.getDato());
						registro.setTratamiento(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroQuiSegPre.PLANTILLLA_EDITOR_QUISEGPRE)) {
				RegistroQuiSegPre registro = new RegistroQuiSegPre();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_APARED.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_APARED;
						v.setValor(campo.getDato());
						registro.setAparatosred(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_ASPIRADOR.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_ASPIRADOR;
						v.setValor(campo.getDato());
						registro.setAspirador(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_AUXILIAR_.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_AUXILIAR_;
						v.setValor(campo.getDato());
						registro.setAuxiliar(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_BOLSAPRE.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_BOLSAPRE;
						v.setValor(campo.getDato());
						registro.setBolsapresion(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_FECHA.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_FECHA;
						v.setValor(campo.getDato());
						registro.setFechaCheck(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_HORA.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_HORA;
						v.setValor(campo.getDato());
						registro.setHoraCheck(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_INTUBA.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_INTUBA;
						v.setValor(campo.getDato());
						registro.setIntubacion(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PREQUI_MEDICA.getItem())) {
						v = registro.VAR_QUISEG_PREQUI_MEDICA;
						v.setValor(campo.getDato());
						registro.setMedicacion(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroQuiSegEntrada.PLANTILLLA_EDITOR_QUISEGENTRA)) {
				RegistroQuiSegEntrada registro = new RegistroQuiSegEntrada();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_ALERGIA.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_ALERGIA;
						v.setValor(campo.getDato());
						registro.setAlergia(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_ALERGIAOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_ALERGIAOKCORR;
						v.setValor(campo.getDato());
						registro.setAlergiaOkcorre(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_ANTIBIO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_ANTIBIO;
						v.setValor(campo.getDato());
						registro.setAntibioti60min(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_ANTIBIONOPRO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_ANTIBIO;
						v.setValor(campo.getDato());
						registro.setAntibiotiNoporce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_ANTIBIOOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_ANTIBIOOKCORR;
						v.setValor(campo.getDato());
						registro.setAlergiaOkcorre(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_HEMO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_HEMO;
						v.setValor(campo.getDato());
						registro.setHemoderiv(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_HEMONOPRO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_HEMONOPRO;
						v.setValor(campo.getDato());
						registro.setHemoderivNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_HEMOOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_HEMOOKCORR;
						v.setValor(campo.getDato());
						registro.setAntibiotiOkcorre(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_IDENTIDAD.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_IDENTIDAD;
						v.setValor(campo.getDato());
						registro.setIdentidad(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INPLAN.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INPLAN;
						v.setValor(campo.getDato());
						registro.setImplante(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INPLANNOPRO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INPLANNOPRO;
						v.setValor(campo.getDato());
						registro.setImplanteNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INPLANOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INPLANOKCORR;
						v.setValor(campo.getDato());
						registro.setImplanteOkcorre(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INTU.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INTU;
						v.setValor(campo.getDato());
						registro.setIntuDificil(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INTUNOPROC.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INTUNOPROC;
						v.setValor(campo.getDato());
						registro.setIntuDificilNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INTUPULXI.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INTUPULXI;
						v.setValor(campo.getDato());
						registro.setPulsioxi(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_INTURIESASPI.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_INTURIESASPI;
						v.setValor(campo.getDato());
						registro.setRiesgoAspira(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_LOCALI.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_LOCALI;
						v.setValor(campo.getDato());
						registro.setLocalizacion(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_LOCALIDANOPRO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_LOCALIDANOPRO;
						v.setValor(campo.getDato());
						registro.setLocalizacionNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_LOCALIOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_LOCALIOKCORR;
						v.setValor(campo.getDato());
						registro.setLocalizacionOkcorr(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_OBSSER.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_OBSSER;
						v.setValor(campo.getDato());
						registro.setObservaciones(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_PROCEDIMIENTO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_PROCEDIMIENTO;
						v.setValor(campo.getDato());
						registro.setProcedimiento(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_TEVTVP.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_TEVTVP;
						v.setValor(campo.getDato());
						registro.setTep_tvp(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_TEVTVPNOPRO.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_TEVTVPNOPRO;
						v.setValor(campo.getDato());
						registro.setTep_tvpNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_ENTRADA_TEVTVPOKCORR.getItem())) {
						v = registro.VAR_QUISEG_ENTRADA_TEVTVPOKCORR;
						v.setValor(campo.getDato());
						registro.setTep_tvpOkcorre(v);
					}
				}
				return registro;
			} else if (id.equals(RegistroQuiSegPausa.PLANTILLLA_EDITOR_QUISEGPAUSA)) {
				RegistroQuiSegPausa registro = new RegistroQuiSegPausa();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_EQUIPO.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_EQUIPO;
						v.setValor(campo.getDato());
						registro.setEquipos(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_EQUIPOOKCORR.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_EQUIPO;
						v.setValor(campo.getDato());
						registro.setEquiposOkCoree(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_GLUCE.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_GLUCE;
						v.setValor(campo.getDato());
						registro.setGlucema(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_GLUNOPRO.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_GLUNOPRO;
						v.setValor(campo.getDato());
						registro.setGlucemaNoProce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_HIPONOPROCE.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_HIPONOPROCE;
						v.setValor(campo.getDato());
						registro.setHiptermiaNoProce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_HIPOTER.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_HIPOTER;
						v.setValor(campo.getDato());
						registro.setHiptermia(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_IMAGENES.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_IMAGENES;
						v.setValor(campo.getDato());
						registro.setImagenes(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_INSTRU.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_INSTRU;
						v.setValor(campo.getDato());
						registro.setInstrumental(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_INTRUOKCORR.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_INTRUOKCORR;
						v.setValor(campo.getDato());
						registro.setInstrumentalOkcorre(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_OBSERVA.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_OBSERVA;
						v.setValor(campo.getDato());
						registro.setObservaciones(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_PASOCRIANES.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_PASOCRIANES;
						v.setValor(campo.getDato());
						registro.setPasosCriticosAnes(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_PASOSCRICIR.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_PASOSCRICIR;
						v.setValor(campo.getDato());
						registro.setPasosCriticos(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_PROCEPOS.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_PROCEPOS;
						v.setValor(campo.getDato());
						registro.setProcePosicion(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_PAUSA_VIAAERE.getItem())) {
						v = registro.VAR_QUISEG_PAUSA_VIAAERE;
						v.setValor(campo.getDato());
						registro.setViaAerea(v);
					}

				}
				return registro;
			} else if (id.equals(RegistroQuiSegSalida.PLANTILLLA_EDITOR_QUISEGSALIDA)) {
				RegistroQuiSegSalida registro = new RegistroQuiSegSalida();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				registro.setEpisodio(new EpisodioDAO().getRegistroPorId(resuSulset.getLong("episodio"),
						registro.getPaciente(), null, null, null, null, null));
				for (Campos_r campo : registro.getListaCampos()) {
					Variable v = new Variable();
					if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_ANTIBIO.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_ANTIBIO;
						v.setValor(campo.getDato());
						registro.setProfiAntibi(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_ANTIBIONOPROCE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_ANTIBIONOPROCE;
						v.setValor(campo.getDato());
						registro.setProfiAntibioNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_CONTAJE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_CONTAJE;
						v.setValor(campo.getDato());
						registro.setContaje(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_FINCHECK.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_FINCHECK;
						v.setValor(campo.getDato());
						registro.setCheckFinalizado(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_GLUCEMIA.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_GLUCEMIA;
						v.setValor(campo.getDato());
						registro.setGlucemiaRealizado(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_GLUCEMIANOPROCE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_GLUCEMIANOPROCE;
						v.setValor(campo.getDato());
						registro.setGlucemiaNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_HIPOTER.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_HIPOTER;
						v.setValor(campo.getDato());
						registro.setHipotermiaRealizado(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_HIPOTERNOPROCE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_HIPOTERNOPROCE;
						v.setValor(campo.getDato());
						registro.setHipotermiaNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_IDENMUESTR.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_IDENMUESTR;
						v.setValor(campo.getDato());
						registro.setIdentiMuestras(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_IDENMUESTRNOPROCE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_IDENMUESTRNOPROCE;
						v.setValor(campo.getDato());
						registro.setIdentiMuestrasNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_NOMBREPRO.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_NOMBREPRO;
						v.setValor(campo.getDato());
						registro.setNombreProce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_OBSERVA.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_OBSERVA;
						v.setValor(campo.getDato());
						registro.setObservaciones(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_PASOSCRIT.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_PASOSCRIT;
						v.setValor(campo.getDato());
						registro.setPasosCritRecu(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_TROBONOPROCE.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_TROBONOPROCE;
						v.setValor(campo.getDato());
						registro.setProfiTromboNoproce(v);
					} else if (campo.getItem().equals(registro.VAR_QUISEG_SALIDA_TROMBO.getItem())) {
						v = registro.VAR_QUISEG_SALIDA_TROMBO;
						v.setValor(campo.getDato());
						registro.setProfiTrombo(v);
					}
				}
				return registro;
			} else {
				Registro registro = new Registro();
				registro.setId(resuSulset.getLong("id"));
				registro.setDescripcion(resuSulset.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(resuSulset.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(resuSulset.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(resuSulset.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(resuSulset.getLong("fecha")));
				registro.setHora(resuSulset.getLong("hora"));
				registro.setEstado(resuSulset.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(resuSulset.getString("userid"), false));
				registro.setProblema(new ProcesoDAO().getRegistroId(resuSulset.getLong("problema"), null));
				registro.setUseridredactor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setUseridtranscriptor(new Usuario(resuSulset.getString("useridredactor")));
				registro.setTiporegistro(resuSulset.getLong("tiporegistro"));
				registro.setCanal(resuSulset.getLong("canal"));
				registro.setListaCampos(getListaCamposRegistro(registro.getId()));
				return registro;
			}
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return null;
	}

	public Registro getRegistroPacientePlantilla(Paciente paciente, Long plantilla_editor) {
		Connection connection = null;
		Registro registro = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM registros  WHERE paciente = ?  AND estado=? AND plantilla_editor=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				return getRegistroResulset(resuSulset);
			}
			statement.close();
			logger.debug("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND plantilla_editor=plantilla_editor");
		} catch (SQLException e) {
			logger.error("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND plantilla_editor=plantilla_editor");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return registro;
	}

	/**
	 * Gets the registro paciente proceso.
	 *
	 * @param paciente         the paciente
	 * @param proceso          the proceso
	 * @param plantilla_editor the plantilla editor
	 * @return the registro paciente proceso
	 */
	public Long getRegistroPacienteProceso(Paciente paciente, Proceso proceso, Long plantilla_editor) {
		Connection connection = null;
		Long id = new Long(0);
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT id FROM registros  WHERE paciente = ?  AND estado=? AND problema= ? AND plantilla_editor=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, proceso.getId());
			statement.setLong(4, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				id = resuSulset.getLong("id");
			}
			statement.close();
			logger.debug("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=plantilla_editor");
		} catch (SQLException e) {
			logger.error("SELECT id FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=plantilla_editor");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return id;
	}

	public Registro getLisRegistro(Paciente paciente, Proceso proceso, Long plantilla_editor) {
		Connection connection = null;
		Registro registro = null;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM registros  WHERE paciente = ?  AND estado=? AND problema= ? AND plantilla_editor=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente.getId());
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, proceso.getId());
			statement.setLong(4, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				registro = getRegistroResulset(resuSulset);
			}
			statement.close();
			logger.debug("SELECT * FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=" + plantilla_editor);
		} catch (SQLException e) {
			logger.error("SELECT * FROM registros  WHERE paciente = " + paciente.getId() + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND problema= " + proceso.getId()
					+ " AND plantilla_editor=" + plantilla_editor);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return registro;
	}

	/**
	 * Gets the registro paciente proceso O 2. Recupera un registro de tipo
	 * oxigenoterapia del subtipo plantilla_editor que no este cerrado, es decir que
	 * no tenga valor en campos_r la variable fecha fin
	 *
	 * @param paciente         the paciente
	 * @param proceso          the proceso
	 * @param plantilla_editor the plantilla editor
	 * @return the registro paciente proceso O 2
	 */
	public Long getRegistroPacienteProcesoO2(Paciente paciente, Proceso proceso, Long plantilla_editor) {
		Connection connection = null;
		Long id = new Long(0);
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=? AND c.item=?"
					+ "	WHERE r.paciente = ?  AND r.estado=? AND r.problema= ? " + " AND plantilla_editor=?"
					+ " AND ( dato=0 OR dato  IS null )";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(2, RegistroOxi.ITEM_FECHAFIN);
			statement.setLong(3, paciente.getId());
			statement.setInt(4, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(5, proceso.getId());
			statement.setLong(6, plantilla_editor);
			ResultSet resuSulset = statement.executeQuery();
			if (resuSulset.next()) {
				id = resuSulset.getLong("id");
			}
			statement.close();
			logger.debug("SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=" + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " AND c.item=" + RegistroOxi.ITEM_FECHAFIN + "" + "	WHERE r.paciente = " + paciente.getId()
					+ "  AND r.estado=? AND r.problema= " + proceso.getId() + " " + " AND plantilla_editor="
					+ plantilla_editor + "" + " AND ( dato=0 OR dato  IS null )");
		} catch (SQLException e) {
			logger.error("SELECT r.id ,c.dato FROM registros  r   "
					+ " LEFT JOIN campos_r c ON r.id=c.registro AND c.estado=" + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " AND c.item=" + RegistroOxi.ITEM_FECHAFIN + "" + "	WHERE r.paciente = " + paciente.getId()
					+ "  AND r.estado=? AND r.problema= " + proceso.getId() + " " + " AND plantilla_editor="
					+ plantilla_editor + "" + " AND ( dato=0 OR dato  IS null )");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return id;
	}

	/**
	 * Gets the lista campos registro.
	 *
	 * @param id the id
	 * @return the lista campos registro
	 */
	public ArrayList<Campos_r> getListaCamposRegistro(Long id) {
		Connection connection = null;
		ArrayList<Campos_r> listaCampos = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT *  FROM campos_r  WHERE registro = ?  AND estado=?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resuSulset = statement.executeQuery();
			while (resuSulset.next()) {
				Campos_r campo_r = getCamposRResulSet(resuSulset);
				listaCampos.add(campo_r);
			}
			statement.close();
			logger.debug("SELECT *  FROM campos_r  WHERE registro = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
		} catch (SQLException e) {
			logger.error("SELECT *  FROM campos_r  WHERE registro = " + id + "  AND estado="
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaCampos;
	}

	/**
	 * Gets the registro resul set.
	 *
	 * @param rsRegistro the rs registro
	 * @return the registro resul set
	 */
	public Registro getRegistroResulSet(ResultSet rsRegistro) {

		Long id = null;
		try {
			id = rsRegistro.getLong("plantilla_editor");
			if (id.equals(RegistroMama.PLANTILLLA_EDITOR_REGISTROMAMA)) {
				RegistroMama registro = new RegistroMama();
				registro.setId(rsRegistro.getLong("id"));
				registro.setDescripcion(rsRegistro.getString("descripcion"));
				registro.setPaciente(new PacienteDAO().getPacientePorId(rsRegistro.getLong("paciente")));
				registro.setCentro(new CentroDAO().getRegistroId(rsRegistro.getLong("centro")));
				registro.setServicio(new ServiciosDAO().getRegistroId(rsRegistro.getLong("servicio")));
				registro.setFecha(Utilidades.getFechaLocalDate(rsRegistro.getLong("fecha")));
				registro.setHora(rsRegistro.getLong("hora"));
				registro.setEstado(rsRegistro.getInt("estado"));
				registro.setUserid(new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("userid"), false));
				if (rsRegistro.getLong("problema") > 0) {
					registro.setProblema(new ProcesoDAO().getRegistroId(rsRegistro.getLong("problema"), null));
				}
				if (rsRegistro.getLong("episodio") > 0) {
					registro.setEpisodio(new EpisodioDAO().getRegistroPorId(rsRegistro.getLong("episodio"),
							registro.getPaciente(), null, null, null, null, null));
					// registro.setEpisodio(new
					// EpisodioDAO().getRegistroPorId(rsRegistro.getLong("episodio")));
				}
				registro.setUseridredactor(
						new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("useridredactor"), false));
				registro.setUseridtranscriptor(
						new UsuarioDAO().getUsuarioUserid(rsRegistro.getString("useridtranscriptor"), false));
				registro.setTiporegistro(rsRegistro.getLong("tiporegistro"));
				registro.setCanal(rsRegistro.getLong("canal"));
				return registro;
			}
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return null;
	}

	/**
	 * Gets the campos R resul set.
	 *
	 * @param rsRegistro the rs registro
	 * @return the campos R resul set
	 */
	public Campos_r getCamposRResulSet(ResultSet rsRegistro) {
		Campos_r campo = new Campos_r();
		try {
			campo.setId(rsRegistro.getLong("id"));
			campo.setRegistro(rsRegistro.getLong("registro"));
			campo.setItem(rsRegistro.getLong("item"));
			campo.setUserid(rsRegistro.getString("userid"));
			campo.setFecha(rsRegistro.getLong("fecha"));
			campo.setHora(rsRegistro.getLong("hora"));
			campo.setDescripcion(rsRegistro.getString("descripcion"));
			campo.setOrden(rsRegistro.getInt("orden"));
			campo.setUnidades(rsRegistro.getString("unidades"));
			campo.setFlags(rsRegistro.getString("flags"));
			campo.setCodigo(rsRegistro.getString("codigo"));
			campo.setTipo_codigo(rsRegistro.getString("tipo_codigo"));
			campo.setEstado(rsRegistro.getInt("estado"));
			campo.setTipobin(rsRegistro.getInt("tipobin"));
			campo.setDato(rsRegistro.getString("dato"));
			campo.setReferencias(rsRegistro.getString("referencias"));
			campo.setDatobin(rsRegistro.getBlob("datobin"));
			campo.setLateralidad(rsRegistro.getLong("lateralidad"));
			campo.setCanal(rsRegistro.getLong("canal"));

		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}

		return campo;
	}

	/**
	 * Gets the lista registros.
	 *
	 * @param desde            the desde
	 * @param hasta            the hasta
	 * @param plantilla_editor the plantilla editor
	 * @return the lista registros
	 */
	public ArrayList<Registro> getListaRegistros(LocalDate desde, LocalDate hasta, Long plantilla_editor,
			Paciente paciente) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM registros WHERE   plantilla_editor=?  and estado = ? ";
			if (desde != null) {
				sql = sql.concat(" AND fecha>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
			}
			if (hasta != null) {
				sql = sql.concat(" AND hasta>=" + Utilidades.getFechaNumeroyyymmddDefecha(hasta));
			}
			if (paciente != null) {
				sql = sql.concat(" AND paciente=" + paciente.getId());
			}
			sql = sql.concat(" ORDER BY  fecha DESC, hora DESC ");
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, plantilla_editor);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug(sql + "  AND plantilla_editor=" + plantilla_editor + "  and estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " ORDER BY  fecha  ");
		} catch (SQLException e) {
			logger.error(sql + "   AND plantilla_editor=" + plantilla_editor + "  and estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " ORDER BY  fecha  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistros(LocalDate desde, LocalDate hasta, Long plantilla_editor) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM registros WHERE   plantilla_editor=?  and estado = ? ";
			if (desde != null) {
				sql = sql.concat(" AND fecha>=" + Utilidades.getFechaNumeroyyymmddDefecha(desde));
			}
			if (hasta != null) {
				sql = sql.concat(" AND hasta>=" + Utilidades.getFechaNumeroyyymmddDefecha(hasta));
			}
			sql = sql.concat(" ORDER BY  fecha ");
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, plantilla_editor);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug(sql + "  AND plantilla_editor=" + plantilla_editor + "  and estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " ORDER BY  fecha  ");
		} catch (SQLException e) {
			logger.error(sql + "   AND plantilla_editor=" + plantilla_editor + "  and estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " ORDER BY  fecha  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	/**
	 * Gets the lista registros.
	 *
	 * @param subambito the subambito
	 * @return the lista registros
	 */
	public ArrayList<Registro> getListaRegistros(Long idPaciente, Long subambito) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			if (subambito.equals(new Long(0)))
				sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
						+ "   WHERE   r.estado = ? AND r.paciente=?  " + "  ORDER BY  fecha DESC, hora DESC  ";
			else {
				sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
						+ "   WHERE   r.estado = ? AND r.paciente=?  " + " AND  p.subambito=" + subambito
						+ " ORDER BY  fecha DESC, hora DESC  ";
			}
			PreparedStatement statement = connection.prepareStatement(sql);

			statement.setInt(1, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(2, idPaciente);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=" + idPaciente
					+ " " + "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistros(Long subambito, Paciente paciente) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =? AND  r.estado = ? AND r.paciente=? "
					+ "ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, subambito);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=paciente.getId() "
					+ "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error("SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente=paciente.getId() "
					+ "ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public Registro getRegistroPacienteEpisodio(Paciente paciente, Episodio episodio, Long plantillaEditor) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r " + " JOIN episodios e ON e.id=r.episodio " + "   WHERE   e.id ="
					+ episodio.getId() + " AND  r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL
					+ " AND r.paciente= " + paciente.getId() + " AND  plantilla_editor=" + plantillaEditor
					+ " ORDER BY  fecha DESC, hora DESC  ";
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			if (resulSet.next()) {
				return getRegistroResulset(resulSet);
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return null;
	}

	public ArrayList<Registro> getListaRegistros(Proceso proceso, Long plantillaEditor) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*   FROM registros r WHERE  r.problema=" + proceso.getId() + " AND plantilla_editor= "
					+ plantillaEditor;
			PreparedStatement statement = connection.prepareStatement(sql);

			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistrosLight(Long subambito, Paciente paciente) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =? AND  r.estado = ? AND r.paciente=? "
					+ "ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, subambito);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				Registro registro = new Registro();
				registro.setId(resulSet.getLong("id"));
				registro.setProblema(new ProcesoDAO().getRegistroId(resulSet.getLong("problema"), null));
				registro.setDescripcion(resulSet.getString("descripcion"));
				registro.setPaciente(new Paciente(resulSet.getLong("paciente")));
				registro.setFecha(Utilidades.getFechaLocalDate(resulSet.getLong("fecha")));
				registro.setHora(resulSet.getLong("hora"));
				registro.setEstado(resulSet.getInt("estado"));
				registro.setUserid(new Usuario(resulSet.getString("userid")));
				registro.setProblema(new Proceso(resulSet.getLong("problema"), registro.getPaciente(), subambito));
				registro.setTiporegistro(resulSet.getLong("tiporegistro"));
				registro.setCanal(resulSet.getLong("canal"));
				registro.setPlantilla_edior(resulSet.getLong("plantilla_editor"));
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.info(sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= " + paciente.getId()
					+ "ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(sql = "SELECT r.*  " + " FROM registros r " + " JOIN problemas p ON p.id=r.problema "
					+ "   WHERE   p.subambito =" + subambito + " AND  r.estado = "
					+ Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= " + paciente.getId()
					+ "ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public ArrayList<Registro> getListaRegistrosPaciTipoEpi(Paciente paciente, Long tipoRegistro, Long episodio) {
		Connection connection = null;
		ArrayList<Registro> listaRegistros = new ArrayList<>();

		try {
			connection = super.getConexionBBDD();
			sql = "SELECT r.*  " + " FROM registros r   WHERE tiporegistro=? " + "AND  r.estado = ? AND r.paciente=? "
					+ " AND episodio = ?" + " ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, tipoRegistro);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			statement.setLong(4, episodio);
			ResultSet resulSet = statement.executeQuery();
			while (resulSet.next()) {
				Registro registro = new Registro();
				registro.setId(resulSet.getLong("id"));
				registro.setProblema(new ProcesoDAO().getRegistroId(resulSet.getLong("problema"), paciente));
				registro.setDescripcion(resulSet.getString("descripcion"));
				registro.setPaciente(new Paciente(resulSet.getLong("paciente")));
				registro.setFecha(Utilidades.getFechaLocalDate(resulSet.getLong("fecha")));
				registro.setHora(resulSet.getLong("hora"));
				registro.setEstado(resulSet.getInt("estado"));
				registro.setUserid(new Usuario(resulSet.getString("userid")));
				// registro.setProblema(new Proceso(resulSet.getLong("problema"),
				// registro.getPaciente(), subambito));
				registro.setTiporegistro(resulSet.getLong("tiporegistro"));
				registro.setCanal(resulSet.getLong("canal"));
				registro.setPlantilla_edior(resulSet.getLong("plantilla_editor"));
				listaRegistros.add(getRegistroResulset(resulSet));
			}
			statement.close();
			logger.info(sql = "SELECT r.*   FROM registros r     " + "   WHERE   tiporegistro= " + tipoRegistro
					+ " AND  r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= "
					+ paciente.getId() + " ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(sql = "SELECT r.*   FROM registros r     " + "   WHERE   tiporegistro= " + tipoRegistro
					+ " AND  r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= "
					+ paciente.getId() + " ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaRegistros;
	}

	public String getTextoListaRegistrosPaciTipoEpi(Paciente paciente, Long tipoRegistro, Long episodio) {
		Connection connection = null;
		String texto = "";
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT fecha, hora   FROM registros r   WHERE tiporegistro=? "
					+ "AND  r.estado = ? AND r.paciente=? " + " AND episodio = ?"
					+ " ORDER BY  fecha DESC, hora DESC  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, tipoRegistro);
			statement.setInt(2, Registro.VAR_RESGISTRO_ESTADO_NORMAL);
			statement.setLong(3, paciente.getId());
			statement.setLong(4, episodio);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				texto = Utilidades.getFechFormatoayyyymmdd(resulSet.getLong("fecha"), Constantes.SEPARADOR_FECHA) + " "
						+ Utilidades.getHoraHH_MM(resulSet.getLong("hora"));
				int total = 0;
				while (resulSet.next()) {
					total++;
				}
				texto = "Evolutivos " + total + " " + texto;
			} else {
				texto = " Sin evolutivos para  episodio ";
			}
			statement.close();
			logger.info(sql = "SELECT count(*)    FROM registros r     " + "   WHERE   tiporegistro= " + tipoRegistro
					+ " AND  r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= "
					+ paciente.getId() + " ORDER BY  fecha DESC, hora DESC  ");
		} catch (SQLException e) {
			logger.error(sql = "SELECT count(*)  FROM registros r     " + "   WHERE   tiporegistro= " + tipoRegistro
					+ " AND  r.estado = " + Registro.VAR_RESGISTRO_ESTADO_NORMAL + " AND r.paciente= "
					+ paciente.getId() + " ORDER BY  fecha DESC, hora DESC  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return texto;
	}

	/**
	 * Gets the referencias externas.
	 *
	 * @param id the id
	 * @return the referencias externas
	 */
	@Override
	public boolean getReferenciasExternas(Long id) {
		return false;
	}

	/**
	 * Graba datos.
	 *
	 * @param object the object
	 * @return true, if successful
	 */
	@Override
	public boolean grabaDatos(Object object) {
		return false;
	}

	/**
	 * Actualiza datos.
	 *
	 * @param mensajeparam the mensajeparam
	 * @return true, if successful
	 */
	@Override
	public boolean actualizaDatos(Object mensajeparam) {
		return false;
	}

	/**
	 * Inserta datos.
	 *
	 * @param mensajeparam the mensajeparam
	 * @return true, if successful
	 */
	@Override
	public boolean insertaDatos(Object mensajeparam) {
		return false;
	}

	/**
	 * Gets the registro id.
	 *
	 * @param id the id
	 * @return the registro id
	 */
	@Override
	public Object getRegistroId(Long id) {
		return null;
	}

	/**
	 * Gets the sql where.
	 *
	 * @param cadena the cadena
	 * @return the sql where
	 */
	@Override
	public String getSqlWhere(String cadena) {
		return null;
	}

	/**
	 * Borra datos.
	 *
	 * @param objeto the objeto
	 * @return true, if successful
	 */
	@Override
	public boolean borraDatos(Object objeto) {
		return false;
	}
}