package com.jnieto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FarmaciaDAO {
	private static final Logger logger = LogManager.getLogger(GacelaDAO.class);

	Connection conn = null;

	public FarmaciaDAO() {
	}

	public Connection conecta() {
		Connection conn = null;

		String dbURL2 = "jdbc:oracle:thin:@10.36.64.161:1525/exhnss01";
		String username = "USR_FARM";
		String password = "AVILA2009";
		String sql;
		try {
			// registers Oracle JDBC driver - though this is no longer required
			// since JDBC 4.0, but added here for backward compatibility
			Class.forName("oracle.jdbc.OracleDriver");
			conn = DriverManager.getConnection(dbURL2, username, password);
		} catch (ClassNotFoundException ex) {
			logger.error("Error conexion his, clase no ecntranda.", ex);
		} catch (SQLException ex) {
			logger.error("Error conexion his, sql ", ex);
		}
		return conn;
	}

}
