package com.jnieto.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Cama;
import com.jnieto.entity.Zona;
import com.jnieto.ui.NotificacionInfo;

public class CamaDAO extends ConexionDAO {

	private static final Logger logger = LogManager.getLogger(CamaDAO.class);

	private Cama cama;

	/**
	 * Instantiates a new accesos DAO.
	 */
	public CamaDAO() {
		super();
	}

//	@Override
	public Cama getRegistroResulset(ResultSet resulSet, Zona zona) {
		cama = new Cama();
		try {
			cama.setId(resulSet.getLong("id"));
			if (zona == null)
				cama.setZona(new ZonaDAO().getRegistrPorId(resulSet.getLong("id")));
			else
				cama.setZona(zona);

			cama.setCama(resulSet.getString("cama"));
			logger.debug("Acceso resulset " + resulSet.toString());
			return cama;
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return cama;
	}

	public Cama getRegistrPorId(Long id, Zona zona) {
		Connection connection = null;
		cama = new Cama();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM camas WHERE ID=" + id;

			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			if (resulSet.next()) {
				cama = getRegistroResulset(resulSet, zona);
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return cama;
	}

	public ArrayList<Cama> getListaCamas(Zona zona) {
		Connection connection = null;
		ArrayList<Cama> listaCamas = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT * FROM camas WHERE estado!='B' AND  zona=" + zona.getId() + " ORDER BY cama ";

			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			while (resulSet.next()) {
				Cama cama = getRegistroResulset(resulSet, zona);
				listaCamas.add(cama);
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaCamas;
	}

}
