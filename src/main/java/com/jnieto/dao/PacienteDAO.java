package com.jnieto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.entity.Agenda;
import com.jnieto.entity.Cama;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Episodio;
import com.jnieto.entity.Historia;
import com.jnieto.entity.Paciente;
import com.jnieto.entity.PagiLisReg;
import com.jnieto.entity.Proceso;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Zona;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.Utilidades;

/**
 * The Class PacienteDAO.
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class PacienteDAO extends ConexionDAO implements InterfaceDAO {

	private Paciente paciente;

	private ResultSet resulSet;

	private static final Logger logger = LogManager.getLogger(PacienteDAO.class);

	/**
	 * Instantiates a new paciente DAO.
	 */
	public PacienteDAO() {
		super();
	}

	/**
	 * Gets the paciente resulset.
	 *
	 * @param resulSet the resul set
	 * @return the paciente resulset
	 */

	public Paciente getRegistroResulset(ResultSet resulSet, String tipo, Centro centro, Servicio servicio,
			Agenda agenda, Zona zona, Cama cama, Boolean completo) {
		try {
			paciente = new Paciente(resulSet.getLong("id"));
			paciente.setNombre(resulSet.getString("nombre"));
			paciente.setApellido1(resulSet.getString("ape1"));
			paciente.setApellido2(resulSet.getString("ape2"));
			paciente.setCodigopostal(resulSet.getString("codigopostal"));
			paciente.setDireccion(resulSet.getString("direccion"));
			paciente.setDni(resulSet.getString("dni"));
			paciente.setPais(resulSet.getString("pais"));
			Long fnacLong = resulSet.getLong("fnac");
			if (!fnacLong.equals(new Long(0))) {
				paciente.setFnac(Utilidades.getFechaLocalDate(resulSet.getLong("fnac")));
			} else {
				paciente.setFnac(null);
			}
			paciente.setTelefono(resulSet.getString("telefono"));
			paciente.setMovil(resulSet.getString("movil"));
			paciente.setNumerohc(resulSet.getString("nhc"));
			if (completo == true) {
				paciente.setMunicipio(new MunicipioDAO().getRegistroId(resulSet.getLong("municipio")));
				paciente.setProvincia(new ProvinciasDAO().getPorCodigo(resulSet.getString("provincia")));
			}
			paciente.setSexo(resulSet.getInt("sexo"));
			switch (tipo) {
			case "PROCESOS":
				Proceso proceso = new ProcesoDAO().getRegistroId(resulSet.getLong("idproblema"), paciente);
				paciente.setProcesoActual(proceso);
				break;
			case "EPISODIOS":
				Episodio episodio = new Episodio();
				episodio = new EpisodioDAO().getRegistroPorId(resulSet.getLong("idepisodio"), paciente, centro,
						servicio, agenda, zona, cama);
				paciente.setEpisodioActual(episodio);
				break;
			}
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		}
		return paciente;
	}

	/**
	 * Gets the paciente por id.
	 *
	 * @param id the id
	 * @return the paciente por id
	 */
	public Paciente getPacientePorId(Long id) {
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			sql = "  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.id=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			resulSet = statement.executeQuery();
			if (resulSet.next()) {
				paciente = getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null, true);
			}
			statement.close();
			logger.debug("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.id= " + id);
		} catch (SQLException e) {
			logger.error("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.id= " + id);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paciente;
	}

	public Paciente getPacientePorNhc(String nhc) {
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			sql = "  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE h.nhc=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, nhc);
			resulSet = statement.executeQuery();
			if (resulSet.next()) {
				paciente = getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null, true);
			}
			statement.close();
			logger.debug("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE h.nhc= " + nhc);
		} catch (SQLException e) {
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paciente;
	}

	public Paciente getPacientePorTf(String tf, Long id) {
		Connection connection = null;
		try {
			connection = super.getConexionBBDD();
			sql = "  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.telefono=?  OR p.movil=? AND p.id != ?";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, tf);
			statement.setString(2, tf);
			statement.setLong(3, id);
			resulSet = statement.executeQuery();
			if (resulSet.next()) {
				paciente = getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null, true);
			}
			statement.close();
			logger.debug("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.telefono='" + tf + "'  OR p.movil='" + tf + "' AND p.id != " + id);
		} catch (SQLException e) {
			logger.error("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE p.telefono=" + tf + "  OR p.movil=" + tf + " AND p.id != " + id);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paciente;
	}

	public Paciente getPacientePorNhcConRegistros(String nhc, Long subambito) {
		Connection connection = null;
		paciente = null;
		try {
			connection = super.getConexionBBDD();
			sql = "  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE h.nhc=? ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, nhc);
			resulSet = statement.executeQuery();
			if (resulSet.next()) {
				paciente = getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null, false);
				paciente.setListaRegistros(new RegistroDAO().getListaRegistros(paciente.getId(), subambito));
			}
			statement.close();
			logger.debug("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE h.nhc='" + nhc + "' ");
		} catch (SQLException e) {
			logger.error("  SELECT p.*,h.nhc " + " FROM pacientes p " + " JOIN historias h ON h.paciente=p.id "
					+ " WHERE h.nhc='" + nhc + "' ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paciente;
	}

	@Override
	public boolean grabaDatos(Object objeto) {
		Paciente paciente = (Paciente) objeto;
		boolean actualizado = false;
		if (paciente.getId() == 0) {
			actualizado = this.insertaDatos(paciente);
		} else {
			actualizado = this.actualizaDatos(paciente);
		}
		return actualizado;
	}

	/**
	 * Inserta datos.
	 *
	 * @param paciente the paciente
	 * @return true, if successful
	 */

	@Override
	public boolean insertaDatos(Object objeto) {
		Paciente paciente = (Paciente) objeto;
		Connection connection = null;
		boolean rowInserted = false;
		long id = 0;
		try {
			id = new UtilidadesDAO().getSiguienteId("pacientes");
			connection = super.getConexionBBDD();
			sql = " INSERT INTO pacientes (id,  nombre,  ape1,  ape2,  dni,"
					+ " direccion,  provincia,  municipio,  codigopostal,  fnac,  sexo,"
					+ " telefono,  movil)  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)  ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, id);
			statement.setString(2, paciente.getNombre());
			statement.setString(3, paciente.getApellido1());
			statement.setString(4, paciente.getApellido2());
			statement.setString(5, paciente.getDni());
			statement.setString(6, paciente.getDireccion());
			statement.setString(7, paciente.getProvincia().getCodigo());
			statement.setLong(8, paciente.getMunicipio().getId());
			statement.setString(9, paciente.getCodigopostal());
			statement.setLong(10, Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()));
			statement.setInt(11, paciente.getSexo());
			statement.setString(12, paciente.getTelefono());
			statement.setString(13, paciente.getMovil());
			rowInserted = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" INSERT INTO pacientes (id,  nombre,  ape1,  ape2,  dni,"
					+ " direccion,  provincia,  municipio,  codigopostal,  fnac,  sexo,"
					+ " telefono,  movil)  VALUES (" + id + ",'" + paciente.getNombre() + "','"
					+ paciente.getApellido1() + "','" + paciente.getApellido2() + "','" + paciente.getDni() + "','"
					+ paciente.getDireccion() + "','" + paciente.getProvincia().getCodigo() + "',"
					+ paciente.getMunicipio().getId() + ",'" + paciente.getCodigopostal() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()) + "," + paciente.getSexo() + ",'"
					+ paciente.getTelefono() + "','" + paciente.getMovil() + "')  ");
			new HistoriaDAO().insertaNhc(new Historia(id, paciente.getNumerohc()));

		} catch (SQLException e) {
			logger.error(" INSERT INTO pacientes (id,  nombre,  ape1,  ape2,  dni,"
					+ " direccion,  provincia,  municipio,  codigopostal,  fnac,  sexo,"
					+ " telefono,  movil)  VALUES (" + id + ",'" + paciente.getNombre() + "','"
					+ paciente.getApellido1() + "','" + paciente.getApellido2() + "','" + paciente.getDni() + "','"
					+ paciente.getDireccion() + "','" + paciente.getProvincia().getCodigo() + "',"
					+ paciente.getMunicipio().getId() + ",'" + paciente.getCodigopostal() + "',"
					+ Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()) + "," + paciente.getSexo() + ",'"
					+ paciente.getTelefono() + "','" + paciente.getMovil() + "')  ");
			logger.error(ConexionDAO.ERROR_BBDD_SQL + sql, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowInserted;
	}

	/**
	 * Actualiza datos.
	 *
	 * @param paciente the paciente
	 * @return true, if successful
	 */
	@Override
	public boolean actualizaDatos(Object objeto) {
		Paciente paciente = (Paciente) objeto;
		Connection connection = null;
		boolean rowUpdated = false;
		try {
			connection = super.getConexionBBDD();
			sql = " UPDATE  pacientes SET   nombre=?,  ape1=?,  ape2=?,  dni=?,"
					+ " direccion=?,  provincia=?,  municipio=?,  codigopostal=?,  fnac=?,  sexo=?,"
					+ " telefono=?,  movil=? WHERE id=?  ";

			PreparedStatement statement = connection.prepareStatement(sql);

			statement.setString(1, paciente.getNombre());
			statement.setString(2, paciente.getApellido1());
			statement.setString(3, paciente.getApellido2());
			statement.setString(4, paciente.getDni());
			statement.setString(5, paciente.getDireccion());
			statement.setString(6, paciente.getProvincia().getCodigo());
			statement.setLong(7, paciente.getMunicipio().getId());
			statement.setString(8, paciente.getCodigopostal());
			statement.setLong(9, Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()));
			statement.setInt(10, paciente.getSexo());
			statement.setString(11, paciente.getTelefono());
			statement.setString(12, paciente.getMovil());
			statement.setLong(13, paciente.getId());
			statement.executeUpdate();
			rowUpdated = true;
			statement.close();
			logger.debug(sql = " UPDATE  pacientes SET   nombre='" + paciente.getNombre() + "',  ape1='"
					+ paciente.getApellido1() + "',  ape2='" + paciente.getApellido2() + "',  dni='" + paciente.getDni()
					+ "'," + " direccion='" + paciente.getDireccion() + "',  provincia='"
					+ paciente.getProvincia().getCodigo() + "',  municipio=" + paciente.getMunicipio().getId()
					+ ",  codigopostal='" + paciente.getCodigopostal() + "',  fnac="
					+ Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()) + ",  sexo=" + paciente.getSexo()
					+ "," + " telefono='" + paciente.getTelefono() + "',  movil=" + paciente.getMovil() + " WHERE id=  "
					+ paciente.getId());
			new HistoriaDAO().actualizaDatos(new Historia(paciente.getId(), paciente.getNumerohc()));

		} catch (SQLException e) {
			logger.error(sql = " UPDATE  pacientes SET   nombre='" + paciente.getNombre() + "',  ape1='"
					+ paciente.getApellido1() + "',  ape2='" + paciente.getApellido2() + "',  dni='" + paciente.getDni()
					+ "'," + " direccion='" + paciente.getDireccion() + "',  provincia='"
					+ paciente.getProvincia().getCodigo() + "',  municipio=" + paciente.getMunicipio().getId()
					+ ",  codigopostal='" + paciente.getCodigopostal() + "',  fnac="
					+ Utilidades.getFechaNumeroyyymmddDefecha(paciente.getFnac()) + ",  sexo=" + paciente.getSexo()
					+ "," + " telefono='" + paciente.getTelefono() + "',  movil=" + paciente.getMovil() + " WHERE id=  "
					+ paciente.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return rowUpdated;
	}

	/**
	 * Gets the sql where.
	 *
	 * @param cadena the cadena
	 * @return the sql where
	 */
	public String getSqlWhere(String cadena) {
		String[] apellidos = cadena.toUpperCase().split(" ");

		String sqlString = "";

		if (Utilidades.validarNIF(cadena) == true) {
			sqlString = sqlString.concat(" AND  dni='" + cadena + "'");
		} else if (Utilidades.isNumeric(cadena)) {
			sqlString = sqlString.concat(" AND nhc='" + cadena + "'");
		} else if (apellidos.length > 0) {
			if (apellidos.length == 1 && apellidos[0] != null && apellidos[0] != "") {
				sqlString = sqlString.concat(" AND ( ape1 LIKE '" + apellidos[0].trim() + "%' ");
				sqlString = sqlString.concat(" OR ape2 LIKE '" + apellidos[0].trim() + "%' ");
				sqlString = sqlString.concat(" OR nombre LIKE '" + apellidos[0].trim() + "%'   )");
			} else if (apellidos.length == 2 && apellidos[0].trim() != null && apellidos[1].trim() != null)
				sqlString = sqlString.concat(" AND  ape1 LIKE '" + apellidos[0].trim() + "%'  AND  ape2 LIKE '"
						+ apellidos[1].trim() + "%' ");
			else if (apellidos.length == 3 && apellidos[0].trim() != null && apellidos[1].trim() != null
					&& apellidos[2].trim() != null)
				sqlString = sqlString.concat(" AND  ape1 LIKE '" + apellidos[0].trim() + "%'  AND  ape2 LIKE '"
						+ apellidos[1].trim() + "%'  AND nombre LIKE '" + apellidos[2].trim() + "%'");
		}

		sqlString = sqlString.concat(" ORDER BY p.ape1,p.ape2,p.nombre ");

		return sqlString;
	}

	public String getSqlWhere(String ape1, String ape2, String nombre) {
		String sqlString = "";
		if (!ape1.isEmpty())
			sqlString = sqlString.concat(" AND ape1 LIKE '" + ape1.trim() + "%' ");
		if (!ape2.isEmpty())
			sqlString = sqlString.concat(" AND ape2 LIKE '" + ape2.trim() + "%' ");
		if (!nombre.isEmpty())
			sqlString = sqlString.concat(" AND nombre LIKE '" + nombre.trim() + "%' ");
		sqlString = sqlString.concat(" ORDER BY ape1,ape2,nombre ");
		return sqlString;
	}

	public String getSqlWhere(Centro centro, Servicio servicio) {
		String sqlString = "";
		if (centro != null) {
			sqlString = sqlString.concat(" AND e.centro=" + centro.getId());
		}
		if (servicio != null) {
			sqlString = sqlString.concat(" AND e.servicio=" + servicio.getId());
		}
		return sqlString;
	}

	public String getSqlWhere(Long claseEpisodio, Centro centro, Servicio servicio, Zona zona, LocalDate fecha,
			Agenda agenda) {
		String sqlString = "";
		if (!claseEpisodio.equals(new Long(0))) {
			sqlString = sqlString.concat(" AND e.clase=  " + claseEpisodio);
		}
		if (centro != null) {
			sqlString = sqlString.concat(" AND e.centro=" + centro.getId());
		}
		if (servicio != null) {
			sqlString = sqlString.concat(" AND e.servicio=" + servicio.getId());
		}
		if (zona != null && zona.getId() != null) {
			sqlString = sqlString.concat(" AND c.zona=" + zona.getId());
		}
		if (agenda != null) {
			sqlString = sqlString.concat(" AND e.agenda =" + agenda.getId());
		}

		/**
		 * Si el episodio es de ingreso clase =1 la condición de fecha es que fecha de
		 * inicio sea menor o igual a el parámetro y fecha final =99999999 o mayor que
		 * la fecha del parámetro.
		 * 
		 */

		if (fecha != null) {
			if (claseEpisodio.equals(Episodio.CLASE_HOSPITALIZACION.getId())) {
				sqlString = sqlString.concat(" AND  finicio<=" + Utilidades.getFechaNumeroyyymmddDefecha(fecha)
						+ " AND ( ffinal=" + Constantes.FEHAFIN_DEFECTO + " OR  ffinal>="
						+ Utilidades.getFechaNumeroyyymmddDefecha(fecha) + ")");
			} else if (claseEpisodio.equals(Episodio.CLASE_URGENCIAS.getId())) {
				sqlString = sqlString.concat(" AND ffinal=" + Constantes.FEHAFIN_DEFECTO);
			} else {
				sqlString = sqlString.concat(" AND finicio=" + Utilidades.getFechaNumeroyyymmddDefecha(fecha));
			}
		}
		return sqlString;
	}

	/**
	 * Gets the paginacion datosa pacientes apellidos.
	 *
	 * @param cadena    the cadena
	 * @param subambito the subambito
	 * @return the paginacion datosa pacientes apellidos
	 */
	public PagiLisReg getPaginacionPaciente(String cadena, String tipo, Long valor, Centro centro, Servicio servicio,
			Zona zona, LocalDate fecha, Agenda agenda) {
		Connection connection = null;
		PagiLisReg paginacion = new PagiLisReg(0, 0, 0, 0, 0, 1);
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			switch (tipo) {
			case "PROCESOS":
				if (!valor.equals(new Long(0))) {
					sql = "SELECT   count(*) as numero " + "	" + "FROM pacientes p,  problemas s, historias h"
							+ " WHERE h.paciente = p.id AND p.id = s.paciente  AND s.subambito=  " + valor
							+ " AND s.fechafin= " + Constantes.FEHAFIN_DEFECTO;
					sql = sql.concat(getSqlWhere(centro, servicio));
				} else {
					sql = " SELECT count(*) as numero  FROM pacientes  WHERE 1=1 ";
				}
				break;
			case "EPISODIOS":
				sql = "SELECT  count(*) as numero " + "	FROM   episodios e" + "	JOIN pacientes p ON p.id=e.paciente"
						+ "	JOIN historias h ON h.paciente=e.paciente"
						+ " LEFT JOIN camas c ON c.id=e.idcama	WHERE 1=1  ";
				sql = sql.concat(getSqlWhere(valor, centro, servicio, zona, fecha, agenda));
				break;
			}

			sql = sql.concat(getSqlWhere(cadena));
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			if (resulSet.next()) {
				contador = resulSet.getInt("numero");
			}
			paginacion.setPrimero(1);
			paginacion.setUltimo(contador);
			paginacion.setRegistrosTotales(contador);
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paginacion;
	}

	public PagiLisReg getPaginacionPaciente(String ape1, String ape2, String nombre, Long subambito) {
		Connection connection = null;
		PagiLisReg paginacion = new PagiLisReg(0, 0, 0, 0, 0, 1);
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			if (!subambito.equals(new Long(0))) {
				sql = "SELECT   count(*) as numero " + "	" + " FROM pacientes p,  problemas s, historias h"
						+ " WHERE h.paciente = p.id ANDp.id = s.paciente  AND s.subambito=  " + subambito
						+ " AND s.fechafin= " + Constantes.FEHAFIN_DEFECTO;
			} else {
				sql = " SELECT count(*) as numero  FROM pacientes  WHERE 1=1 ";
			}
			sql = sql.concat(getSqlWhere(ape1, ape2, nombre));
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			if (resulSet.next()) {
				contador = resulSet.getInt("numero");
			}
			paginacion.setPrimero(1);
			paginacion.setUltimo(contador);
			paginacion.setRegistrosTotales(contador);
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return paginacion;
	}

	/**
	 * Gets the lista pacientes. Método sobre esccrito que recupera datos de
	 * pacientes. Si el valor del parametro es un dni hace la búsqueda por dni. Si
	 * es no hace la busqueda por nhc
	 * 
	 * @param dninhc the dninhc
	 * @return the lista pacientes
	 */
	public ArrayList<Paciente> getListaPacientes(String dninhc) {
		Connection connection = null;
		ArrayList<Paciente> listaPacientes = new ArrayList<>();
		try {
			connection = super.getConexionBBDD();

			sql = " SELECT p.*,h.nhc FROM pacientes p,historias h  WHERE p.id=h.paciente  ";

			if (Utilidades.validarNIF(dninhc) == true) {
				sql = sql.concat(" AND  dni='" + dninhc + "'");
			} else {
				sql = sql.concat(" AND nhc='" + dninhc + "'");
			}
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			while (resulSet.next()) {
				listaPacientes.add(
						(Paciente) getRegistroResulset(resulSet, "PACIENTES", null, null, null, null, null, false));
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaPacientes;
	}

	/**
	 * Gets the lista pacientes apellidos.
	 *
	 * @param cadena     the cadena
	 * @param paginacion the paginacion
	 * @param subambito  the subambito
	 * @return the lista pacientes apellidos
	 */
	public ArrayList<Paciente> getListaPacientes(String cadena, PagiLisReg paginacion, String tipo, Long valor,
			Centro centro, Servicio servicio, Zona zona, LocalDate fecha, Agenda agenda) {
		Connection connection = null;
		ArrayList<Paciente> listaPacientes = new ArrayList<>();
		Cama cama = null;
		Zona zonavar = zona;

		int contador = 0;
		try {
			connection = super.getConexionBBDD();

			switch (tipo) {
			case "PROCESOS":
				if (persistencia.equals(Constantes.MYSQL_STRING)) {
					if (!valor.equals(new Long(0))) {
						sql = "SELECT  @rownum:=@rownum+1  as numeroorden , p.*, s.id as idproblema "
								+ "	FROM pacientes p,  (SELECT @rownum:=0) R, problemas s"
								+ " JOIN historias h On h.paciente=p.id"
								+ " WHERE  p.id = s.paciente  AND s.subambito=  " + valor + " AND s.fechafin= "
								+ Constantes.FEHAFIN_DEFECTO;
						sql = "SELECT  @rownum:=@rownum+1  as numeroorden ,p.*,h.nhc,s.id as idproblema "
								+ "	FROM pacientes  p ,  (SELECT @rownum:=0) r,historias h,problemas s "
								+ "	WHERE h.paciente=p.id " + "	AND s.paciente=p.id AND s.subambito=" + valor
								+ " AND s.fechafin=" + Constantes.FEHAFIN_DEFECTO;
					} else {
						sql = "SELECT  @rownum:=@rownum+1  as numeroorden , p.* "
								+ " JOIN historias h On h.paciente=p.id" + "WHERE  1=1  ";
					}
				} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
					if (!valor.equals(new Long(0))) {
						sql = "SELECT  row_number() over (ORDER BY ape1,ape2,nombre) as numeroorden , p.* ,h.nhc, s.id as idproblema  "
								+ "	FROM pacientes p,   problemas s, historias h "
								+ " WHERE  h.paciente=p.id and p.id = s.paciente  AND s.subambito=  " + valor
								+ " AND s.fechafin= " + Constantes.FEHAFIN_DEFECTO;
					} else {
						sql = "SELECT  row_number() over (ORDER BY ape1,ape2,nombre) as numeroorden , p.*,h.nhc  "
								+ "	FROM pacientes p, historias h  WHERE  h.paciente=p.id   ";
					}
				}
				break;
			case "EPISODIOS":
				if (persistencia.equals(Constantes.MYSQL_STRING)) {
					sql = "SELECT  @rownum:=@rownum+1  as numeroorden , v.* "
							+ "	FROM view_pacientes v,  (SELECT @rownum:=0) R, episodios e"
							+ " WHERE  v.id = e.paciente  ";
					sql = "SELECT   @rownum:=@rownum+1  as numeroorden , p.* ,h.nhc,e.id as idepisodio "
							+ "	FROM   pacientes p,   (SELECT @rownum:=0) r, episodios e  "
							+ "	JOIN historias h ON h.paciente=e.paciente" + " LEFT JOIN camas c ON c.id=e.idcama	"
							+ " LEFT JOIN agendas a ON a.id=e.agenda" + " WHERE 	 p.id=e.paciente  ";
				} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
					sql = "SELECT  row_number() over (ORDER BY p.ape1,p.ape2,p.nombre) as numeroorden "
							+ ", p.* ,h.nhc,e.id as idepisodio,u.apellido1,u.apellido2,u.nombre as usunombre "
							+ " ,c.cama,c.id as idcama, c.zona as zonacama,c.estado as estadocama"
							+ ", z.zona as zonadescripcion,z.id as zonaid,z.tipo as zonatipo,z.centro as zonacentro "
							+ "	FROM   episodios e " + "	JOIN pacientes p ON p.id=e.paciente"
							+ "	JOIN historias h ON h.paciente=e.paciente" + " JOIN usuarios u ON u.userid=e.userid "
							+ " LEFT JOIN camas c ON c.id=e.idcama	" + " LEFT JOIN zonas z ON z.id=c.zona "
							+ " LEFT JOIN agendas a ON a.id=e.agenda" + " WHERE 1=1  ";
				}
				sql = sql.concat(getSqlWhere(valor, centro, servicio, zona, fecha, agenda));
				break;
			}

			sql = sql.concat(getSqlWhere(cadena));
			logger.debug(sql);
			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			while (resulSet.next()) {
				if (valor.equals(Episodio.CLASE_HOSPITALIZACION.getId())) {
					zonavar = new Zona(resulSet.getLong("zonaid"), resulSet.getString("zonadescripcion"),
							resulSet.getInt("zonatipo"), centro);
					cama = new Cama(resulSet.getLong("idcama"), resulSet.getString("cama"),
							resulSet.getString("estadocama"), zonavar);
				}
				if (paginacion.getDireccion() == 1) {
					if (resulSet.getInt("numeroorden") > paginacion.getAnterior()) {
						// Cama(Long id,String cama,String estado,Long zona) {
						paciente = (Paciente) getRegistroResulset(resulSet, tipo, centro, servicio, agenda, zonavar,
								cama, false);
						paciente.setNumeroOrden(resulSet.getInt("numeroorden"));
						listaPacientes.add(paciente);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				} else {
					if (resulSet.getInt("numeroorden") >= paginacion.getAnterior()) {
						paciente = (Paciente) getRegistroResulset(resulSet, tipo, centro, servicio, agenda, zonavar,
								cama, false);
						paciente.setNumeroOrden(resulSet.getInt("numeroorden"));
						listaPacientes.add(paciente);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				}
			}
			statement.close();
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaPacientes;
	}

	public ArrayList<Paciente> getListaPacientes(String ape1, String ape2, String nombre, PagiLisReg paginacion,
			Long subambito) {
		Connection connection = null;
		ArrayList<Paciente> listaPacientes = new ArrayList<>();
		int contador = 0;
		try {
			connection = super.getConexionBBDD();
			// para mysql
			/*
			 * if (!subambito.equals(new Long(0))) { sql =
			 * "SELECT  @rownum:=@rownum+1  as numeroorden , v.* " +
			 * "	FROM view_pacientes v,  (SELECT @rownum:=0) R, problemas s" +
			 * " WHERE  v.id = s.paciente  AND s.subambito=  " + subambito +
			 * " AND s.fechafin= " + Constantes.FEHAFIN_DEFECTO; } else { // row_count sql =
			 * "SELECT  @rownum:=@rownum+1  as numeroorden , v.* " +
			 * "	FROM view_pacientes v,  (SELECT @rownum:=0) R " + " WHERE  1=1  "; }
			 */

			if (persistencia.equals(Constantes.MYSQL_STRING)) {
				if (!subambito.equals(new Long(0))) {
					sql = "SELECT  @rownum:=@rownum+1  as numeroorden , v.* "
							+ "	FROM view_pacientes v,  (SELECT @rownum:=0) R, problemas s"
							+ " WHERE  v.id = s.paciente  AND s.subambito=  " + subambito + " AND s.fechafin= "
							+ Constantes.FEHAFIN_DEFECTO;
				} else {
					sql = "SELECT  @rownum:=@rownum+1  as numeroorden , v.* "
							+ "	FROM view_pacientes v,  (SELECT @rownum:=0) R  WHERE  1=1  ";
				}
			} else if (persistencia.equals(Constantes.ORACLE_STRING)) {
				if (!subambito.equals(new Long(0))) {
					sql = "SELECT  row_number() over (ORDER BY ape1,ape2,nombre) as numeroorden , p.*,h.nhc "
							+ "	FROM pacientes p,   problemas s, historias h "
							+ " WHERE  h.paciente=p.id and p.id = s.paciente  AND s.subambito=  " + subambito
							+ " AND s.fechafin= " + Constantes.FEHAFIN_DEFECTO;
				} else {
					sql = "SELECT  row_number() over (ORDER BY ape1,ape2,nombre) as numeroorden , p.* ,h.nhc "
							+ "	FROM pacientes p, historias h  WHERE  h.paciente=p.id   ";
				}
			}

			sql = sql.concat(getSqlWhere(ape1, ape2, nombre));

			Statement statement = connection.createStatement();
			ResultSet resulSet = statement.executeQuery(sql);
			while (resulSet.next()) {
				int orden = resulSet.getInt("numeroorden");

				if (paginacion.getDireccion() == 1) {
					if (resulSet.getInt("numeroorden") > paginacion.getAnterior()) {
						paciente = (Paciente) getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null,
								false);
						paciente.setNumeroOrden(orden);
						listaPacientes.add(paciente);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				} else {
					if (resulSet.getInt("numeroorden") >= paginacion.getAnterior()) {
						paciente = (Paciente) getRegistroResulset(resulSet, "PACIENTE", null, null, null, null, null,
								false);
						paciente.setNumeroOrden(resulSet.getInt("numeroorden"));
						listaPacientes.add(paciente);
						contador++;
						if (contador >= paginacion.getNumeroRegistrosPagina())
							break;
					}
				}
			}
			statement.close();
			logger.debug(sql);
		} catch (SQLException e) {
			logger.error(sql);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return listaPacientes;
	}

	/**
	 * Borra datos.
	 *
	 * @param paciente the paciente
	 * @return true, if successful
	 */
	@Override

	public boolean borraDatos(Object objeto) {
		Paciente paciente2 = (Paciente) objeto;
		Connection connection = null;
		boolean borrado = false;
		try {
			borrado = new HistoriaDAO().borraDatos(paciente2);
			connection = super.getConexionBBDD();
			sql = " DELETE FROM pacientes WHERE id=?   ";
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, paciente2.getId());
			borrado = statement.executeUpdate() > 0;
			statement.close();
			logger.debug(" DELETE FROM pacientes WHERE id=  " + paciente2.getId());
		} catch (SQLException e) {
			logger.error(" DELETE FROM pacientes WHERE id=  " + paciente2.getId());
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return borrado;
	}

	public boolean getReferenciasExternas(Long idPaciente) {
		Connection connection = null;
		boolean referencias = true;
		try {
			connection = super.getConexionBBDD();
			sql = "SELECT paciente FROM  problemas WHERE paciente=? "
					+ " UNION SELECT paciente FROM registros WHERE paciente=? "
					+ " UNION SELECT paciente FROM peticiones WHERE paciente=? ";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setLong(1, idPaciente);
			statement.setLong(2, idPaciente);
			statement.setLong(3, idPaciente);
			ResultSet resulSet = statement.executeQuery();
			if (resulSet.next()) {
				referencias = false;
			}
			statement.close();
			logger.debug("SELECT paciente FROM  problemas WHERE paciente= " + idPaciente
					+ " UNION SELECT paciente FROM registros WHERE paciente= " + idPaciente
					+ " UNION SELECT paciente FROM peticiones WHERE paciente= " + idPaciente);
		} catch (SQLException e) {
			logger.error("SELECT paciente FROM  problemas WHERE paciente= " + idPaciente
					+ " UNION SELECT paciente FROM registros WHERE paciente= " + idPaciente
					+ " UNION SELECT paciente FROM peticiones WHERE paciente= " + idPaciente);
			logger.error(ConexionDAO.ERROR_BBDD_SQL, e);
		} catch (Exception e) {
			logger.error(NotificacionInfo.EXCEPTION_ERROR, e);
		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				logger.error(ConexionDAO.ERROR_CLOSE_BBDD_SQL, e);
			}
		}
		return referencias;
	}

	@Override
	public Object getRegistroId(Long id) {
		return null;
	}

	@Override
	public Object getRegistroResulset(ResultSet rs) {
		// TODO Auto-generated method stub
		return null;
	}

}
