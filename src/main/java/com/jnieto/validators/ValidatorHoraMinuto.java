package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

public class ValidatorHoraMinuto extends RegexpValidator {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5585550350392615093L;

	private static final String PATTERN = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$";
	private static final String MENSAJE_STRING = "HH:mm";

	/*
	 * public ValidatorHoraMinuto(String regexp) { super("HH:mm", PATTERN, true); }
	 */
	public ValidatorHoraMinuto(String regexp) {
		super(MENSAJE_STRING, regexp);
	}

	public ValidatorHoraMinuto(String errorMessage, String regexp, boolean complete) {
		super(errorMessage, regexp, complete);
	}

	public boolean isValid(String value) {
		if (value == null || value.isEmpty()) {
			return true;
		} else if (value.matches(PATTERN)) {
			return true;
		} else {
			return false;
		}
	}
}
