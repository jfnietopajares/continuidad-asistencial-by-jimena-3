package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

public class ValidatorEntero extends RegexpValidator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -295551862313733063L;
	private static final String PATTERN = "[0-9]*";
	private static final String MENSAJE_STRING = "Números enteros ";

	private Integer inferior;
	private Integer superior;

	public ValidatorEntero(String regexp, Integer inferior, Integer superior) {
		super(MENSAJE_STRING + " mayores de " + inferior + " y menores de " + superior, regexp);
		this.inferior = inferior;
		this.superior = superior;
	}

	public ValidatorEntero(String errorMessage, String regexp, boolean complete) {
		super(errorMessage, regexp, complete);
	}

	public boolean isValid(String value) {
		int valorEntero;
		if (value == null || value.isEmpty()) {
			return true;
		} else if (value.matches(PATTERN)) {
			valorEntero = Integer.parseInt(value);
			if (inferior != null) {
				if (valorEntero < inferior) {
					return false;
				}
			}
			if (superior != null) {
				if (valorEntero > superior) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
