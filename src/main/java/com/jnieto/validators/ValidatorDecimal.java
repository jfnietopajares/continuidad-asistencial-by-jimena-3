package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

public class ValidatorDecimal extends RegexpValidator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4203754584267033791L;
	private static final String PATTERN = "[0-9]+([.])?([0-9]+)";
	private static final String MENSAJE_STRING = "Números decimales. Separador .";

	private Double inferior;
	private Double superior;

	public ValidatorDecimal(String regexp, Double inferior, Double superior) {
		super(MENSAJE_STRING + " mayores de " + inferior + " y menores de " + superior, regexp);
		this.inferior = inferior;
		this.superior = superior;
	}

	public ValidatorDecimal(String errorMessage, String regexp, boolean complete) {
		super(errorMessage, regexp, complete);
	}

	public boolean isValid(String value) {
		Double valorDecimal;
		if (value == null || value.isEmpty()) {
			return true;
		} else if (value.matches(PATTERN)) {
			valorDecimal = Double.parseDouble(value);
			if (inferior != null) {
				if (valorDecimal < inferior) {
					return false;
				}
			}
			if (superior != null) {
				if (valorDecimal > superior) {
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}
}
