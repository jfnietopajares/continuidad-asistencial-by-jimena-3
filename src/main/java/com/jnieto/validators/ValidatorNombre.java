package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

@SuppressWarnings("serial")
public class ValidatorNombre extends RegexpValidator {
//	private static final String PATTERN = "^([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\']+[\\s])+([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\'])+[\\s]?([A-Za-zÁÉÍÓÚñáéíóúÑ]{0}?[A-Za-zÁÉÍÓÚñáéíóúÑ\\'])?$";
//	private static final String PATTERN = "^[a-zA-Z\\sÑñóÓ]*$";

	private static final String PATTERN = "^[A-Za-zƒŠŒŽšœžŸÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèé êëìíîïðñòóôõöøùúûüýþÿáéíóúáéíóú ']*$";

	public ValidatorNombre(String errorMessage) {
		super(errorMessage, PATTERN, true);
	}
}
