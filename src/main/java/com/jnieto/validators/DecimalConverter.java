package com.jnieto.validators;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.StringToDoubleConverter;

public class DecimalConverter extends StringToDoubleConverter implements Converter<String, Double> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5090747162305327958L;

	public DecimalConverter(String errorMessageProvider) {
		super(errorMessageProvider);
	}

	@Override
	public Result<Double> convertToModel(String fieldValue, ValueContext context) {
		try {
			return Result.ok(Double.parseDouble(fieldValue));

		} catch (NumberFormatException e) {
			return Result.error(" No es un valor numérico ");
		}
	}

	@Override
	public String convertToPresentation(Double value, ValueContext context) {
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
		DecimalFormat formateador = new DecimalFormat("####.##", otherSymbols);
		return formateador.format(value);
	}
}
