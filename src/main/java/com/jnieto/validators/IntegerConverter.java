package com.jnieto.validators;

import com.vaadin.data.ValueContext;
import com.vaadin.data.converter.StringToIntegerConverter;

public class IntegerConverter extends StringToIntegerConverter {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8321095951808357394L;

	public IntegerConverter() {
		super(("mensaje.error.validar.formato.numerico"));
	}

	@Override
	public String convertToPresentation(Integer value, ValueContext context) {
		if (value == null) {
			return "";
		}
		return value.toString();
	}
}