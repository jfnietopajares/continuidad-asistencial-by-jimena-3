package com.jnieto.validators;

import com.vaadin.data.validator.RegexpValidator;

@SuppressWarnings("serial")
public class ValidatorMovil extends RegexpValidator {

	private static final String PATTERN = "[0-9]{1,9}";
	private static final String MENSAJE_STRING = " 9 dígitos numéricos ";

	public ValidatorMovil(String regexp) {
		super(MENSAJE_STRING, regexp);
	}

	public boolean isValid(String value) {
		if (value == null || value.isEmpty()) {
			return true;
		} else if (value.matches(PATTERN)) {
			return true;
		} else {
			return false;
		}
	}
}
