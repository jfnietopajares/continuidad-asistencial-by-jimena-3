package com.jnieto.entity;

import java.time.format.DateTimeFormatter;

import com.jnieto.utilidades.Utilidades;

/**
 * The Class Variable. *
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */
public class Variable {

	private String code;

	private String codesystem;

	private Long item;

	private String descripcion;

	private String valor;

	private Double valorInferior;

	private Double valorSuperior;

	private String textoAyuda;

	private String tipoVariable;

	public final static String TIPO_VARIABLE_STRING = "string";
	public final static String TIPO_VARIABLE_DATE = "date";
	public final static String TIPO_VARIABLE_CHECK = "check";

	public final static Variable ONCO_PRESTACION_TTL = new Variable("13995056", "99G2", new Long(13995056),
			"Quimioterapia intravenoso largo");
	public final static Variable ONCO_PRESTACION_TTM = new Variable("13995057", "99G2", new Long(13995057),
			"Quimioterapia intravenoso medio");
	public final static Variable ONCO_PRESTACION_TTC = new Variable("13995058", "99G2", new Long(13995058),
			"Quimioterapia intravenoso corto");
	public final static Variable ONCO_PRESTACION_TTO = new Variable("13995059", "99G2", new Long(13995059),
			"Quimioterapia oral");

	public final static Variable CITA_PRESTACION_NUEVO = new Variable("13995059", "99G2", new Long(13995059), "Nuevo ");

	public final static Variable CITA_PRESTACION_REVISION = new Variable("13995059", "99G2", new Long(13995059),
			"Revision ");

	/**
	 * Instantiates a new variable.
	 */
	public Variable() {

	}

	/**
	 * Instantiates a new variable.
	 *
	 * @param code        the code
	 * @param codesystem  the codesystem
	 * @param item        the item
	 * @param descripcion the descripcion
	 */
	public Variable(String code, String codesystem, Long item, String descripcion) {
		this.code = code;
		this.codesystem = codesystem;
		this.item = item;
		this.descripcion = descripcion;
		this.valor = "";
	}

	public Variable(Long item, String descripcion) {

		this.item = item;
		this.descripcion = descripcion;

	}

	public Variable(String code, String codesystem, Long item, String descripcion, String textoAyuda) {
		this.code = code;
		this.codesystem = codesystem;
		this.item = item;
		this.descripcion = descripcion;
		this.textoAyuda = textoAyuda;
		this.valor = "";
	}

	public Variable(String code, String codesystem, Long item, String descripcion, String textoAyuda, String tipo) {
		this.code = code;
		this.codesystem = codesystem;
		this.item = item;
		this.descripcion = descripcion;
		this.textoAyuda = textoAyuda;
		this.tipoVariable = tipo;
		this.valor = "";
	}

	public Variable(String code, String codesystem, Long item, String descripcion, Double valorInferior,
			Double valorSuperior) {
		this.code = code;
		this.codesystem = codesystem;
		this.item = item;
		this.descripcion = descripcion;
		this.valor = "";
		this.valorInferior = valorInferior;
		this.valorSuperior = valorSuperior;
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the codesystem.
	 *
	 * @return the codesystem
	 */
	public String getCodesystem() {
		return codesystem;
	}

	/**
	 * Sets the codesystem.
	 *
	 * @param codesystem the new codesystem
	 */
	public void setCodesystem(String codesystem) {
		this.codesystem = codesystem;
	}

	/**
	 * Gets the item.
	 *
	 * @return the item
	 */
	public Long getItem() {
		return item;
	}

	/**
	 * Sets the item.
	 *
	 * @param item the new item
	 */
	public void setItem(Long item) {
		this.item = item;
	}

	/**
	 * Gets the descripcion.
	 *
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * Sets the descripcion.
	 *
	 * @param descripcion the new descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Gets the valor.
	 *
	 * @return the valor
	 */
	public String getValor() {
		if (valor != null)
			return valor.trim();
		else {
			return "";
		}
	}

	/**
	 * Sets the valor.
	 *
	 * @param valor the new valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	public Double getValorInferior() {
		return valorInferior;
	}

	public void setValorInferior(Double valorInferior) {
		this.valorInferior = valorInferior;
	}

	public Double getValorSuperior() {
		return valorSuperior;
	}

	public void setValorSuperior(Double valorSuperior) {
		this.valorSuperior = valorSuperior;
	}

	public String getTextoAyuda() {
		return textoAyuda;
	}

	public void setTextoAyuda(String textoAyuda) {
		this.textoAyuda = textoAyuda;
	}

	public String getTipoVariable() {
		return tipoVariable;
	}

	public void setTipoVariable(String tipoVariable) {
		this.tipoVariable = tipoVariable;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	public String toString() {
		return this.item.toString() + "=" + this.getItem() + "\n" + this.getDescripcion() + "=" + this.getValor()
				+ this.getValor() + "=" + this.getValor() + "\n";
	}

	public String getDescripyDato() {
		String resultadoString = "";
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		if (this.getTipoVariable() != null) {
			if (this.getValor() != null && !this.getValor().isEmpty() && !this.getValor().equals("")) {
				switch (this.getTipoVariable()) {
				case TIPO_VARIABLE_STRING:
					resultadoString = this.getDescripcion() + ":" + this.getValor();
					break;
				case TIPO_VARIABLE_CHECK:
					resultadoString = this.getDescripcion() + ":ok";
					break;
				case TIPO_VARIABLE_DATE:
					resultadoString = this.getDescripcion() + ":"
							+ fechadma.format(Utilidades.getFechaLocalDate(this.getValor()));
					break;
				default:
					resultadoString = this.getDescripcion() + ":" + this.getValor();
					break;
				}
			}
		} else {
			if (this.getValor() != null && !this.getValor().isEmpty() && !this.getValor().equals("")) {
				resultadoString = this.getDescripcion() + ":" + this.getValor();
			}
		}
		return resultadoString;
	}

	public String getDatoTipoFormato() {
		String resultadoString = "";
		DateTimeFormatter fechadma = DateTimeFormatter.ofPattern("dd/MM/YYYY");
		if (this.getTipoVariable() != null) {
			if (this.getValor() != null && !this.getValor().isEmpty() && !this.getValor().equals("")) {
				switch (this.getTipoVariable()) {
				case TIPO_VARIABLE_STRING:
					resultadoString = this.getValor();
					break;
				case TIPO_VARIABLE_CHECK:
					resultadoString = "ok";
					break;
				case TIPO_VARIABLE_DATE:
					resultadoString = fechadma.format(Utilidades.getFechaLocalDate(this.getValor()));
					break;
				default:
					resultadoString = this.getValor();
					break;
				}
			}
		} else {
			if (this.getValor() != null && !this.getValor().isEmpty() && !this.getValor().equals("")) {
				resultadoString = this.getValor();
			}
		}
		return resultadoString;
	}
}
