package com.jnieto.entity;

import java.sql.Blob;

public class Categoria {

	private Long id;
	private String codigo;
	private String grupo;
	private String categoria;
	private Long seguridad;
	private Blob funcionBlob;

	public static Categoria MEDICO = new Categoria(new Long(1), "MEDICO");

	public Categoria(Long id, String categoria) {
		this.id = id;
		this.categoria = categoria;
	}

}
