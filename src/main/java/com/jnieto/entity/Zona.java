package com.jnieto.entity;

public class Zona {
	private Long id;
	private String zona;
	private int tipo;
	private Centro centro;

	public static final int ZONA_TIPO_HOSP = 1;
	public static final int ZONA_TIPO_URG = 2;
	public static final int ZONA_TIPO_QUIRO = 3;
	public static final int ZONA_TIPO_HDIA = 4;
	public static final int ZONA_TIPO_URPA = 5;
	// public static final int ZONA_TIPO_CONSULTAS = 6;

	public static final Zona ZONA_HDIA_ONCO = new Zona(new Long(27));
	public static final Zona ZONA_HDIA_MED = new Zona(new Long(38));
	public static final Zona ZONA_QUIROHNSS = new Zona(new Long(24));
	public static final Zona ZONA_QUIROPROV = new Zona(new Long(25));
	public static final Zona ZONA_QUIROARE = new Zona(new Long(26));

	public static final Zona ZONA_UCA = new Zona(new Long(39));

	public Zona() {
	}

	public Zona(Long id, String zona, int tipo, Centro centro) {
		this.id = id;
		this.zona = zona;
		this.centro = centro;
		this.tipo = tipo;
	}

	public Zona(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public String toString() {
		return this.getId() + " " + this.getZona() + " " + this.getTipo();
	}
}
