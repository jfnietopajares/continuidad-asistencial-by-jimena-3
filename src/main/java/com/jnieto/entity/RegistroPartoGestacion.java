package com.jnieto.entity;

import java.time.LocalDate;

import com.jnieto.utilidades.Utilidades;

public class RegistroPartoGestacion extends Registro {

	private Variable edad;
	private Variable edadGestacional;
	private Variable fechaUltimaRegla;
	private Variable fechaprobableParto;

	private Variable paridad;
	private Variable grupoABO;

	private Variable patoMatNo;
	private Variable patoMatTrombofilia;
	private Variable patoMatDiabetesGestional;
	private Variable TVP;
	private Variable DMPG;
	private Variable DMG;
	private Variable EII;
	private Variable reumatogicas;
	private Variable cancer;
	private Variable IPuterinas_95;
	private Variable AR;
	private Variable HTA;
	private Variable APPE;
	private Variable Epilepsia;
	private Variable fetomuerto;
	private Variable patoMatOtros;

	private Variable alergia;

	private Variable patoFetoNo;
	private Variable patoFetoDefCremiento;
	private Variable patoFetoAlteracionLiq;
	private Variable patoFetoMalformacion;
	private Variable patoFetoHidramnios;
	private Variable patoFetoOligoamnios;
	private Variable patoFetoOtros;

	private Variable eco20semanas;

	private Variable sero1TRubeola;
	private Variable sero1Tsifilis;
	private Variable sero1TToxo;
	private Variable sero1TVHB;
	private Variable sero1TVHC;
	private Variable sero1TVIH;

	private Variable sero3TRubeola;
	private Variable sero3Tsifilis;
	private Variable sero3TToxo;
	private Variable sero3TVHB;
	private Variable sero3TVHC;
	private Variable sero3TVIH;

	private Variable esteGrupoBFecha;
	private Variable esteGrupoBValor;

	private Variable tecReproAsist;
	// private Variable pesoEstimado;

	private Variable gemelar;
	private Variable numeroFetos;

	public final static Long PLANTILLLA_EDITOR_PAR_GESTACION = new Long(794855482);
	public final static Long TIPO_REGISTRO_PARTO = new Long(21);

	public final Variable VAR_PARTO_GESTA_EDAD = new Variable("397669002", "SNM3", new Long(13391551), "Edad");
	public final Variable VAR_PARTO_GESTA_EGESTAIONAL = new Variable("57036006", "SNM3", new Long(7967),
			" E.Gestacional");
	public final Variable VAR_PARTO_GESTA_FUR = new Variable("21840007", "SNM3", new Long(7747091), " FUR");
	public final Variable VAR_PARTO_GESTA_FPP = new Variable("161714006", "SNM3", new Long(47311455), " FPP");

	public final Variable VAR_PARTO_GESTA_PARIDAD = new Variable("364325004", "SNM3", new Long(7747089), " Paridad");

	public final Variable VAR_PARTO_GESTA_GABO = new Variable("882-1", "LN", new Long(1495), " GABO");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_NO = new Variable("13994579", "99G2", new Long(13994579),
			" PatMatNo");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_TROMBO = new Variable("13994580", "99G2", new Long(13994580),
			"PatMatTrombofilia");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_DIABE = new Variable("11687002", "SNM3", new Long(35000250),
			"PatMatDiabetesGes");
	// VARIABLES NUEVAS
	public final Variable VAR_PARTO_GESTA_PAT_MAT_TVP = new Variable("1", "99G2", new Long(1), "TVP");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_DMPG = new Variable("2", "99G2", new Long(2), "DMPG");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_DMP = new Variable("3", "99G2", new Long(3), "DMP");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_EII = new Variable("4", "99G2", new Long(4), "EII");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_REUMATOL = new Variable("5", "99G2", new Long(5), "Reumatol");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_CANCER = new Variable("6", "99G2", new Long(6), "Cáncer");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_IP_UTERINAS = new Variable("7", "99G2", new Long(7), "IPuterinas>95");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_AR = new Variable("8", "99G2", new Long(8), "AR");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_HTA = new Variable("9", "99G2", new Long(9), "HTA");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_APPE = new Variable("10", "99G2", new Long(10), "APPE");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_EPILEPSIA = new Variable("11", "99G2", new Long(11), "Epilepsia");
	public final Variable VAR_PARTO_GESTA_PAT_MAT_FETOMUERTO = new Variable("12", "99G2", new Long(12), "Feto muerto");

	public final Variable VAR_PARTO_GESTA_PAT_MAT_OTROS = new Variable("13994581", "99G2", new Long(13994581),
			"PatMatOtros");

	public final Variable VAR_PARTO_GESTA_ALERGIA = new Variable("106190000", "99G2", new Long(13524547), "Alergia");

	public final Variable VAR_PARTO_GESTA_PAT_FET_NO = new Variable("13994582", "99G2", new Long(13994582), "PatFetNo");
	public final Variable VAR_PARTO_GESTA_PAT_FET_CRECIMI = new Variable("13994583", "99G2", new Long(13994583),
			"PatFetCreci");
	public final Variable VAR_PARTO_GESTA_PAT_FET_LIQADMI = new Variable("13994598", "99G2", new Long(13994598),
			"PatFetLiaAqdmi");
	public final Variable VAR_PARTO_GESTA_PAT_FET_MALFORMACIÓN = new Variable("13", "99G2", new Long(13),
			"Malformación");
	public final Variable VAR_PARTO_GESTA_PAT_FET_HIDRAMNIOS = new Variable("14", "99G2", new Long(14), "Hidramnios");
	public final Variable VAR_PARTO_GESTA_PAT_FET_OLIGOAMNIOS = new Variable("15", "99G2", new Long(15), "Hidramnios");
	public final Variable VAR_PARTO_GESTA_PAT_FET_OTROS = new Variable("13994584", "99G2", new Long(13994584),
			"PatFetOtros");

	public final Variable VAR_PARTO_GESTA_ECO20 = new Variable("13994585", "99G2", new Long(13994585), "ECO20");

	public final Variable VAR_PARTO_GESTA_SERO1_RUBE = new Variable("13994586", "99G2", new Long(13994586),
			"Sero1 Rubeola");
	public final Variable VAR_PARTO_GESTA_SERO1_SIFI = new Variable("13994587", "99G2", new Long(13994587),
			"Sero1 Sífilis");
	public final Variable VAR_PARTO_GESTA_SERO1_TOXO = new Variable("13994588", "99G2", new Long(13994588),
			"Sero1 Toxo");
	public final Variable VAR_PARTO_GESTA_SERO1_VHB = new Variable("13994589", "99G2", new Long(13994589), "Sero1 VHB");
	public final Variable VAR_PARTO_GESTA_SERO1_VHC = new Variable("13994590", "99G2", new Long(13994590), "Sero1 VHC");
	public final Variable VAR_PARTO_GESTA_SERO1_VIH = new Variable("13994591", "99G2", new Long(13994591), "Sero1 VIH");

	public final Variable VAR_PARTO_GESTA_SERO3_RUBE = new Variable("13994592", "99G2", new Long(13994592),
			"Sero3 Rubeola");
	public final Variable VAR_PARTO_GESTA_SERO3_SIFI = new Variable("13994593", "99G2", new Long(13994593),
			"Sero3 Sífilis");
	public final Variable VAR_PARTO_GESTA_SERO3_TOXO = new Variable("13994594", "99G2", new Long(13994594),
			"Sero Toxo");
	public final Variable VAR_PARTO_GESTA_SERO3_VHB = new Variable("13994595", "99G2", new Long(13994595), "Sero3 VHB");
	public final Variable VAR_PARTO_GESTA_SERO3_VHC = new Variable("13994596", "99G2", new Long(13994596), "Sero3 VHC");
	public final Variable VAR_PARTO_GESTA_SERO3_VIH = new Variable("13994597", "99G2", new Long(13994597), "Sero3 VIH");

	public final Variable VAR_PARTO_GESTA_SGB_FECHA = new Variable("13994599", "99G2", new Long(13994599), "SGB Fecha");
	public final Variable VAR_PARTO_GESTA_SGB_VALOR = new Variable("13994600", "99G2", new Long(13994600), "SGB Valor");

	public final Variable VAR_PARTO_GESTA_TECREPUASIST = new Variable("16", "99G2", new Long(16),
			"Técnicas de reproducción asistida");

	public final Variable VAR_PARTO_GESTA_GEMELAR = new Variable("65147003", "SNM3", new Long(35004087), "Gemalar");

	public final Variable VAR_PARTO_GESTA_NUMEROFETOS = new Variable("246435002", "SNM3", new Long(35000247),
			"Nº Fetos");

	public RegistroPartoGestacion() {
		super();
		this.iniciaGestacion();
	}

	public RegistroPartoGestacion(Long id) {
		super(id);
		this.iniciaGestacion();
	}

	public RegistroPartoGestacion(RegistroPartoGestacion r) {
		super(r);
		this.edad = r.getEdad();
		this.edadGestacional = r.getEdadGestacional();
		this.fechaUltimaRegla = r.getFechaUltimaRegla();
		this.fechaprobableParto = r.getFechaprobableParto();

		this.paridad = r.getParidad();
		this.grupoABO = r.getGrupoABO();

		this.patoMatNo = r.getPatoMatNo();
		this.patoMatTrombofilia = r.getPatoMatTrombofilia();
		this.patoMatDiabetesGestional = r.getPatoMatDiabetesGestional();
		this.TVP = r.getTVP();
		this.DMPG = r.getDMPG();
		this.DMG = r.getDMG();
		this.EII = r.getEII();
		this.reumatogicas = r.reumatogicas;
		this.cancer = r.getCancer();
		this.IPuterinas_95 = r.getIPuterinas_95();
		this.AR = r.getAR();
		this.HTA = r.getHTA();
		this.APPE = r.getAPPE();
		this.Epilepsia = r.getEpilepsia();
		this.fetomuerto = r.getFetomuerto();
		this.patoMatOtros = r.getPatoMatOtros();

		this.alergia = r.getAlergia();

		this.patoFetoNo = r.getPatoFetoNo();
		this.patoFetoDefCremiento = r.getPatoFetoDefCremiento();
		this.patoFetoAlteracionLiq = r.getPatoFetoAlteracionLiq();
		this.patoFetoMalformacion = r.getPatoFetoMalformacion();
		this.patoFetoHidramnios = r.getPatoFetoHidramnios();
		this.patoFetoOligoamnios = r.getPatoFetoOligoamnios();
		this.patoFetoOtros = r.getPatoFetoOtros();

		this.eco20semanas = r.getEco20semanas();

		this.sero1TRubeola = r.getSero1TRubeola();
		this.sero1Tsifilis = r.getSero1Tsifilis();
		this.sero1TToxo = r.getSero1TToxo();
		this.sero1TVHB = r.getSero1TVHB();
		this.sero1TVHC = r.getSero1TVHC();
		this.sero1TVIH = r.getSero1TVIH();

		this.sero3TRubeola = r.getSero3TRubeola();
		this.sero3Tsifilis = r.getSero3Tsifilis();
		this.sero3TToxo = r.getSero3TToxo();
		this.sero3TVHB = r.getSero3TVHB();
		this.sero3TVHC = r.getSero3TVHC();
		this.sero3TVIH = r.getSero3TVIH();

		this.esteGrupoBFecha = r.getEsteGrupoBFecha();
		this.esteGrupoBValor = r.getEsteGrupoBValor();
		this.tecReproAsist = r.getTecReproAsist();
		this.gemelar = r.getGemelar();
		this.numeroFetos = r.getNumeroFetos();

	}

	public void iniciaGestacion() {
		this.setTiporegistro(RegistroPartoGestacion.TIPO_REGISTRO_PARTO);
		this.setDescripcion("1.-Registro Parto Gestación");
		this.setPlantilla_edior(RegistroPartoGestacion.PLANTILLLA_EDITOR_PAR_GESTACION);
		this.setServicio(new Servicio(new Long(40), "OBS", "Obstetricia y Ginecologia"));

		this.edad = this.VAR_PARTO_GESTA_EDAD;
		this.edadGestacional = this.VAR_PARTO_GESTA_EGESTAIONAL;
		this.fechaUltimaRegla = this.VAR_PARTO_GESTA_FUR;
		this.fechaprobableParto = this.VAR_PARTO_GESTA_FPP;

		this.paridad = this.VAR_PARTO_GESTA_PARIDAD;

		this.grupoABO = this.VAR_PARTO_GESTA_GABO;

		this.patoMatNo = this.VAR_PARTO_GESTA_PAT_MAT_NO;
		this.patoMatTrombofilia = this.VAR_PARTO_GESTA_PAT_MAT_TROMBO;
		this.patoMatDiabetesGestional = this.VAR_PARTO_GESTA_PAT_MAT_DIABE;
		this.TVP = this.VAR_PARTO_GESTA_PAT_MAT_TVP;
		this.DMPG = this.VAR_PARTO_GESTA_PAT_MAT_DMPG;
		this.DMG = this.VAR_PARTO_GESTA_PAT_MAT_DMP;
		this.EII = this.VAR_PARTO_GESTA_PAT_MAT_EII;
		this.reumatogicas = this.VAR_PARTO_GESTA_PAT_MAT_REUMATOL;
		this.cancer = this.VAR_PARTO_GESTA_PAT_MAT_CANCER;
		this.IPuterinas_95 = this.VAR_PARTO_GESTA_PAT_MAT_IP_UTERINAS;
		this.AR = this.VAR_PARTO_GESTA_PAT_MAT_AR;
		this.HTA = this.VAR_PARTO_GESTA_PAT_MAT_HTA;
		this.APPE = this.VAR_PARTO_GESTA_PAT_MAT_APPE;
		this.Epilepsia = this.VAR_PARTO_GESTA_PAT_MAT_EPILEPSIA;
		this.fetomuerto = this.VAR_PARTO_GESTA_PAT_MAT_FETOMUERTO;
		this.patoMatOtros = this.VAR_PARTO_GESTA_PAT_MAT_OTROS;

		this.alergia = this.VAR_PARTO_GESTA_ALERGIA;

		this.patoFetoNo = this.VAR_PARTO_GESTA_PAT_FET_NO;
		this.patoFetoDefCremiento = this.VAR_PARTO_GESTA_PAT_FET_CRECIMI;
		this.patoFetoAlteracionLiq = this.VAR_PARTO_GESTA_PAT_FET_LIQADMI;
		this.patoFetoMalformacion = this.VAR_PARTO_GESTA_PAT_FET_MALFORMACIÓN;
		this.patoFetoHidramnios = this.VAR_PARTO_GESTA_PAT_FET_HIDRAMNIOS;
		this.patoFetoOligoamnios = this.VAR_PARTO_GESTA_PAT_FET_OLIGOAMNIOS;
		this.patoFetoOtros = this.VAR_PARTO_GESTA_PAT_FET_OTROS;

		this.eco20semanas = this.VAR_PARTO_GESTA_ECO20;

		this.sero1TRubeola = this.VAR_PARTO_GESTA_SERO1_RUBE;
		this.sero1Tsifilis = this.VAR_PARTO_GESTA_SERO1_SIFI;
		this.sero1TToxo = this.VAR_PARTO_GESTA_SERO1_TOXO;
		this.sero1TVHB = this.VAR_PARTO_GESTA_SERO1_VHB;
		this.sero1TVHC = this.VAR_PARTO_GESTA_SERO1_VHC;
		this.sero1TVIH = this.VAR_PARTO_GESTA_SERO1_VIH;

		this.sero3TRubeola = this.VAR_PARTO_GESTA_SERO3_RUBE;
		this.sero3Tsifilis = this.VAR_PARTO_GESTA_SERO3_SIFI;
		this.sero3TToxo = this.VAR_PARTO_GESTA_SERO3_TOXO;
		this.sero3TVHB = this.VAR_PARTO_GESTA_SERO3_VHB;
		this.sero3TVHC = this.VAR_PARTO_GESTA_SERO3_VHC;
		this.sero3TVIH = this.VAR_PARTO_GESTA_SERO3_VIH;

		this.esteGrupoBFecha = this.VAR_PARTO_GESTA_SGB_FECHA;
		this.esteGrupoBValor = this.VAR_PARTO_GESTA_SGB_VALOR;

		this.tecReproAsist = this.VAR_PARTO_GESTA_TECREPUASIST;
		this.gemelar = this.VAR_PARTO_GESTA_GEMELAR;
		this.numeroFetos = this.VAR_PARTO_GESTA_NUMEROFETOS;
	}

	public Variable getEdad() {
		return edad;
	}

	public Variable getVariableEdad() {
		return edad;
	}

	public String getEdadString() {
		return edad.getValor();
	}

	public void setEdad(Variable edad) {
		this.edad = edad;
	}

	public void setEdad(String valor) {
		this.edad.setValor(valor);
	}

	public void setEdad(int valor) {
		this.edad.setValor(Integer.toString(valor));
	}

	public Variable getEdadGestacional() {
		return edadGestacional;
	}

	public String getEdadGestacionalString() {
		return edadGestacional.getValor();
	}

	public Variable getVariableEdadGestacional() {
		return edadGestacional;
	}

	public void setEdadGestacional(Variable edadGestacional) {
		this.edadGestacional = edadGestacional;
	}

	public void setEdadGestacional(String valor) {
		this.edadGestacional.setValor(valor);
	}

	public Variable getFechaUltimaRegla() {
		return fechaUltimaRegla;
	}

	public LocalDate getFechaUltimaReglaDate() {
		LocalDate fecha = null;
		if (this.fechaUltimaRegla != null) {
			if (this.fechaUltimaRegla.getValor() != null && !this.fechaUltimaRegla.getValor().isEmpty()) {
				if (Utilidades.isNumero(this.fechaUltimaRegla.getValor())) {
					fecha = Utilidades.getFechaLocalDate(this.fechaUltimaRegla.getValor());
				}
			}
		}
		return fecha;
	}

	public Variable getVariableFechaUltimaRegla() {
		return fechaUltimaRegla;
	}

	public void setFechaUltimaRegla(Variable fechaUltimaRegla) {
		this.fechaUltimaRegla = fechaUltimaRegla;
	}

	public void setFechaUltimaRegla(String valor) {
		this.fechaUltimaRegla.setValor(valor);
	}

	public void setFechaUltimaRegla(LocalDate fecha) {
		this.fechaUltimaRegla.setValor(Long.toString(Utilidades.getFechaNumeroyyymmddDefecha(fecha)));
	}

	public Variable getFechaprobableParto() {
		return fechaprobableParto;
	}

	public Variable getVariableFechaprobableParto() {
		return fechaprobableParto;
	}

	public LocalDate getFechaprobablePartoDate() {
		LocalDate fecha = null;
		if (this.fechaprobableParto != null) {
			if (this.fechaprobableParto.getValor() != null && !this.fechaprobableParto.getValor().isEmpty()) {
				if (Utilidades.isNumero(this.fechaprobableParto.getValor())) {
					fecha = Utilidades.getFechaLocalDate(Long.parseLong(this.fechaprobableParto.getValor()));
				}
			}
		}
		return fecha;
	}

	public void setFechaprobableParto(Variable fechaprobableParto) {
		this.fechaprobableParto = fechaprobableParto;
	}

	public void setFechaprobableParto(String valor) {
		this.fechaprobableParto.setValor(valor);
	}

	public void setFechaprobableParto(LocalDate fecha) {
		this.fechaprobableParto.setValor(Long.toString(Utilidades.getFechaNumeroyyymmddDefecha(fecha)));
	}

	public Variable getParidad() {
		return paridad;
	}

	public String getParidadString() {
		return paridad.getValor();
	}

	public Variable getVariableParidad() {
		return paridad;
	}

	public void setParidad(Variable paridad) {
		this.paridad = paridad;
	}

	public void setParidad(String valor) {
		this.paridad.setValor(valor);
	}

	public Variable getGrupoABO() {
		return grupoABO;
	}

	public Variable getVariableGrupoABO() {
		return grupoABO;
	}

	public String getGrupoABOString() {
		return grupoABO.getValor();
	}

	public void setGrupoABO(Variable grupoABO) {
		this.grupoABO = grupoABO;
	}

	public void setGrupoABO(String valor) {
		this.grupoABO.setValor(valor);
	}

	public Variable getPatoMatNo() {
		return patoMatNo;
	}

	public Variable getVariablePatoMatNo() {
		return patoMatNo;
	}

	public String getPatoMatNoString() {
		return patoMatNo.getValor();
	}

	public Boolean getPatoMatNoBoolean() {
		if (patoMatNo != null && patoMatNo.getValor() != null && !patoMatNo.getValor().isEmpty())
			return true;

		else
			return false;
	}

	public void setPatoMatNo(Variable patoMatNo) {
		this.patoMatNo = patoMatNo;
	}

	public void setPatoMatNo(String valor) {
		this.patoMatNo.setValor(valor);
	}

	public void setPatoMatNo(Boolean valor) {
		if (valor == true)
			this.patoMatNo.setValor("No patología madre");
		else
			this.patoMatNo.setValor(null);
	}

	public Variable getPatoMatTrombofilia() {
		return patoMatTrombofilia;
	}

	public Variable getVariablePatoMatTrombofilia() {
		return patoMatTrombofilia;
	}

	public Boolean getPatoMatTrombofiliaBoolean() {
		if (patoMatTrombofilia != null && patoMatTrombofilia.getValor() != null
				&& !patoMatTrombofilia.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public String getPatoMatTrombofiliaString() {
		return patoMatTrombofilia.getValor();
	}

	public void setPatoMatTrombofilia(Variable patoMatTrombofilia) {
		this.patoMatTrombofilia = patoMatTrombofilia;
	}

	public void setPatoMatTrombofilia(String valor) {
		this.patoMatTrombofilia.setValor(valor);
	}

	public void setPatoMatTrombofilia(Boolean valor) {
		if (valor == true)
			this.patoMatTrombofilia.setValor("Trombofilia");
		else
			this.patoMatTrombofilia.setValor(null);
	}

	public Variable getPatoMatDiabetesGestional() {
		return patoMatDiabetesGestional;
	}

	public Variable getVariablePatoMatDiabetesGestional() {
		return patoMatDiabetesGestional;
	}

	public Boolean getPatoMatDiabetesGestionalBoolean() {
		if (patoMatDiabetesGestional != null && patoMatDiabetesGestional.getValor() != null
				&& !patoMatDiabetesGestional.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoMatDiabetesGestional(Variable patoMatDiabetesGestional) {
		this.patoMatDiabetesGestional = patoMatDiabetesGestional;
	}

	public String getVariablePatoMatDiabetesGestionalString() {
		return patoMatDiabetesGestional.getValor();
	}

	public void setPatoMatDiabetesGestional(String valor) {
		this.patoMatDiabetesGestional.setValor(valor);
	}

	public void setPatoMatDiabetesGestional(Boolean valor) {
		if (valor == true)
			this.patoMatDiabetesGestional.setValor("Diabetes gestacional");
		else
			this.patoMatDiabetesGestional.setValor(null);
	}

	public Variable getTVP() {
		return TVP;
	}

	public Variable getVariableTVP() {
		return TVP;
	}

	public Boolean getTVPBoolean() {
		if (TVP != null && TVP.getValor() != null && !TVP.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setTVP(Variable tVP) {
		TVP = tVP;
	}

	public void setTVP(Boolean valor) {
		if (valor == true)
			TVP.setValor("TVP");
		else
			TVP.setValor("");
	}

	public Variable getDMPG() {
		return DMPG;
	}

	public Variable getVariableDMPG() {
		return DMPG;
	}

	public Boolean getDMPGBooelan() {
		if (DMPG != null && DMPG.getValor() != null && !DMPG.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setDMPG(Variable dMPG) {
		DMPG = dMPG;
	}

	public void setDMPG(Boolean valor) {
		if (valor == true)
			DMPG.setValor("DMPG");
		else
			DMPG.setValor("");
	}

	public Variable getDMG() {
		return DMG;
	}

	public Variable getVariableDMG() {
		return DMG;
	}

	public Boolean getDMGBooelan() {
		if (DMG != null && DMG.getValor() != null && !DMG.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setDMG(Variable dMG) {
		DMG = dMG;
	}

	public void setDMG(Boolean valor) {
		if (valor == true)
			DMG.setValor("DMG");
		else
			DMG.setValor("");
	}

	public Variable getEII() {
		return EII;
	}

	public Variable getVariableEII() {
		return EII;
	}

	public Boolean getEIIBoolean() {
		if (EII != null && EII.getValor() != null && !EII.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setEII(Variable eII) {
		EII = eII;
	}

	public void setEII(Boolean valor) {
		if (valor == true)
			EII.setValor("EII");
		else
			EII.setValor("");
	}

	public Variable getReumatogicas() {
		return reumatogicas;
	}

	public Variable getVariableReumatogicas() {
		return reumatogicas;
	}

	public Boolean getReumatogicasBoolean() {
		if (reumatogicas != null && reumatogicas.getValor() != null && !reumatogicas.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setReumatogicas(Variable reumatogicas) {
		this.reumatogicas = reumatogicas;
	}

	public void setReumatogicas(Boolean valor) {
		if (valor == true)
			reumatogicas.setValor("reumatogicas");
		else
			reumatogicas.setValor("");
	}

	public Variable getCancer() {
		return cancer;
	}

	public Variable getVariableCancer() {
		return cancer;
	}

	public Boolean getCancerBoolean() {
		if (cancer != null && cancer.getValor() != null && !cancer.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setCancer(Variable cancer) {
		this.cancer = cancer;
	}

	public void setCancer(Boolean valor) {
		if (valor == true)
			cancer.setValor("cancer");
		else
			cancer.setValor("");
	}

	public Variable getIPuterinas_95() {
		return IPuterinas_95;
	}

	public Variable getVariableIPuterinas_95() {
		return IPuterinas_95;
	}

	public Boolean getIPuterinas_95Boolean() {
		if (IPuterinas_95 != null && IPuterinas_95.getValor() != null && !IPuterinas_95.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setIPuterinas_95(Variable iPuterinas_95) {
		IPuterinas_95 = iPuterinas_95;
	}

	public void setIPuterinas_95(Boolean valor) {
		if (valor == true)
			IPuterinas_95.setValor("IPuterinas>95");
		else
			IPuterinas_95.setValor("");
	}

	public Variable getAR() {
		return AR;
	}

	public Variable getVariableAR() {
		return AR;
	}

	public Boolean getARBoolean() {
		if (AR != null && AR.getValor() != null && !AR.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setAR(Variable aR) {
		AR = aR;
	}

	public void setAR(Boolean valor) {
		if (valor == true)
			AR.setValor("AR>95");
		else
			AR.setValor("");
	}

	public Variable getHTA() {
		return HTA;
	}

	public Variable getVariableHTA() {
		return HTA;
	}

	public Boolean getHTABoolean() {
		if (HTA != null && HTA.getValor() != null && !HTA.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setHTA(Variable hTA) {
		HTA = hTA;
	}

	public void setHTA(Boolean valor) {
		if (valor == true)
			HTA.setValor("HTA");
		else
			HTA.setValor("");
	}

	public Variable getAPPE() {
		return APPE;
	}

	public Variable getVariableAPPE() {
		return APPE;
	}

	public Boolean getAPPEBoolean() {
		if (APPE != null && APPE.getValor() != null && !APPE.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setAPPE(Variable aPPE) {
		APPE = aPPE;
	}

	public void setAPPE(Boolean valor) {
		if (valor == true)
			APPE.setValor("APPE");
		else
			APPE.setValor("");
	}

	public Variable getEpilepsia() {
		return Epilepsia;
	}

	public Variable getVariableEpilepsia() {
		return Epilepsia;
	}

	public Boolean getEpilepsiaBoolean() {
		if (Epilepsia != null && Epilepsia.getValor() != null && !Epilepsia.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setEpilepsia(Variable epilepsia) {
		Epilepsia = epilepsia;
	}

	public void setEpilepsia(Boolean valor) {
		if (valor == true)
			Epilepsia.setValor("Epilepsia");
		else
			Epilepsia.setValor("");
	}

	public Variable getFetomuerto() {
		return fetomuerto;
	}

	public Variable getVariableFetomuerto() {
		return fetomuerto;
	}

	public Boolean getFetomuertoBoolean() {
		if (fetomuerto != null && fetomuerto.getValor() != null && !fetomuerto.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setFetomuerto(Variable fetomuerto) {
		this.fetomuerto = fetomuerto;
	}

	public void setFetomuerto(Boolean valor) {
		if (valor == true)
			fetomuerto.setValor("fetomuerto");
		else
			fetomuerto.setValor("");
	}

	public Variable getPatoMatOtros() {
		return patoMatOtros;
	}

	public Variable getVariablePatoMatOtros() {
		return patoMatOtros;
	}

	public String getPatoMatOtrosString() {
		return patoMatOtros.getValor();
	}

	public void setPatoMatOtros(Variable patoMatOtros) {
		this.patoMatOtros = patoMatOtros;
	}

	public void setPatoMatOtros(String valor) {
		this.patoMatOtros.setValor(valor);
	}

	public Variable getAlergia() {
		return alergia;
	}

	public Variable getVariableAlergia() {
		return alergia;
	}

	public String getAlergiaString() {
		return alergia.getValor();
	}

	public void setAlergia(Variable alergia) {
		this.alergia = alergia;
	}

	public void setAlergia(String valor) {
		this.alergia.setValor(valor);
	}

	public Variable getPatoFetoNo() {
		return patoFetoNo;
	}

	public Variable getVariablePatoFetoNo() {
		return patoFetoNo;
	}

	public void setPatoFetoNo(Variable patoFetoNo) {
		this.patoFetoNo = patoFetoNo;
	}

	public Boolean getPatoFetoNoBoolean() {
		if (patoFetoNo != null && patoFetoNo.getValor() != null && !patoFetoNo.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoNo(String valor) {
		this.patoFetoNo.setValor(valor);
	}

	public void setPatoFetoNo(Boolean valor) {
		if (valor == true) {
			this.patoFetoNo.setValor("No patología fetal");
		} else {
			this.patoFetoNo.setValor(null);
		}
	}

	public Variable getPatoFetoDefCremiento() {
		return patoFetoDefCremiento;
	}

	public Variable getVariablePatoFetoDefCremiento() {
		return patoFetoDefCremiento;
	}

	public Boolean getPatoFetoDefCremientoBoolean() {
		if (patoFetoDefCremiento != null && patoFetoDefCremiento.getValor() != null
				&& !patoFetoDefCremiento.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoDefCremiento(Variable patoFetoDefCremiento) {
		this.patoFetoDefCremiento = patoFetoDefCremiento;
	}

	public void setPatoFetoDefCremiento(String valor) {
		this.patoFetoDefCremiento.setValor(valor);
	}

	public void setPatoFetoDefCremiento(Boolean valor) {
		if (valor == true) {
			this.patoFetoDefCremiento.setValor("Déficit de crecimiento");
		} else {
			this.patoFetoDefCremiento.setValor(null);
		}
	}

	public Variable getPatoFetoAlteracionLiq() {
		return patoFetoAlteracionLiq;
	}

	public Variable getVariablePatoFetoAlteracionLiq() {
		return patoFetoAlteracionLiq;
	}

	public Boolean getPatoFetoAlteracionLiqBoolean() {
		if (patoFetoAlteracionLiq != null && patoFetoAlteracionLiq.getValor() != null
				&& !patoFetoAlteracionLiq.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoAlteracionLiq(Variable patoFetoAlteracionLiq) {
		this.patoFetoAlteracionLiq = patoFetoAlteracionLiq;
	}

	public void setPatoFetoAlteracionLiq(String valor) {
		this.patoFetoAlteracionLiq.setValor(valor);
	}

	public void setPatoFetoAlteracionLiq(Boolean valor) {
		if (valor == true) {
			this.patoFetoAlteracionLiq.setValor("Alteración líquido amniótico ");
		} else {
			this.patoFetoAlteracionLiq.setValor(null);
		}
	}

	public Variable getPatoFetoMalformacion() {
		return patoFetoMalformacion;
	}

	public Variable getVariablePatoFetoMalformacion() {
		return patoFetoMalformacion;
	}

	public Boolean getPatoFetoMalformacionBoolean() {
		if (patoFetoMalformacion != null && patoFetoMalformacion.getValor() != null
				&& !patoFetoMalformacion.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoMalformacion(Variable patoFetoMalformacion) {
		this.patoFetoMalformacion = patoFetoMalformacion;
	}

	public void setPatoFetoMalformacion(Boolean valor) {
		if (valor == true) {
			this.patoFetoMalformacion.setValor("Malformación");
		} else {
			this.patoFetoMalformacion.setValor(null);
		}
	}

	public Variable getPatoFetoHidramnios() {
		return patoFetoHidramnios;
	}

	public Variable getVariablePatoFetoHidramnios() {
		return patoFetoHidramnios;
	}

	public Boolean getPatoFetoHidramniosoBoolean() {
		if (patoFetoHidramnios != null && patoFetoHidramnios.getValor() != null
				&& !patoFetoHidramnios.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoHidramnios(Variable patoFetoHidramnios) {
		this.patoFetoHidramnios = patoFetoHidramnios;
	}

	public void setPatoFetoHidramnios(Boolean valor) {
		if (valor == true) {
			this.patoFetoHidramnios.setValor("Hidramnios");
		} else {
			this.patoFetoHidramnios.setValor(null);
		}
	}

	public Variable getPatoFetoOligoamnios() {
		return patoFetoOligoamnios;
	}

	public Variable getVariablePatoFetoOligoamnios() {
		return patoFetoOligoamnios;
	}

	public Boolean getPatoFetoOligoamniosolBoolean() {
		if (patoFetoOligoamnios != null && patoFetoOligoamnios.getValor() != null
				&& !patoFetoOligoamnios.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPatoFetoOligoamnios(Variable patoFetoOligoamnios) {
		this.patoFetoOligoamnios = patoFetoOligoamnios;
	}

	public void setPatoFetoOligoamnios(Boolean valor) {
		if (valor == true) {
			this.patoFetoOligoamnios.setValor("Oligoamnios");
		} else {
			this.patoFetoOligoamnios.setValor(null);
		}
	}

	public Variable getPatoFetoOtros() {
		return patoFetoOtros;
	}

	public Variable getVariablePatoFetoOtros() {
		return patoFetoOtros;
	}

	public String getPatoFetoOtrosString() {
		return patoFetoOtros.getValor();
	}

	public void setPatoFetoOtros(Variable patoFetoOtros) {
		this.patoFetoOtros = patoFetoOtros;
	}

	public void setPatoFetoOtros(String valor) {
		this.patoFetoOtros.setValor(valor);
	}

	public Variable getEco20semanas() {
		return eco20semanas;
	}

	public Variable getVariableEco20semanas() {
		return eco20semanas;
	}

	public String getEco20semanasString() {
		return eco20semanas.getValor();
	}

	public void setEco20semanas(String valor) {
		this.eco20semanas.setValor(valor);
	}

	public void setEco20semanas(Variable eco20semanas) {
		this.eco20semanas = eco20semanas;
	}

	public void setVariableEco20semanas(Variable eco20semanas) {
		this.eco20semanas = eco20semanas;
	}

	public Variable getSero1TRubeola() {
		return sero1TRubeola;
	}

	public Variable getVariableSero1TRubeola() {
		return sero1TRubeola;
	}

	public String getSero1TRubeolaString() {
		return sero1TRubeola.getValor();
	}

	public void setSero1TRubeola(Variable sero1tRubeola) {
		sero1TRubeola = sero1tRubeola;
	}

	public void setSero1TRubeola(String valor) {
		sero1TRubeola.setValor(valor);
	}

	public Variable getSero1Tsifilis() {
		return sero1Tsifilis;
	}

	public Variable getVariableSero1Tsifilis() {
		return sero1Tsifilis;
	}

	public String getSero1TsifilisString() {
		return sero1Tsifilis.getValor();
	}

	public void setSero1Tsifilis(Variable sero1Tsifilis) {
		this.sero1Tsifilis = sero1Tsifilis;
	}

	public void setSero1Tsifilis(String valor) {
		this.sero1Tsifilis.setValor(valor);
	}

	public Variable getSero1TToxo() {
		return sero1TToxo;
	}

	public Variable getVariableSero1TToxo() {
		return sero1TToxo;
	}

	public String getSero1TToxoString() {
		return sero1TToxo.getValor();
	}

	public void setSero1TToxo(Variable sero1tToxo) {
		sero1TToxo = sero1tToxo;
	}

	public void setSero1TToxo(String valor) {
		sero1TToxo.setValor(valor);
	}

	public Variable getSero1TVHB() {
		return sero1TVHB;
	}

	public Variable getVariableSero1TVHB() {
		return sero1TVHB;
	}

	public String getSero1TVHBString() {
		return sero1TVHB.getValor();
	}

	public void setSero1TVHB(Variable sero1tvhb) {
		sero1TVHB = sero1tvhb;
	}

	public void setSero1TVHB(String valor) {
		sero1TVHB.setValor(valor);
	}

	public Variable getSero1TVHC() {
		return sero1TVHC;
	}

	public Variable getVariableSero1TVHC() {
		return sero1TVHC;
	}

	public String getSero1TVHCString() {
		return sero1TVHC.getValor();
	}

	public void setSero1TVHC(Variable sero1tvhc) {
		sero1TVHC = sero1tvhc;
	}

	public void setSero1TVHC(String valor) {
		sero1TVHC.setValor(valor);
	}

	public Variable getSero1TVIH() {
		return sero1TVIH;
	}

	public Variable getVariableSero1TVIH() {
		return sero1TVIH;
	}

	public String getSero1TVIHString() {
		return sero1TVIH.getValor();
	}

	public void setSero1TVIH(Variable sero1tvih) {
		sero1TVIH = sero1tvih;
	}

	public void setSero1TVIH(String valor) {
		sero1TVIH.setValor(valor);
	}

	public Variable getSero3TRubeola() {
		return sero3TRubeola;
	}

	public Variable getVariableSero3TRubeola() {
		return sero3TRubeola;
	}

	public String getSero3TRubeolaString() {
		return sero3TRubeola.getValor();
	}

	public void setSero3TRubeola(Variable sero3tRubeola) {
		sero3TRubeola = sero3tRubeola;
	}

	public void setSero3TRubeola(String valor) {
		sero3TRubeola.setValor(valor);
	}

	public Variable getSero3Tsifilis() {
		return sero3Tsifilis;
	}

	public Variable getVariableSero3Tsifilis() {
		return sero3Tsifilis;
	}

	public String getSero3TsifilisString() {
		return sero3Tsifilis.getValor();
	}

	public void setSero3Tsifilis(Variable variable) {
		this.sero3Tsifilis = variable;
	}

	public void setSero3Tsifilis(String valor) {
		this.sero3Tsifilis.setValor(valor);
	}

	public Variable getSero3TToxo() {
		return sero3TToxo;
	}

	public Variable getVariableSero3TToxo() {
		return sero3TToxo;
	}

	public String getSero3TToxoString() {
		return sero3TToxo.getValor();
	}

	public void setSero3TToxo(Variable sero3tToxo) {
		sero3TToxo = sero3tToxo;
	}

	public void setSero3TToxo(String valor) {
		sero3TToxo.setValor(valor);
	}

	public Variable getSero3TVHB() {
		return sero3TVHB;
	}

	public Variable getVariableSero3TVHB() {
		return sero3TVHB;
	}

	public String getSero3TVHBString() {
		return sero3TVHB.getValor();
	}

	public void setSero3TVHB(Variable sero3tvhb) {
		sero3TVHB = sero3tvhb;
	}

	public void setSero3TVHB(String valor) {
		sero3TVHB.setValor(valor);
	}

	public Variable getSero3TVHC() {
		return sero3TVHC;
	}

	public Variable getVariableSero3TVHC() {
		return sero3TVHC;
	}

	public String getSero3TVHCString() {
		return sero3TVHC.getValor();
	}

	public void setSero3TVHC(Variable sero3tvhc) {
		sero3TVHC = sero3tvhc;
	}

	public void setSero3TVHC(String valor) {
		sero3TVHC.setValor(valor);
	}

	public Variable getSero3TVIH() {
		return sero3TVIH;
	}

	public Variable getVariableSero3TVIH() {
		return sero3TVIH;
	}

	public String getSero3TVIHString() {
		return sero3TVIH.getValor();
	}

	public void setSero3TVIH(Variable sero3tvih) {
		sero3TVIH = sero3tvih;
	}

	public void setSero3TVIH(String valor) {
		sero3TVIH.setValor(valor);
	}

	public Variable getEsteGrupoBFecha() {
		return esteGrupoBFecha;
	}

	public Variable getVariableEsteGrupoBFecha() {
		return esteGrupoBFecha;
	}

	public LocalDate getEsteGrupoBFechaDate() {
		LocalDate fecha = null;
		if (this.esteGrupoBFecha != null) {
			if (this.esteGrupoBFecha.getValor() != null && !this.esteGrupoBFecha.getValor().isEmpty()) {
				if (Utilidades.isNumero(this.esteGrupoBFecha.getValor())) {
					fecha = Utilidades.getFechaLocalDate(Long.parseLong(this.esteGrupoBFecha.getValor()));
				}
			}
		}
		return fecha;
	}

	public void setEsteGrupoBFecha(Variable esteGrupoBFecha) {
		this.esteGrupoBFecha = esteGrupoBFecha;
	}

	public void setEsteGrupoBFecha(String valor) {
		this.esteGrupoBFecha.setValor(valor);
	}

	public void setEsteGrupoBFecha(LocalDate fecha) {
		this.esteGrupoBFecha.setValor(Long.toString(Utilidades.getFechaNumeroyyymmddDefecha(fecha)));
	}

	public Variable getEsteGrupoBValor() {
		return esteGrupoBValor;
	}

	public Variable getVariableEsteGrupoBValor() {
		return esteGrupoBValor;
	}

	public String getEsteGrupoBValorString() {
		return esteGrupoBValor.getValor();
	}

	public void setEsteGrupoBValor(Variable esteGrupoBValor) {
		this.esteGrupoBValor = esteGrupoBValor;
	}

	public void setEsteGrupoBValor(String valor) {
		this.esteGrupoBValor.setValor(valor);
	}

	public Variable getTecReproAsist() {
		return tecReproAsist;
	}

	public Variable getVariableTecReproAsist() {
		return tecReproAsist;
	}

	public String getTecReproAsistString() {
		if (tecReproAsist != null)
			return tecReproAsist.getValor();
		else
			return "";
	}

	public void setTecReproAsist(Variable tecReproAsist) {
		this.tecReproAsist = tecReproAsist;
	}

	public void setTecReproAsist(String valor) {
		this.tecReproAsist.setValor(descripcion);
	}

	public Variable getGemelar() {
		return gemelar;
	}

	public Variable getVariableGemelar() {
		return gemelar;
	}

	public String getGemelarString() {
		return gemelar.getValor();
	}

	public void setGemelar(Variable gemelar) {
		this.gemelar = gemelar;
	}

	public void setGemelar(String valor) {
		this.gemelar.setValor(valor);
	}

	public Variable getNumeroFetos() {
		return numeroFetos;
	}

	public Variable getVariableNumeroFetos() {
		return numeroFetos;
	}

	public String getNumeroFetosString() {
		return numeroFetos.getValor();
	}

	public void setNumeroFetos(Variable numeroFetos) {
		this.numeroFetos = numeroFetos;
	}

	public void setNumeroFetos(String valor) {
		this.numeroFetos.setValor(valor);
		;
	}
}

/*
 * 
 * Select * from ( SELECT 1, code,codesystem, id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='SNM3' and
 * code='397669002' UNION SELECT 2,code,codesystem,id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='SNM3' and
 * code='57036006' UNION SELECT 3,code,codesystem,id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='SNM3' and
 * code='21840007' UNION SELECT 4, code,codesystem,id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='SNM3' and
 * code='161714006' UNION SELECT 5,code,codesystem,id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='SNM3' and
 * code='364325004' UNION SELECT 6,code,codesystem,id,descripcion FROM
 * catalogo,codigos WHERE catalogo.id=codigos.catalogo AND codesystem='LN' and
 * code='882-1'
 * 
 * UNION SELECT 7,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994579' UNION
 * SELECT 8,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994580' UNION
 * SELECT 9,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='SNM3' and code='11687002' UNION
 * SELECT 10,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994581' UNION
 * SELECT 11,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='SNM3' and code='106190000' UNION
 * SELECT 11,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994582' UNION
 * SELECT 12,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994583' UNION
 * SELECT 13,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994598' UNION
 * SELECT 14,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994584' UNION
 * SELECT 15,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994585' UNION
 * SELECT 16,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994586' UNION
 * SELECT 18,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994587' UNION
 * SELECT 19,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994588' UNION
 * SELECT 20,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994589' UNION
 * SELECT 21,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994590' UNION
 * SELECT 22,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994591'
 * 
 * UNION SELECT 23,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994592' UNION
 * SELECT 24,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994593' UNION
 * SELECT 25,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994594' UNION
 * SELECT 26,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994595' UNION
 * SELECT 27,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994596' UNION
 * SELECT 28,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994597' UNION
 * SELECT 29,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994599' UNION
 * SELECT 30,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='99G2' and code='13994600'
 * 
 * UNION SELECT 30,code,codesystem,id,descripcion FROM catalogo,codigos WHERE
 * catalogo.id=codigos.catalogo AND codesystem='SNM3' and code='65147003'
 * 
 * 
 * ) order by 1 ;
 * 
 * 
 */