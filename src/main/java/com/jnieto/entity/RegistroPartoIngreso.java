package com.jnieto.entity;

import java.time.LocalDate;

import com.jnieto.utilidades.Utilidades;

public class RegistroPartoIngreso extends Registro {

	private Variable fechaIngreso;
	private Variable horaIngreso;
	private Variable observador;
	private Variable tipoIngreso;
	private Variable situacion;
	private Variable presentacion;
	private Variable bishop;

	public final static Long PLANTILLLA_EDITOR_PAR_INGRESO = new Long(794856079);
	public final static Long TIPO_REGISTRO_PARTO = new Long(21);

	public final Variable VAR_PARTO_INGRESO_FECHA = new Variable("9709", "99G2", new Long(9709), "Fecha Ingreso");
	public final Variable VAR_PARTO_INGRESO_HORA = new Variable("9710", "99G2", new Long(9710), "Hora ingreso");
	public final Variable VAR_PARTO_INGRESO_OBSERVADOR = new Variable("13994601", "99G2", new Long(13994601),
			"Observador");
	public final Variable VAR_PARTO_INGRESO_TIPO_INGRESO = new Variable("13824485", "99G2", new Long(13824485),
			"Tipo ingreso");
	public final Variable VAR_PARTO_INGRESO_SITUACION = new Variable("13994602", "99G2", new Long(13994602),
			"Exploración Situación");
	public final Variable VAR_PARTO_INGRESO_PRESENTACION = new Variable("246105001", "SNM3", new Long(35003303),
			"Presentación");
	public final Variable VAR_PARTO_INGRESO_BISHOP = new Variable("249023008", "SNM3", new Long(13994603),
			"Escala Bishop ");

	public RegistroPartoIngreso() {
		super();
		iniciaIngreso();
	}

	public RegistroPartoIngreso(Long id) {
		super(id);
		iniciaIngreso();
	}

	public RegistroPartoIngreso(RegistroPartoIngreso r) {
		super(r);
		this.fechaIngreso = r.fechaIngreso;
		this.horaIngreso = r.horaIngreso;
		this.observador = r.observador;
		this.tipoIngreso = r.tipoIngreso;
		this.situacion = r.tipoIngreso;
		this.presentacion = r.presentacion;
		this.bishop = r.bishop;
	}

	public void iniciaIngreso() {
		this.setTiporegistro(RegistroPartoIngreso.TIPO_REGISTRO_PARTO);
		this.setDescripcion("2.-Registro Parto Ingreso.");
		this.setPlantilla_edior(RegistroPartoIngreso.PLANTILLLA_EDITOR_PAR_INGRESO);
		this.setServicio(new Servicio(new Long(40), "OBS", "Obstetricia y Ginecologia"));
		this.fechaIngreso = this.VAR_PARTO_INGRESO_FECHA;
		this.horaIngreso = this.VAR_PARTO_INGRESO_HORA;
		this.observador = this.VAR_PARTO_INGRESO_OBSERVADOR;
		this.tipoIngreso = this.VAR_PARTO_INGRESO_TIPO_INGRESO;
		this.situacion = this.VAR_PARTO_INGRESO_SITUACION;
		this.presentacion = this.VAR_PARTO_INGRESO_PRESENTACION;
		this.bishop = this.VAR_PARTO_INGRESO_BISHOP;
	}

	public Variable getFechaIngreso() {
		return fechaIngreso;
	}

	public Variable getVariableFechaIngreso() {
		return fechaIngreso;
	}

	public LocalDate getFechaIngresoDate() {
		LocalDate fecha = null;
		if (this.fechaIngreso != null) {
			if (this.fechaIngreso.getValor() != null && !this.fechaIngreso.getValor().isEmpty()) {
				if (Utilidades.isNumero(this.fechaIngreso.getValor())) {
					fecha = Utilidades.getFechaLocalDate(Long.parseLong(this.fechaIngreso.getValor()));
				}
			}
		}
		return fecha;
	}

	public void setFechaIngreso(Variable fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public void setFechaIngreso(String valor) {
		this.fechaIngreso.setValor(valor);
	}

	public void setFechaIngreso(LocalDate fecha) {
		this.fechaIngreso.setValor(Long.toString(Utilidades.getFechaNumeroyyymmddDefecha(fecha)));
	}

	public Variable getHoraIngreso() {
		return horaIngreso;
	}

	public Variable getVariableHoraIngreso() {
		return horaIngreso;
	}

	public String getHoraIngresoString() {
		return horaIngreso.getValor();
	}

	public void setHoraIngreso(Variable horaIngreso) {
		this.horaIngreso = horaIngreso;
	}

	public void setHoraIngreso(String valor) {
		this.horaIngreso.setValor(valor);
	}

	public Variable getObservador() {
		return observador;
	}

	public Variable getVariableObservador() {
		return observador;
	}

	public String getObservadorString() {
		return observador.getValor();
	}

	public void setObservador(Variable observador) {
		this.observador = observador;
	}

	public void setObservador(String valor) {
		this.observador.setValor(valor);
		;
	}

	public Variable getTipoIngreso() {
		return tipoIngreso;
	}

	public Variable getVariableTipoIngreso() {
		return tipoIngreso;
	}

	public String getTipoIngresoString() {
		return tipoIngreso.getValor();
	}

	public void setTipoIngreso(Variable tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}

	public void setTipoIngreso(String valor) {
		this.tipoIngreso.setValor(valor);
	}

	public Variable getSituacion() {
		return situacion;
	}

	public Variable getVariableSituacion() {
		return situacion;
	}

	public String getSituacionString() {
		return situacion.getValor();
	}

	public void setSituacion(Variable situacion) {
		this.situacion = situacion;
	}

	public void setSituacion(String valor) {
		this.situacion.setValor(valor);
		;
	}

	public Variable getPresentacion() {
		return presentacion;
	}

	public Variable getVariablePresentacion() {
		return presentacion;
	}

	public String getPresentacionString() {
		return presentacion.getValor();
	}

	public void setPresentacion(Variable presentacion) {
		this.presentacion = presentacion;
	}

	public void setPresentacion(String valor) {
		this.presentacion.setValor(valor);
	}

	public Variable getBishop() {
		return bishop;
	}

	public Variable getVariable() {
		return bishop;
	}

	public String getBishopString() {
		return bishop.getValor();
	}

	public void setBishop(Variable bishop) {
		this.bishop = bishop;
	}

	public void setBishop(String valor) {
		this.bishop.setValor(valor);
	}
}
