package com.jnieto.entity;

public class RegistroPartoAlivioDolor extends Registro {

	private Variable farmacologico;
	private Variable movimiento;
	private Variable calorLocal;
	private Variable masaje;
	private Variable pelota;
	private Variable ducha;
	private Variable otros;

	public final static Long PLANTILLLA_EDITOR_PAR_ALIVIODOLOR = new Long(794859506);
	public final static Long TIPO_REGISTRO_PARTO = new Long(21);

	public final Variable VAR_PARTO_DOLOR_FARMA = new Variable("13994617", "99G2", new Long(13994617),
			"Alivio farmacológico");

	public final Variable VAR_PARTO_DOLOR_MOVIMIENTO = new Variable("13994618", "99G2", new Long(13994618),
			"Movimiento");

	public final Variable VAR_PARTO_DOLOR_CALOR = new Variable("13994619", "99G2", new Long(13994619), "Calor Local");
	public final Variable VAR_PARTO_DOLOR_MASAJE = new Variable("13994620", "99G2", new Long(13994620), "Masaje");
	public final Variable VAR_PARTO_DOLOR_PELOTA = new Variable("13994621", "99G2", new Long(13994621), "Pelota ");
	public final Variable VAR_PARTO_DOLOR_DUCHA = new Variable("13994622", "99G2", new Long(13994622), "Ducha ");
	public final Variable VAR_PARTO_DOLOR_OTROS = new Variable("13994623", "99G2", new Long(13994623), "Otros ");

	public RegistroPartoAlivioDolor() {
		super();
		iniciaDolor();
	}

	public RegistroPartoAlivioDolor(Long id) {
		super(id);
		iniciaDolor();
	}

	public RegistroPartoAlivioDolor(RegistroPartoAlivioDolor r) {
		super(r);
		farmacologico = r.getFarmacologico();
		movimiento = r.getMovimiento();
		calorLocal = r.getCalorLocal();
		masaje = r.getMasaje();
		pelota = r.getPelota();
		ducha = r.getDucha();
		otros = r.getOtros();
	}

	public void iniciaDolor() {
		this.setTiporegistro(RegistroPartoAlivioDolor.TIPO_REGISTRO_PARTO);
		this.setDescripcion("Registro Parto Alivio dolor");
		this.setPlantilla_edior(RegistroPartoAlivioDolor.PLANTILLLA_EDITOR_PAR_ALIVIODOLOR);
		this.setServicio(new Servicio(new Long(40), "OBS", "Obstetricia y Ginecologia"));
		farmacologico = VAR_PARTO_DOLOR_FARMA;
		movimiento = VAR_PARTO_DOLOR_MASAJE;
		calorLocal = VAR_PARTO_DOLOR_CALOR;
		masaje = VAR_PARTO_DOLOR_MASAJE;
		pelota = VAR_PARTO_DOLOR_PELOTA;
		ducha = VAR_PARTO_DOLOR_DUCHA;
		otros = VAR_PARTO_DOLOR_OTROS;
	}

	public Variable getFarmacologico() {
		return farmacologico;
	}

	public Variable getVariableFarmacologico() {
		return farmacologico;
	}

	public String getFarmacologicoString() {
		return farmacologico.getValor();
	}

	public void setFarmacologico(Variable farmacologico) {
		this.farmacologico = farmacologico;
	}

	public void setFarmacologico(String valor) {
		this.farmacologico.setValor(valor);
	}

	public Variable getMovimiento() {
		return movimiento;
	}

	public Variable getVariableMovimiento() {
		return movimiento;
	}

	public boolean getMovimientoBolean() {
		if (movimiento != null && movimiento.getValor() != null && !movimiento.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setMovimiento(Variable movimiento) {
		this.movimiento = movimiento;
	}

	public void setMovimiento(String valor) {
		this.movimiento.setValor(valor);
	}

	public void setMovimiento(Boolean valor) {
		if (valor == true)
			movimiento.setValor("Alivio no farmacológico movimiento");
		else
			movimiento.setValor("");

	}

	public Variable getCalorLocal() {
		return calorLocal;
	}

	public Variable getVariableCalorLocal() {
		return calorLocal;
	}

	public boolean getCalorLocalBoolean() {
		if (calorLocal != null && calorLocal.getValor() != null && !calorLocal.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public String getCalorLocalString() {
		return calorLocal.getValor();
	}

	public void setCalorLocal(Variable calorLocal) {
		this.calorLocal = calorLocal;
	}

	public void setCalorLocal(String valor) {
		this.calorLocal.setValor(valor);
	}

	public void setCalorLocal(Boolean valor) {
		if (valor == true)
			calorLocal.setValor("Alivio no farmacológico calor local");
		else
			calorLocal.setValor("");
	}

	public Variable getMasaje() {
		return masaje;
	}

	public Variable getVariableMasaje() {
		return masaje;
	}

	public String getMasajeString() {
		return masaje.getValor();
	}

	public boolean getMasajeBoolean() {
		if (masaje != null && masaje.getValor() != null && !masaje.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setMasaje(Variable masaje) {
		this.masaje = masaje;
	}

	public void setMasaje(String valor) {
		this.masaje.setValor(valor);
	}

	public void setMasaje(boolean valor) {
		if (valor == true)
			masaje.setValor("Alivio no farmacológico masaje");
		else
			masaje.setValor("");
	}

	public Variable getPelota() {
		return pelota;
	}

	public Variable getVariablePelota() {
		return pelota;
	}

	public String getPelotaString() {
		return pelota.getValor();
	}

	public boolean getPelotaBoolean() {
		if (pelota != null && pelota.getValor() != null && !pelota.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setPelota(Variable pelota) {
		this.pelota = pelota;
	}

	public void setPelota(String valor) {
		this.pelota.setValor(valor);
		;
	}

	public void setPelota(boolean valor) {
		if (valor == true)
			pelota.setValor("Alivio no farmacológico pelota");
		else
			pelota.setValor("");
	}

	public Variable getDucha() {
		return ducha;
	}

	public Variable getVariableDucha() {
		return ducha;
	}

	public String getDuchaString() {
		return ducha.getValor();
	}

	public boolean getDuchaBoolean() {
		if (ducha != null && ducha.getValor() != null && !ducha.getValor().isEmpty())
			return true;
		else
			return false;
	}

	public void setDucha(Variable ducha) {
		this.ducha = ducha;
	}

	public void setDucha(String valor) {
		this.ducha.setValor(valor);
	}

	public void setDucha(boolean valor) {
		if (valor == true)
			ducha.setValor("Alivio no farmacológico ducha");
		else
			ducha.setValor("");
	}

	public Variable getOtros() {
		return otros;
	}

	public Variable getVariableOtros() {
		return otros;
	}

	public String getOtrosString() {
		return otros.getValor();
	}

	public void setOtros(Variable otros) {
		this.otros = otros;
	}

	public void setOtros(String valor) {
		this.otros.setValor(valor);
	}
}
