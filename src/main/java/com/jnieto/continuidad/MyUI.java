package com.jnieto.continuidad;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jnieto.controlador.AccesoControlador;
import com.jnieto.controlador.AuthService;
import com.jnieto.dao.AgendaDAO;
import com.jnieto.dao.CentroDAO;
import com.jnieto.dao.ConexionDAO;
import com.jnieto.dao.MensajesDAO;
import com.jnieto.dao.PacienteDAO;
import com.jnieto.dao.ParametroDAO;
import com.jnieto.dao.ServiciosDAO;
import com.jnieto.dao.UsuarioDAO;
import com.jnieto.entity.Agenda;
import com.jnieto.entity.Centro;
import com.jnieto.entity.Mensaje;
import com.jnieto.entity.ParametBBDD;
import com.jnieto.entity.Servicio;
import com.jnieto.entity.Usuario;
import com.jnieto.excepciones.SistemaException;
import com.jnieto.ui.FrmPaciente;
import com.jnieto.ui.MenuPrincipal;
import com.jnieto.ui.NotificacionInfo;
import com.jnieto.ui.PantallaCalendario;
import com.jnieto.ui.PantallaLogin;
import com.jnieto.ui.master.VentanaAvisosUsu;
import com.jnieto.utilidades.Constantes;
import com.jnieto.utilidades.MD5;
import com.jnieto.utilidades.MandarMensajesSingleton;
import com.jnieto.utilidades.Parametros;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

/**
 * This UI is the application entry point. A UI may either represent a browser
 * window (or tab) or some part of an HTML page where a Vaadin application is
 * embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is
 * intended to be overridden to add component to the user interface and
 * initialize non-component functionality.
 */

/**
 * 
 * @author Juan Nieto
 * @version 23.5.2018
 */

@PreserveOnRefresh
@Theme("mytheme")
public class MyUI extends UI {

	private static final long serialVersionUID = 8259262371833341186L;

	VerticalLayout contenedor = new VerticalLayout();

	private static final Logger logger = LogManager.getLogger(MyUI.class);

	public static Properties objParametros = null;

	private Usuario usuario = null;

	private String numerohistoria = null;

	static {
		try {
			objParametros = new Parametros().getParametros();
		} catch (Exception e) {
			logger.error("Error", e);
		}
	}

	/**
	 * Inits the.
	 *
	 * @param vaadinRequest the vaadin request
	 */
	@Override
	protected void init(VaadinRequest vaadinRequest) {
		// UI.getCurrent().setLocale(Locale.S);
		this.getReconnectDialogConfiguration().setDialogText(" Cliente desconectado. Intentando reconectar. ");

		setErrorHandler(e -> controlErroresUI(e.getThrowable()));

		Page.getCurrent().setTitle(Constantes.APLICACION_NOMBRE_VENTANA);

		this.setSizeFull();

		this.setResizeLazy(true);

		this.setScrollLeft(getScrollLeft());

		this.contenedor.setMargin(false);
		this.contenedor.setSpacing(false);

		try {
			logger.debug("Validaciones previas");
			/*
			 * Connection con = new HpHisDAO().conecta(); if (new HpHisDAO().conecta() ==
			 * null) new NotificacionInfo("error his", true); else { new
			 * NotificacionInfo("ok his", true); try { con.close(); } catch (Exception e) {
			 * // TODO: handle exception } }
			 */

			/*
			 * Connection con = new FarmaciaDAO().conecta(); if (con == null) { new
			 * NotificacionInfo("error his", true); } else { new NotificacionInfo("ok far",
			 * true); try { con.close(); } catch (Exception e) {
			 * 
			 * } }
			 */

			validacionesPrevias();
			doTimerMensajes();
			logger.debug("Acceso remoto ");
			if (doRemoteLlamada(vaadinRequest)) {

			} else if (doRemoteLogin(vaadinRequest)) {
				new AccesoControlador().doAccesoUsuarioLoginOk(usuario);
				showPrivateComponent(usuario, numerohistoria);
			} else if (!AuthService.isAuthenticated()) {
				logger.debug("Acceso local  login ");
				showPublicComponent();
			} else {
				logger.debug("Pantalla menu   ");
				showPrivateComponent((Usuario) VaadinSession.getCurrent().getAttribute(Constantes.SESSION_USERNAME),
						null);
			}
		} catch (SistemaException e1) {
			new NotificacionInfo(e1.getMessage(), true);
		} catch (IOException e1) {
			new NotificacionInfo(e1.getMessage(), true);
		} catch (Exception e1) {
			new NotificacionInfo(e1.getMessage(), true);
		}
	}

	/**
	 * Validaciones del entorno del servidor imprescindibles para la ejecución de la
	 * aplcaciín
	 * 
	 * 
	 * Comprueba que haya conectividad con la base de datos
	 * 
	 * Comprueba que se pueda leer (si no existe lo crea) el fichero properties con
	 * los parámetros de configuración.
	 * 
	 */
	public void validacionesPrevias() throws SistemaException, IOException, Exception {
		if (new ConexionDAO().getConexionBBDD() == null) {
			logger.error(ConexionDAO.ERROR_BBDD_SIN_CONEXION);
			throw new SistemaException(ConexionDAO.ERROR_BBDD_SIN_CONEXION);
		}

		new Parametros();
	}

	/**
	 * doTimerMensajes
	 * 
	 * Timer para envío de mensajes
	 */
	public static void doTimerMensajes() {
		Timer timerObj = new Timer();
		TimerTask timerTaskObj = new TimerTask() {
			public void run() {
				// new MensajeCtrProcesa();
				MandarMensajesSingleton mSingleton = MandarMensajesSingleton.getSingletonInstance();
				mSingleton.procesa();
			}
		};
		// timerObj.schedule(timerTaskObj, 0, 600000);
		timerObj.schedule(timerTaskObj, 0, 60000000);
	}

	/**
	 * Actualiza UI con único compoente.
	 *
	 * @param c the c
	 */
	public void actualiza(Component c) {
		contenedor.removeAllComponents();
		contenedor.addComponent(c);
		contenedor.setSizeFull();
	}

	/**
	 * Actualizas. Renueva los componentes de UI a partir de la lista de objetos UI
	 * de la lista de parámetros
	 *
	 * @param list the list
	 */
	public void actualizas(List<Object> list) {
		contenedor.removeAllComponents();
		contenedor.setSizeFull();
		for (Object com : list)
			contenedor.addComponent((Component) com);
	}

	/**
	 * Show public component. Pantalla de login
	 */
	public void showPublicComponent() {
		setContent(new PantallaLogin());
	}

	/**
	 * Show private component. Menú del usuario
	 *
	 * @param usuario the usuario
	 */
	public void showPrivateComponent(Usuario usuario, String historia) {
		final VerticalLayout layout = new VerticalLayout();
		layout.addStyleName(ValoTheme.LAYOUT_WELL);
		layout.setMargin(false);
		layout.setSpacing(false);
		// layout.setSizeFull();

		MenuBar barmenu = new MenuPrincipal(usuario);
		contenedor.setMargin(false);
		contenedor.setSizeFull();
		contenedor.setSpacing(false);

		if (historia != null) {
			if (!historia.isEmpty()) {
				contenedor.addComponent(new FrmPaciente(new PacienteDAO().getPacientePorNhc(historia)));
			}
		}
		layout.addComponents(barmenu, contenedor);
		barmenu.setEnabled(true);
		ArrayList<Mensaje> mensajesPendientes = new MensajesDAO().getListaMensajes(usuario.getUserid(),
				Mensaje.MENSAJE_PENDIENTE_ENVIO.getEstado());
		if (mensajesPendientes.size() > 0) {
			new VentanaAvisosUsu(this.getUI(), mensajesPendientes);
		}
		setContent(layout);
	}

	/**
	 * Control errores UI. Gestiona las excepciones UI de vaadin
	 *
	 * @param e the e
	 */
	public void controlErroresUI(Throwable e) {
		logger.error(NotificacionInfo.ERROR_UI, e);
		new NotificacionInfo(NotificacionInfo.ERROR_UI + e.getMessage(), true);
	}

//	@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
	/*
	 * @VaadinServletConfiguration(ui = MyUI.class, productionMode = false) public
	 * static class MyUIServlet extends VaadinServlet {
	 * 
	 * 
	 * private static final long serialVersionUID = 3898877499397673570L;
	 * 
	 * }
	 */

	boolean doRemoteLlamada(VaadinRequest request) {
		String APL = String.valueOf(getSession().getAttribute("APL"));
		String USR = String.valueOf(getSession().getAttribute("USR"));
		String ADDR = String.valueOf(getSession().getAttribute("ADDR"));
		String TIPO = String.valueOf(getSession().getAttribute("TIPO"));
		String CENTRO = String.valueOf(getSession().getAttribute("CENTRO"));
		String SERVICIO = String.valueOf(getSession().getAttribute("SERVICIO"));
		String AGENDA = String.valueOf(getSession().getAttribute("AGENDA"));
		System.out.print(TIPO.toString());
		System.out.println(APL);
		if (TIPO.equals("calendario")) {
			Centro centro = new CentroDAO().getRegistroId(Long.parseLong(CENTRO));
			Servicio servicio = new ServiciosDAO().getPorCodigo(SERVICIO);
			Agenda agenda = new AgendaDAO().getRegistrPorCodigo(AGENDA);
			LocalDate fecha = LocalDate.now();
			setContent(new PantallaCalendario(fecha, centro, servicio, agenda));
			return true;
		}
		return false;
	}

	/**
	 * Haz remote login. Método copiado de Imago.
	 *
	 * @param request the request
	 * @return true, if successful
	 */
	boolean doRemoteLogin(VaadinRequest request) {
		try {
			String APL = String.valueOf(getSession().getAttribute("APL"));
			String USR = String.valueOf(getSession().getAttribute("USR"));
			String ADDR = String.valueOf(getSession().getAttribute("ADDR"));
			String TIME = String.valueOf(getSession().getAttribute("TIME"));
			String TKN = String.valueOf(getSession().getAttribute("TKN"));
			String PAC = String.valueOf(getSession().getAttribute("PAC"));
			String password = new ParametroDAO().getRegistroPorCodigo(ParametBBDD.CLAVE_APLICACIONES).getValor();
			boolean horaOK = true;

			// Comprobamos la hora en milisegundos

			// Hora que nos llega
			Date horaRecibida = new Date(Long.parseLong(TIME));

			// Calculamos la hora máxima
			GregorianCalendar tmpCal1 = new GregorianCalendar();
			GregorianCalendar tmpCal2 = new GregorianCalendar();
			tmpCal1.add(Calendar.MINUTE, 60);
			tmpCal2.add(Calendar.MINUTE, -60);
			Date horaLimiteArriba = tmpCal1.getTime();
			Date horaLimiteAbajo = tmpCal2.getTime();

			// Comprobamos
			if (horaRecibida.after(horaLimiteArriba) || horaRecibida.before(horaLimiteAbajo)) {
				logger.error("Login remoto erróneo (tiempo límite excedido)");
				horaOK = false;
			}

			// Comprobamos la hora en segundos (si ha fallado)
			if (!horaOK) {

				// Volvemos a iniciarlo
				horaOK = true;

				// Hora que nos llega
				horaRecibida = new Date(Long.parseLong(TIME) * 1000);

				// Calculamos la hora máxima
				horaLimiteArriba = tmpCal1.getTime();
				horaLimiteAbajo = tmpCal2.getTime();

				// Comprobamos
				if (horaRecibida.after(horaLimiteArriba) || horaRecibida.before(horaLimiteAbajo)) {
					logger.error("Login remoto erróneo (tiempo límite excedido)");
					horaOK = false;
				}

			}

			if (horaOK) {

				String churro = password + APL + USR;
				if (ADDR != null && !"null".equals(ADDR))
					churro += ADDR;
				churro += TIME;
				if (PAC != null && !"null".equals(PAC))
					churro += PAC;

				try {
					String tkn = MD5.SHA1(churro).toUpperCase();
					/*
					 * if (!(TKN.toUpperCase().equals(tkn))) {
					 * logger.error("Login remoto erróneo (toker erróneo.)"); return false; } else {
					 */
					// Login correcto

					usuario = new UsuarioDAO().getUsuarioLogin(USR);
					if (usuario != null) {
						VaadinSession.getCurrent().setAttribute(Constantes.SESSION_USERNAME, usuario);
						numerohistoria = PAC;
						return true;
					}

					/* } */
				} catch (NoSuchAlgorithmException e) {
					logger.error("Error de configuración ", e);
				}

			}
		} catch (

		Exception e) {
			logger.error("Remote login: " + e.getMessage());
			return false;
		}

		return false;
	}
}
